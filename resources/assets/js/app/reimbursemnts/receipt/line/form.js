var app = new Vue({
    el: '#app',
    data: {
        lineForm: {
            before_vat_amount: '',
            vat_rate: '',
        },
        category: '',

        // amountWithVat: 0,
        maxReimAmount: 0,
        totalReimburse: 0,
        showLineForm: false,
    },

    methods: {
        toggleLineForm: function() {
            this.showLineForm = ! this.showLineForm;
        },
        getMaxReimAmount: function() {
            if (this.category == 'cat1') {
                this.maxReimAmount = 1000;
                return
            }
            if (this.category == 'cat2') {
                this.maxReimAmount = 2000;
                return
            }
            if (this.category == 'cat3') {
                this.maxReimAmount = 3000;
                return
            }

            this.maxReimAmount = 0;
        },
    },

    computed: {
        amountWithVat: function() {
            if (this.lineForm.vat_rate > 100) {
                this.lineForm.vat_rate = 100;
            }
            var vatAmount =  this.lineForm.before_vat_amount * this.lineForm.vat_rate / 100;
            var totalAmount = parseFloat(this.lineForm.before_vat_amount) + parseFloat(vatAmount);
            return totalAmount || 0;
        },

        totalReimburse: function() {
            return Math.min(this.lineForm.before_vat_amount, this.maxReimAmount);
        },

        reimAmount: function() {
            return Math.min(this.amountWithVat, this.maxReimAmount);
        }

    }
})