var app = new Vue({
    el: '#app',
    data: {
        showOtherVendor: false,
        vendor: '',
    },
    methods: {
        showOtherVendorForm: function() {
            this.showOtherVendor = (this.vendor == 'other') ? true : false;
        }
    }
})