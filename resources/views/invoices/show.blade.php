@extends('layouts.app')

@section('title', 'Invoices')

@section('page-title')
    <h2>
        Invoice (pay to vendor)<br/>
        <small>ใบเบิกเพื่อจ่ายผู้ให้บริการ</small>
    </h2>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('invoices.index') }}">
                Invoices (pay to vendor)
            </a>
        </li>
        <li class="active">
            <strong>
                {{ $invoice->document_no }}
            </strong>
        </li>
    </ol>
@stop

@section('page-title-action')
    @if($invoice->isRequester() && $invoice->isNotLock())

    <div class="text-right m-t-lg">
        {{-- BUTTON EDIT --}}
        <button class="btn btn-resize btn-success btn-outline" data-toggle="modal" href="#modal-edit-form">
            <i class="fa fa-edit"></i> Edit
        </button>
        {{-- BUTTON CANCEL --}}
        <div class="btn-resize">
        {!! Form::open(['route' => ['invoices.set_status',$invoice->id],
                    'method' => 'POST',
                    'id' => 'form-cancel-request']) !!}

            {!! Form::hidden('activity','CANCEL_REQUEST') !!}
            <button type="submit" class="btn btn-resize btn-danger btn-outline" disabled="disabled">
                <i class="fa fa-ban"></i> Cancel
            </button>

        {!! Form::close() !!}
        </div>
    </div>
    @endif
@stop

@section('content')
    @include('layouts._error_messages')

    <div class="row">
        <div class="col-md-12">
        {{-- ERR MSG OVER BUDGET --}}
        <div id="div_over_budget_err_msg_by_account" class="hide"></div>
        {{-- BLOCKED (UNCLEAR ALERT MESSAGE) --}}
        @if($invoice->status == 'BLOCKED')
            @include('commons._unclear_alert_message')
        @endif
        </div>
    </div>

    <div class="row">
        {{-- HEADER DETAIL (DOC# & AMOUNT) --}}
        @include('invoices.show._header_detail')
    </div>

    <div class="row">

        <div class="col-md-9">
            <div class="ibox">
                {{-- MAIN DETAIL --}}
                @include('invoices.show._main_detail')
            </div>
        </div>
        <div class="col-md-3">
            <div class="ibox">
                {{-- ATTACHMENT DETAIL --}}
                @include('invoices.show._attachment_detail')
            </div>
        </div>

    </div>

    {{-- RECEIPT TABLE & BUTTON --}}
    <div class="row">
        <div class="col-md-9">
            <div class="ibox">
                @include('invoices.show._receipts')
            </div>
            <div class="ibox">
                <div class="text-right">
                    {{-- REIM REQUEST TRANS BUTTON --}}
                    @include('invoices.show._button')
                </div>
            </div>
        </div>
        <div class="col-md-3">
            {{-- APPROVAL DETAIL --}}
            @include('invoices.show._approval_detail')
        </div>
    </div>

    {{-- ACTIVITY LOG --}}
    <div class="row">
        <div class="col-md-9 no-padding-xs">
            <div class="ibox-content activity-content mini-scroll-bar m-b-md"
                style="max-height: 400px;overflow: auto;">
                {{-- ACTIVITY LOG --}}
                @include('layouts._activities')
            </div>
        </div>
    </div>

    {{-- MODAL FOR EDIT --}}
    @include('invoices._modal_edit')
    {{-- MODAL FOR REIM REQUEST TRANS --}}
    @include('invoices.show._modal_approval')

@stop

@section('scripts')

    {{-- SCRIPT INVOICE TRANSACTIONS --}}
    @include('invoices._script')

        {{-- SCRIPT RECEIPT --}}
    @include('commons.receipts._script')

@stop