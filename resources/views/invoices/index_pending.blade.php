@extends('layouts.app')

@section('title', 'Invoices')

@section('page-title')
    <h2>
        Invoices (pay to vendor) : My Pending Activities <br/>
        <small>รายการใบเบิกเพื่อจ่ายผู้ให้บริการที่คุณกำลังดำเนินการ</small>
    </h2>
    <ol class="breadcrumb">
        <li class="active">
            <strong>
                Invoices (pay to vendor) : My Pending Activities
            </strong>
        </li>
    </ol>
@stop

@section('page-title-action')
    
@stop

@section('content')
<div class="ibox float-e-margins">
    @include('invoices._table')
    @if(isset($invoices))
    <div class="m-t-sm text-right">
        {!! $invoices->appends($search)->render() !!}
    </div>
    @endif
</div>
@stop