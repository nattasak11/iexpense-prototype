@extends('layouts.app')

@section('title', 'Invoices')

@section('page-title')
    <h2>
        <div>Invoices (pay to vendor) : New Request</div>
        <div><small>สร้างใบเบิกเพื่อจ่ายผู้ให้บริการใหม่</small></div>
    </h2>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('invoices.index') }}">
                Invoices (pay to vendor)
            </a>
        </li>
        <li class="active">
            <strong>
               New Request
            </strong>
        </li>
    </ol>
@stop

@section('content')
@include('layouts._error_messages')
@include('layouts._warning_create_request')
<div class="ibox float-e-margins">
    <div class="row">
        <div class="col-md-12">
        <strong>Request Form</strong>
        <span class="pull-right">
            <p>Request Date : {{ date(trans('date.format')) }}</p>
        </span>
        </div>
    </div>
    <hr class="m-t-sm">
    {!! Form::open(['route' => ['invoices.store'], 'id' => 'form-invoice','class' => 'form-horizontal']) !!}
    <div class="row">
    {{-- FORM INVOICE --}}
        <div class="col-md-8 b-r">

            {{-- FORM INVOICE HTML --}}
            @include('invoices._form')

        </div>
        <div class="col-md-4">
            <div class="form-group">
                <div class="col-sm-12">
                    <label>Attachments (ไฟล์แนบ)</label>
                    <div class="text-center dropzone m-t-xs m-b-xs" id="dropzoneFileUpload">
                        <div class="dz-message p-lg">
                            <div>
                                <i class="fa fa-file-text-o fa-3x"></i>
                            </div>
                            <div class="m-t-md">Drop files or Click</div>
                        </div>
                    </div>
                    <small style="color:#aaa"> Allow: jpeg, png, pdf, doc, docx, xls, xlsx, rar, zip and size < 5mb </small><br>
                    <small style="color:#aaa"> Max files : 2</small>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    @include('commons._unclear_alert_message')
                </div>
            </div>
            @include('layouts._dropzone_template')
        </div>
    {{-- FORM --}}
    </div>
    <hr class="m-t-sm m-b-sm">
    <div class="row">
        <div class="col-md-8">
            <div class="form-group">
                <div class="col-md-offset-2 col-md-10 text-right">
                    @if(\Auth::user()->isAllowCreateRequest())
                        <button id="btn_submit" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Creating ..." type="button" class="btn btn-primary">
                            Create
                        </button>
                    @endif
                    <a href="{{ route('invoices.index') }}" class="btn btn-white">Cancel</a>
                </div>
            </div>
        </div>
    </div>

    {!! Form::close()!!}
</div>
@stop

@section('scripts')
    <script>

        $(document).ready(function() {

            showOracleVendorDetail("#form-invoice");
            showOtherVendorForm("#form-invoice");
            $('#form-invoice select[name="vendor_id"]').change(function(e){
                showOracleVendorDetail("#form-invoice");
                showOtherVendorForm("#form-invoice");
            });

            $("#form-invoice").find(".select2").select2();

            $("#btn_submit").click(function(e) {
                e.preventDefault();
                e.stopPropagation();
                var valid = validateBeforeSubmit();
                if(valid){
                    $(this).button('loading');
                    if (myDropzone.getQueuedFiles().length > 0) {
                        myDropzone.processQueue();
                    } else {
                        $("#form-invoice").submit();
                    }
                }
            });

            // ==== DROPZONE ====
            var token = "{{ Session::getToken() }}";
            var myDropzone = new Dropzone("div#dropzoneFileUpload", {
                previewTemplate: $('#template-container').html(),
                url: "{{ url('/') }}/invoices/store",
                params: {
                   _token: token
                },
                autoProcessQueue: false,
                uploadMultiple: true,
                maxFiles: 2,
                maxFilesize: 5, // MB
                acceptedFiles: '.jpg, .jpeg, .bmp, .png, .pdf, .doc, .docx, .xls, .xlsx, .rar, .zip, .7z, .txt',
                // Dropzone settings
                init: function() {
                    var myDropzone = this;
                    this.on("maxfilesexceeded", function(file) {
                        toastr.options = {
                            "timeOut": "3000",
                        }
                        toastr.error(file.name + ' upload failed.');
                    });
                    this.on('error', function(file, response) {
                        if(file){ this.removeFile(file); }
                        toastr.options = {
                            "timeOut": "3000",
                        }
                        toastr.error(response);
                    });
                    this.on('queuecomplete', function(){
                        $("#btn_submit").button('reset');
                    });
                }
            });
            myDropzone.on('sending', function(file, xhr, formData){
                // formData.append('position_id', $("input[name='position_id']").val());
                formData.append('bank_account_id', $("input[name='bank_account_id']").val());
                formData.append('company_id', $("input[name='company_id']").val());
                formData.append('department_id', $("input[name='department_id']").val());
                formData.append('bank_account_id', $("input[name='bank_account_id']").val());
                formData.append('purpose', $("textarea[name='purpose']").val());
                formData.append('vendor_id', $("select[name='vendor_id'] option:selected").val());
                formData.append('vendor_name', $("input[name='vendor_name']").val());
                formData.append('vendor_tax_id', $("input[name='vendor_tax_id']").val());
                formData.append('vendor_branch_name', $("input[name='vendor_branch_name']").val());
            });
            myDropzone.on("success", function(file, result) {
                if((result.status).toUpperCase() == 'ERROR'){
                    swal({
                      title: "Error !",
                      text: "sorry something went wrong, this page will refresh in 2 seconds.",
                      type: "error",
                      timer: 2000,
                      showConfirmButton: false
                    },function(){
                        location.reload();
                    });
                }else{
                    $("#btn_submit").attr('disabled','disabled');
                    setTimeout(function(){
                        window.location.href = ("{{ url('/') }}/invoices/"+result.reimId);
                    }, 1000);
                }
            });

            function showOracleVendorDetail(formId)
            {
                var selectValue =  $(formId).find('select[name="vendor_id"] option:selected');
                var divOracleVendorDetail = $(formId).find('#div_oracle_vendor_detail');

                if (selectValue.val() != '' && selectValue.val() != 'other') {
                    divOracleVendorDetail.fadeIn('slow');
                    $.ajax({
                        url: "{{ url('/') }}/receipts/get_vendor_detail/"+selectValue.val(),
                        type: 'GET',
                        beforeSend: function( xhr ) {
                            //
                        }
                    })
                    .done(function(result) {
                        divOracleVendorDetail.html(result);
                    });

                } else {
                    divOracleVendorDetail.fadeOut('slow');
                    divOracleVendorDetail.html('');
                }
            }

            function showOtherVendorForm(formId) {
                var selectValue =  $(formId).find('select[name="vendor_id"] option:selected');
                var tagShowOtherVendor = $(formId).find('#showOtherVendor');

                if (selectValue.val() == 'other') {
                    tagShowOtherVendor.fadeIn('slow')
                } else {
                    tagShowOtherVendor.fadeOut('slow');
                }
            }

            function validateBeforeSubmit()
            {
                $("#select2-ddl_vendor_id-container").parent().next("div.error_msg").remove();
                $("input[name='vendor_name']").next("div.error_msg").remove();
                $("input[name='vendor_tax_id']").next("div.error_msg").remove();
                $("input[name='vendor_branch_name']").next("div.error_msg").remove();
                $("input[name='vendor_name']").removeClass('err_validate');
                $("input[name='vendor_tax_id']").removeClass('err_validate');
                $("input[name='vendor_branch_name']").removeClass('err_validate');
                $("textarea[name='purpose']").next("div.error_msg").remove();
                $("textarea[name='purpose']").removeClass('err_validate');

                var valid = true;
                var vendor_id = $("select[name='vendor_id'] option:selected").val();
                var vendor_name = $("input[name='vendor_name']").val();
                var vendor_tax_id = $("input[name='vendor_tax_id']").val();
                var vendor_branch_name = $("input[name='vendor_branch_name']").val();
                var purpose = $("textarea[name='purpose']").val();

                if(!vendor_id){
                    valid = false;
                    $("#select2-ddl_vendor_id-container").parent().after('<div class="error_msg"> vendor is required. </div>');
                }
                if(vendor_id == 'other'){
                    if(!vendor_name){
                        valid = false;
                        $("input[name='vendor_name']").addClass('err_validate');
                        $("input[name='vendor_name']").after('<div class="error_msg"> vendor name is required.</div>');
                    }
                    if(!vendor_tax_id){
                        valid = false;
                        $("input[name='vendor_tax_id']").addClass('err_validate');
                        $("input[name='vendor_tax_id']").after('<div class="error_msg"> tax id is required.</div>');
                    }else{
                        if(vendor_tax_id.length != 13){
                            valid = false;
                            $("input[name='vendor_tax_id']").addClass('err_validate');
                            $("input[name='vendor_tax_id']").after('<div class="error_msg"> tax id must be 13 digits.</div>');
                        }
                    }

                    if(!vendor_branch_name){
                        // valid = false;
                        // $("input[name='vendor_branch_name']").addClass('err_validate');
                        // $("input[name='vendor_branch_name']").after('<div class="error_msg"> branch number is required.</div>');
                    }else{
                        if(vendor_branch_name.length > 5){
                            valid = false;
                            $("input[name='vendor_branch_name']").addClass('err_validate');
                            $("input[name='vendor_branch_name']").after('<div class="error_msg"> branch number maximum is 5 digits.</div>');
                        }
                    }
                }

                if(!purpose){
                    valid = false;
                    $("textarea[name='purpose']").addClass('err_validate');
                    $("textarea[name='purpose']").after('<div class="error_msg"> purpose is required.</div>');
                }
                return valid;
            }

        });
    </script>
@stop