{{-- Requester --}}
<div class="row hidden-sm hidden-xs">
    <label for="" class="col-md-2 control-label">
        <div>Requester</div>
        <div>ผู้ขอเบิก</div>
    </label>
    <div class="col-md-3">
        <p class="form-control-static">
        {{ \Auth::user()->name }}
        {{-- {{ \Auth::user()->employee->full_name }} --}}
        </p>
    </div>
    <label for="" class="col-md-3 control-label">
        <div>Bank Account</div>
        <div>เลขที่บัญชี</div>
    </label>
    <div class="col-md-4">
        <p class="form-control-static">
        {{ \Auth::user()->employee->bank_name }} &nbsp; {{ \Auth::user()->employee->bank_account_num }}
        </p>
        {!! Form::hidden('bank_account_id', null ) !!}
    </div>
</div>

<div class="form-group hidden-sm hidden-xs">
    <label for="" class="col-md-2 control-label">
        <div>Company</div>
        <div>ชื่อบริษัท</div>
    </label>
    <div class="col-md-3">
        <p class="form-control-static">
        {{-- TMITH --}}
        {{ \Auth::user()->employee->company_name }}
        </p>
        {!! Form::hidden('company_id', null ) !!}
    </div>
    <label for="" class="col-md-3 control-label">
        <div>Department</div>
        <div>ชื่อแผนก</div>
    </label>
    <div class="col-md-4">
        <p class="form-control-static">
        {{-- IT --}}
        {{ \Auth::user()->employee->department_name }}
        </p>
        {!! Form::hidden('department_id', null ) !!}
    </div>
</div>

<div class="hr-line-dashed m-t-sm m-b-sm hidden-sm hidden-xs"></div>

{{-- VENDOR --}}
<div class="row">
    <label for="" class="col-md-2 control-label label-no-padding">
        <div>Vendor
            <span class="text-danger">*</span>
        </div>
        <div><small>ผู้ให้บริการ</small></div>
    </label>
    <div class="col-md-10">
        {!! Form::select('vendor_id', [''=>'-','other'=>'Other'] + $vendorLists, null , ["class" => 'form-control input-sm select2', "autocomplete" => "off", "style"=>"font-size:11px;width:100%" , 'id'=>'ddl_vendor_id']) !!}
    </div>
</div>

<div class="clearfix m-b-md">
    <div class="m-t-sm" id="showOtherVendor" style="display: none;">
        <div class="row m-b-sm">
            <div class="col-md-offset-2 col-md-10">
                {!! Form::text('vendor_name', null, ['class'=>'form-control input-sm', 'placeholder' => 'Vendor Name', 'id'=>'txt_vendor_name']) !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-offset-2 col-md-5 m-b-sm">
                {!! Form::text('vendor_tax_id', null, ['class'=>'form-control input-sm', 'placeholder' => 'Tax ID', 'id'=>'txt_vendor_tax_id']) !!}
            </div>
            <div class="col-md-5">
                {!! Form::text('vendor_branch_name', null, ['class'=>'form-control input-sm', 'placeholder' => 'Branch Number', 'id'=>'txt_vendor_branch_name']) !!}
            </div>
        </div>
    </div>
    <div id="div_oracle_vendor_detail">

    </div>
</div>

{{-- PURPOSE --}}
<div class="form-group">
    <label for="" class="col-md-2 control-label label-no-padding">
        <div>Purpose <span class="text-danger">*</span></div>
        <div class="m-r-sm">วัตถุประสงค์</div>
    </label>
    <div class="col-md-10">
       {!! Form::textarea('purpose', null , ["class" => 'form-control', "autocomplete" => "off" , "rows" => "4"]) !!}
    </div>
</div>
