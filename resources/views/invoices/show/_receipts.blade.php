<div class="row">
    <div class="col-md-12 no-padding-xs">
        @if($invoice->isRequester() && $invoice->isNotLock())
            <div class="text-right m-b-sm mm-sm">
                <button type="button" class="btn btn-primary btn-xs btn-outline" data-toggle="modal" data-target="#modal_create_receipt" data-backdrop="static" data-keyboard="false">Add Receipt</button>
            </div>
        @endif
		<div class="clearfix" id="div_clearing_receipt_list">

	        @include('commons.receipts._table',['parent'=>$invoice])

		</div>
    </div>
    @include('commons.receipts._modal_create')
    @include('commons.receipts._modal_edit')
</div>