{{-- SHOW ONLY ON PC SCREEN --}}
<div class="row hidden-xs">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <dl class="dl-horizontal">
                    <dt>
                        <small>Requester</small>
                        <small>(ผู้ขอเบิก)</small>
                    </dt>
                    <dd>{{ $invoice->user->name }}</dd>

                    <dt>
                        <small>Company</small>
                        <small>(ชื่อบริษัท)</small>
                    </dt>
                    <dd>
                        {{ $invoice->user->employee->company_name }}
                    </dd>

                    <dt>
                        <small>Department</small>
                        <small>(ชื่อแผนก)</small>
                    </dt>
                    <dd>
                        {{ $invoice->user->employee->department_name }}
                    </dd>
                </dl>
            </div>
            <div class="col-md-6">
                <dl class="dl-horizontal">
                    <dt>
                        <small>Bank Name</small>
                        <small>(ธนาคาร)</small>
                    </dt>
                    <dd>
                        {{ $invoice->user->employee->bank_name }}
                    </dd>

                    <dt>
                        <small>Account No.</small>
                        <small>(เลขที่บัญชี)</small>
                    </dt>
                    <dd>
                        {{ $invoice->user->employee->bank_account_num }}
                    </dd>

                    <dt>
                        <small>Account Name</small>
                        <small>(ชื่อบัญชี)</small>
                    </dt>
                    <dd>
                        {{ $invoice->user->employee->vendor_name }}
                    </dd>
                </dl>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <dl class="dl-horizontal m-b-xs">
        @if($invoice->vendor_id)
            @if($invoice->vendor_id != 'other')
                <dt>
                    <small>Vendor</small>
                    <small>(ผู้ให้บริการ)</small>
                </dt>
                <dd>{{ $vendorLists[$invoice->vendor_id] }}</dd>
            @else
                @if($invoice->vendor_name)
                    <dt>
                        <div>Vendor</div>
                        <div><small>ผู้ให้บริการ</small></div>
                    </dt>
                    <dd>{{ $invoice->vendor_name }}</dd>
                @endif
{{--                 @if($invoice->tax_id)
                    <dt></dt>
                    <dd>{{ $invoice->tax_id }}</dd>
                @endif
                @if($invoice->branch_name)
                    <dt></dt>
                    <dd>{{ $invoice->branch_name }}</dd>
                @endif --}}
            @endif
        @else
            <dt>
                <div>Vendor</div>
                <div><small>ผู้ให้บริการ</small></div>
            </dt>
            <dd>None</dd>
        @endif
        </dl>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <dl class="dl-horizontal">
            <dt>
                <small>Purpose</small>
                <small>(วัตถุประสงค์)</small>
            </dt>
            <dd class="mini-scroll-bar" style="max-height: 200px;overflow: auto;">
                {!! $invoice->purpose ? nl2br($invoice->purpose) : '-' !!}
            </dd>
        </dl>
    </div>
</div>
