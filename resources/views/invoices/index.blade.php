@extends('layouts.app')

@section('title', 'Invoices')

@section('page-title')
    <h2>
        Invoices (pay to vendor) : My Request <br/>
        <small>รายการใบเบิกเพื่อจ่ายผู้ให้บริการของคุณ</small>
    </h2>
    <ol class="breadcrumb">
        <li class="active">
            <strong>
                Invoices (pay to vendor) : My Request 
            </strong>
        </li>
    </ol>
@stop

@section('page-title-action')
    <div class="text-right m-t-lg">
        <a href="{{ route('invoices.create') }}" class="btn btn-primary">
            <i class="fa fa-plus"></i> Create Invoice (pay to vendor)
        </a>
    </div>
@stop

@section('content')
<div class="ibox float-e-margins">
    @include('invoices._table')
    @if(isset($invoices))
    <div class="m-t-sm text-right">
        {!! $invoices->appends($search)->render() !!}
    </div>
    @endif
</div>
@stop

@section('scripts')
    <script type="text/javascript">

        $(document).ready(function() {

            $("[id^='btn_duplicate_']").click(function(e){
                e.preventDefault(); // Prevent the href from redirecting directly
                var reimId = $(this).attr("data-id");
                swal({
                    html: true,
                    title: 'Duplicate this invoice ?',
                    text: '<h2 class="m-t-sm m-b-lg"><span style="font-size: 16px">Are you sure to duplicate this invoice request ?</span></h2>',
                    // type: "info",
                    showCancelButton: true,
                    // confirmButtonColor: "#DD6B55",
                    confirmButtonText: " Yes !",
                    cancelButtonText: "cancel",
                    confirmButtonClass: 'btn btn-info',
                    cancelButtonClass: 'btn btn-white',
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function(isConfirm){
                    if (isConfirm) {
                        $.ajax({
                            type: "post",
                            url:  "{{ url('/') }}/invoices/"+reimId+"/duplicate",
                            data: {
                                _token: "{{ csrf_token() }}",
                            },
                            beforeSend: function() {
                                //
                            },
                            success: function (data) {
                                swal({
                                  title: "Duplicate completed !",
                                  text: "this page will refresh in 2 seconds.",
                                  type: "success",
                                  timer: 2000,
                                  showConfirmButton: false
                                },function(){
                                    location.reload();
                                });
                            },
                            error: function(evt, xhr, status) {
                                  swal(evt.responseJSON.message, null, "error");
                            },
                            complete: function(data) {
                                  //
                            }
                        });
                    }
                });
            });

        });

    </script>
@stop