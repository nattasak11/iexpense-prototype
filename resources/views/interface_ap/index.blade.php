@extends('layouts.app')

@section('title', 'Interface Log')

@section('page-title')
    <h2>
        <div>Interface Log</div>
        <div><small>ติดตามผลการอินเตอร์เฟซ</small></div>
    </h2>
@stop

@section('content')

@include('layouts._error_messages')

    <div class="ibox">
        @include('interface_ap._search_form')
    </div>
    <div class="ibox float-e-margins">
        <div class="clearfix table-responsive">
            <table class="table table-hover" id="tableInterfaceLogs">
                <thead>
                    <tr class="active">
                        <th width="8%" class="text-center">
                            <div>Status</div>
                        </th>
                        <th class="no-sort" width="10%">
                            <div>Date/Time</div>
                        </th>
                        <th class="no-sort" width="10%">
                            <div>Document #</div>
                        </th>
                        <th class="no-sort" width="10%">
                            <div>Request Type</div>
                        </th>
                        <th class="no-sort" width="25%">
                            <div>Requester</div>
                        </th>
                        <th class="no-sort" width="15%">
                            {{-- <div>Interface MSG</div> --}}
                        </th>
                        <th class="no-sort" width="15%">

                        </th>
                    </tr>
                </thead>
                <tbody>
                @if(count($interfaceHeaders) > 0)
                    @foreach ($interfaceHeaders as $header)
                        <tr>
                            <td class="text-center" id="status_interface_{{ $header->id }}">
                                <span class="hidden-xs">
                                    {!! statusIconInterface($header->interface_status) !!}
                                </span>
                                <span class="show-xs-only">
                                    {!! statusMiniIconInterface($header->interface_status) !!}
                                </span>
                            </td>
                            <td>
                                <small>
                                    {{ $header->created_at ? date(trans('date.time-format'),strtotime($header->created_at)) : '-' }}
                                </small>
                            </td>
                            <td>
                                @if($header->request_type == 'CLEARING')
                                    {{ $header->request->clearing_document_no }}
                                @else
                                    {{ $header->request->document_no }}
                                @endif
                            </td>
                            <td>
                                {{ $header->request_type }}
                            </td>
                            <td>
                                {{ $header->request->user->name }}
                            </td>
                            <td>
                            @if($header->interface_message)
                                <div id="message_interface_{{ $header->id }}" title="{{ $header->interface_message }}">
                                {{ strlen($header->interface_message) > 20 ? substr($header->interface_message, 0, 20) . '...' : $header->interface_message }}
                                </div>
                            @endif
                            </td>
                            <td class="text-right">
                                @if($header->interface_status == 'E')
                                <a href="#" id="btn_re_process_{{ $header->id }}" data-header-id="{{ $header->id }}" class="btn btn-outline btn-warning btn-xs">
                                    <i class="fa fa-refresh"></i> re-process
                                </a>
                                @endif
                                <a href="#" id="btn_view_{{ $header->id }}" data-header-id="{{ $header->id }}" class="btn btn-outline btn-primary btn-xs">
                                    <i class="fa fa-search"></i> view
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td class="text-center" colspan="7">
                            <h2 style="color:#AAA;margin-top: 30px;margin-bottom: 30px;">
                                Not Found.
                            </h2>
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
        @if(isset($interfaceHeaders))
        <div class="m-t-sm clearfix">
            {!! $interfaceHeaders->appends($search)->appends($search_date)->render() !!}
        </div>
        @endif
    </div>

    {{-- MODAL SHOW INTERFACE LINE --}}
    @include('interface_ap._modal_show')

@stop

@section('scripts')

    <script>

        $(document).ready(function(){

            $('#date_from,#date_to').datepicker({
                format: "{{ trans('date.js-format') }}",
                todayBtn: true,
                multidate: false,
                keyboardNavigation: false,
                autoclose: true,
                todayBtn: "linked"
            });

            // EVENT SHOW INTERFACE INFORMATIONS
            $("[id^='btn_view_']").click(function(){
                var headerId = $(this).attr('data-header-id');
                $("#modal_show_interface_info").modal('show');
                renderShowInterfaceInformations(headerId);
            });

            function renderShowInterfaceInformations(headerId)
            {
                $.ajax({
                    url: "{{ url('/') }}/interface_ap/"+headerId+"/show",
                    type: 'GET',
                    beforeSend: function( xhr ) {
                        $("#modal_content_show_interface_info").html('\
                            <div class="m-t-lg m-b-lg">\
                                <div class="text-center sk-spinner sk-spinner-wave">\
                                    <div class="sk-rect1"></div>\
                                    <div class="sk-rect2"></div>\
                                    <div class="sk-rect3"></div>\
                                    <div class="sk-rect4"></div>\
                                    <div class="sk-rect5"></div>\
                                </div>\
                            </div>');
                    }
                })
                .done(function(result) {
                    $("#modal_content_show_interface_info").html(result);
                });
            }

            // EVENT RE-PROCESS INTERFACE
            $("[id^='btn_re_process_']").click(function(){
                var headerId = $(this).attr('data-header-id');
                swal({
                    html: true,
                    title: 'Re-process interface data ?',
                    text: '<h2 class="m-t-sm m-b-lg"><span style="font-size: 18px"> Are you sure to re-process this interface data ? </span></h2>',
                    showCancelButton: true,
                    confirmButtonText: 'OK',
                    cancelButtonText: 'cancel',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-white',
                    closeOnConfirm: false,
                    closeOnCancel: true,
                    showLoaderOnConfirm: true
                },
                function(isConfirm){
                    if (isConfirm) {
                        reProcessInterface(headerId);
                    }
                });
                event.preventDefault();
            });


            // ====== AJAX RE-PROCESS FUNCTION ====
            function reProcessInterface(headerId)
            {
                $.ajax({
                    url: "{{ url('/') }}/interface_ap/"+headerId+"/re_process",
                    type: 'POST',
                    data: { _token: "{{ csrf_token() }}" },
                    beforeSend: function() {
                        //
                    },
                    success: function (data) {
                        //
                    },
                    error: function(evt, xhr, status) {
                        // swal(evt.responseJSON.message, null, "error");
                        swal({
                          title: "Error !",
                          text: "sorry something went wrong.",
                          type: "error",
                          // timer: 2000,
                          showConfirmButton: true
                        });
                    },
                    complete: function(data) {
                        //
                    }
                })
                .done(function(result) {
                    changeStatusToPending(headerId);
                    swal.close();
                });
            }

            function changeStatusToPending(headerId)
            {
                var statusPending = '<span class="label-status btn-warning btn-outline"> PENDING... </span>';
                $("#status_interface_"+headerId).html(statusPending);
                $("#message_interface_"+headerId).remove();
                $("#btn_re_process_"+headerId).remove();
            }

        });

    </script>
@stop