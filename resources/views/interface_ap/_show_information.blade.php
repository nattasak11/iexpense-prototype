{{-- INTERFACE HEADER --}}
<div class="row clearfix">
    <div class="col-md-6">
        <div class="row clearfix m-b-sm">
            <label class="col-sm-5 control-label">
                <div>Document #</div>
            </label>
            <div class="col-sm-7">
                <span style="font-weight:normal;font-size:14px;"> 
                    @if($interfaceHeader->request_type == 'CLEARING')
                        {{ $interfaceHeader->request->clearing_document_no }}
                    @else
                        {{ $interfaceHeader->request->document_no }}
                    @endif
                </span>
            </div>
        </div>
        <div class="row clearfix m-b-sm">
            <label class="col-sm-5 control-label">
                <div>Request Type</div>
            </label>
            <div class="col-sm-7">
                <span style="font-weight:normal;font-size:14px;"> 
                    {{ $interfaceHeader->request_type }}
                </span>
            </div>
        </div>
        <div class="row clearfix m-b-sm">
            <label class="col-sm-5 control-label">
                <div>Requester</div>
            </label>
            <div class="col-sm-7">
                <span style="font-weight:normal;font-size:14px;">
                    {{ $interfaceHeader->request->user->name }}
                </span>
            </div>
        </div>
        <div class="row clearfix m-b-sm">
            <label class="col-sm-5 control-label">
                <div>Invoice Date</div>
            </label>
            <div class="col-sm-7">
                <span style="font-weight:normal;font-size:14px;">
                    {{ dateFormatDisplay($interfaceHeader->invoice_date) }}
                </span>
            </div>
        </div>
        <div class="row clearfix m-b-sm">
            <label class="col-sm-5 control-label">
                <div>GL Date</div>
            </label>
            <div class="col-sm-7">
                <span style="font-weight:normal;font-size:14px;">
                    {{ dateFormatDisplay($interfaceHeader->gl_date) }}
                </span>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row clearfix m-b-sm">
            <label class="col-sm-5 control-label">
                <div>Status</div>
            </label>
            <div class="col-sm-7">
                {!! statusIconInterface($interfaceHeader->interface_status) !!}
            </div>
        </div>
        <div class="row clearfix m-b-sm">
            <label class="col-sm-5 control-label">
                <div>Date/Time</div>
            </label>
            <div class="col-sm-7">
                <span style="font-weight:normal;font-size:14px;"> 
                    {{ date(trans('date.time-format'),strtotime($interfaceHeader->created_at)) }}
                </span>
            </div>
        </div>
        <div class="row clearfix m-b-sm">
            <label class="col-sm-5 control-label">
                <div>Invoice Number</div>
            </label>
            <div class="col-sm-7">
                <span style="font-weight:normal;font-size:14px;">
                    {{ $interfaceHeader->invoice_number }}
                </span>
            </div>
        </div>
        @if($interfaceHeader->request_type == 'CLEARING')
        <div class="row clearfix m-b-sm">
            <label class="col-sm-5 control-label">
                <div>Apply To Invoice Number</div>
            </label>
            <div class="col-sm-7">
                <span style="font-weight:normal;font-size:14px;">
                    {{ $interfaceHeader->apply_invoice_number ? $interfaceHeader->apply_invoice_number : '-' }}
                </span>
            </div>
        </div>
        @endif
        <div class="row clearfix m-b-sm">
            <label class="col-sm-5 control-label">
                <div>Payment Method</div>
            </label>
            <div class="col-sm-7">
                <span style="font-weight:normal;font-size:14px;">
                    {{ $interfaceHeader->payment_method_code }}
                </span>
            </div>
        </div>
        <div class="row clearfix m-b-sm">
            <label class="col-sm-5 control-label">
                <div>Invoice Amount</div>
            </label>
            <div class="col-sm-7">
                <span style="font-weight:normal;font-size:14px;">
                    {{ is_numeric($interfaceHeader->invoice_amount) ? number_format($interfaceHeader->invoice_amount,2) : '-' }}
                    <small> {{ $interfaceHeader->currency }} </small>
                </span>
            </div>
        </div>

    </div>
</div>
{{-- INTERFACE LINE --}}
<div class="row clearfix m-t-sm">
    <div class="col-lg-12">
        <div id="div_table_interface_line" class="table-responsive clearfix" 
            style="max-height: 400px;">
            <table id="table_interface_line" class="table table-hover m-b-xs">
                <thead>
                    <tr>
                        <th class="text-center" width="5%">
                            #
                        </th>
                        <th class="text-center" width="15%">
                            Concatenated Segments
                        </th>
                        {{-- <th class="text-center" width="8%">
                            ACCT Date
                        </th> --}}
                        <th class="text-center" width="5%">
                            QTY
                        </th>
                        <th class="text-right" width="10%">
                            UNT Price
                        </th>
                        <th class="text-right" width="10%">
                            Line Amount
                        </th>
                        <th class="text-right" width="10%">
                            Tax Amount
                        </th>
                        <th class="text-center" width="8%">
                            Tax
                        </th>
                        <th class="text-center" width="8%">
                            Status
                        </th>
                        <th class="text-center" width="30%">
                            {{-- Interface MSG --}}
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {{-- FOR EACH ROW DETAILS --}}
                @if(isset($interfaceLines))
                    @foreach($interfaceLines as $key => $line)
                        <tr data-detail-index="{{ $line->id }}" id="interface_line_{{ $line->id }}">
                            <td class="text-center">
                                {{ $line->line_number }}
                            </td>
                            <td class="text-center">
                                <small>{{ $line->concatenated_segments }}</small>
                            </td>
                            {{-- <td class="text-center">
                                {{ $line->accounting_date ? date(trans('date.format'),strtotime($line->accounting_date)) : '-' }}
                            </td> --}}
                            <td class="text-center">
                                {{ $line->quantity_invoiced }}
                            </td>
                            <td class="text-right">
                                {{ is_numeric($line->unit_price) ? number_format($line->unit_price,2) : '-' }}
                            </td>
                            <td class="text-right">
                                {{ is_numeric($line->line_amt) ? number_format($line->line_amt,2) : '-' }}
                            </td>
                            <td class="text-right">
                                {{ is_numeric($line->tax_amt) ? number_format($line->tax_amt,2) : '-' }}
                            </td>
                            <td class="text-center">
                                {{ $line->tax ? $line->tax : '-' }}
                            </td> 
                            <td class="text-center">
                                {!! statusIconInterface($line->interface_status) !!}
                            </td> 
                            <td>
                                <div title="{{ $line->interface_message }}">
                                {{ strlen($line->interface_message) > 35 ? substr($line->interface_message, 0, 35) . '...' : $line->interface_message }}
                                </div>
                            </td> 
                        </tr>
                    @endforeach
                @else
                    
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
