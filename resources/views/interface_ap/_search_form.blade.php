<div>
    {!! Form::open(['route' => ['interface_ap.index'], 'class' => 'form-horizontal', 'method'=>'GET' ]) !!}

      <div class="form-group clearfix">
        <div class="col-md-3">
          <div>
            <label>Status</label>
            {!! Form::select('interface_status',[''=>'All','P'=>'PENDING...','E'=>'ERROR','S'=>'SUCCESS'], (isset($search['interface_status']))? $search['interface_status'] : null, array('class' => 'form-control','id'=>'interface_status')) !!} 
          </div>
        </div>
        <div class="col-md-4">
          <div>
            <label>Date</label>
          </div>
          <div class="input-group date">
           <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
           {{-- <input class="form-control " id="date_from" name="date_from" type="text"> --}}
           {!! Form::text('date_from', (isset($search_date['date_from'])) ? $search_date['date_from'] ? date(trans('date.format'), strtotime($search_date['date_from'])): null : null, array('class' => 'form-control', 'id' => 'date_from')) !!}
           <span class="input-group-addon"> - </span>
           {!! Form::text('date_to', (isset($search_date['date_to'])) ? $search_date['date_to'] ? date(trans('date.format'), strtotime($search_date['date_to'])) : null : null, array('class' => 'form-control', 'id' => 'date_to')) !!}
          </div>
        </div>
        <div class="col-md-3">
          <div>
            <label>Request Type</label>
            {!! Form::select('request_type',[''=>'All','REIMBURSEMENT'=>'REIMBURSEMENT','CASH-ADVANCE'=>'CASH-ADVANCE','CLEARING'=>'CLEARING'], (isset($search['request_type']))? $search['request_type'] : null, array('class' => 'form-control','id'=>'request_type')) !!} 
          </div>
        </div>
        <div class="col-md-2 text-right">
          <div><label> &nbsp; </label></div>
          {!! Form::submit('Search', array('class' => 'btn btn-sm btn-primary')) !!}
          <a href="{{ route('interface_ap.index') }}" class="btn btn-link">clear</a>
        </div>
      </div>
    {!! Form::close() !!}
</div>