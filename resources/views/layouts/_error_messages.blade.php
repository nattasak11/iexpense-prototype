@if ($errors->count())
  <ul class="list-unstyled alert alert-warning alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  @foreach ($errors->all() as $error)
    <li>{{ $error }} </li>
  @endforeach
  </ul>
@endif

{{-- FOR Javascript Validation --}}
<div id="div_js_error_messages" style="display:none;">
    <ul class="list-unstyled alert alert-warning alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        {{-- <li> Error List </li> --}}
    </ul>
</div>
{{-- END Javascript Validation --}}