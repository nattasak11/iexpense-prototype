{{-- ############################################### --}}
{{-- CALL FUNCTION GEN NAVIGATION BAR (TOP & MMMENU) --}}
{{-- ############################################### --}}

{!!
    genNavElement(
        [
            [
                'href'  =>  route('main'),
                'text'  =>  '<i class="fa fa-th-large" aria-hidden="true fa-fw"></i>
                            Main Page'
            ],
            [
                'href'  =>  '/',
                'text'  =>  '<i class="fa fa-money" aria-hidden="true fa-fw"></i>
                            Reimbursement',
                'second'=>  [
                                [
                                    'href'  =>  route('reimbursements.index_pending'),
                                    'text'  =>  'My Pending Activity'
                                ],
                                [
                                    'href'  =>  route('reimbursements.index'),
                                    'text'  =>  'My Request'
                                ]
                            ]
            ],
            [
                'href'  =>  '/',
                'text'  =>  '<i class="fa fa-bolt" aria-hidden="true fa-fw"></i>
                            Cash Advance',
                'second'=>  [
                                [
                                    'href'  =>  route('cash-advances.index_pending'),
                                    'text'  =>  'My Pending Activity'
                                ],
                                [
                                    'href'  =>  route('cash-advances.index'),
                                    'text'  =>  'My Request'
                                ]
                            ]
            ],
            [
                'href'  =>  '/',
                'text'  =>  '<i class="fa fa-file-text-o" aria-hidden="true fa-fw"></i>
                            Invoice <small>(pay to vendor)</small>',
                'second'=>  [
                                [
                                    'href'  =>  route('invoices.index_pending'),
                                    'text'  =>  'My Pending Activity'
                                ],
                                [
                                    'href'  =>  route('invoices.index'),
                                    'text'  =>  'My Request'
                                ]
                            ]
            ],
            [
                'href'  =>  '/',
                'text'  =>  '<i class="fa fa-sliders" aria-hidden="true fa-fw"></i>
                            Settings',
                'second'=>  [
                                [
                                    'href'  =>  route('settings.categories.index'),
                                    'text'  =>  '<i class="fa fa-th-large fa-fw"></i> REIM Category'
                                ],
                                [
                                    'href'  =>  route('settings.ca_categories.index'),
                                    'text'  =>  '<i class="fa fa-th-large fa-fw"></i> CA Category'
                                ],
                                [
                                    'href'  =>  route('settings.locations.index'),
                                    'text'  =>  '<i class="fa fa-globe fa-fw"></i> Location'
                                ],
                                [
                                    'href'  =>  route('settings.users.index'),
                                    'text'  =>  '<i class="fa fa-users fa-fw"></i> User'
                                ],
                                [
                                    'href'  =>  route('settings.preferences.index'),
                                    'text'  =>  '<i class="fa fa-cog fa-fw"></i> Preference'
                                ]
                            ]
            ],
            [
                'href'  =>  route('interface_ap.index'),
                'text'  =>  '<i class="fa fa-tasks" aria-hidden="true fa-fw"></i>
                            Interface Log'
            ],
        ],
        isset($top_navigation) ? $top_navigation : null)
!!}