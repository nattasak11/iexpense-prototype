<div class="row border-bottom">
    <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            {{-- <form role="search" class="navbar-form-custom" method="post" action="/">
                <div class="form-group">
                    <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search" />
                </div>
            </form> --}}
            {{-- <a class="minimalize-styl-2 btn btn-white m-l-xs" id="btn-navbar-top-back"
                style="padding-left:8px;padding-right:8px;" type="button">
                <i class="fa fa-reply"></i> Back
            </a> --}}
        </div>
        <ul class="nav navbar-top-links navbar-right text-right">
            <li>
                <a href="{{ url('/') }}/logout">
                    <span class="hidden-sm hidden-xs">
                        <i class="fa fa-sign-out"></i> Log out
                    </span>
                    <span class="show-sm-only show-xs-only m-r-sm">
                        <i class="fa fa-sign-out fa-2x"></i>
                    </span>
                </a>
            </li>
        </ul>
    </nav>
</div>
