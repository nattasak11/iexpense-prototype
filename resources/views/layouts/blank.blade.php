<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>iExpense - @yield('title') </title>
    
    <link rel="shortcut icon" href="/images/title.png" type="image/png" />
    <link href="{{ elixir('css/bower_components_all.css') }}" type="text/css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/vendor.css" />
    <link rel="stylesheet" type="text/css" href="/css/app.css" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

</head>
<body>
    <div id="wrapper" class="light-grey-bg" style="height:100%;">

            <div class="wrapper wrapper-content">  

                @yield('content')

            </div>

    </div>

    <script src="/js/app.js" type="text/javascript"></script>

    <!-- Bower Component All -->
    <script src="{{ asset('/js/bower_components_all.js') }}"></script>

    <script>
        Dropzone.autoDiscover = false;
    </script>

@section('scripts')
@show

</body>
</html>
