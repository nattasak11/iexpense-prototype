<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>iExpense - @yield('title') </title>

    <link rel="shortcut icon" href="/images/title.png" type="image/png" />
    <link href="{{ elixir('css/bower_components_all.css') }}" type="text/css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/vendor.css" />
    <link rel="stylesheet" type="text/css" href="/css/app.css" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

</head>
<body class="top-navigation">

    <!-- Wrapper-->
    <div id="wrapper">
        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom white-bg">
                <nav class="navbar navbar-static-top" role="navigation">
                    <div class="navbar-header show-sm-only show-xs-only" style="width:100%;">
                        <button id="my-icon" class="hamburger hamburger--collapse" type="button">
                           <span class="hamburger-box">
                              <span class="hamburger-inner"></span>
                           </span>
                        </button>
                        <span class="pull-right p-sm">
                            <a href="login.html">
                                <i class="fa fa-2x fa-sign-out" style="color : #777;"></i>
                            </a>
                        </span>
                    </div>
                    <div class="navbar-header hidden-sm hidden-xs">
                        <a href="#" class="navbar-brand">iExpense</a>
                    </div>
                    <div class="navbar-collapse collapse hidden-sm hidden-xs" id="navbar">
                        @include('layouts.navigation',['top_navigation'=>true])
                        <ul class="nav navbar-top-links navbar-right">
                            <li>
                                <a href="login.html">
                                    <i class="fa fa-sign-out"></i> Log out
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-9">
                    @yield('page-title')
                </div>
                <div class="col-lg-3 text-right">
                    @yield('page-title-action')
                </div>
            </div>

            <div class="wrapper wrapper-content wrapper-content-mobile">
                <div class="p-md white-bg">
                    @yield('content')
                </div>
            </div>
            <nav id="my-menu">
               @include('layouts.navigation')
            </nav>
        </div>
    </div>

    <!-- End wrapper-->

    <script src="/js/app.js" type="text/javascript"></script>
    <!-- Bower Component All -->
    <script src="{{ asset('/js/bower_components_all.js') }}"></script>

    <script>
        Dropzone.autoDiscover = false;
    </script>

    <script type="text/javascript">
       $(document).ready(function() {

            var $menu = $("#my-menu").mmenu({
                // options
                extensions  : [ 'fx-menu-slide', 'shadow-page', 'shadow-panels', 'listview-large', 'pagedim-white' ],
                iconPanels  : true,
                // counters    : true,
                keyboardNavigation : {
                    enable  : true,
                    enhance : true
                },
                "slidingSubmenus": false,
                navbar: {
                    title: '<i class="fa fa-bars"></i> iExpense'
                },
            }, {
                // configuration
                clone: true,
            });

            $(".mm-menu").find(".mm-next").addClass("mm-fullsubopen");

            var $icon = $("#my-icon");
            var API = $menu.data( "mmenu" );

            $icon.on( "click", function() {
               API.open();
            });

            API.bind( "open:finish", function() {
               setTimeout(function() {
                  $icon.addClass( "is-active" );
               }, 100);
            });
            API.bind( "close:finish", function() {
               setTimeout(function() {
                  $icon.removeClass( "is-active" );
               }, 100);
            });
       });
    </script>

    @section('scripts')

@show

</body>
</html>
