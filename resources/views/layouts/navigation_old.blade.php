<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header p-sm">
                <div class="dropdown profile-element">
                    <span class="clear">
                        <span class="block m-b-xs">
                            <a href="{{ route('main') }}">
                                <h2 class="font-bold" style="font-size: 40px;">iExpense</h2>
                            </a>
                        </span>
                        <span class="block m-b-sm">
                            <small style="color:#eee">
                                <i class="fa fa-user"></i>&nbsp; 
                                {{ Auth::user()->name }}
                            </small>
                        </span>
                    </span>
                </div>
                <div class="logo-element">
                    <a href="{{ route('main') }}"> iEx </a>
                </div>
            </li>

            {{-- Main --}}
             <li class="{{ isActiveRoute('main') }}">
                <a href="{{ route('main') }}" >
                    <i class="fa fa-th-large" aria-hidden="true fa-fw"></i>
                    <span class="nav-label">Main Page</span>
                </a>
            </li>
            
            {{-- Reimbursement --}}
             <li class="{{ isActiveRoute('reimbursements.index') }}
                        {{ isActiveRoute('reimbursements.index_pending') }}">
                <a href="#" >
                    <i class="fa fa-money" aria-hidden="true fa-fw"></i>
                    <span class="nav-label">Reimbursement</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    <li class="">
                        <a href="{{ route('reimbursements.index_pending') }}" style="font-size: 11px;">
                            My Pending Activity
                        </a>
                    </li>
                    <li class="">
                        <a href="{{ route('reimbursements.index') }}" style="font-size: 11px;">
                            My Request
                        </a>
                    </li>
                </ul>
            </li>
            {{-- Cash Advance --}}
            <li class="{{ isActiveRoute('cash-advances.index') }}
                    {{ isActiveRoute('cash-advances.index_pending') }}">
                <a href="#" >
                    <i class="fa fa-bolt" aria-hidden="true fa-fw"></i>
                    <span class="nav-label">Cash Advance</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    <li class="">
                        <a href="{{ route('cash-advances.index_pending') }}" style="font-size: 11px;">
                            My Pending Activity
                        </a>
                    </li>
                    <li class="">
                        <a href="{{ route('cash-advances.index') }}" style="font-size: 11px;">
                            My Request
                        </a>
                    </li>
                </ul>
            </li>
            {{-- INVOICE --}}
            <li class="{{ isActiveRoute('invoices.index') }}
                    {{ isActiveRoute('invoices.index_pending') }}">
                <a href="#" >
                    <i class="fa fa-file-text-o" aria-hidden="true fa-fw"></i>
                    <span class="nav-label">Invoice <small>(pay to vendor)</small></span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    <li class="">
                        <a href="{{ route('invoices.index_pending') }}" style="font-size: 11px;">
                            My Pending Activity
                        </a>
                    </li>
                    <li class="">
                        <a href="{{ route('invoices.index') }}" style="font-size: 11px;">
                            My Request
                        </a>
                    </li>
                </ul>
            </li>
            {{-- SETTING --}}
            @if (Gate::allows('superuser'))
            <li class="{{ isActiveRoute('settings.ca_categories.index') }}
                        {{ isActiveRoute('settings.categories.index') }}
                        {{ isActiveRoute('settings.locations.index') }}
                        {{ isActiveRoute('settings.preferences.index') }}">
                <a href="#" >
                    <i class="fa fa-sliders" aria-hidden="true fa-fw"></i>
                    <span class="nav-label">Settings</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    <li class="{{ isActiveRoute('settings.categories.index') }}
                            {{ isActiveRoute('settings.ca_categories.index') }}">
                        <a href="#">
                            <i class="fa fa-th-large fa-fw"></i>  
                            Category
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-third-level" style="font-size: 11px">
                            <li class="{{ isActiveRoute('settings.categories.index') }}">
                                <a href="{{ route('settings.categories.index') }}">
                                    <i class="fa fa-fw"></i> REIM Category
                                </a>
                            </li>
                             <li class="{{ isActiveRoute('settings.ca_categories.index') }}">
                                <a href="{{ route('settings.ca_categories.index') }}">
                                    <i class="fa fa-fw"></i> CA Category
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="{{ isActiveRoute('settings.locations.index') }}">
                        <a href="{{ route('settings.locations.index') }}">
                            <i class="fa fa-globe fa-fw"></i>
                            Location
                        </a>
                    </li>
                    <li class="{{ isActiveRoute('settings.users.index') }}">
                        <a href="{{ route('settings.users.index') }}">
                            <i class="fa fa-users fa-fw"></i>
                            User
                        </a>
                    </li>
                    <li class="{{ isActiveRoute('settings.preferences.index') }}">
                        <a href="{{ route('settings.preferences.index') }}">
                            <i class="fa fa-cog fa-fw"></i>
                            Preference
                        </a>
                    </li>
                </ul>
            </li>
            @endif
            {{-- Main --}}
            @if (\Auth::user()->isAdmin())
            <li class="{{ isActiveRoute('interface_ap.index') }}">
                <a href="{{ route('interface_ap.index') }}" >
                    <i class="fa fa-tasks" aria-hidden="true fa-fw"></i>
                    <span class="nav-label">Interface Log</span>
                </a>
            </li>
            @endif
        </ul>

    </div>
</nav>
