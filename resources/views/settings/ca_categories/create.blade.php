@extends('layouts.app')

@section('title', 'Cash Advance Categories')

@section('page-title')
    {{-- PC --}}
    <h2 class="hidden-xs hidden-sm"> 
        Create New CA Category <br>
        <small>สร้างประเภทการเบิกเงินทดรองจ่ายใหม่</small>
    </h2>
    <ol class="breadcrumb hidden-xs hidden-sm">
        <li class="active">
            <a href="{{ route('settings.ca_categories.index') }}"> All Categories </a>
        </li>
        <li class="active">
            <strong>Create New CA Category </strong>
        </li>
    </ol>
    {{-- MOBILE --}}
    <h3 class="m-t-md m-b-sm show-xs-only show-sm-only">
        Create New CA Category <br>
        <small>สร้างประเภทการเบิกเงินทดรองจ่ายใหม่</small>
    </h3>
@stop

@section('content')
    @include('layouts._error_messages')
    <div class="ibox float-e-margins">
        {!! Form::open(['route' => ['settings.ca_categories.store'], 'class' => 'form-horizontal']) !!}
          @include('settings.ca_categories._form')
        {!! Form::close()!!}
    </div>
@endsection
