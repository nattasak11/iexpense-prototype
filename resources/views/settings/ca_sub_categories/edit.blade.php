@extends('layouts.app')

@section('title', 'Cash Advance Sub-Categories')

@section('page-title')
    {{-- PC --}}
    <h2 class="hidden-xs hidden-sm"> 
        {{ $ca_category->name }} : CA Sub-Categories <br>
        <small>แก้ไขข้อมูลประเภทการเบิกเงินทดรองจ่ายย่อย</small>
    </h2>
    <ol class="breadcrumb hidden-xs hidden-sm">
        <li class="active">
            <a href="{{ route('settings.ca_categories.index') }}"> All Cash Advance Categories </a>
        </li>
        <li class="active">
            <a href="{{ route('settings.ca_sub_categories.index',[$ca_category->id]) }}"> {{ $ca_category->name }} : CA Sub-Categories</a>
        </li>
        <li class="active">
            <strong>Edit CA Sub-Category : {{ $ca_sub_category->name }}</strong>
        </li>
    </ol>
    {{-- MOBILE --}}
    <h3 class="m-t-md m-b-sm show-xs-only show-sm-only">
        <strong>Edit CA Sub-Category : {{ $ca_sub_category->name }}</strong> <br>
        <small>แก้ไขข้อมูลประเภทการเบิกเงินทดรองจ่ายย่อย</small>
    </h3>
@stop

@section('content')
    @include('layouts._error_messages')
    <div class="ibox float-e-margins">
        {!! Form::model($ca_sub_category, ['route' => ['settings.ca_sub_categories.update', $ca_category->id, $ca_sub_category->id], 'class' => 'form-horizontal', 'method' => 'patch'] ) !!}
          @include('settings.ca_sub_categories._form')
        {!! Form::close()!!}
    </div>
@endsection

