@extends('layouts.app')

@section('title', 'Cash Advance Sub-Categories')

@section('page-title')
    {{-- PC --}}
    <h2 class="hidden-xs hidden-sm"> 
        {{ $ca_category->name }} : Create New CA Sub-Category <br>
        <small>สร้างประเภทการเบิกเงินทดรองจ่ายย่อยใหม่</small>
    </h2>
    <ol class="breadcrumb hidden-xs hidden-sm">
        <li class="active">
            <a href="{{ route('settings.ca_categories.index') }}"> All Cash Advane Categories </a>
        </li>
        <li class="active">
            <a href="{{ route('settings.ca_sub_categories.index',[$ca_category->id]) }}"> {{ $ca_category->name }} : CA Sub-Categories</a>
        </li>
        <li class="active">
            <strong>Create New CA Sub-Category</strong>
        </li>
    </ol>
    {{-- MOBILE --}}
    <h3 class="m-t-md m-b-sm show-xs-only show-sm-only">
        <strong>Create New CA Sub-Category</strong> <br>
        <small>สร้างประเภทการเบิกเงินทดรองจ่ายย่อยใหม่</small>
    </h3>
@stop

@section('content')
    @include('layouts._error_messages')
    <div class="ibox float-e-margins">
        {!! Form::open(['route' => ['settings.ca_sub_categories.store',$ca_category->id], 'class' => 'form-horizontal']) !!}
          @include('settings.ca_sub_categories._form')
        {!! Form::close()!!}
    </div>
@endsection
