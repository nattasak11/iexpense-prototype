@extends('layouts.app')

@section('title', 'Sub-Categories')

@section('page-title')
    {{-- PC --}}
    <h2 class="hidden-xs hidden-sm"> 
        {{ $category->name }} : Sub-Categories <br>
        <small>แก้ไขข้อมูลประเภทการเบิกย่อย</small>
    </h2>
    <ol class="breadcrumb hidden-xs hidden-sm">
        <li class="active">
            <a href="{{ route('settings.categories.index') }}"> All Categories </a>
        </li>
        <li class="active">
            <a href="{{ route('settings.sub_categories.index',[$category->id]) }}"> {{ $category->name }} : Sub-Categories</a>
        </li>
        <li class="active">
            <strong>Edit Sub-Category : {{ $sub_category->name }}</strong>
        </li>
    </ol>
    {{-- MOBILE --}}
    <h3 class="m-t-md m-b-sm show-xs-only show-sm-only">
        <strong>Edit Sub-Category : {{ $sub_category->name }}</strong> <br>
        <small>แก้ไขข้อมูลประเภทการเบิกย่อย</small>
    </h3>
@stop

@section('content')
    @include('layouts._error_messages')
    <div class="ibox float-e-margins">
        {!! Form::model($sub_category, ['route' => ['settings.sub_categories.update', $category->id, $sub_category->id], 'class' => 'form-horizontal', 'method' => 'patch'] ) !!}
          @include('settings.sub_categories._form')
        {!! Form::close()!!}
    </div>
@endsection

