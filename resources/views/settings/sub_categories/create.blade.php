@extends('layouts.app')

@section('title', 'Sub-Categories')

@section('page-title')
    {{-- PC --}}
    <h2 class="hidden-xs hidden-sm"> 
        {{ $category->name }} : Create New Sub-Category <br>
        <small>สร้างประเภทการเบิกย่อยใหม่</small>
    </h2>
    <ol class="breadcrumb hidden-xs hidden-sm">
        <li class="active">
            <a href="{{ route('settings.categories.index') }}"> All Categories </a>
        </li>
        <li class="active">
            <a href="{{ route('settings.sub_categories.index',[$category->id]) }}"> {{ $category->name }} : Sub-Categories</a>
        </li>
        <li class="active">
            <strong>Create New Sub-Category</strong>
        </li>
    </ol>
    {{-- MOBILE --}}
    <h3 class="m-t-md m-b-sm show-xs-only show-sm-only">
        <strong>Create New Sub-Category</strong> <br>
        <small>สร้างประเภทการเบิกย่อยใหม่</small>
    </h3>
@stop

@section('content')
    @include('layouts._error_messages')
    <div class="ibox float-e-margins">
        {!! Form::open(['route' => ['settings.sub_categories.store',$category->id], 'class' => 'form-horizontal']) !!}
          @include('settings.sub_categories._form')
        {!! Form::close()!!}
    </div>
@endsection
