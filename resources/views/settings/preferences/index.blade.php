@extends('layouts.app')

@section('title', 'Preferences')

@section('page-title')
    <h2> 
        <div>Preferences</div>
        <div><small>การตั้งค่าพื้นฐาน</small></div>
    </h2>
    <ol class="breadcrumb hidden-xs hidden-sm">
        <li class="active">
            <strong>Preferences</strong>
        </li>
    </ol>
@stop

@section('page-title-action')
    <span id="ajax_spinner" class="pull-right m-t-md">
        @include('layouts._progress_bar',['size'=>20])
    </span>
@stop

@section('content')
    <div class="ibox float-e-margins">
        <div class="form-group row">
            <div class="col-md-2">
                <h4>
                    <div>Base Currency</div>
                    <div><small>สกุลเงินหลัก</small></div>
                </h4>
            </div>
            <div class="col-md-2">
                <p class="form-control-static">{{ $currency }}</p>
            </div>
        </div>
        <hr>
        <div class="form-group row">
            <div class="col-md-2">
                <h4>
                    <div>Base Mileage Unit</div>
                    <div><small>หน่วยวัดระยะทางหลัก</small></div>
                </h4>
            </div>
            <div class="col-md-2">
                {{-- {!! Form::select('mileage_unit',$mileageUnitLists, $mileageUnit, ['class'=>'form-control']) !!} --}}
                <p class="form-control-static">{{ $mileageUnitLists[$mileageUnit] }}</p> 
            </div>
        </div>
        <hr>
        <div class="form-group row">
            <div class="col-md-5">
                <h4>
                    <div>Cash advance unclear pending day(s) for blocking next request</div>
                    <div><small>จำนวนวันดำเนินการเคลียร์ก่อนถูกบล็อคคำขอเบิกถัดไป</small></div>
                </h4>
            </div>
            <div class="col-md-1">
                {!! Form::text('pending_day_blocking', $pendingDayBlocking, ['class'=>'form-control input-sm']) !!}
            </div>
            <div class="col-md-1" style="padding-left: 0px">
                <a type="button" id="btn_save_pending_day_blocking" class="btn btn-sm btn-success btn-block hide" style="margin-bottom: 0px;">
                    <i class="fa fa-save"></i> Save
                </a>
            </div>
        </div>
        <hr>
        <div class="form-group row">
            <div class="col-md-5">
                <h4>
                    <div>Over Budget Approver Job Level</div>
                    <div><small>ระดับของผู้อนุมัติรายการเบิกเกินงบประมาน</small></div>
                </h4>
            </div>
            <div class="col-md-2">
                {!! Form::select('over_budget_approver_job',$jobLists, $overBudgetApproverJob, ['class'=>'form-control']) !!}
            </div>
        </div>
        <hr>
        <div class="form-group row">
            <div class="col-md-8">
            <h4>
                <div>UnBlocking Responsible Users</div>
                <div><small>ผู้มีสิทธิ์ปลดบล็อค</small></div>
                <span class="pull-right">
                    <button class="btn btn-warning btn-outline btn-xs" data-toggle="modal" href="#modal-edit-unblock-users">
                        <i class="fa fa-edit"></i> Edit 
                    </button>
                </span>
            </h4>
            @include('settings.preferences._table_unblock_users')
            </div>
        </div>
    </div>

    @include('settings.preferences._modal_edit_unblock_users')

@endsection

@section('scripts')
    <script>
        $(document).ready(function() {

            $("#ajax_spinner").hide();

            // // ===== CURRENCY =====
            // $("select[name='currency']").change(function(){
            //     var currency = $("select[name='currency'] option:selected").val();
            //     ajaxUpdatePreference('currency',currency);
            // });

            // // ===== MILEAGE UNIT =====
            // $("select[name='mileage_unit']").change(function(){
            //     var mileage_unit = $("select[name='mileage_unit'] option:selected").val();
            //     ajaxUpdatePreference('mileage_unit',mileage_unit);
            // });

            
            // ===== PENDING DAY BLOCK NEXT REQUEST =====
            $("input[name='pending_day_blocking']").keypress(function(){
                $("#btn_save_pending_day_blocking").removeClass("hide");
            });

            // ===== OVER BUDGET APPROVER JOB =====
            $("select[name='over_budget_approver_job']").change(function(){
                var over_budget_approver_job = $("select[name='over_budget_approver_job'] option:selected").val();
                ajaxUpdatePreference('over_budget_approver_job',over_budget_approver_job);
            });

            $("#btn_save_pending_day_blocking").click(function(){
                var day = $("input[name='pending_day_blocking']").val();
                if(day != '' && $.isNumeric(day) && parseInt(day) >= 0 ) {
                    ajaxUpdatePreference('pending_day_blocking', parseInt(day),true);
                }else{
                    $("input[name='pending_day_blocking']").addClass('err_validate');
                }
            })

            //  ====== UNBLOCKING USER ======
            $("#btn_save_edit_unblock_users").click(function(event){
                event.preventDefault();
                var unblock_users = $("input[name='unblock_users[]']:checked").map(function(){
                    return $(this).val();
                }).get();
                ajaxUpdatePreference('unblock_users',unblock_users,true);
                $("#modal-edit-unblock-users").modal('hide');
            });

            // REMOVE UNBLOCKER EVENT
            $("[id^='btn_remove_unblocker_']").click(function(event){
                event.preventDefault();
                let userId = $(this).attr('data-value');
                ajaxSliceJsonData('unblock_users', userId, true);
            });

            // ====== AJAX UPDATE FUNCTION ==== 
            function ajaxUpdatePreference(code, data_value, refresh)
            {
                // DEFAULT DATA
                refresh = typeof refresh !== 'undefined' ? refresh : false;

                $.ajax({
                    url: "{{ url('/') }}/settings/preferences/update",
                    type: 'POST',
                    data: { _token: "{{ csrf_token() }}", 
                            code: code, 
                            data_value: data_value },
                    beforeSend: function() {
                        //
                    },
                    success: function (data) {
                        if(refresh){
                            swal({
                              title: "Preferences was edited !",
                              text: "this page will refresh in 2 seconds.",
                              type: "success", 
                              timer: 2000,
                              showConfirmButton: false
                            },function(){
                                location.reload();
                            });
                        }else{
                            toastr.options = {
                                "timeOut": "1000",
                            }
                            toastr.success('Preferences was edited !');
                        }
                    },
                    error: function(evt, xhr, status) {
                        swal(evt.responseJSON.message, null, "error");
                    },
                    complete: function(data) {
                        //
                    }
                });
            }

            function disableForm(){
                $("#btn_save_edit_unblock_users").attr("disabled","disabled");
                $("#btn_save_pending_day_blocking").attr("disabled","disabled");
                $("select[name='currency']").attr("disabled","disabled");
                $("select[name='mileage_unit']").attr("disabled","disabled");
            }

            function enabledForm(){
                $("#btn_save_edit_unblock_users").removeAttr("disabled");
                $("#btn_save_pending_day_blocking").removeAttr("disabled");
                $("select[name='currency']").removeAttr("disabled");
                $("select[name='mileage_unit']").removeAttr("disabled");
            }

            $( document ).ajaxStart(function() {
                disableForm();
                $("#ajax_spinner").show();
            });

            $( document ).ajaxStop(function() {
                enabledForm();
                $("#ajax_spinner").hide();
            });

            function ajaxSliceJsonData(code, data_value, refresh)
            {
                // DEFAULT DATA
                refresh = typeof refresh !== 'undefined' ? refresh : false;

                swal({   
                    title: "Are you sure?",    
                    type: "warning",   
                    showCancelButton: true,    
                    confirmButtonText: "Yes, remove it!",   
                    cancelButtonText: "cancel",
                    confirmButtonClass: 'btn btn-danger',
                    cancelButtonClass: 'btn btn-white',
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true
                }, 
                function(isConfirm){   
                    if (isConfirm) {    
                        $.ajax({
                          type: "post",
                          url: "{{ url('/') }}/settings/preferences/slice_json",
                          data: { _token: "{{ csrf_token() }}", 
                            code: code, 
                            data_value: data_value },
                          beforeSend: function() {
                            //
                          },
                          success: function (data) {
                            if(refresh){
                                swal({
                                  title: "Preferences was edited !",
                                  text: "this page will refresh in 2 seconds.",
                                  type: "success", 
                                  timer: 2000,
                                  showConfirmButton: false
                                },function(){
                                    location.reload();
                                });
                            }else{
                                toastr.options = {
                                    "timeOut": "1000",
                                }
                                toastr.success('Preferences was edited !');
                            }
                          },
                          error: function(evt, xhr, status) {
                              swal(evt.responseJSON.message, null, "error");
                          },
                          complete: function(data) {
                              //
                          }
                      });    
                    } 
                });
            }
        })
    </script>
@stop