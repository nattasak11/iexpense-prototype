<div id="modal-edit-unblock-users" class="modal fade" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body" id="modal-body-edit-unblock-users">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                	<h4>
                		<div>Select UnBlocking Responsible Users</div>
                		<div><small>เลือกผู้มีสิธิ์ปลดบล็อค</small></div>
                	</h4>
                	<div class="table-responsive">
	                    <table id="table-edit-unblock-users" class="table table-hover">
					        <tbody>
					        @if(count($users))
					        @foreach($users as $user)
								<tr>
									<td width="5%">
										{!! Form::checkbox('unblock_users[]',$user->id,in_array($user->id, $unblockers)) !!}
									</td>
						            <td width="40%">
										{{ $user->name }}
						            </td>
						            <td width="55%" class="text-left hidden-sm hidden-xs">
						            	<i style="color:#bbb" class="fa fa-envelope"></i> &nbsp;
						            	{{ $user->employee->email_address }}
						            </td>
						        </tr>
					        @endforeach
					        @else
								<tr>
									<td colspan="5">
										<h3 class="text-center m-t-md m-b-md" style="color:#bbb">
											Not found finance user.
										</h3>
									</td>
								</tr>
					        @endif
					        </tbody>
					    </table>
					</div>
					<div class="text-right">
						<button id="btn_save_edit_unblock_users"
								class="btn btn-sm btn-primary" type="submit">
			                Save
			            </button>
			            <button type="button" class="btn btn-sm btn-white" data-dismiss="modal">Cancel</button>
					</div>
                </div>
            </div>
        </div>
</div>
