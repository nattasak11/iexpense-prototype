@extends('layouts.app')

@section('title', 'Users')

@section('page-title')
    <h2>
        <div>Users</div>
        <div><small>รายชื่อผู้ใช้งานทั้งหมด</small></div>
    </h2>
    <ol class="breadcrumb hidden-xs hidden-sm">
        <li class="active">
            <strong>Users</strong>
        </li>
    </ol>
@stop

@section('page-title-action')
<div class="text-right m-t-lg">

    <button id="btn_submit_sync_users" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Syncing ..." type="button" class="btn btn-info btn-outline">
        <i class="fa fa-refresh"></i>&nbsp; Sync Users
    </button>

</div>
@stop

@section('content')
@include('layouts._error_messages')

<div class="ibox float-e-margins">
    <div class="table-responsive">
        <table class="table table-hover" id="tableUsers">
            <thead>
                <tr class="active">
                    <th class="no-sort" width="10%"></th>
                    <th width="30%">
                        <div>Name</div>
                        <div><small>ชื่อ</small></div>
                    </th>
                    <th width="30%" class="hidden-sm hidden-xs">
                        <div>Email Adress</div>
                        <div><small>อีเมล์</small></div>
                    </th>
                    <th width="10%" class="hidden-sm hidden-xs">
                        <div>ORG</div>
                        <div><small>บริษัท</small></div>
                    </th>
                    <th width="10%" class="hidden-xs">
                        <div>Role</div>
                        <div><small>บทบาท</small></div>
                    </th>
                    <th class="no-sort" width="10%"></th>
                </tr>
            </thead>
            <tbody>
            @if(count($users) > 0)
                @foreach ($users as $user)
                    <tr>
                        <td class="text-center">
                            <span class="hidden-xs">
                                {!! activeIcon($user->active) !!}
                            </span>
                            <span class="show-xs-only">
                                {!! activeMiniIcon($user->active) !!}
                            </span>
                        </td>
                        <td>
                            {{ $user->name }}
                            {{-- {{ $user->employee->full_name }} --}}
                        </td>
                        <td class="hidden-sm hidden-xs">
                            <i style="color:#bbb" class="fa fa-envelope"></i> &nbsp;
                            {{ $user->employee->email_address }}
                        </td>
                        <td class="hidden-sm hidden-xs">
                            {{ $user->org_id }}
                            {{-- {{ $user->employee->full_name }} --}}
                        </td>
                        <td class="hidden-xs">
                            {{ strtoupper($user->role) }}
                        </td>
                        <td class="text-right">
                            <a href="#" id="btn_edit_{{ $user->id }}" data-user-id="{{ $user->id }}" class="btn btn-block btn-outline btn-warning btn-xs">
                                <i class="fa fa-pencil"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td class="text-center" colspan="6">
                        <h2 style="color:#AAA;margin-top: 30px;margin-bottom: 30px;">Not Found.</h2>
                    </td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
    @if(isset($users))
    <div class="text-right">
        {{ $users->links() }}
    </div>
    @endif
</div>

{{-- MODAL EDIT USER --}}
@include('settings.users._modal_edit')

@endsection


@section('scripts')
@parent
<script type="text/javascript">
    $(document).ready(function(){

        bindEventSyncUsers();

        $("[id^='btn_edit_']").click(function(){
            $("#modal-edit-form").modal('show');
            let id = $(this).attr('data-user-id');
            $.ajax({
                url: "{{ url('/') }}/settings/users/"+id+"/edit",
                type: 'GET',
                beforeSend: function( xhr ) {
                    $("#modal-body-edit-form").html('\
                        <div class="m-t-lg m-b-lg">\
                            <div class="text-center sk-spinner sk-spinner-wave">\
                                <div class="sk-rect1"></div>\
                                <div class="sk-rect2"></div>\
                                <div class="sk-rect3"></div>\
                                <div class="sk-rect4"></div>\
                                <div class="sk-rect5"></div>\
                            </div>\
                        </div>');
                }
            })
            .done(function(result) {
                $("#modal-body-edit-form").html(result);
                // FORM EDIT SWITCHERY
                var elem = document.querySelector('#modal-body-edit-form .js-switch');
                var switchery = new Switchery(elem, { color: '#1AB394' });
            });
        });

        function bindEventSyncUsers(){
             $("#btn_submit_sync_users").click(function(){
                $.ajax({
                    url: "{{ url('/') }}/settings/users/sync_all_users",
                    type: 'POST',
                    data: { _token: "{{ csrf_token() }}" },
                    beforeSend: function( xhr ) {
                        swal({
                          title: "<h1>Syncing...</h1>",
                          text: '<div><span style="font-size: 14px;">Please wait, the system is retriving data from oracle employee ...</span></div><div class="p-lg"><div class="sk-spinner sk-spinner-three-bounce"><div class="sk-bounce1"></div><div class="sk-bounce2"></div><div class="sk-bounce3"></div></div></div>',
                          // type: "error",
                          html: true,
                          showConfirmButton: false
                        });
                        $("#btn_submit_sync_users").button('loading');
                    },
                    error: function(xhr, error){
                        swal({
                          title: "<h1>Error !</h1>",
                          text: '<span style="font-size: 14px;">sorry something went wrong, this page will refresh in 2 seconds.</span>',
                          type: "error",
                          html: true,
                          timer: 2000,
                          showConfirmButton: false
                        },function(){
                            location.reload();
                        });
                    },
                })
                .done(function() {
                    swal({
                      title: "<h1>Sync completed !</h1>",
                      text: '<span style="font-size: 16px;">sync user data completed, this page will refresh in 2 seconds.</span>',
                      type: "success",
                      html: true,
                      timer: 2000,
                      showConfirmButton: false
                    },function(){
                        location.reload();
                    });
                    $("#btn_submit_sync_users").button('reset');
                });
            });
        }

    });
</script>
@endsection