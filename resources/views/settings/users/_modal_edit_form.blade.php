{!! Form::model($user, ['route' => ['settings.users.update', $user->id], 'class' => 'form-horizontal', 'method' => 'patch'] ) !!}
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h3 class="m-b-md">
        <div>Edit User</div>
        <div><small>แก้ไขข้อมูลผู้ใช้งาน</small></div>
    </h3>
    <hr class="m-b-xs">

    <div class="form-group clearfix">
        <div class="col-sm-6">
            <label>
                <div>Name</div>
                <div><small>ชื่อ</small></div>
            </label>
            <p class="form-control-static">{{ $user->employee->full_name }}</p>
        </div>
        <div class="col-sm-6">
            <label>
                <div>Email</div>
                <div><small>อีเมล์</small></div>
            </label>
            <p class="form-control-static">{{ $user->employee->email_address }}</p>
        </div>
    </div>

    <div class="form-group clearfix">
        <div class="col-sm-12">
            <label>
                <div>Role <span class="text-danger">*</span></div>
                <div><small>บทบาท</small></div>
            </label>
            {!! Form::select('role',$roleLists ,$user->role , ["class" => 'form-control', "autocomplete" => "off"]) !!}
        </div>
    </div>
    <div class="form-group clearfix">
        <div class="col-sm-12">
            <label class="col-xs-6 control-label no-padding" style="text-align: left;">
                <div>Active ?</div>
                <div class="m-r-sm"><small>เปิดใช้งาน</small></div>
            </label>
            <div class="col-xs-6">
                <div class="checkbox">
                    {!! Form::checkbox('active', true, null,["class"=>"js-switch"] ) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix m-t-lg text-right">
        <div class="col-sm-12">
            <button class="btn btn-sm btn-primary" type="submit">
                Save
            </button>
            <button type="button" class="btn btn-sm btn-white" data-dismiss="modal">Cancel</button>
        </div>
    </div>
{!! Form::close()!!}