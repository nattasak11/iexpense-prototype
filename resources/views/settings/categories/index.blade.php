@extends('layouts.app')

@section('title', 'Categories')

@section('page-title')
    <h2>
        Categories  <br>
        <small>ประเภทการเบิก</small>
    </h2>
    <ol class="breadcrumb hidden-xs hidden-sm">
        <li class="active">
            <strong>Categories</strong>
        </li>
    </ol>
@stop

@section('page-title-action')
    <div class="text-right m-t-lg">
        <a href="{{ route('settings.categories.create') }}" 
            class="btn btn-primary pull-right">
            <i class="fa fa-plus"></i> New Category
        </a>
    </div>
@stop

@section('content')
    <div class="ibox float-e-margins">
        <div class="m-l-md m-r-md">
            @foreach($categories as $category)
            <div class="forum-item" style="margin: 7px 0; padding: 7px 0 10px;"> 
                <div class="row">
                    <div class="col-md-8">
                        <div class="forum-icon-policy">
                            <i class="fa {{ $category->icon }}"></i>
                        </div>
                        <div class="forum-item-policy-title">
                            {{ $category->name }}
                        </div>
                        <div class="forum-sub-title clearfix m-b-xs" style="margin-left: 70px;">
                            {{ $category->description }}
                        </div>
                    </div>
                    <div class="col-md-4 text-right">
                        <a href="{{ route('settings.sub_categories.index',[$category->id]) }}" class="btn btn-resize btn-white">
                            <i class="fa fa-th-list m-r-xs"></i> Sub-Categories 
                        </a>
                        <a href="{{ route('settings.categories.edit',[$category->id]) }}" class="btn btn-resize btn-white">
                            <i class="fa fa-edit m-r-xs"></i> Edit 
                        </a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
@endsection