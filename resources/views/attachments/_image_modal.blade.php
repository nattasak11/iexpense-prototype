<div class="modal-dialog modal-lg">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      {{-- <h4 class="modal-title">Modal Header</h4> --}}
    </div>
    <div class="modal-body">
      <img src="{{ url('attachments/'.$attachment->id.'/image') }}" class="img-in-modal" title="{{$attachment->original_name}}">
    </div>
    {{-- <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div> --}}
  </div>
</div>