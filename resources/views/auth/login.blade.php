@extends('layouts.blank')

@section('title', 'Login')

@section('content')

<div class="container" style="width: auto;">
    <div class="row m-t-xl">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default" style="border: none;">
                <div class="clearfix">
                    <p class="logo-name-mini hidden-xs" style="color: #ccc;">
                        iEXPENSE
                    </p>
                    <p class="logo-name-mini show-xs-only" style="font-size:50px;color: #ccc;">
                        iEXPENSE
                    </p>
                </div>
                <div class="panel-body p-lg" style="border-top: 7px solid #fafafa;">

                    @if (Session::has('err_login') && Session::get('err_login'))
                    <ul class="list-unstyled alert alert-warning alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <li>{!! Session::get('err_login') !!}</li>
                    </ul>
                    @endif

                   {{--  <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}"> --}}
                    <form class="form-horizontal" role="form" method="POST" 
                        action="{{ url('/login_ad') }}">

                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label for="username" class="col-md-4 control-label">AD Username</label>

                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required autofocus>

                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="remember"> Remember Me
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 text-right">
                                        <button type="submit" class="btn btn-success">
                                            Login
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
