<!DOCTYPE html>
<html>
    <head>
        <title>ERROR 503 : Be right back.</title>

        <link rel="stylesheet" href="/css/error.css" />
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Be right back.</div>
                <a href="{{ url('/') }}" class="btn btn-default" style="text-decoration:none;">
                    <i class="fa fa-home"></i> 
                    <strong>Home</strong> 
                </a>
            </div>
        </div>
    </body>
</html>
