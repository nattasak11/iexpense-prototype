@extends('layouts.app')

@section('title', 'Cash Advance')

@section('page-title')
    <h2>
        Cash Advance : New Request <br/>
        <small>สร้างใบเบิกเงินทดรองจ่ายใหม่</small>
    </h2>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('cash-advances.index') }}">Cash Advance</a>
        </li>
        <li class="active">
            <strong>New Request</strong>
        </li>
    </ol>
@stop

@section('content')
@include('layouts._error_messages')
@include('layouts._warning_create_request')
<div class="ibox float-e-margins">
    <div class="row">
        <div class="col-md-12">
        <strong>Request Form</strong>
        <span class="pull-right">
            <p>Request Date : {{ date(trans('date.format')) }}</p>
        </span>
        </div>
    </div>
    <hr class="m-t-sm mm-b-sm">
    {!! Form::open(['route' => ['cash-advances.store'], 'id' => 'form-cash-advance','class' => 'form-horizontal']) !!}
    <div class="row">
    {{-- FORM CASH ADVANCE --}}
        <div class="col-md-8 b-r">
            {{-- FORM CASH ADVANCE HTML --}}
            @include('cash-advances._form')
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <div class="col-sm-12">
                    <label>Attachments</label>
                    <div class="text-center dropzone m-t-xs m-b-xs" id="dropzoneFileUpload">
                        <div class="dz-message p-lg">
                            <div>
                                <i class="fa fa-file-text-o fa-3x"></i>
                            </div>
                            <div class="m-t-md">Drop files or Click</div>
                        </div>
                    </div>
                    <small style="color:#aaa"> Allow: jpeg, png, pdf, doc, docx, xls, xlsx, rar, zip and size < 5mb </small><br>
                    <small style="color:#aaa"> Max files : 2</small>
                </div>
            </div> 
            <div class="form-group">
                <div class="col-sm-12">
                    @include('commons._unclear_alert_message')
                </div>
            </div>
            @include('layouts._dropzone_template')
        </div>
    {{-- FORM --}}
    </div>
    <hr class="m-t-sm m-b-sm">
    <div class="row">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-offset-2 col-md-10 text-right">
                    @if(\Auth::user()->isAllowCreateRequest())
                        <button id="btn_submit" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Creating ..." type="button" class="btn btn-primary">
                            Create
                        </button>
                    @endif
                    <a href="{{ route('cash-advances.index') }}" 
                        id="btn_cancel" 
                        class="btn btn-white">
                        Cancel
                    </a>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close()!!}
</div>
@stop

@section('scripts')
<script>
    $(document).ready(function() {

        var formId = '#form-cash-advance';

        $(formId).find("select[name='ca_category_id']").select2();
        bindEventSubCategory();
        
        $(formId).find("select[name='ca_category_id']").change(function(){
            var caCategoryId = $(formId).find("select[name='ca_category_id'] option:selected").val();
            getSubCategories(caCategoryId);
            $("#div_form_informations").html('');
        });

        $("#btn_submit").click(function(e) {
            e.preventDefault();
            e.stopPropagation();
            var valid = validateBeforeSubmit();
            if(valid){
                $(this).button('loading');
                if (myDropzone.getQueuedFiles().length > 0) {                       
                    myDropzone.processQueue();  
                } else {                       
                    $("#form-cash-advance").submit();
                }
            }
        });

        // ==== DROPZONE ====
        var token = "{{ Session::getToken() }}";
        var myDropzone = new Dropzone("div#dropzoneFileUpload", {
            previewTemplate: $('#template-container').html(),
            url: "{{ url('/') }}/cash-advances/store",
            params: {
               _token: token
            },
            autoProcessQueue: false,
            uploadMultiple: true,
            maxFiles: 2,
            maxFilesize: 5, // MB
            acceptedFiles: '.jpg, .jpeg, .bmp, .png, .pdf, .doc, .docx, .xls, .xlsx, .rar, .zip, .7z, .txt',
            // Dropzone settings
            init: function() {
                var myDropzone = this;
                this.on("maxfilesexceeded", function(file) {
                    toastr.options = {
                        "timeOut": "3000",
                    }
                    toastr.error(file.name + ' upload failed.');
                });
                this.on('error', function(file, response) {
                    if(file){ this.removeFile(file); }
                    toastr.options = {
                        "timeOut": "3000",
                    }
                    toastr.error(response);
                });
                this.on('queuecomplete', function(){
                    $("#btn_submit").button('reset');
                });
            }
        });
        myDropzone.on('sending', function(file, xhr, formData){
            formData.append('amount', $("input[name='amount']").val());
            // formData.append('position_id', $("input[name='position_id']").val());
            formData.append('bank_account_id', $("input[name='bank_account_id']").val());
            formData.append('company_id', $("input[name='company_id']").val());
            formData.append('department_id', $("input[name='department_id']").val());
            formData.append('bank_account_id', $("input[name='bank_account_id']").val());
            formData.append('ca_category_id', $("select[name='ca_category_id'] option:selected").val());
            formData.append('ca_sub_category_id', $("select[name='ca_sub_category_id'] option:selected").val());
            formData.append('currency_id', $("input[name='currency_id']").val());
            formData.append('need_by_date', $("input[name='need_by_date']").val());
            formData.append('purpose', $("textarea[name='purpose']").val());
            $("[id^='ip_ca_sub_category_infos_']").each(function(index, element) {
                let formName = $(element).attr('name');
                let formValue = $(element).val();
                formData.append(formName, formValue);
            });
        });
        myDropzone.on("success", function(file, result) {
            if((result.status).toUpperCase() == 'ERROR'){
                swal({
                  title: "Error !",
                  text: "sorry something went wrong, this page will refresh in 2 seconds.",
                  type: "error", 
                  timer: 2000,
                  showConfirmButton: false
                },function(){
                    location.reload();
                });
            }else{
                $("#btn_submit").attr('disabled','disabled');
                setTimeout(function(){
                    window.location.href = ("{{ url('/') }}/cash-advances/"+result.cashAdvanceId);
                }, 1000);
            }
        });

        $('#txt_need_by_date').datepicker({
            format: "{{ trans('date.js-format') }}",
            todayBtn: true,
            multidate: false,
            keyboardNavigation: false,
            autoclose: true,
            todayBtn: "linked"
        });

        function bindEventSubCategory()
        {
            $(formId).find("select[name='ca_sub_category_id']").select2();
            $(formId).find("select[name='ca_sub_category_id']").change(function(){
            var caSubCategoryId = $(formId).find("select[name='ca_sub_category_id'] option:selected").val();
                getFormInformations(caSubCategoryId);
                console.log(caSubCategoryId);
            });
        }

        function getSubCategories(caCategoryId)
        {
            $.ajax({
                url: "{{ url('/') }}/cash-advances/get_sub_categories/",
                type: 'GET',
                data: {ca_category_id: caCategoryId},
                beforeSend: function( xhr ) {
                    $("#progress_modal").removeClass('hide');
                    $(formId).find("select[name='ca_sub_category_id']").prop("disabled", true);
                }
            })
            .done(function(result) {
                $("#div_ddl_ca_sub_category_id").html(result);
                $("#progress_modal").addClass('hide');
                bindEventSubCategory();
            });
        }

        function getFormInformations(caSubCategoryId)
        {
            if(caSubCategoryId){
                $.ajax({
                    url: "{{ url('/') }}/cash-advances/ca_sub_category/"+caSubCategoryId+"/get_form_informations",
                    type: 'GET',
                    beforeSend: function( xhr ) {
                        $("#progress_modal").removeClass('hide');
                    }
                })
                .done(function(result) {
                    $("#div_form_informations").html(result);
                    bindEventFormInformations();
                    $("#progress_modal").addClass('hide');
                });
            }else{
                $("#div_form_informations").html('');
            }
        }

        function bindEventFormInformations()
        {
            $('.info-date-picker').datepicker({
                format: "{{ trans('date.js-format') }}",
                todayBtn: true,
                multidate: false,
                keyboardNavigation: false,
                autoclose: true,
                todayBtn: "linked"
            });
        }

        function validateBeforeSubmit()
        {
            $("select[name='ca_sub_category_id']").parent().next("div.error_msg").remove();
            $("select[name='ca_sub_category_id']").removeClass('err_validate');
            $("input[name='amount']").parent().next("div.error_msg").remove();
            $("input[name='amount']").removeClass('err_validate');
            $("input[name='need_by_date']").next("div.error_msg").remove();
            $("input[name='need_by_date']").removeClass('err_validate');
            $("textarea[name='purpose']").next("div.error_msg").remove();
            $("textarea[name='purpose']").removeClass('err_validate');

            var valid = true; 
            var ca_sub_category_id = $("select[name='ca_sub_category_id'] option:selected").val();
            var amount = $("input[name='amount']").val();
            var need_by_date = $("input[name='need_by_date']").val();
            var purpose = $("textarea[name='purpose']").val();

            if(!ca_sub_category_id){
                valid = false;
                $("select[name='ca_sub_category_id']").addClass('err_validate');
                $("select[name='ca_sub_category_id']").parent().after('<div class="error_msg"> sub category is required. </div>');
            }
            
            if(!$.isNumeric(amount)){
                valid = false;
                $("input[name='amount']").addClass('err_validate');
                $("input[name='amount']").parent().after('<div class="error_msg"> amount is invalid. </div>');
            }else{
                if(parseFloat(amount) <= 0){
                    valid = false;
                    $(formId).find("input[name='amount']").addClass('err_validate');
                    $(formId).find("input[name='amount']").parent().after('<div class="error_msg"> amount must be greater than zero. </div>');
                }
            }

            if(!need_by_date){
                valid = false;
                $("input[name='need_by_date']").addClass('err_validate');
                $("input[name='need_by_date']").after('<div class="error_msg"> need by date is required.</div>');
            }
            if(!purpose){
                valid = false;
                $("textarea[name='purpose']").addClass('err_validate');
                $("textarea[name='purpose']").after('<div class="error_msg"> purpose is required.</div>');
            }

            var validInfos = validateReceiptLineInformations();
            if(valid){ valid = validInfos; }

            return valid;
        }

        function validateReceiptLineInformations()
        {
            var validInfos = true;

            $("[id^='ip_ca_sub_category_infos_']").each(function(index, element) {

                $(element).removeClass('err_validate');
                $(element).next("div.error_msg").remove();

                var value = $(element).val();
                var required = $(element).attr('data-required');
                var label = $(element).attr('data-label');
                if(required == 'required' && !value){
                    validInfos = false;
                    $(element).addClass('err_validate');
                    $(element).after('<div class="error_msg"> '+label+' is required. </div>');
                }

            });

            return validInfos;
        }

        $( document ).ajaxStart(function() {
            disableForm();
        });

        $( document ).ajaxStop(function() {
            enableForm();
        });

        function disableForm(){
            $(formId).find("select[name='ca_category_id']").attr('disabled','disabled');
            $(formId).find("select[name='ca_sub_category_id']").attr('disabled','disabled');
            $("#btn_submit").attr('disabled','disabled');
            $("#btn_cancel").attr('disabled','disabled');
        }

        function enableForm(){
            $(formId).find("select[name='ca_category_id']").removeAttr('disabled','disabled');
            $(formId).find("select[name='ca_sub_category_id']").removeAttr('disabled','disabled');
            $("#btn_submit").removeAttr('disabled','disabled');
            $("#btn_cancel").removeAttr('disabled','disabled');
        }

    });
</script>
@stop