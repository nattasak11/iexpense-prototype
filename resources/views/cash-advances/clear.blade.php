@extends('layouts.app')

@section('title', 'Cash Advance')

@section('page-title')
    <h2>
        Clearing Cash Advance <br/>
        <small>ใบเคลียร์เงินทดรองจ่าย</small>
    </h2>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('cash-advances.index') }}">Cash Advance</a>

        </li>
        <li class="active">
            <strong>Clearing : {{ $cashAdvance->clearing_document_no }}</strong>
        </li>
    </ol>
@stop

@section('page-title-action')

@stop

@section('content')
    @include('layouts._error_messages')
    {{-- ERR MSG OVER BUDGET --}}
    <div id="div_over_budget_err_msg_by_account" class="hide"></div>
    <div class="row">
        <div class="col-md-9">
            <div class="ibox">
                {{-- COMPARE CA & CLEAR --}}
                @include('cash-advances.clear._compare_clearing_amount')
            </div>
            <div class="ibox">
                {{-- MAIN DETAIL --}}
                @include('cash-advances.clear._main_detail')
            </div>
            <div class="ibox-content">
                @include('cash-advances.clear._receipts')
            </div>
            <div class="ibox-content">
                <div class="text-right">
                    {{-- CA REQUEST TRANS BUTTON --}}
                    @include('cash-advances.clear._button')
                </div>
            </div>
            <div class="ibox-content activity-content mini-scroll-bar m-b-md" 
                style="max-height: 400px;overflow: auto;">
                {{-- ACTIVITY LOG --}}
                @include('layouts._activities')
                {{-- @include('commons._activities') --}}
            </div>
        </div>
        <div class="col-md-3">
            <div class="ibox">
                @include('cash-advances.clear._other_detail')
            </div>
        </div>
    </div>
    {{-- MODAL FOR CLEARING REQUEST TRANS --}}
    @include('cash-advances.clear._modal_approval')
@stop

@section('scripts')

    {{-- SCRIPT CASH ADVANCE TRANSACTIONS --}}
    @include('cash-advances._script')

    {{-- SCRIPT RECEIPT --}}
    @include('commons.receipts._script')

@stop