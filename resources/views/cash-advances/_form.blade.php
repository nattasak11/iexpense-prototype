{{-- Requester --}}
<div class="row hidden-sm hidden-xs">
    <label for="" class="col-md-2 control-label">
        <div>Requester</div>
        <div>ผู้ขอเบิก</div>
    </label>
    <div class="col-md-3">
        <p class="form-control-static">
        {{ \Auth::user()->name }}
        {{-- {{ \Auth::user()->employee->full_name }} --}}
        </p>
    </div>
    <label for="" class="col-md-3 control-label">
        <div>Bank Account</div>
        <div>เลขที่บัญชี</div>
    </label>
    <div class="col-md-4">
        <p class="form-control-static">
        {{ \Auth::user()->employee->bank_name }} &nbsp; {{ \Auth::user()->employee->bank_account_num }}
        </p>
        {!! Form::hidden('bank_account_id', null ) !!}
    </div>
</div>

<div class="form-group hidden-sm hidden-xs">
    <label for="" class="col-md-2 control-label">
        <div>Company</div>
        <div>ชื่อบริษัท</div>
    </label>
    <div class="col-md-3">
        <p class="form-control-static">
        {{-- TMITH --}}
        {{ \Auth::user()->employee->company_name }}
        </p>
        {!! Form::hidden('company_id', null ) !!}
    </div>
    <label for="" class="col-md-3 control-label">
        <div>Department</div>
        <div>ชื่อแผนก</div>
    </label>
    <div class="col-md-4">
        <p class="form-control-static">
        {{-- IT --}}
        {{ \Auth::user()->employee->department_name }}
        </p>
        {!! Form::hidden('department_id', null ) !!}
    </div>
</div>

<div class="hr-line-dashed m-t-sm m-b-sm hidden-sm hidden-xs"></div>

<div class="form-group">
    <label for="" class="col-md-2 control-label label-no-padding">
        <div>Category <span class="text-danger">*</span></div>
        <div class="m-r-sm">ประเภท</div>
    </label>
    <div class="col-md-4">
        @if(!isset($cashAdvance))
            {!! Form::select('ca_category_id', [''=>'-'] + $CACategoryLists, null, ['class'=>'form-control','id'=>'ddl_ca_category_id']) !!}
        @else
            {{ $cashAdvance->category->name }}
        @endif
    </div>
    <label for="" class="col-md-2 control-label label-no-padding" style="padding-left: 0px;padding-right: 0px;">
        <div>Sub-Category <span class="text-danger">*</span></div>
        <div class="m-r-sm">ประเภทย่อย</div>
    </label>
    <div class="col-md-4">
        <div id="div_ddl_ca_sub_category_id">
        @if(!isset($cashAdvance))
            {!! Form::select('ca_sub_category_id', [''=>'-'] + $CASubCategoryLists , null,  ["class" => 'form-control input-sm select2', "id"=>"ddl_ca_sub_category_id", "autocomplete" => "off","style"=>"width:100%"]) !!}
        @else
            {{ $cashAdvance->subCategory->name }}
        @endif
        </div>
    </div>
</div>

<div class="form-group">
    <label for="" class="col-md-2 control-label label-no-padding" style="padding-left: 0px;padding-right: 0px;">
        <div>Need by Date <span class="text-danger">*</span></div>
        <div class="m-r-sm">วันที่ต้องการรับเงิน</div>
    </label>
    <div class="col-md-4">
        {!! Form::text('need_by_date', null, ['class'=>'form-control','id'=>'txt_need_by_date']) !!}
    </div>
    <label for="" class="col-md-2 control-label label-no-padding">
        <div>Amount <span class="text-danger">*</span></div>
        <div class="m-r-sm">จำนวนเงิน</div>
    </label>
    <div class="col-md-4">
        <div class="input-group">
            {!! Form::text('amount', null , ["class" => 'form-control text-right', "autocomplete" => "off"]) !!}
            <span class="input-group-addon">
                @if(!isset($cashAdvance->currency_id)) {{-- FORM CREATE --}}
                    {{ $baseCurrencyId }}
                    {!! Form::hidden('currency_id',$baseCurrencyId) !!}
                @else {{-- FORM EDIT --}}
                    {{ $cashAdvance->currency_id }}
                    {!! Form::hidden('currency_id',$cashAdvance->currency_id) !!}
                @endif
            </span>
        </div>
    </div>
</div>

{{-- <div class="row m-b-sm">
    <div class="col-sm-offset-2 col-md-10">
        <strong><small class="text-success">
            แผนกการเงินจะโอนเงินให้อย่างน้อย 5 วันทำการ หลังจากวันที่ Cash advance ได้รับอนุมัติ<br>(Please note that Finance Dept. will transfer money to your bank account at lease 5 working days after approval date in cash advance) 
        </small></strong>
    </div>
</div> --}}

<div class="form-group">
    <label for="" class="col-md-2 control-label label-no-padding">
        <div>Purpose <span class="text-danger">*</span></div>
        <div class="m-r-sm">วัตถุประสงค์</div>
    </label>
    <div class="col-md-10">
       {!! Form::textArea('purpose', null , ["class" => 'form-control', "autocomplete" => "off", "rows" => "2"]) !!}
    </div>
</div>

{{-- FORM INFORMATIONS --}}
<div class="form-group" id="div_form_informations">

    @include('cash-advances._form_informations')
    
</div>
