@extends('layouts.app')

@section('title', 'Cash Advance')

@section('page-title')
    <h2>
        Cash Advance : My Pending Activities<br/>
        <small>รายการใบเบิกเงินทดรองจ่ายที่คุณกำลังดำเนินการ</small>
    </h2>
    <ol class="breadcrumb">
        <li class="active">
            <strong>Cash Advance : My Pending Activities</strong>
        </li>
    </ol>
@stop

@section('page-title-action')

@stop

@section('content')
<div class="ibox float-e-margins">
    @include('cash-advances._table')
    @if(isset($cashAdvances))
    <div class="m-t-sm text-right">
        {!! $cashAdvances->appends($search)->render() !!}
    </div>
    @endif
</div>
@stop

@section('scripts')
    <script type="text/javascript">

        $(document).ready(function() {

            $("[id^='btn_clear_request_']").click(function(e){
                e.preventDefault(); // Prevent the href from redirecting directly
                var linkURL = $(this).attr("href");
                swal({   
                    html: true,
                    title: 'Create clearing request ?',   
                    text: '<h2 class="m-t-sm m-b-lg"><span style="font-size: 16px">Are you sure to create clearing cash advance request ?</span></h2>',   
                    // type: "info",   
                    showCancelButton: true,   
                    // confirmButtonColor: "#DD6B55",   
                    confirmButtonText: " Yes !",   
                    cancelButtonText: "cancel",
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-white',   
                    closeOnConfirm: true,   
                    closeOnCancel: true 
                }, 
                function(isConfirm){   
                    if (isConfirm) {     
                        window.location.href = linkURL;
                    }
                });
            });

        });

    </script>
@stop