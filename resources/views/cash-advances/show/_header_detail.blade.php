<div class="col-md-9">
    <div class="row">
        <div class="col-md-6">
            <p style="font-size: 1.1em">
                Document # : {{ $cashAdvance->document_no }}
            </p>
        </div>
        <div class="col-md-6 text-right">
            <span>{!! statusIconCA($cashAdvance->status) !!}</span>
        </div>
    </div>
    <hr class="m-t-sm">
</div>
<div class="col-md-3">
    <div class="clearfix">
        <small class="font-bold">Amount (จำนวนเงินที่เบิก)</small>
        <div class="text-right m-t-sm">
            <h2 style="font-size: 36px" class="no-margins">
                {{ number_format($cashAdvance->amount,2)}} <small>{{ $cashAdvance->currency_id }}</small>
            </h2>
        </div>
    </div>
    <hr class="m-b-xs">
</div>