@if($cashAdvance->next_approver)

{{-- NEXT APPROVER --}}
<dl id="recipt-details" class="dl-horizontal text-right m-b-xs" style="font-size: 12px;">
    <dt>
        <div><small>Next Approver</small></div>
        <div><small>ผู้อนุมัติคนถัดไป</small></div>
    </dt>
    <dd>
        <small>{{ $cashAdvance->next_approver ? $cashAdvance->next_approver : '-' }}</small>
    </dd>
</dl>

<hr class="m-t-sm m-b-sm">

@endif

<h5> Request Approval</h5>

<hr class="m-t-sm m-b-sm">

<dl id="recipt-details" class="dl-horizontal dl-request-approval text-right" style="font-size: 12px;">

    @if($cashAdvance->approvals->where('process_type','CASH-ADVANCE')->where('approver_type','APPROVER')->count() > 0)
        @foreach($cashAdvance->approvals->where('process_type','CASH-ADVANCE')->where('approver_type','APPROVER')->sortBy('created_at')->values()->all() as $key => $approval)

            <dt>
            @if($key == 0) 
                <div><small>Approved by </small></div> 
                <div><small>ผู้อนุมัติ </small></div> 
            @endif
            </dt>
            <dd>
                <div><small>{{ $approval->user->name }}</small></div>
                <div><small>{{ date(trans('date.time-format'),strtotime($approval->created_at)) }}</small></div>
            </dd>

        @endforeach
    @else
        <dt>
            <div><small>Approved by </small></div> 
            <div><small>ผู้อนุมัติ </small></div> 
        </dt>
        <dd><small>-</small></dd>
        <dt><small></small></dt>
        <dd><small>-</small></dd>
    @endif

    @if($cashAdvance->approvals->where('process_type','CASH-ADVANCE')->where('approver_type','FINANCE')->count() > 0)
        @foreach($cashAdvance->approvals->where('process_type','CASH-ADVANCE')->where('approver_type','FINANCE')->sortBy('created_at')->values()->all() as $key => $approval)

            <dt>
            @if($key == 0) 
                <div><small>Finance Approved by </small></div> 
                <div><small>เจ้าหน้าที่การเงินที่อนุมัติ </small></div> 
            @endif
            </dt>
            <dd>
                <div><small>{{ $approval->user->name }}</small></div>
                <div><small>{{ date(trans('date.time-format'),strtotime($approval->created_at)) }}</small></div>
            </dd>

        @endforeach
    @else
        <dt>
            <div><small>Finance Approved by </small></div> 
            <div><small>เจ้าหน้าที่การเงินที่อนุมัติ </small></div> 
        </dt>
        <dd><small>-</small></dd>
        <dt><small></small></dt>
        <dd><small>-</small></dd>
    @endif

</dl>

<hr class="m-t-sm m-b-sm">

<h5> Clearing Approval</h5>

<hr class="m-t-sm m-b-sm">

<dl id="recipt-details" class="dl-horizontal dl-request-approval text-right">

    @if($cashAdvance->approvals->where('process_type','CLEARING')->where('approver_type','APPROVER')->count() > 0)
        @foreach($cashAdvance->approvals->where('process_type','CLEARING')->where('approver_type','APPROVER')->sortBy('created_at')->values()->all() as $key => $approval)

            <dt>
            @if($key == 0) 
                <div><small>Approved by </small></div> 
                <div><small>ผู้อนุมัติ </small></div> 
            @endif
            </dt>
            <dd>
                <div><small>{{ $approval->user->name }}</small></div>
                <div><small>{{ date(trans('date.time-format'),strtotime($approval->created_at)) }}</small></div>
            </dd>

        @endforeach
    @else
        <dt>
            <div><small>Approved by </small></div> 
            <div><small>ผู้อนุมัติ </small></div> 
        </dt>
        <dd><small>-</small></dd>
        <dt><small></small></dt>
        <dd><small>-</small></dd>
    @endif
    
    @if($cashAdvance->approvals->where('process_type','CLEARING')->where('approver_type','FINANCE')->count() > 0)
        @foreach($cashAdvance->approvals->where('process_type','CLEARING')->where('approver_type','FINANCE')->sortBy('created_at')->values()->all() as $key => $approval)

            <dt>
            @if($key == 0) 
                <div><small>Finance Approved by </small></div> 
                <div><small>เจ้าหน้าที่การเงินที่อนุมัติ </small></div> 
            @endif
            </dt>
            <dd>
                <div><small>{{ $approval->user->name }}</small></div>
                <div><small>{{ date(trans('date.time-format'),strtotime($approval->created_at)) }}</small></div>
            </dd>

        @endforeach
    @else
        <dt>
            <div><small>Finance Approved by </small></div> 
            <div><small>เจ้าหน้าที่การเงินที่อนุมัติ </small></div> 
        </dt>
        <dd><small>-</small></dd>
        <dt><small></small></dt>
        <dd><small>-</small></dd>
    @endif

</dl>