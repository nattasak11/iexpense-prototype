<div class="row hidden-xs">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <dl class="dl-horizontal">
                    <dt>
                        <small>Requester</small>
                        <small>(ผู้ขอเบิก)</small>
                    </dt>
                    <dd>{{ $cashAdvance->user->name }}</dd>

                    <dt>
                        <small>Company</small>
                        <small>(ชื่อบริษัท)</small>
                    </dt>
                    <dd>
                        {{ $cashAdvance->user->employee->company_name }}
                    </dd>

                    <dt>
                        <small>Department</small>
                        <small>(ชื่อแผนก)</small>
                    </dt>
                    <dd>
                        {{ $cashAdvance->user->employee->department_name }}
                    </dd>
                </dl>
            </div>
            <div class="col-md-6">
                <dl class="dl-horizontal">
                    <dt>
                        <small>Bank Name</small>
                        <small>(ธนาคาร)</small>
                    </dt>
                    <dd>
                        {{ $cashAdvance->user->employee->bank_name }}
                    </dd>

                    <dt>
                        <small>Account No.</small>
                        <small>(เลขที่บัญชี)</small>
                    </dt>
                    <dd>
                        {{ $cashAdvance->user->employee->bank_account_num }}
                    </dd>

                    <dt>
                        <small>Account Name</small>
                        <small>(ชื่อบัญชี)</small>
                    </dt>
                    <dd>
                        {{ $cashAdvance->user->employee->vendor_name }}
                    </dd>
                </dl>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <dl class="dl-horizontal">
            <dt>
                <small>Category</small>
                <small>(ประเภท)</small>
            </dt>
            <dd>{{ $cashAdvance->category->name }}</dd>
            <dt>
                <small>Sub-Category</small>
                <small>(ประเภทย่อย)</small>
            </dt>
            <dd>{{ $cashAdvance->subCategory->name }}</dd>
        </dl>
        <dl class="dl-horizontal">
            <dt>
                <small>Purpose</small>
                <small>(วัตถุประสงค์)</small>
            </dt>
            <dd class="mini-scroll-bar" style="max-height: 200px;overflow: auto;">
                {!! $cashAdvance->purpose ? nl2br($cashAdvance->purpose) : '-' !!}
            </dd>
        </dl>
    </div>
    <div class="col-md-6">
        <dl class="dl-horizontal">
        @if($cashAdvanceInfos)
            @foreach($cashAdvanceInfos as $info)
                <dt><small>{{ $info->subCategoryInfo->attribute_name }}</small></dt>
                <dd><small>{{ $info->description_for_show }}</small></dd>
            @endforeach
        @endif
        </dl>
    </div>
</div>
