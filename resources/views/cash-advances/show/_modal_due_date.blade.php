@if($cashAdvance->status == 'FINANCE_PROCESS')

{{-- MODAL ENTER DUE DATE --}}
<div class="modal fade" id="enter-due-date" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document" style="width: 400px;">
        <div class="modal-content">
        
        {!! Form::open(['route' => ['cash-advances.set_due_date',$cashAdvance->id], 
              'method' => 'POST',
              'id' => 'form-enter-due-date',
              'class' => 'form-horizontal']) !!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Enter Due Date & Payment Method</h4>
            </div>
            <div class="modal-body clearfix">
                <div class="col-md-12">
                    <div class="row m-b-md">
                        <label>Due Date (วันที่กำหนดจ่าย)</label>
                        {!! Form::text('due_date', $cashAdvance->due_date, ['class'=>'form-control','id'=>'txt_due_date']) !!}
                    </div>
                    <div class="row">
                        <div>
                            <label>Payment Method (วิธีการชำระเงิน)</label>
                        </div>
                        <div class="clearfix m-b-sm">
                            <label class="radio-inline"> 
                                {!! Form::radio('payment_method_type', 'method', true) !!} 
                                Method Payment 
                            </label>
                            <label class="radio-inline"> 
                                {!! Form::radio('payment_method_type', 'manual', false) !!} 
                                Manual Payment 
                            </label>
                        </div>
                        <div id="div_payment_method_id" class="m-b-sm">
                            {!! Form::select('payment_method_id', [''=>''] + $APPaymentMethodLists, null,['class'=>'form-control','id'=>'ddl_payment_method_id']) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-sm btn-success">Save</button>
                <button type="button" class="btn btn-sm btn-white" data-dismiss="modal">Close</button>
            </div>
        {!! Form::close() !!}

        </div>
    </div>
</div>

@endif