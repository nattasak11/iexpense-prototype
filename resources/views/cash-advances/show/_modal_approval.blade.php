
{{-- ################################ --}}
{{-- ####### MODAL CA REQUEST ####### --}}
{{-- ################################ --}}

<div class="text-left">

@if($cashAdvance->status == 'NEW_REQUEST' && $cashAdvance->isRequester() && $cashAdvance->isNotLock())

    {{-- SEND REQUET WITH REASON --}}
    <div class="modal fade" id="modal-send-request-with-reason" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Send Request</h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-primary" disabled="disabled">Send Request</button>
                    <button type="button" class="btn btn-sm btn-white" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@endif

@if($cashAdvance->status == 'BLOCKED')

    {{-- UNBLOCK REQUEST  --}}
    <div class="modal fade" id="modal-unblock" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
            </div>
            <div class="modal-content">

            {!! Form::open(['route' => ['cash-advances.set_status',$cashAdvance->id],
                'method' => 'POST',
                'id' => 'form-unblock',
                'class' => 'form-horizontal']) !!}

                {!! Form::hidden('type','CASH-ADVANCE') !!}
                {!! Form::hidden('activity','UNBLOCK') !!}

                <div class="modal-body">
                    <div class="clearfix m-b-sm text-center">
                        <h1>Unblock Request ?</h1>
                        <h2><span style="font-size: 18px">Please enter reason to unblock request.</span></h2>
                    </div>
                    <div class="clearfix">
                        <label>Reason (เหตุผลประกอบ) <span class="text-danger">*</span></label>
                        {!! Form::textArea('reason', null , ["class" => 'form-control', "autocomplete" => "off", "style" => "height:100px;"]) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-warning">Unblock</button>
                    <button type="button" class="btn btn-sm btn-white" data-dismiss="modal">Close</button>
                </div>
            {!! Form::close() !!}

            </div>
        </div>
    </div>
@endif

@if($cashAdvance->status == 'APPROVER_DECISION' && $cashAdvance->isNextApprover())

    {{-- MODAL APPROVER APPROVE --}}

    <div class="modal fade" id="approve" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
            </div>
            <div class="modal-content">

            {!! Form::open(['route' => ['cash-advances.set_status',$cashAdvance->id],
                'method' => 'POST',
                'id' => 'form-approver-approve',
                'class' => 'form-horizontal']) !!}

                {!! Form::hidden('type','CASH-ADVANCE') !!}
                {!! Form::hidden('activity','APPROVER_APPROVE') !!}

                <div class="modal-body">
                    <div class="clearfix m-b-sm text-center">
                        <h1>Approve Request ?</h1>
                        <h2><span style="font-size: 18px">Are you sure to approve request ?</span></h2>
                    </div>
                    <div class="clearfix">
                        <label>Remark (หมายเหตุ) </label>
                        {!! Form::textArea('reason', null , ["class" => 'form-control', "autocomplete" => "off", "style" => "height:60px;"]) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary">Approve</button>
                    <button type="button" class="btn btn-sm btn-white" data-dismiss="modal">Close</button>
                </div>
            {!! Form::close() !!}

            </div>
        </div>
    </div>

    {{-- MODAL APPROVER SENDBACK  --}}
    <div class="modal fade" id="send-back" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
            </div>
            <div class="modal-content">

            {!! Form::open(['route' => ['cash-advances.set_status',$cashAdvance->id],
                'method' => 'POST',
                'id' => 'form-approver-send-back',
                'class' => 'form-horizontal']) !!}

                {!! Form::hidden('type','CASH-ADVANCE') !!}
                {!! Form::hidden('activity','APPROVER_SENDBACK') !!}

                <div class="modal-body">
                    <div class="clearfix m-b-sm text-center">
                        <h1>Send Back Request ?</h1>
                        <h2><span style="font-size: 18px">Please enter reason to send back request.</span></h2>
                    </div>
                    <div class="clearfix">
                        <label>Reason (เหตุผลประกอบ) <span class="text-danger">*</span></label>
                        {!! Form::textArea('reason', null , ["class" => 'form-control', "autocomplete" => "off", "style" => "height:100px;"]) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-warning">Send Back</button>
                    <button type="button" class="btn btn-sm btn-white" data-dismiss="modal">Close</button>
                </div>
            {!! Form::close() !!}

            </div>
        </div>
    </div>

    {{-- MODAL APPROVER REJECT --}}
    <div class="modal fade" id="reject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
            </div>
            <div class="modal-content">

            {!! Form::open(['route' => ['cash-advances.set_status',$cashAdvance->id],
                  'method' => 'POST',
                  'id' => 'form-approver-reject',
                  'class' => 'form-horizontal']) !!}

                {!! Form::hidden('type','CASH-ADVANCE') !!}
                {!! Form::hidden('activity','APPROVER_REJECT') !!}

                <div class="modal-body">
                    <div class="clearfix m-b-sm text-center">
                        <h1>Reject Request ?</h1>
                        <h2><span style="font-size: 18px">Please enter reason to reject request.</span></h2>
                    </div>
                    <div class="clearfix">
                        <label>Reason (เหตุผลประกอบ) <span class="text-danger">*</span></label>
                        {!! Form::textArea('reason', null , ["class" => 'form-control', "autocomplete" => "off", "style" => "height:100px;"]) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-danger">Reject the request</button>
                    <button type="button" class="btn btn-sm btn-white" data-dismiss="modal">Close</button>
                </div>
            {!! Form::close() !!}

            </div>
        </div>
    </div>

@endif

@if($cashAdvance->status == 'FINANCE_PROCESS' && \Auth::user()->isFinance())

    {{-- MODAL FINANCE APPROVE  --}}
    @if($cashAdvance->due_date)
    <div class="modal fade" id="finance-approve" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
            </div>
            <div class="modal-content">

            {!! Form::open(['route' => ['cash-advances.set_status',$cashAdvance->id],
                'method' => 'POST',
                'id' => 'form-finance-approve',
                'class' => 'form-horizontal']) !!}

                {!! Form::hidden('type','CASH-ADVANCE') !!}
                {!! Form::hidden('activity','FINANCE_APPROVE') !!}

                <div class="modal-body">
                    <div class="clearfix m-b-sm text-center">
                        <h1>Approve Request ?</h1>
                        <h2><span style="font-size: 18px">Are you sure to approve request ?</span></h2>
                    </div>
                    <div class="clearfix">
                        <label>Remark (หมายเหตุ) </label>
                        {!! Form::textArea('reason', null , ["class" => 'form-control', "autocomplete" => "off", "style" => "height:60px;"]) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary">Approve</button>
                    <button type="button" class="btn btn-sm btn-white" data-dismiss="modal">Close</button>
                </div>
            {!! Form::close() !!}

            </div>
        </div>
    </div>
    @endif

    {{-- MODAL FINANCE SENDBACK  --}}
    <div class="modal fade" id="finance-send-back" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
            </div>
            <div class="modal-content">

            {!! Form::open(['route' => ['cash-advances.set_status',$cashAdvance->id],
                'method' => 'POST',
                'id' => 'form-finance-send-back',
                'class' => 'form-horizontal']) !!}

                {!! Form::hidden('type','CASH-ADVANCE') !!}
                {!! Form::hidden('activity','FINANCE_SENDBACK') !!}

                <div class="modal-body">
                    <div class="clearfix m-b-sm text-center">
                        <h1>Send Back Request ?</h1>
                        <h2><span style="font-size: 18px">Please enter reason to send back request.</span></h2>
                    </div>
                    <div class="clearfix">
                        <label>Reason (เหตุผลประกอบ) <span class="text-danger">*</span></label>
                        {!! Form::textArea('reason', null , ["class" => 'form-control', "autocomplete" => "off", "style" => "height:100px;"]) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-warning">Send Back</button>
                    <button type="button" class="btn btn-sm btn-white" data-dismiss="modal">Close</button>
                </div>
            {!! Form::close() !!}

            </div>
        </div>
    </div>

    {{-- MODAL FINANCE REJECT --}}
    <div class="modal fade" id="finance-reject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
            </div>
            <div class="modal-content">

            {!! Form::open(['route' => ['cash-advances.set_status',$cashAdvance->id],
                  'method' => 'POST',
                  'id' => 'form-finance-reject',
                  'class' => 'form-horizontal']) !!}

                {!! Form::hidden('type','CASH-ADVANCE') !!}
                {!! Form::hidden('activity','FINANCE_REJECT') !!}

                <div class="modal-body">
                    <div class="clearfix m-b-sm text-center">
                        <h1>Reject Request ?</h1>
                        <h2><span style="font-size: 18px">Please enter reason to reject request.</span></h2>
                    </div>
                    <div class="clearfix">
                        <label>Reason (เหตุผลประกอบ) <span class="text-danger">*</span></label>
                        {!! Form::textArea('reason', null , ["class" => 'form-control', "autocomplete" => "off", "style" => "height:100px;"]) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-danger">Reject the request</button>
                    <button type="button" class="btn btn-sm btn-white" data-dismiss="modal">Close</button>
                </div>
            {!! Form::close() !!}

            </div>
        </div>
    </div>

@endif

</div>