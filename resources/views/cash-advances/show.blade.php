@extends('layouts.app')

@section('title', 'Cash Advance')

@section('page-title')
    <h2>
        Cash Advance <br/>
        <small>ใบเบิกเงินทดรองจ่าย</small>
    </h2>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('cash-advances.index') }}">Cash Advance</a>

        </li>
        <li class="active">
            <strong>{{ $cashAdvance->document_no }}</strong>
        </li>
    </ol>
@stop

@section('page-title-action')
    <div class="text-right m-t-lg">
    @if($cashAdvance->isRequester() && $cashAdvance->isNotLock())
        {{-- BUTTON EDIT --}}
        <button class="btn btn-resize btn-success btn-outline" data-toggle="modal" href="#modal-edit-form">
            <i class="fa fa-edit"></i> Edit
        </button>
        {{-- BUTTON CANCEL --}}
        <div class="btn-resize">
        {!! Form::open(['route' => ['cash-advances.set_status',$cashAdvance->id],
                    'method' => 'POST',
                    'id' => 'form-cancel-request']) !!}

            {!! Form::hidden('type','CASH-ADVANCE') !!}
            {!! Form::hidden('activity','CANCEL_REQUEST') !!}
            <button type="submit" class="btn btn-resize btn-danger btn-outline" disabled="disabled">
                <i class="fa fa-ban"></i> Cancel
            </button>

        {!! Form::close() !!}
        </div>
    @endif
    {{--  BUTTON ENTER DUE DATE  --}}
    @if($cashAdvance->status == 'FINANCE_PROCESS' && Auth::user()->isFinance())
        <button class="btn btn-resize btn-info btn-outline" data-toggle="modal" href="#enter-due-date">
            <i class="fa fa-edit"></i>&nbsp; Enter Due Date & Payment Method
        </button>
    @endif
    </div>
@stop

@section('content')

    @include('layouts._error_messages')

    <div class="row">
        <div class="col-md-12">
        {{-- BLOCKED (UNCLEAR ALERT MESSAGE) --}}
        @if($cashAdvance->status == 'BLOCKED')
            @include('commons._unclear_alert_message')
        @endif
        </div>
    </div>

    <div class="row">
        {{-- HEADER DETAIL (DOC# & AMOUNT) --}}
        @include('cash-advances.show._header_detail')
    </div>

    <div class="row">
        <div class="col-md-9">
            <div class="ibox">
                {{-- MAIN DETAIL --}}
                @include('cash-advances.show._main_detail')
            </div>
            <div class="ibox-content">
                <div class="text-right">
                    {{-- CA REQUEST TRANS BUTTON --}}
                    @include('cash-advances.show._button')
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="ibox">
                {{-- OTHER DETAIL --}}
                @include('cash-advances.show._other_detail')
                {{-- APPROVAL DETAIL --}}
                @include('cash-advances.show._approval_detail')
            </div>
        </div>
    </div>

    {{-- ACTIVITY LOG --}}
    <div class="row">
        <div class="col-md-9 no-padding-xs">
            <div class="ibox-content activity-content mini-scroll-bar m-b-md"
                style="max-height: 400px;overflow: auto;">
                {{-- ACTIVITY LOG --}}
                @include('layouts._activities')
            </div>
        </div>
    </div>

    {{-- MODAL FOR EDIT --}}
    @include('cash-advances._modal_edit')
    {{-- MODAL FOR CA REQUEST TRANS --}}
    @include('cash-advances.show._modal_approval')
    {{-- MODAL FOR ENTER DUE DATE --}}
    @if($cashAdvance->status == 'FINANCE_PROCESS' && Auth::user()->isFinance())
        @include('cash-advances.show._modal_due_date')
    @endif
@stop

@section('scripts')

    {{-- SCRIPT CASH ADVANCE TRANSACTIONS --}}
    @include('cash-advances._script')

@stop