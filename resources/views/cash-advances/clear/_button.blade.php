
{{-- ################################### --}}
{{-- ##### BUTTON CLEARING REQUEST ##### --}}
{{-- ################################### --}}


@if($cashAdvance->status == 'CLEARING_REQUEST' && $cashAdvance->isRequester())
    
    {{-- ######### BUTTON CLEARING REQUEST ######## --}}
    
    {!! Form::open(['route' => ['cash-advances.set_status',$cashAdvance->id], 
                    'method' => 'POST',
                    'id' => 'form-clear-send-request']) !!}
        
        {!! Form::hidden('type','CLEARING') !!}
        {!! Form::hidden('activity','CLEARING_SEND_REQUEST') !!}
        {!! Form::hidden('reason',null) !!}
        <button type="submit" class="btn btn-primary btn-resize" disabled="disabled">
            <i class="fa fa-rss"></i> Send Request
        </button>

    {!! Form::close() !!}

    {{-- <button class="btn btn-primary btn-resize" id="btn_clear_send_request">
        <i class="fa fa-rss"></i> Send Request
    </button> --}}
    
@endif


@if($cashAdvance->status == 'CLEARING_APPROVER_DECISION' && $cashAdvance->isNextClearingApprover())
    
    {{-- ######### BUTTON CLEARING APPROVER_DECISION ######## --}}
    {{-- <div class="btn-resize">
    {!! Form::open(['route' => ['cash-advances.set_status',$cashAdvance->id], 
                    'method' => 'POST',
                    'id' => 'form-clear-approver-approve',
                    'style' => 'display: inline;']) !!}

        {!! Form::hidden('type','CLEARING') !!}
        {!! Form::hidden('activity','CLEARING_APPROVER_APPROVE') !!}
        <button type="submit" class="btn btn-primary btn-resize" disabled="disabled">
            <i class="fa fa-check-square-o"></i> Approve
        </button>

    {!! Form::close() !!}
    </div> --}}

    <button class="btn btn-primary btn-resize" data-toggle="modal" data-target="#clear-approve">
        <i class="fa fa-check-square-o"></i> Approve
    </button>

    <button class="btn btn-warning btn-resize" data-toggle="modal" data-target="#clear-send-back">
        <i class="fa fa-reply"></i> Send Back
    </button>

@endif


@if($cashAdvance->status == 'CLEARING_FINANCE_PROCESS' && \Auth::user()->isFinance())
    
    {{-- ######### BUTTON CLEARING FINANCE PROCESS ######## --}}
    {{-- <div class="btn-resize">
    {!! Form::open(['route' => ['cash-advances.set_status',$cashAdvance->id], 
                    'method' => 'POST',
                    'id' => 'form-clear-finance-approve',
                    'style' => 'display: inline;']) !!}

        {!! Form::hidden('type','CLEARING') !!}
        {!! Form::hidden('activity','CLEARING_FINANCE_APPROVE') !!}
        <button type="submit" class="btn btn-primary btn-resize" disabled="disabled">
            <i class="fa fa-check-square-o"></i> Approve
        </button>

    {!! Form::close() !!}
    </div> --}}
    
    <button class="btn btn-primary btn-resize" data-toggle="modal" data-target="#clear-finance-approve">
        <i class="fa fa-check-square-o"></i> Approve
    </button>

    <button class="btn btn-warning btn-resize" data-toggle="modal" data-target="#clear-finance-send-back">
        <i class="fa fa-reply"></i> Send Back
    </button>

@endif
