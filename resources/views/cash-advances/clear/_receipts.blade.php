<div class="row">
    <div class="col-md-12">
        @if($cashAdvance->isRequester() && $cashAdvance->status == 'CLEARING_REQUEST')
        <div class="row text-right m-b-sm">
            <button type="button" class="btn btn-primary btn-xs btn-outline" data-toggle="modal" data-target="#modal_create_receipt">Add Receipt</button>
        </div>
        @endif
		<div class="row" id="div_clearing_receipt_list">

	        @include('commons.receipts._table',['parent'=>$cashAdvance])

		</div>
    </div>
    @include('commons.receipts._modal_create')
    @include('commons.receipts._modal_edit')
</div>