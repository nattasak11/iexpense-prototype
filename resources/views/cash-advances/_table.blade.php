<div class="table-responsive">
    <table class="table table-striped" id="tableCashAdvances">
        <thead>
            <tr>
                <th width="11%" class="text-center">
                    <div class="hidden-xs">Status</div>
                    <div class="hidden-xs">สถานะ</div>
                </th>
                <th width="14%">
                    <div class="hidden-sm hidden-xs">Document #</div>
                    <div class="hidden-sm hidden-xs">หมายเลขเอกสาร</div>
                    <div class="show-sm-only show-xs-only">Doc #</div>
                    <div class="show-sm-only show-xs-only">เอกสาร #</div>
                </th>
                <th width="10%" title="Request Date" class="hidden-xs hidden-sm">
                    <div>Req. Date</div>
                    <div>วันที่ขอเบิก</div>
                </th>
                <th width="18%" class="hidden-xs">
                    <div>Requester</div>
                    <div>ผู้ขอเบิก</div>
                </th>
                <th width="18%" class="hidden-xs hidden-sm">
                    <div>Pending User</div>
                    <div>ผู้ที่กำลังดำเนินการ</div>
                </th>
                <th width="5%" class="text-center hidden-xs hidden-sm no-sort" style="padding-left: 0px;padding-right: 0px;">
                    <div>Cleared</div>
                    <div>เคลียร์</div>
                </th>
                <th width="15%" class="text-right">
                    <div>Amount</div>
                    <div>จำนวนเงิน</div>
                </th>
                <th width="3%" class="no-sort hidden-xs hidden-sm" style="padding-left: 0px;padding-right: 0px;"></th>
                <th width="10%" class="text-right no-sort"></th>
            </tr>
        </thead>
        <tbody>
        @if(count($cashAdvances) > 0)
            @foreach($cashAdvances as $cashAdvance)
                <tr>
                    <td class="text-center">
                        {{-- {!! statusIconCA($cashAdvance->status) !!} --}}
                        <span class="hidden-xs">
                            {!! statusIconCA($cashAdvance->status) !!}
                        </span>
                        <span class="show-xs-only">
                            {!! statusMiniIconCA($cashAdvance->status) !!}
                        </span>
                    </td>
                    <td>{{ $cashAdvance->document_no }}</td>
                    <td class="hidden-xs hidden-sm">{{ dateFormatDisplay($cashAdvance->created_at) }}</td>
                    <td class="hidden-xs">{{ $cashAdvance->user ? $cashAdvance->user->name : '-' }}</td>
                    <td class="hidden-xs hidden-sm">{{ $cashAdvance->pending_user }}</td>
                    <td class="text-center hidden-xs hidden-sm" style="padding-left: 0px;padding-right: 0px;">
                    @if($cashAdvance->status == 'CLEARED')
                        <i class="fa fa-check-circle-o text-navy"></i>
                    @endif
                    </td>
                    <td class="text-right">
                        <div>
                        <span class="pull-left hidden-xs hidden-sm" style="font-size: 0.8em;">cash advance</span>
                        {{ number_format($cashAdvance->amount) }}
                        </div>
                        @if($cashAdvance->onClearing())
                        <div>
                        <span class="pull-left hidden-xs hidden-sm" style="font-size: 0.8em;">clear</span>
                        {{ $cashAdvance->total_receipt_amount ? number_format($cashAdvance->total_receipt_amount) : '-' }}
                        </div>
                        @endif
                    </td>
                    <td class="hidden-xs hidden-sm" style="padding-left: 0px;padding-right: 0px;">
                        <div>{{ $cashAdvance->currency_id }}</div>
                        @if($cashAdvance->onClearing())
                        <div>{{ $cashAdvance->currency_id }}</div>
                        @endif
                    </td>
                    <td class="text-right">
                        @if($cashAdvance->status == 'APPROVED' && $cashAdvance->isRequester())
                            {{-- SET STATUS TO CLEAR REQUEST --}}
                            <a id="btn_clear_request_{{ $cashAdvance->id }}" 
                                class="btn btn-xs btn-block btn-success btn-outline"
                                href="{{ route('cash-advances.clear-request', $cashAdvance->id) }}">
                                <i class="fa fa-check-square"></i> clear
                            </a>
                        @endif
                        @if($cashAdvance->onClearing())
                            <a class="btn btn-xs btn-block btn-success btn-outline" href="{{ route('cash-advances.clear', $cashAdvance->id) }}">
                                <i class="fa fa-check-square"></i> clear
                            </a>
                        @endif
                        <a class="btn btn-xs btn-block btn-primary btn-outline" href="{{ route('cash-advances.show', $cashAdvance->id) }}">
                            <i class="fa fa-file-text-o"></i> view
                        </a>
                        @if(isset($allowDuplicate))
                            <a id="btn_duplicate_{{ $cashAdvance->id }}" 
                                data-id="{{ $cashAdvance->id }}"
                                class="btn btn-xs btn-block btn-info btn-outline">
                                <i class="fa fa-copy"></i> duplicate
                            </a>
                        @endif
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td class="text-center" colspan="9">
                    <h2 style="color:#AAA;margin-top: 30px;margin-bottom: 30px;">Not Found.</h2>
                </td>
            </tr>
        @endif
        </tbody>
    </table>
</div>