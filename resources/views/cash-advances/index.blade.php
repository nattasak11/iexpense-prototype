@extends('layouts.app')

@section('title', 'Cash Advance')

@section('page-title')
    <h2>
        Cash Advance : My Request <br/>
        <small>รายการใบเบิกเงินทดรองจ่ายของคุณ</small>
    </h2>
    <ol class="breadcrumb">
        <li class="active">
            <strong>Cash Advance : My Request</strong>
        </li>
    </ol>
@stop

@section('page-title-action')
    <div class="text-right m-t-lg">
        <a href="{{ route('cash-advances.create') }}" class="btn btn-primary">
            <i class="fa fa-plus"></i> Request Cash Advance
        </a>
    </div>
@stop

@section('content')
<div class="ibox float-e-margins">
    @include('cash-advances._table')
    @if(isset($cashAdvances))
    <div class="m-t-sm text-right">
        {!! $cashAdvances->appends($search)->render() !!}
    </div>
    @endif
</div>
@stop

@section('scripts')
    <script type="text/javascript">

        $(document).ready(function() {

            $("[id^='btn_clear_request_']").click(function(e){
                e.preventDefault(); // Prevent the href from redirecting directly
                var linkURL = $(this).attr("href");
                swal({
                    html: true,
                    title: 'Create clearing request ?',
                    text: '<h2 class="m-t-sm m-b-lg"><span style="font-size: 16px">Are you sure to create clearing cash advance request ?</span></h2>',
                    // type: "info",
                    showCancelButton: true,
                    // confirmButtonColor: "#DD6B55",
                    confirmButtonText: " Yes !",
                    cancelButtonText: "cancel",
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-white',
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function(isConfirm){
                    if (isConfirm) {
                        window.location.href = linkURL;
                    }
                });
            });

            $("[id^='btn_duplicate_']").click(function(e){
                e.preventDefault(); // Prevent the href from redirecting directly
                var cashAdvanceId = $(this).attr("data-id");
                swal({
                    html: true,
                    title: 'Duplicate this cash advance ?',
                    text: '<h2 class="m-t-sm m-b-lg"><span style="font-size: 16px">Are you sure to duplicate this cash advance request ?</span></h2>',
                    // type: "info",
                    showCancelButton: true,
                    // confirmButtonColor: "#DD6B55",
                    confirmButtonText: " Yes !",
                    cancelButtonText: "cancel",
                    confirmButtonClass: 'btn btn-info',
                    cancelButtonClass: 'btn btn-white',
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function(isConfirm){
                    if (isConfirm) {
                        $.ajax({
                            type: "post",
                            url:  "{{ url('/') }}/cash-advances/"+cashAdvanceId+"/duplicate",
                            data: {
                                _token: "{{ csrf_token() }}",
                            },
                            beforeSend: function() {
                                //
                            },
                            success: function (data) {
                                swal({
                                  title: "Duplicate completed !",
                                  text: "this page will refresh in 2 seconds.",
                                  type: "success",
                                  timer: 2000,
                                  showConfirmButton: false
                                },function(){
                                    location.reload();
                                });
                            },
                            error: function(evt, xhr, status) {
                                  swal(evt.responseJSON.message, null, "error");
                            },
                            complete: function(data) {
                                  //
                            }
                        });
                    }
                });
            });

        });

    </script>
@stop