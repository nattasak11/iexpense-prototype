@extends('layouts.app-no-heading')

@section('title', 'Main page')

@section('content')
    <div class="ibox float-e-margins">
        {!! Form::open(['route' => ['main'], 'method' => 'GET', 'id' => 'form-search-home-page','class' => 'form-horizontal']) !!}
            {!! Form::hidden('default_tab', $defaultTab ) !!}
            <div class="tabs-container">
                <ul class="nav nav-tabs mini-tabs">
                    <li class="{{ $defaultTab == 'REIMBURSEMENT' ? 'active' : '' }}">
                        <a data-toggle="tab" href="#tab-reimbursement">
                            <div>
                                <i class="fa fa-money"></i>
                                <span class="hidden-sm hidden-xs">Reimbursement</span>
                            </div>
                            <div><small class="hidden-sm hidden-xs">รายการใบเบิกเงินชดเชย</small></div>
                        </a>
                    </li>
                    <li class="{{ $defaultTab == 'CASH-ADVANCE' ? 'active' : '' }}">
                        <a data-toggle="tab" href="#tab-cash-advance">
                            <div>
                                <i class="fa fa-bolt"></i>
                                <span class="hidden-sm hidden-xs">Cash Advance</span>
                            </div>
                            <div><small class="hidden-sm hidden-xs">รายการใบเบิกเงินทดรองจ่าย</small></div>
                        </a>
                    </li>
                    <li class="{{ $defaultTab == 'INVOICE' ? 'active' : '' }}">
                        <a data-toggle="tab" href="#tab-invoice">
                            <div ><i class="fa fa-file-text-o"></i>
                                <span class="hidden-sm hidden-xs">Invoice (pay to vendor)</span>
                            </div>
                            <div><small class="hidden-sm hidden-xs">รายการใบเบิกเพื่อจ่ายผู้ให้บริการ</small></div>
                        </a>
                    </li>
                    <li style="float: right !important;">
                        <div class="text-right">
                            <button type="button" class="btn btn-white"
                                style="cursor:default;border: 1px solid #e7eaec;">
                                <strong>{{ $yearShowing }}</strong>
                            </button>
                            <a class="btn btn-white" href="{{ route('main',['year_showing'=>(int)$yearShowing-1]) }}" style="border: 1px solid #e7eaec;">
                                <i class="fa fa-angle-left"></i>
                            </a>
                            @if($yearShowing < date('Y'))
                                <a class="btn btn-white" href="{{ route('main',['year_showing'=>(int)$yearShowing+1]) }}" style="border: 1px solid #e7eaec;">
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            @else
                                <a class="btn btn-white disabled" href="#"
                                    style="border: 1px solid #e7eaec;">
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            @endif
                        </div>
                    </li>
                </ul>
                <div class="tab-content">
                    <div id="tab-reimbursement" class="tab-pane {{ $defaultTab == 'REIMBURSEMENT' ? 'active' : '' }}">
                        <div class="panel-body no-padding-xs">
                            <div class="clearfix">
                                <div class="row">
                                    <div class="col-md-2 text-right m-b-xs">
                                        <button type="button" class="btn btn-block btn-sm btn-primary btn-outline" data-toggle="modal" data-target="#modal-export-reimbursements">
                                            <i class="fa fa-file-text-o"></i> Export
                                        </button>
                                    </div>
                                    <div class="col-md-10 m-b-xs">
                                        <div class="input-group">
                                            {!! Form::text('keyword_reim', $keyword['REIMBURSEMENT'], ['class' => 'form-control input-sm', 'placeholder' => 'Search Document #, Status, Requester, Pending User, ...', 'id' => 'input_keyword_reim']) !!}
                                            <span class="input-group-addon with-button">
                                                <button type="button" id="btn_search_reim" class="btn btn-sm btn-primary">
                                                    Search
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix" style="padding-top:0px;">
                            @include('reimbursements._table')
                            </div>
                        </div>
                    </div>
                    <div id="tab-cash-advance" class="tab-pane {{ $defaultTab == 'CASH-ADVANCE' ? 'active' : '' }}">
                        <div class="panel-body no-padding-xs">
                            <div class="clearfix">
                                <div class="row">
                                    <div class="col-md-2 text-right m-b-xs">
                                        <button type="button" class="btn btn-block btn-sm btn-primary btn-outline" data-toggle="modal" data-target="#modal-export-cash-advances">
                                            <i class="fa fa-file-text-o"></i> Export
                                        </button>
                                    </div>
                                    <div class="col-md-10 m-b-xs">
                                        <div class="input-group">
                                            {!! Form::text('keyword_ca', $keyword['CASH-ADVANCE'], ['class' => 'form-control input-sm', 'placeholder' => 'Search Document #, Status, Requester, Pending User, ...', 'id' => 'input_keyword_ca']) !!}
                                            <span class="input-group-addon with-button">
                                                <button type="button" id="btn_search_ca" class="btn btn-sm btn-primary">
                                                    Search
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix" style="padding-top:0px;">
                            @include('cash-advances._table')
                            </div>
                        </div>
                    </div>
                    <div id="tab-invoice" class="tab-pane {{ $defaultTab == 'INVOICE' ? 'active' : '' }}">
                        <div class="panel-body no-padding-xs">
                            <div class="clearfix">
                                <div class="row">
                                    <div class="col-md-2 text-right m-b-xs">
                                        <button type="button" class="btn btn-block btn-sm btn-primary btn-outline" data-toggle="modal" data-target="#modal-export-invoices">
                                            <i class="fa fa-file-text-o"></i> Export
                                        </button>
                                    </div>
                                    <div class="col-md-10 m-b-xs">
                                        <div class="input-group">
                                            {!! Form::text('keyword_invoice', $keyword['INVOICE'], ['class' => 'form-control input-sm', 'placeholder' => 'Search Document #, Status, Requester, Pending User, ...', 'id' => 'input_keyword_invoice']) !!}
                                            <span class="input-group-addon with-button">
                                                <button type="button" id="btn_search_invoice" class="btn btn-sm btn-primary">
                                                    Search
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix" style="padding-top:0px;">
                            @include('invoices._table')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
    </div>

    {{-- MODAL EXPORT REIMBURSEMENT --}}
    @include('reimbursements.export._modal_export')
    {{-- MODAL EXPORT CASHADVANCE --}}
    @include('cash-advances.export._modal_export')
    {{-- MODAL EXPORT INVOICE --}}
    @include('invoices.export._modal_export')

@endsection

@section('scripts')
    <script>
        $(document).ready(function(){

            $('.date-picker').datepicker({
                format: "{{ trans('date.js-format') }}",
                todayBtn: true,
                multidate: false,
                keyboardNavigation: false,
                autoclose: true,
                todayBtn: "linked"
            });

            $("#btn_search_reim").click(function(e){
                $("input[name='default_tab']").val('REIMBURSEMENT');
                $("#form-search-home-page").submit();
            });

            $("#btn_search_ca").click(function(e){
                $("input[name='default_tab']").val('CASH-ADVANCE');
                $("#form-search-home-page").submit();
            });

            $("#btn_search_invoice").click(function(e){
                $("input[name='default_tab']").val('INVOICE');
                $("#form-search-home-page").submit();
            });

            $("[id^='btn_clear_request_']").click(function(e){
                e.preventDefault(); // Prevent the href from redirecting directly
                var linkURL = $(this).attr("href");
                swal({
                    html: true,
                    title: 'Create clearing request ?',
                    text: '<h2 class="m-t-sm m-b-lg"><span style="font-size: 16px">Are you sure to create clearing cash advance request ?</span></h2>',
                    // type: "info",
                    showCancelButton: true,
                    // confirmButtonColor: "#DD6B55",
                    confirmButtonText: " Yes, create request now !",
                    cancelButtonText: "cancel",
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-white',
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function(isConfirm){
                    if (isConfirm) {
                        window.location.href = linkURL;
                    }
                });
            });

        });
    </script>
@endsection
