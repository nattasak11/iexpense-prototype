<?php
// \Auth::loginUsingId(\App\User::first()->id);
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

if (!env('APP_DEBUG', false)) {
    \Debugbar::disable();
}

Auth::routes();
Route::post('login_ad',array('uses' => 'Auth\LoginController@loginAD'))
    ->name('login_ad');

Route::get('login_by_id/{user_id}',array('uses' => 'Auth\LoginController@loginById'))
    ->name('login_by_id');
Route::get('logout', array('uses' => 'Auth\LoginController@logout'))
    ->name('logout');


//////// TEST LDAP LOGIN //////////
Route::get('/ldap', function(){
    // phpinfo(); exit;
    $user = 'ERPORA01';
    $password = 'P@$$W0RD!!!!';

    if(Adldap::auth()->attempt($user, $password)){
        $login='Login Passed';
    } else
    {
        $login='Failed';
    }
    return $login;
});

Route::group(['middleware' => ['auth']], function () {

    Route::get('/', 'HomeController@index')->name("main");
    Route::get('/minor', 'HomeController@minor')->name("minor");
    Route::get('/icon', 'HomeController@icon')->name("icon");
    Route::post('/over_budget_err_msg_by_account', 'HomeController@overBudgetErrMsgByAccount')->name("over_budget_err_msg_by_account");

    /////////////////////
    //// REIMBURSEMENT

    Route::get('reimbursements/pending', 'ReimbursementController@indexPending')->name("reimbursements.index_pending");
    Route::resource('reimbursements', 'ReimbursementController', ['only' => ['index', 'create','update','show']]);

    Route::post('/reimbursements/store', 'ReimbursementController@store')->name('reimbursements.store');
    Route::post('/reimbursements/export', 'ReimbursementController@export')->name('reimbursements.export');
    Route::group(['prefix'=>'reimbursements/{reimId}', 'as' => 'reimbursements.'], function () {

        Route::post('/set_status', 'ReimbursementController@setStatus')->name('set_status');
        Route::post('/add_attachment', 'ReimbursementController@addAttachment')->name('add_attachment');
        Route::get('/get_total_amount', 'ReimbursementController@getTotalAmount')->name('get_total_amount');
        Route::post('/duplicate', 'ReimbursementController@duplicate')->name('duplicate');

        Route::post('/combine_receipt_gl_code_combination', 'ReimbursementController@combineReceiptGLCodeCombination')
                ->name('combine_receipt_gl_code_combination');
        Route::post('/check_over_budget', 'ReimbursementController@checkOverBudget')
                ->name('check_over_budget');
        Route::post('/check_exceed_policy', 'ReimbursementController@checkExceedPolicy')
                ->name('check_exceed_policy');

        Route::post('/validate_receipt', 'ReimbursementController@validateReceipt')
                ->name('validate_receipt');

        Route::get('/form_send_request_with_reason','ReimbursementController@formSendRequestWithReason')->name('form_send_request_with_reason');
    });


    /////////////////////
    //// CASH-ADVANCE

    // Route::resource('cash-advances.receipt', 'ReceiptController');
    Route::get('cash-advances/pending', 'CashAdvanceController@indexPending')->name("cash-advances.index_pending");

    Route::get('cash-advances/get_sub_categories', 'CashAdvanceController@getSubCategories')
                    ->name('cash-advances.get_sub_categories');

    Route::get('cash-advances/ca_sub_category/{ca_sub_category}/get_form_informations', 'CashAdvanceController@getInputFormInformations')
            ->name('cash-advances.get_form_informations');

    Route::resource('cash-advances', 'CashAdvanceController', ['only' => ['index', 'create','update','show']]);

    Route::post('/cash-advances/store', 'CashAdvanceController@store')->name('cash-advances.store');
    Route::post('/cash-advances/export', 'CashAdvanceController@export')->name('cash-advances.export');

    Route::group(['prefix'=>'cash-advances/{cashAdvanceId}', 'as' => 'cash-advances.']
        , function () {

        Route::post('/set_status', 'CashAdvanceController@setStatus')->name('set_status');
        Route::post('/set_due_date', 'CashAdvanceController@setDueDate')->name('set_due_date');

        Route::post('/add_attachment', 'CashAdvanceController@addAttachment')->name('add_attachment');
        Route::get('/get_total_amount', 'CashAdvanceController@getTotalAmount')->name('get_total_amount');
        Route::get('/get_diff_ca_and_clearing_amount', 'CashAdvanceController@getDiffCAAndClearingAmount')->name('get_diff_ca_and_clearing_amount');

        Route::get('get_diff_ca_and_clearing_data', 'CashAdvanceController@getDiffCAAndClearingData')->name('get_diff_ca_and_clearing_data');

        Route::post('/duplicate', 'CashAdvanceController@duplicate')->name('duplicate');

        Route::get('/clear-request', 'CashAdvanceController@clearRequest')->name('clear-request');
        Route::get('/clear', 'CashAdvanceController@clear')->name('clear');

        Route::post('/check_ca_attachment', 'CashAdvanceController@checkCAAttachment')
                ->name('check_ca_attachment');
        Route::post('/check_ca_min_amount', 'CashAdvanceController@checkCAMinAmount')
                ->name('check_ca_min_amount');
        Route::post('/check_ca_max_amount', 'CashAdvanceController@checkCAMaxAmount')
                ->name('check_ca_max_amount');

        Route::post('/combine_receipt_gl_code_combination', 'CashAdvanceController@combineReceiptGLCodeCombination')
                ->name('combine_receipt_gl_code_combination');
        Route::post('/check_over_budget', 'CashAdvanceController@checkOverBudget')
                ->name('check_over_budget');
        Route::post('/check_exceed_policy', 'CashAdvanceController@checkExceedPolicy')
                ->name('check_exceed_policy');

        Route::post('/validate_receipt', 'CashAdvanceController@validateReceipt')
                ->name('validate_receipt');

        Route::get('/form_send_request_with_reason','CashAdvanceController@formSendRequestWithReason')->name('form_send_request_with_reason');

    });

    //////////////////////////////
    //// INVOICE (PAY TO VENDOR)

    Route::get('invoices/pending', 'InvoiceController@indexPending')->name("invoices.index_pending");
    Route::resource('invoices', 'InvoiceController', ['only' => ['index', 'create','update','show']]);

    Route::post('/invoices/store', 'InvoiceController@store')->name('invoices.store');
    Route::post('/invoices/export', 'InvoiceController@export')->name('invoices.export');
    Route::group(['prefix'=>'invoices/{reimId}', 'as' => 'invoices.'], function () {

        Route::post('/set_status', 'InvoiceController@setStatus')->name('set_status');
        Route::post('/add_attachment', 'InvoiceController@addAttachment')->name('add_attachment');
        Route::get('/get_total_amount', 'InvoiceController@getTotalAmount')->name('get_total_amount');
        Route::post('/duplicate', 'InvoiceController@duplicate')->name('duplicate');

        Route::post('/combine_receipt_gl_code_combination', 'InvoiceController@combineReceiptGLCodeCombination')
                ->name('combine_receipt_gl_code_combination');
        Route::post('/check_over_budget', 'InvoiceController@checkOverBudget')
                ->name('check_over_budget');
        Route::post('/check_exceed_policy', 'InvoiceController@checkExceedPolicy')
                ->name('check_exceed_policy');

        Route::post('/validate_receipt', 'InvoiceController@validateReceipt')
                ->name('validate_receipt');

        Route::get('/form_send_request_with_reason','InvoiceController@formSendRequestWithReason')->name('form_send_request_with_reason');
    });

    //////////////
    //// RECEIPT

    Route::group(['prefix' => 'receipts', 'as' => 'receipts.'], function () {

        Route::get('/get_vendor_detail/{vendor_id}', 'ReceiptController@getVendorDetail')->name('get_vendor_detail');
        Route::get('/get_rows', 'ReceiptController@getRows')->name('get_rows');
        Route::get('/get_table_total_rows', 'ReceiptController@getTableTotalRows')->name('get_table_total_rows');
        Route::get('/form_create', 'ReceiptController@formCreate')->name('form_create');
        Route::post('/store', 'ReceiptController@store')->name('store');
        Route::get('/{receipt}/show', 'ReceiptController@show')->name('show');
        Route::get('/{receipt}/form_edit', 'ReceiptController@formEdit')->name('form_edit');
        Route::post('/{receipt}/update', 'ReceiptController@update')->name('update');
        Route::post('/{receipt}/add_attachment', 'ReceiptController@addAttachment')->name('add_attachment');
        Route::post('/{receipt}/duplicate', 'ReceiptController@duplicate')->name('duplicate');
        Route::post('/{receipt}/remove', 'ReceiptController@remove')->name('remove');

        Route::group(['prefix' => '/{receipt}/lines', 'as' => 'lines.'], function () {

            Route::post('/store', 'ReceiptLineController@store')
                    ->name('store');
            Route::post('/{line}/update', 'ReceiptLineController@update')
                    ->name('update');
            Route::post('/{line}/duplicate', 'ReceiptLineController@duplicate')
                    ->name('duplicate');
            Route::post('/{line}/remove', 'ReceiptLineController@remove')
                    ->name('remove');
            Route::get('/{line}/get_show_infos', 'ReceiptLineController@getShowInfos')
                    ->name('get_show_infos');

            // GET FORM FOR CREATE RECEIPT LINE
            Route::get('/form_create', 'ReceiptLineController@formCreate')
                    ->name('form_create');
            Route::get('/get_sub_categories', 'ReceiptLineController@getSubCategories')
                    ->name('get_sub_categories');
            Route::get('/sub_category/{sub_category}/get_form_informations', 'ReceiptLineController@getInputFormInformations')
                    ->name('sub_category.get_form_informations');
            Route::get('/sub_category/{sub_category}/get_form_amount', 'ReceiptLineController@getInputFormAmount')
                    ->name('sub_category.get_form_amount');

            // GET FORM FOR EDIT RECEIPT LINE
            Route::get('/{line}/form_edit', 'ReceiptLineController@formEdit')
                    ->name('form_edit');
            Route::get('/{line}/get_form_edit_informations', 'ReceiptLineController@getInputFormEditInformations')
                    ->name('get_form_edit_informations');
            Route::get('/{line}/get_form_edit_amount', 'ReceiptLineController@getInputFormEditAmount')
                    ->name('get_form_edit_amount');

        });

    });


    //////////////
    //// SETTING

    Route::group(['prefix' => 'settings', 'namespace' => 'Settings', 'as' => 'settings.','middleware'=>['superuser']],
        	function () {

        // CA Categories
        Route::resource('ca_categories', 'CACategoriesController',
            ['only' => ['index', 'create', 'store', 'edit', 'update']]
        );
        Route::post('ca_categories/{ca_category}/remove', 'CACategoriesController@remove')->name("ca_categories.remove");

        Route::group(['prefix'=>'ca_categories/{ca_category}'], function () {

            // CA Sub-Categories
            Route::get('ca_sub_categories/input_sub_account_code/', 'CASubCategoryController@inputSubAccountCode')->name("input_sub_account_code");
            Route::resource('ca_sub_categories', 'CASubCategoryController');
            Route::group(['prefix'=>'ca_sub_categories/{ca_sub_category}'], function () {

                // CA Sub-Categories Info.
                Route::group(['as'=>'ca_sub_categories.'], function () {
                    Route::resource('infos', 'CASubCategoryInfoController');
                    Route::get('/input_form_type/{input_form_type}', 'CASubCategoryInfoController@inputFormType')
                                ->name("input_form_type");
                    Route::group(['prefix'=>'infos/{info}', 'as'=>'infos.'], function () {
                        // Sub-Categories Info. Inactive
                        Route::post('/inactive', 'CASubCategoryInfoController@inactive')
                                    ->name("inactive");
                    });
                });

            });
        });

        // REIM Categories
        Route::resource('categories', 'CategoriesController',
            ['only' => ['index', 'create', 'store', 'edit', 'update']]
        );
        Route::post('categories/{category}/remove', 'CategoriesController@remove')
                                    ->name("categories.remove");

        Route::group(['prefix'=>'categories/{category}'], function () {

            // Sub-Categories
            Route::get('sub_categories/input_sub_account_code/', 'SubCategoryController@inputSubAccountCode')->name("input_sub_account_code");
            Route::resource('sub_categories', 'SubCategoryController');
            Route::group(['prefix'=>'sub_categories/{sub_category}'], function () {

                // Sub-Categories Info.
                Route::group(['as'=>'sub_categories.'], function () {
                    Route::resource('infos', 'SubCategoryInfoController');
                    Route::get('/input_form_type/{input_form_type}', 'SubCategoryInfoController@inputFormType')
                                ->name("input_form_type");
                    Route::group(['prefix'=>'infos/{info}', 'as'=>'infos.'], function () {
                        // Sub-Categories Info. Inactive
                        Route::post('/inactive', 'SubCategoryInfoController@inactive')
                                    ->name("inactive");
                    });
                });

                // Policy
                Route::resource('policies', 'PolicyController',
                    ['only' => ['index', 'create', 'store']]);
                Route::group(['prefix'=>'policies/{policy}', 'as'=>'policies.'], function () {
                    // Policy Inactive
                    Route::post('/inactive', 'PolicyController@inactive')
                                ->name("inactive");

                    Route::resource('rates', 'PolicyRateController');
                    Route::group(['prefix'=>'rates/{rate}', 'as'=>'rates.'], function () {
                        // Policy Rate Inactive
                        Route::post('/inactive', 'PolicyRateController@inactive')
                                    ->name("inactive");
                    });

            	});

            });
        });

        // Locations
        Route::resource('locations', 'LocationController',['only'=>['index', 'create', 'store', 'edit', 'update']]);
        Route::post('locations/inactive', 'LocationController@inactive')
                                    ->name("locations.inactive");

        // User
        Route::resource('users', 'UserController',['only'=>['index', 'edit', 'update']]);
        Route::post('users/inactive', 'UserController@inactive')
                                    ->name("users.inactive");
        Route::post('users/sync_all_users', 'UserController@syncAllUsers')
                                    ->name("users.sync_all_users");

        Route::get('preferences', 'PreferenceController@index')->name('preferences.index');
        Route::post('preferences/update', 'PreferenceController@update')->name('preferences.update');
        Route::post('preferences/slice_json', 'PreferenceController@sliceJson')->name('preferences.slice_json');

    });

    //////////////////
    //// ATTACHMENTS

    Route::get('attachments/{attachment_id}/download', ['as' => 'attachments.download', 'uses' => 'AttachmentsController@download']);
    Route::get('attachments/{attachment_id}/image', ['as' => 'attachments.image', 'uses' => 'AttachmentsController@image']);
    Route::post('attachments/{attachment_id}/image_modal', ['as' => 'attachments.image_modal', 'uses' => 'AttachmentsController@imageModal']);
    Route::post('/attachments/{attachment_id}/remove', ['as' => 'attachments.remove', 'uses' => 'AttachmentsController@remove']);
    Route::delete('/attachments/{attachment_id}/remove', ['as' => 'attachments.remove', 'uses' => 'AttachmentsController@remove']);

    /////////////////////
    // AP INTERFACE LOG
    Route::group(['middleware'=>['superuser']], function () {

        Route::get('interface_ap', [
                    'as' => 'interface_ap.index',
                    'uses' => 'InterfaceAPController@index'
                ]);
        Route::get('interface_ap/{interface_ap_header_id}/show', [
                    'as' => 'interface_ap.show',
                    'uses' => 'InterfaceAPController@show'
                ]);
        Route::post('interface_ap/{interface_ap_header_id}/re_process', [
                    'as' => 'interface_ap.re_process',
                    'uses' => 'InterfaceAPController@reProcess'
                ]);

    });

});

