const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {

    mix.sass('app.scss')
       .webpack('app.js')
       .webpack('resources/assets/js/app/reimbursemnts/receipt/line/form.js',
        './public/js/app/reimbursemnts/receipt/line/form.js')
       .webpack('resources/assets/js/app/reimbursemnts/receipt/form.js',
        './public/js/app/reimbursemnts/receipt/form.js');

    mix.styles([
        // './resources/bower_components/jquery-mobile/css/themes/default/jquery.mobile.css',
        './resources/bower_components/bootstrap-sweetalert/dist/sweetalert.css',
        './resources/bower_components/ladda/dist/ladda-themeless.min.css',
        './resources/bower_components/dropzone/dist/dropzone.css',
        './resources/assets/css/plugins/select2/select2.min.css',
        './resources/assets/css/plugins/footable/footable.core.css',
        './resources/assets/css/plugins/iCheck/custom.css',
        './resources/assets/css/plugins/switchery/switchery.css',
        './resources/assets/css/plugins/toastr/toastr.min.css',
        './resources/assets/css/plugins/dataTables/datatables.min.css',
        './resources/assets/css/plugins/morris/morris-0.4.3.min.css',
        './resources/assets/css/plugins/chosen/bootstrap-chosen.css',
        './resources/assets/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css',
        './node_modules/jquery.mmenu/dist/jquery.mmenu.all.css',
    ], 'public/css/bower_components_all.css')

    .version([
      'css/bower_components_all.css',
    ]);

    // iCheck Image
    mix.copy([
        './resources/assets/css/plugins/chosen/chosen-sprite.png',
        './resources/assets/css/plugins/chosen/chosen-sprite@2x.png',
        './resources/assets/css/plugins/iCheck/green.png',
        './resources/assets/css/plugins/iCheck/green@2x.png',
    ],'public/build/css');

    //java scripts
    mix.scripts([
        // './resources/bower_components/jquery-mobile/js/jquery.mobile.js',
        './resources/bower_components/bootstrap-sweetalert/dist/sweetalert.min.js',
        './resources/bower_components/ladda/dist/spin.min.js',
        './resources/bower_components/ladda/dist/ladda.min.js',
        './resources/bower_components/ladda/dist/ladda.jquery.min.js',
        './resources/bower_components/dropzone/dist/dropzone.js',
        './resources/assets/js/plugins/metisMenu/jquery.metisMenu.js',
        './resources/assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
        './resources/assets/js/plugins/select2/select2.full.min.js',
        './resources/assets/js/plugins/pace/pace.min.js',
        './resources/assets/js/plugins/footable/footable.all.min.js',
        './resources/assets/js/plugins/iCheck/icheck.min.js',
        './resources/assets/js/plugins/switchery/switchery.js',
        './resources/assets/js/plugins/toastr/toastr.min.js',
        './resources/assets/js/plugins/dataTables/datatables.min.js',
        './resources/assets/js/plugins/morris/morris.js',
        './resources/assets/js/plugins/chosen/chosen.jquery.js',
        './resources/assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js',
        './node_modules/jquery.mmenu/dist/jquery.mmenu.all.js',
    ], 'public/js/bower_components_all.js');


});
