<?php

use Illuminate\Database\Seeder;

class GlCodeCombinationsKfvTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('xxweb_gl_code_combinations_kfv')->truncate();
        DB::table('xxweb_gl_code_combinations_kfv')->insert($this->items());
    }

    public function items()
    {
        $lists = [];

        array_push($lists, [
         'code_combination_id' => '2001',
         'chart_of_accounts_id' => '50388', 
         'concatenated_segments' => '01.SM.000.000.00.19505.000.000.00.0.000.000',
         'padded_concatenated_segments' => '01.SM.000.000.00.19505.000.000.00.0.000.000', 
         'gl_account_type' => 'L',
         'ledger_segment' => null,
         'enabled_flag' => 'Y',
         'summary_flag' => 'N',
         'segment1' => '01',
         'segment2' => 'SM', 
         'segment3' => '000',
         'segment4' => '000',
         'segment5' => '00',
         'segment6' => '19505',
         'segment7' => '000',
         'segment8' => '000', 
         'segment9' => '00',
         'segment10' => '0',
         'segment11' => '000',
         'segment12' => '000',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        return $lists;
        
    }
}
