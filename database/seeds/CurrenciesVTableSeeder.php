<?php

use Illuminate\Database\Seeder;

class CurrenciesVTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('xxweb_currencies_v')->truncate();
        DB::table('xxweb_currencies_v')->insert($this->items());
    }

    public function items()
    {
        $lists = [];

        array_push($lists, [
         'currency_code' => 'THB', 
         'currency_name' => 'Baht', 
         'currency_description' => 'Baht',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'currency_code' => 'JPY', 
         'currency_name' => 'Yen', 
         'currency_description' => 'Japanese yen',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'currency_code' => 'EUR', 
         'currency_name' => 'Euro', 
         'currency_description' => 'Pan-European Currency',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'currency_code' => 'SGD', 
         'currency_name' => 'Singapore Dollar', 
         'currency_description' => 'Singapore Dollar',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'currency_code' => 'USD', 
         'currency_name' => 'US Dollar', 
         'currency_description' => 'US Dollar',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        return $lists;
        
    }
}
