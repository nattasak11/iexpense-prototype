<?php

use Illuminate\Database\Seeder;

class FinancialsSystemParamsAllTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('financials_system_params_all')->truncate();
        DB::table('financials_system_params_all')->insert($this->items());
    }

    public function items()
    {
        $lists = [];

        array_push($lists, [
         'set_of_books_id' => '1110', 
         'payment_method_lookup_code' => '2021', 
         'ship_to_location_id' => '142',
         'bill_to_location_id' => '142',
         'accts_pay_code_combination_id' => '3001', 
         'prepay_code_combination_id' => '3002',
         'org_id' => '81',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'set_of_books_id' => '1110', 
         'payment_method_lookup_code' => '2041', 
         'ship_to_location_id' => '142',
         'bill_to_location_id' => '142',
         'accts_pay_code_combination_id' => '6000', 
         'prepay_code_combination_id' => '6001',
         'org_id' => '121',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        return $lists;
        
    }
}
