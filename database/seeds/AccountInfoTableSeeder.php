<?php

use Illuminate\Database\Seeder;

class AccountInfoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('xxweb_account_info')->truncate();
        DB::table('xxweb_account_info')->insert($this->items());
    }

    public function items()
    {
        $lists = [];

        $flexCode = 'ACCOUNTING_FLEXFIELD';
        $flexName = 'Accounting Flexfield';

        array_push($lists, [
         'id_flex_structure_code' => $flexCode, 
         'id_flex_structure_name' => $flexName, 
         'application_column_name' => 'SEGMENT1',
         'segment_name' => 'Company',
         'flex_value_set_name' => 'TMITH_GL_COMPANY', 
         'chart_of_accounts_id' => '50388',
         'parent_flex_value_set_name' => '',
         'ledger_id' => '2021',
         'org_id' => '81',
         'ou_name' => 'TMITH_OU',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'id_flex_structure_code' => $flexCode, 
         'id_flex_structure_name' => $flexName, 
         'application_column_name' => 'SEGMENT2',
         'segment_name' => 'Branch',
         'flex_value_set_name' => 'TMITH_GL_BRANCH', 
         'chart_of_accounts_id' => '50388',
         'parent_flex_value_set_name' => '',
         'ledger_id' => '2021',
         'org_id' => '81',
         'ou_name' => 'TMITH_OU',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'id_flex_structure_code' => $flexCode, 
         'id_flex_structure_name' => $flexName, 
         'application_column_name' => 'SEGMENT3',
         'segment_name' => 'Department',
         'flex_value_set_name' => 'TMITH_GL_DEPARTMENT', 
         'chart_of_accounts_id' => '50388',
         'parent_flex_value_set_name' => '',
         'ledger_id' => '2021',
         'org_id' => '81',
         'ou_name' => 'TMITH_OU',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'id_flex_structure_code' => $flexCode, 
         'id_flex_structure_name' => $flexName, 
         'application_column_name' => 'SEGMENT4',
         'segment_name' => 'Product',
         'flex_value_set_name' => 'TMITH_GL_PRODUCT', 
         'chart_of_accounts_id' => '50388',
         'parent_flex_value_set_name' => '',
         'ledger_id' => '2021',
         'org_id' => '81',
         'ou_name' => 'TMITH_OU',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'id_flex_structure_code' => $flexCode, 
         'id_flex_structure_name' => $flexName, 
         'application_column_name' => 'SEGMENT5',
         'segment_name' => 'Source',
         'flex_value_set_name' => 'TMITH_GL_SOURCE', 
         'chart_of_accounts_id' => '50388',
         'parent_flex_value_set_name' => '',
         'ledger_id' => '2021',
         'org_id' => '81',
         'ou_name' => 'TMITH_OU',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'id_flex_structure_code' => $flexCode, 
         'id_flex_structure_name' => $flexName, 
         'application_column_name' => 'SEGMENT6',
         'segment_name' => 'Account',
         'flex_value_set_name' => 'TMITH_GL_ACCOUNT', 
         'chart_of_accounts_id' => '50388',
         'parent_flex_value_set_name' => '',
         'ledger_id' => '2021',
         'org_id' => '81',
         'ou_name' => 'TMITH_OU',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'id_flex_structure_code' => $flexCode, 
         'id_flex_structure_name' => $flexName, 
         'application_column_name' => 'SEGMENT7',
         'segment_name' => 'Sub Account',
         'flex_value_set_name' => 'TMITH_GL_SUB_ACCOUNT', 
         'chart_of_accounts_id' => '50388',
         'parent_flex_value_set_name' => 'TMITH_GL_ACCOUNT',
         'ledger_id' => '2021',
         'org_id' => '81',
         'ou_name' => 'TMITH_OU',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'id_flex_structure_code' => $flexCode, 
         'id_flex_structure_name' => $flexName, 
         'application_column_name' => 'SEGMENT8',
         'segment_name' => 'Project/Activity',
         'flex_value_set_name' => 'TMITH_GL_PROJECT', 
         'chart_of_accounts_id' => '50388',
         'parent_flex_value_set_name' => '',
         'ledger_id' => '2021',
         'org_id' => '81',
         'ou_name' => 'TMITH_OU',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'id_flex_structure_code' => $flexCode, 
         'id_flex_structure_name' => $flexName, 
         'application_column_name' => 'SEGMENT9',
         'segment_name' => 'Intercompany',
         'flex_value_set_name' => 'TMITH_GL_INTERCOMPANY', 
         'chart_of_accounts_id' => '50388',
         'parent_flex_value_set_name' => '',
         'ledger_id' => '2021',
         'org_id' => '81',
         'ou_name' => 'TMITH_OU',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'id_flex_structure_code' => $flexCode, 
         'id_flex_structure_name' => $flexName, 
         'application_column_name' => 'SEGMENT10',
         'segment_name' => 'Allocation',
         'flex_value_set_name' => 'TMITH_GL_ALLOCATION', 
         'chart_of_accounts_id' => '50388',
         'parent_flex_value_set_name' => '',
         'ledger_id' => '2021',
         'org_id' => '81',
         'ou_name' => 'TMITH_OU',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'id_flex_structure_code' => $flexCode, 
         'id_flex_structure_name' => $flexName, 
         'application_column_name' => 'SEGMENT11',
         'segment_name' => 'Future Used1',
         'flex_value_set_name' => 'TMITH_GL_FUTURE_USED1', 
         'chart_of_accounts_id' => '50388',
         'parent_flex_value_set_name' => '',
         'ledger_id' => '2021',
         'org_id' => '81',
         'ou_name' => 'TMITH_OU',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'id_flex_structure_code' => $flexCode, 
         'id_flex_structure_name' => $flexName, 
         'application_column_name' => 'SEGMENT12',
         'segment_name' => 'Future Used2',
         'flex_value_set_name' => 'TMITH_GL_FUTURE_USED2', 
         'chart_of_accounts_id' => '50388',
         'parent_flex_value_set_name' => '',
         'ledger_id' => '2021',
         'org_id' => '81',
         'ou_name' => 'TMITH_OU',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);


        return $lists;
        
    }
}
