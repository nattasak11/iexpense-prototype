<?php

use Illuminate\Database\Seeder;

class GlPeriodStatusesVTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('xxweb_gl_period_statuses_v')->truncate();
        DB::table('xxweb_gl_period_statuses_v')->insert($this->items());
    }
    
    public function items()
    {
        $lists = [];

        array_push($lists, [
         'period_name' => 'ALLTIME', 
         'set_of_books_id' => '2021', 
         'application_id' => '101',
         'closing_status' => 'O', 
         'org_id' => '81', 
         'start_date' => date('Y-m-d H:i:s'),
         'end_date' => null,
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'period_name' => 'ALLTIME', 
         'set_of_books_id' => '2021', 
         'application_id' => '200',
         'closing_status' => 'O', 
         'org_id' => '81', 
         'start_date' => date('Y-m-d H:i:s'),
         'end_date' => null,
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        return $lists;
    }
}
