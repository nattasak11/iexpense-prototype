<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('xxweb_users')->insert([
            'oracle_person_id' => '00000000',
            'name' => 'Mr Admin Krup',
            'username' => 'system',
            'org_id' => '81',
            'email' => 'admin@email.com',
            'password' => bcrypt('11111111'),
            'role' => 'admin'
        ]);

        DB::table('xxweb_users')->insert([
            'oracle_person_id' => '11111111',
            'name' => 'Mr User Krup',
            'username' => 'user',
            'org_id' => '81',
            'email' => 'user@email.com',
            'password' => bcrypt('11111111'),
            'role' => 'user'
        ]);

        DB::table('xxweb_users')->insert([
            'oracle_person_id' => '99999999',
            'name' => 'Mr Finance Krup',
            'username' => 'finance',
            'org_id' => '81',
            'email' => 'finance@email.com',
            'password' => bcrypt('11111111'),
            'role' => 'finance'
        ]);
    }
}