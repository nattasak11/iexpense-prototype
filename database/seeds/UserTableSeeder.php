<?php

use Illuminate\Database\Seeder;
use App\Repositories\UserRepo;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('xxweb_users')->truncate();
        // DB::table('xxweb_users')->insert($this->users());
        UserRepo::syncAll();
    }
    
    // public function users()
    // {
    //     $lists = [
    //         [
    //             'oracle_person_id' => '106',
    //             'name' => 'Miki, Shinkichi',
    //             'role' => 'admin'
    //         ],
    //         [
    //             'oracle_person_id' => '106',
    //             'name' => 'TEST_OTHER_USER',
    //             'role' => 'user'
    //         ]
    //     ];

    //     return $lists;
    // }
}
