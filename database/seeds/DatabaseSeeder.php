<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(HrOperatingUnitsTableSeeder::class);
        $this->call(FndLovsVTableSeeder::class);
        $this->call(AccountInfoTableSeeder::class);
        $this->call(ApPaymentMethodsVTableSeeder::class);
        $this->call(CurrenciesVTableSeeder::class);
        $this->call(EstablishmentsVTableSeeder::class);
        $this->call(FinancialsSystemParamsAllTableSeeder::class);
        $this->call(GlCodeCombinationsKfvTableSeeder::class);
        $this->call(GlLedgersTableSeeder::class);
        $this->call(GlPeriodStatusesVTableSeeder::class);

        $this->call(PerJobsVTableSeeder::class);
        $this->call(PoVendorsVTableSeeder::class);
        $this->call(PositionsInfoTableSeeder::class);
        $this->call(PoHierarchyVTableSeeder::class);
        $this->call(PoEmployeesVTableSeeder::class);

        $this->call(TaxsVTableSeeder::class);
        $this->call(WhtsVTableSeeder::class);

        $this->call(MileageUnitTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(LocationTableSeeder::class);
        // REIM CATEGORY
        $this->call(CategoryTableSeeder::class);
        $this->call(SubCategoryTableSeeder::class);
        $this->call(SubCategoryInfoTableSeeder::class);
        $this->call(PolicyTableSeeder::class);
        $this->call(PolicyRateTableSeeder::class);
        // CA CATEGORY
        $this->call(CACategoryTableSeeder::class);
        $this->call(CASubCategoryTableSeeder::class);
        $this->call(CASubCategoryInfoTableSeeder::class);
        
        // $this->call(PreferenceTableSeeder::class);
    }
}
