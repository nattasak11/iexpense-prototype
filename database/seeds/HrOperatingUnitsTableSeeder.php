<?php

use Illuminate\Database\Seeder;

class HrOperatingUnitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('xxweb_hr_operating_units')->truncate();
        DB::table('xxweb_hr_operating_units')->insert($this->items());
    }

    public function items()
    {
        $lists = [];

        array_push($lists, [
         'organization_id' => '81',
         'name' => 'TMITH_OU', 
         'date_from' => date('Y-m-d H:i:s'), 
         'date_to' => null,
         'short_code' => 'O', 
         'set_of_books_id' => '2041', 
         'default_legal_context_id' => '24277',
         'usable_flag' => null,
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'organization_id' => '121',
         'name' => 'TMIB_OU', 
         'date_from' => date('Y-m-d H:i:s'), 
         'date_to' => null,
         'short_code' => 'O', 
         'set_of_books_id' => '2021', 
         'default_legal_context_id' => '23273',
         'usable_flag' => null,
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        return $lists;
    }
}
