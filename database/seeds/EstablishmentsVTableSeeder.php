<?php

use Illuminate\Database\Seeder;

class EstablishmentsVTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('xxweb_establishments_v')->truncate();
        DB::table('xxweb_establishments_v')->insert($this->items());
    }

    public function items()
    {
        $lists = [];

        array_push($lists, [
         'establishment_id' => '1453923', 
         'org_id' => '121', 
         'establishment_name' => 'ADDRESS01',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'establishment_id' => '1452924', 
         'org_id' => '82', 
         'establishment_name' => 'ADDRESS02',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'establishment_id' => '1452923', 
         'org_id' => '81', 
         'establishment_name' => 'ADDRESS03',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        return $lists;
        
    }
}
