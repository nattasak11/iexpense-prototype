<?php

use Illuminate\Database\Seeder;

class PerJobsVTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('xxweb_per_jobs_v')->truncate();
        DB::table('xxweb_per_jobs_v')->insert($this->items());
    }

    public function items()
    {
        $lists = [];

        array_push($lists, [
         'JOB_ID' => '78', 
         'JOB_NAME' => 'User', 
         'APPROVAL_AUTHORITY' => '10',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'JOB_ID' => '74', 
         'JOB_NAME' => 'Manager', 
         'APPROVAL_AUTHORITY' => '20',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'JOB_ID' => '62', 
         'JOB_NAME' => 'AGM', 
         'APPROVAL_AUTHORITY' => '30',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'JOB_ID' => '68', 
         'JOB_NAME' => 'GM', 
         'APPROVAL_AUTHORITY' => '40',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'JOB_ID' => '70', 
         'JOB_NAME' => 'HoD', 
         'APPROVAL_AUTHORITY' => '50',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'JOB_ID' => '66', 
         'JOB_NAME' => 'EO', 
         'APPROVAL_AUTHORITY' => '60',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'JOB_ID' => '76', 
         'JOB_NAME' => 'P', 
         'APPROVAL_AUTHORITY' => '70',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        return $lists;
        
    }
}
