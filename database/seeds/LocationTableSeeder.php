<?php

use Illuminate\Database\Seeder;

// use App\HrOperatingUnit;

class LocationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('xxweb_locations')->truncate();
        DB::table('xxweb_locations')->insert($this->locations());

        $locations = DB::table('xxweb_locations')->get();
        // dd($locations);
        foreach ($locations as $key => $location) {
            # code...
            // dd($location);
            DB::table('xxweb_accessible_orgs')->insert(
                [
                    'accessible_orgable_id' => $location->id,
                    'accessible_orgable_type' => 'App\Location',
                    'org_id' => '81'
                ]
            );
        }
    }

    public function locations()
    {
        $lists = [];

        array_push($lists,
            [
            'name' => 'Domestic',
            'description' => 'Domestic (ภายในประเทศ)' ,
            'active' => true,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists,
            [
            'name' => 'Japan',
            'description' => 'Japan (ประเทศญี่ปุ่น)' ,
            'active' => true,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists,
            [
            'name' => 'Zone A',
            'description' => 'Brunei, Cambodia, Indonesia, Laos, Malaysia, Myanmar, Philippines and Vietnam (บรูไน, กัมพูชา, อินโดนีเซีย, ลาว, มาเลเซีย, พม่า, ฟิลิปปินส์ และเวียดนาม)' ,
            'active' => true,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists,
            [
            'name' => 'Zone B',
            'description' => 'Australia, Bahrain, China, Fiji, India, Israel, North / South Korea, Lebanon, Macau, Maldives, Mauritius, Mongolia, Nepal, New Zealand, Oman, Pakistan, Qatar (ออสเตรเลีย, บาห์เรน, จีน, ฟิจิ, อินเดีย, อิสราเอล, เกาหลีเหนือ/ใต้, เลบานอน, มาเก๊า, มัลดีฟส์, มอริเชียส, มองโกเลีย, เนปาล, นิวซีเลนด์, โอมาน, ปากีสถาน, การ์ต้า)',
            'active' => true,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists,
            [
            'name' => 'Zone C',
            'description' => 'Other Country not in Zone A,B (ประเทศอื่นๆ ที่ไม่อยู่ในโซน A,B)' ,
            'active' => true,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')]);

        return $lists;
    }

}