<?php

use Illuminate\Database\Seeder;

class TaxsVTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('xxweb_taxs_v')->truncate();
        DB::table('xxweb_taxs_v')->insert($this->items());
    }

    public function items()
    {
        $lists = [];

        array_push($lists, [
         'tax_rate_code' => 'INPUT', 
         'tax_regime_code' => 'TH-TMITH', 
         'tax' => 'VAT 7%',
         'tax_status_code' => 'INPUT', 
         'tax_account_ccid' => '4027',
         'percentage_rate' => 7, 
         'org_id' => '81', 
         'tax_account' => '01.SM.000.000.00.19210.102.000.00.0.000.000']);

        array_push($lists, [
         'tax_rate_code' => 'INPUT', 
         'tax_regime_code' => 'TH-TMITH', 
         'tax' => 'VAT 7%',
         'tax_status_code' => 'INPUT', 
         'tax_account_ccid' => '9001',
         'percentage_rate' => 7, 
         'org_id' => '121', 
         'tax_account' => '01.SM.000.000.00.19210.102.000.00.0.000.000']);

         array_push($lists, [
         'tax_rate_code' => 'INPUT-ND', 
         'tax_regime_code' => 'TH-TMITH', 
         'tax' => 'VAT 7%',
         'tax_status_code' => 'INPUT-ND', 
         'tax_account_ccid' => '4027',
         'percentage_rate' => 7, 
         'org_id' => '81', 
         'tax_account' => '01.SM.000.000.00.19210.102.000.00.0.000.000']);

        array_push($lists, [
         'tax_rate_code' => 'INPUT-ND', 
         'tax_regime_code' => 'TH-TMITH', 
         'tax' => 'VAT 7%',
         'tax_status_code' => 'INPUT-ND', 
         'tax_account_ccid' => '9001',
         'percentage_rate' => 7, 
         'org_id' => '121', 
         'tax_account' => '01.SM.000.000.00.19210.102.000.00.0.000.000']);

        return $lists;
        
    }
}
