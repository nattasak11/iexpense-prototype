<?php

use Illuminate\Database\Seeder;

class MileageUnitTableSeeder extends Seeder
{
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('xxweb_mileage_units')->truncate();
        DB::table('xxweb_mileage_units')->insert($this->mileage_units());
    }
    
    public function mileage_units()
    {
        $lists = [
            ['code' => 'KM', 'description' => 'Kilometre(s)' ,'active' => true],
            ['code' => 'MILE', 'description' => 'Mile(s)' ,'active' => true],
        ];

        return $lists;
    }
} 