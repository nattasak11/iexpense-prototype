<?php

use Illuminate\Database\Seeder;

class ApPaymentMethodsVTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('xxweb_ap_payment_methods_v')->truncate();
        DB::table('xxweb_ap_payment_methods_v')->insert($this->items());
    }

    public function items()
    {
        $lists = [];

        $author = 'MERCURY';

        $name = 'AUTO DEBIT';
        array_push($lists, [
         'payment_method_name' => $name, 
         'payment_method_code' => $name, 
         'description' => $name,
         'inactive_date' => null,
         'status' => 'Active', 
         'status_code' => 'Y',
         'author' => $author,
         'pm_code' => $name,
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        $name = 'BAHTNET';
        array_push($lists, [
         'payment_method_name' => $name, 
         'payment_method_code' => $name, 
         'description' => $name,
         'inactive_date' => null,
         'status' => 'Active', 
         'status_code' => 'Y',
         'author' => $author,
         'pm_code' => $name,
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        $name = 'BAHTNET';
        array_push($lists, [
         'payment_method_name' => $name, 
         'payment_method_code' => $name, 
         'description' => $name,
         'inactive_date' => null,
         'status' => 'Active', 
         'status_code' => 'Y',
         'author' => $author,
         'pm_code' => $name,
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        $name = 'MANUAL CHEQUE';
        array_push($lists, [
         'payment_method_name' => $name, 
         'payment_method_code' => $name, 
         'description' => $name,
         'inactive_date' => null,
         'status' => 'Active', 
         'status_code' => 'Y',
         'author' => $author,
         'pm_code' => $name,
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

		$name = 'MANUAL TRANSFER';
		array_push($lists, [
         'payment_method_name' => $name, 
         'payment_method_code' => $name, 
         'description' => $name,
         'inactive_date' => null,
         'status' => 'Active', 
         'status_code' => 'Y',
         'author' => $author,
         'pm_code' => $name,
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

		$name = 'CHECK-EXP';
		array_push($lists, [
         'payment_method_name' => $name, 
         'payment_method_code' => $name, 
         'description' => $name,
         'inactive_date' => null,
         'status' => 'Active', 
         'status_code' => 'Y',
         'author' => $author,
         'pm_code' => $name,
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

		$name = 'PETTY CASH';
		array_push($lists, [
         'payment_method_name' => $name, 
         'payment_method_code' => $name, 
         'description' => $name,
         'inactive_date' => null,
         'status' => 'Active', 
         'status_code' => 'Y',
         'author' => $author,
         'pm_code' => $name,
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

		$name = 'REMITTANCE';
		array_push($lists, [
         'payment_method_name' => $name, 
         'payment_method_code' => $name, 
         'description' => $name,
         'inactive_date' => null,
         'status' => 'Active', 
         'status_code' => 'Y',
         'author' => $author,
         'pm_code' => $name,
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

		$name = 'SAME DAY';
		array_push($lists, [
         'payment_method_name' => $name, 
         'payment_method_code' => $name, 
         'description' => $name,
         'inactive_date' => null,
         'status' => 'Active', 
         'status_code' => 'Y',
         'author' => $author,
         'pm_code' => $name,
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        return $lists;
        
    }
}
