<?php

use Illuminate\Database\Seeder;

class PoHierarchyVTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('xxweb_po_hierarchy_v')->truncate();
        DB::table('xxweb_po_hierarchy_v')->insert($this->items());
    }

    public function items()
    {
        $lists = [];

        array_push($lists, [
         'business_group_id' => '0', 
         'pos_structure_version_id' => '1061', 
         'name' => 'TMITH_iExpense_Hierarchy',
         'hierarchy' => '      054.Information Technology.L8B4', 
         'version_number' => '1', 
         'rep_level' => '1',
         'parent_position_id' => '6063', 
         'parent_name' => '036.Personal Line Operation.L10B4', 
         'parent_job_id' => '66',
         'parent_job' => 'EO', 
         'parent_approval_authority' => '60', 
         'child_position_id' => '6064',
         'child_name' => '054.Information Technology.L8B4', 
         'child_job_id' => '70', 
         'child_job' => 'HoD',
         'child_approval_authority' => '50', 
         'subordinate_position_id' => '6064', 
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'business_group_id' => '0', 
         'pos_structure_version_id' => '1061', 
         'name' => 'TMITH_iExpense_Hierarchy',
         'hierarchy' => '      054.Planning and Management.L6B3', 
         'version_number' => '1', 
         'rep_level' => '2',
         'parent_position_id' => '6064', 
         'parent_name' => '054.Information Technology.L8B4', 
         'parent_job_id' => '70',
         'parent_job' => 'HoD', 
         'parent_approval_authority' => '50', 
         'child_position_id' => '6065',
         'child_name' => '054.Planning and Management.L6B3', 
         'child_job_id' => '62', 
         'child_job' => 'AGM',
         'child_approval_authority' => '30', 
         'subordinate_position_id' => '6065', 
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'business_group_id' => '0', 
         'pos_structure_version_id' => '1061', 
         'name' => 'TMITH_iExpense_Hierarchy',
         'hierarchy' => '      054.Planning and Management.L4B2', 
         'version_number' => '1', 
         'rep_level' => '3',
         'parent_position_id' => '6065', 
         'parent_name' => '054.Planning and Management.L6B3', 
         'parent_job_id' => '62',
         'parent_job' => 'AGM', 
         'parent_approval_authority' => '30', 
         'child_position_id' => '4446',
         'child_name' => '054.Planning and Management.L4B2', 
         'child_job_id' => '78', 
         'child_job' => 'User',
         'child_approval_authority' => '10', 
         'subordinate_position_id' => '4446', 
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

    	return $lists;
        
    }
}
