<?php

use Illuminate\Database\Seeder;

class GlLedgersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('gl_ledgers')->truncate();
        DB::table('gl_ledgers')->insert($this->items());
    }

    public function items()
    {
        $lists = [];

        array_push($lists, [
         'ledger_id' => '2063', 
         'name' => 'TMF_LEDGER', 
         'short_name' => 'TMF_LE',
         'description' => 'TMF Holding (Thailand) Co., Ltd.',
         'ledger_category_code' => 'PRIMARY', 
         'chart_of_accounts_id' => '50428',
         'currency_code' => 'THB',
         'period_set_name' => 'TMITH_CALENDAR',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'ledger_id' => '2081', 
         'name' => 'TMITH_Secondary Ledger', 
         'short_name' => 'TMITH_Secondary',
         'description' => 'Tokio Marine Insurance (Thailand)',
         'ledger_category_code' => 'SECONDARY', 
         'chart_of_accounts_id' => '50388',
         'currency_code' => 'THB',
         'period_set_name' => 'TMITH_CALENDAR',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'ledger_id' => '2101', 
         'name' => 'TMITH Ledger set', 
         'short_name' => 'TMITH Ledger set',
         'description' => 'TMITH Ledger set',
         'ledger_category_code' => 'NONE', 
         'chart_of_accounts_id' => '50388',
         'currency_code' => 'X',
         'period_set_name' => 'TMITH_CALENDAR',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'ledger_id' => '2021', 
         'name' => 'TMITH_LEDGER', 
         'short_name' => 'TMITH_LEDGER',
         'description' => 'Tokio Marine Insurance (Thailand) PCL',
         'ledger_category_code' => 'PRIMARY', 
         'chart_of_accounts_id' => '50388',
         'currency_code' => 'THB',
         'period_set_name' => 'TMITH_CALENDAR',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'ledger_id' => '2023', 
         'name' => 'TMST_LEDGER', 
         'short_name' => 'TMST',
         'description' => 'Tokio Management Service (Thailand) Co., Ltd.',
         'ledger_category_code' => 'PRIMARY', 
         'chart_of_accounts_id' => '50388',
         'currency_code' => 'THB',
         'period_set_name' => 'TMITH_CALENDAR',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'ledger_id' => '2041', 
         'name' => 'TMIB_LEDGER', 
         'short_name' => 'TMIB',
         'description' => 'Tokio Marine Insurane Broker Co., Ltd.',
         'ledger_category_code' => 'PRIMARY', 
         'chart_of_accounts_id' => '50408',
         'currency_code' => 'THB',
         'period_set_name' => 'TMITH_CALENDAR',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        return $lists;
        
    }
}
