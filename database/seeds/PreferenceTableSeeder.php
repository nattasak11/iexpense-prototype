<?php

use Illuminate\Database\Seeder;

class PreferenceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('xxweb_preferences')->truncate();
        DB::table('xxweb_preferences')->insert($this->preferences());
    }
    
    public function preferences()
    {
        $lists = [
            ['org_id' => '81', 'code' => 'pending_day_blocking', 'data_type'=> 'varchar', 'data_value' => '["0"]'],
            ['org_id' => '81', 'code' => 'unblock_users', 'data_type'=> 'json', 'data_value' => null],
        ];

        return $lists;
    }
}
