<?php

use Illuminate\Database\Seeder;

class PositionsInfoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('xxweb_positions_info')->truncate();
        DB::table('xxweb_positions_info')->insert($this->items());
    }

    public function items()
    {
        $lists = [];

        array_push($lists, [
         'hierarchy_name' => 'TMITH_iExpense_Hierarchy', 
         'version_number' => '1', 
         'parent_pos_id' => '6063',
         'parent_pos' => '036.Personal Line Operation.L10B4', 
         'parent_job_id' => '66',
         'parent_job' => 'EO', 
         'parent_approval_authority' => '60', 
         'parent_amount_limit' => 999999999,
         'control_function_name' => 'Approve Planned Purchase Orders', 
         'child_pos_id' => '6064', 
         'child_pos' => '054.Information Technology.L8B4U1',
         'child_job_id' => '70', 
         'child_job' => 'HoD',
         'child_approval_authority' => '50', 
         'control_rule_id' => '2007',
         'org_id' => '81']);

        array_push($lists, [
         'hierarchy_name' => 'TMITH_iExpense_Hierarchy', 
         'version_number' => '1', 
         'parent_pos_id' => '6064',
         'parent_pos' => '054.Information Technology.L8B4', 
         'parent_job_id' => '70',
         'parent_job' => 'HoD', 
         'parent_approval_authority' => '50', 
         'parent_amount_limit' => 100000,
         'control_function_name' => 'Approve Planned Purchase Orders', 
         'child_pos_id' => '6065', 
         'child_pos' => '054.Planning and Management.L6B3U1',
         'child_job_id' => '62', 
         'child_job' => 'AGM',
         'child_approval_authority' => '30', 
         'control_rule_id' => '2005',
         'org_id' => '81']);

        array_push($lists, [
         'hierarchy_name' => 'TMITH_iExpense_Hierarchy', 
         'version_number' => '1', 
         'parent_pos_id' => '6065',
         'parent_pos' => '054.Planning and Management.L6B3', 
         'parent_job_id' => '62',
         'parent_job' => 'AGM', 
         'parent_approval_authority' => '30', 
         'parent_amount_limit' => 10000,
         'control_function_name' => 'Approve Planned Purchase Orders', 
         'child_pos_id' => '4446', 
         'child_pos' => '054.Planning and Management.L4B2',
         'child_job_id' => '78', 
         'child_job' => 'User',
         'child_approval_authority' => '10', 
         'control_rule_id' => '2001',
         'org_id' => '81']);


    	return $lists;
        
    }
}
