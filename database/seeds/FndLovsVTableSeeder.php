<?php

use Illuminate\Database\Seeder;

class FndLovsVTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('xxweb_fnd_lovs_v')->truncate();
        DB::table('xxweb_fnd_lovs_v')->insert($this->items());
    }

    public function items()
    {
        $lists = [];

        ///////////////////
        ///// ACCOUNT 

        array_push($lists, [
         'flex_value_set_id' => '1016832', 
         'flex_value_set_name' => 'TMITH_GL_ACCOUNT', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => '76105', 
         'flex_value_meaning' => '76105',
         'description' => 'Business Trip/ Meeting Expense - Local', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016832', 
         'flex_value_set_name' => 'TMITH_GL_ACCOUNT', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => '76106', 
         'flex_value_meaning' => '76106',
         'description' => 'Business Trip/ Meeting Expense - Oversea', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016832', 
         'flex_value_set_name' => 'TMITH_GL_ACCOUNT', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => '76107', 
         'flex_value_meaning' => '76107',
         'description' => 'Entertainment/gift', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016832', 
         'flex_value_set_name' => 'TMITH_GL_ACCOUNT', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => '76108', 
         'flex_value_meaning' => '76108',
         'description' => 'Marketing Promotion & Support Activity', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016832', 
         'flex_value_set_name' => 'TMITH_GL_ACCOUNT', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => '76109', 
         'flex_value_meaning' => '76109',
         'description' => 'Promotion campaign', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016832', 
         'flex_value_set_name' => 'TMITH_GL_ACCOUNT', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => '76110', 
         'flex_value_meaning' => '76110',
         'description' => 'Seminar expenses', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016832', 
         'flex_value_set_name' => 'TMITH_GL_ACCOUNT', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => '19505', 
         'flex_value_meaning' => '19505',
         'description' => 'Staff Advance', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        ////////////////////////
        //// SUB ACCOUNT

        array_push($lists, [
         'flex_value_set_id' => '1016833', 
         'flex_value_set_name' => 'TMITH_GL_SUB_ACCOUNT', 
         'parent_flex_value_set_id' => '1016832',
         'parent_flex_value_set_name' => 'TMITH_GL_ACCOUNT', 
         'flex_value' => '000', 
         'flex_value_meaning' => '000',
         'description' => 'Default', 
         'parent_flex_value_low' => '76105', 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016833', 
         'flex_value_set_name' => 'TMITH_GL_SUB_ACCOUNT', 
         'parent_flex_value_set_id' => '1016832',
         'parent_flex_value_set_name' => 'TMITH_GL_ACCOUNT', 
         'flex_value' => 'T', 
         'flex_value_meaning' => 'T',
         'description' => 'Total', 
         'parent_flex_value_low' => '76105', 
         'parent_flex_value_high' => null,
         'summary_flag' => 'Y',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016833', 
         'flex_value_set_name' => 'TMITH_GL_SUB_ACCOUNT', 
         'parent_flex_value_set_id' => '1016832',
         'parent_flex_value_set_name' => 'TMITH_GL_ACCOUNT', 
         'flex_value' => '000', 
         'flex_value_meaning' => '000',
         'description' => 'Default', 
         'parent_flex_value_low' => '76106', 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016833', 
         'flex_value_set_name' => 'TMITH_GL_SUB_ACCOUNT', 
         'parent_flex_value_set_id' => '1016832',
         'parent_flex_value_set_name' => 'TMITH_GL_ACCOUNT', 
         'flex_value' => 'T', 
         'flex_value_meaning' => 'T',
         'description' => 'Total', 
         'parent_flex_value_low' => '76106', 
         'parent_flex_value_high' => null,
         'summary_flag' => 'Y',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016833', 
         'flex_value_set_name' => 'TMITH_GL_SUB_ACCOUNT', 
         'parent_flex_value_set_id' => '1016832',
         'parent_flex_value_set_name' => 'TMITH_GL_ACCOUNT', 
         'flex_value' => '000', 
         'flex_value_meaning' => '000',
         'description' => 'Default', 
         'parent_flex_value_low' => '76107', 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016833', 
         'flex_value_set_name' => 'TMITH_GL_SUB_ACCOUNT', 
         'parent_flex_value_set_id' => '1016832',
         'parent_flex_value_set_name' => 'TMITH_GL_ACCOUNT', 
         'flex_value' => 'T', 
         'flex_value_meaning' => 'T',
         'description' => 'Total', 
         'parent_flex_value_low' => '76107', 
         'parent_flex_value_high' => null,
         'summary_flag' => 'Y',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016833', 
         'flex_value_set_name' => 'TMITH_GL_SUB_ACCOUNT', 
         'parent_flex_value_set_id' => '1016832',
         'parent_flex_value_set_name' => 'TMITH_GL_ACCOUNT', 
         'flex_value' => '000', 
         'flex_value_meaning' => '000',
         'description' => 'Default', 
         'parent_flex_value_low' => '76108', 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016833', 
         'flex_value_set_name' => 'TMITH_GL_SUB_ACCOUNT', 
         'parent_flex_value_set_id' => '1016832',
         'parent_flex_value_set_name' => 'TMITH_GL_ACCOUNT', 
         'flex_value' => 'T', 
         'flex_value_meaning' => 'T',
         'description' => 'Total', 
         'parent_flex_value_low' => '76108', 
         'parent_flex_value_high' => null,
         'summary_flag' => 'Y',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016833', 
         'flex_value_set_name' => 'TMITH_GL_SUB_ACCOUNT', 
         'parent_flex_value_set_id' => '1016832',
         'parent_flex_value_set_name' => 'TMITH_GL_ACCOUNT', 
         'flex_value' => '000', 
         'flex_value_meaning' => '000',
         'description' => 'Default', 
         'parent_flex_value_low' => '76109', 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016833', 
         'flex_value_set_name' => 'TMITH_GL_SUB_ACCOUNT', 
         'parent_flex_value_set_id' => '1016832',
         'parent_flex_value_set_name' => 'TMITH_GL_ACCOUNT', 
         'flex_value' => 'T', 
         'flex_value_meaning' => 'T',
         'description' => 'Total', 
         'parent_flex_value_low' => '76109', 
         'parent_flex_value_high' => null,
         'summary_flag' => 'Y',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016833', 
         'flex_value_set_name' => 'TMITH_GL_SUB_ACCOUNT', 
         'parent_flex_value_set_id' => '1016832',
         'parent_flex_value_set_name' => 'TMITH_GL_ACCOUNT', 
         'flex_value' => '000', 
         'flex_value_meaning' => '000',
         'description' => 'Default', 
         'parent_flex_value_low' => '76110', 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016833', 
         'flex_value_set_name' => 'TMITH_GL_SUB_ACCOUNT', 
         'parent_flex_value_set_id' => '1016832',
         'parent_flex_value_set_name' => 'TMITH_GL_ACCOUNT', 
         'flex_value' => 'T', 
         'flex_value_meaning' => 'T',
         'description' => 'Total', 
         'parent_flex_value_low' => '76110', 
         'parent_flex_value_high' => null,
         'summary_flag' => 'Y',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016833', 
         'flex_value_set_name' => 'TMITH_GL_SUB_ACCOUNT', 
         'parent_flex_value_set_id' => '1016832',
         'parent_flex_value_set_name' => 'TMITH_GL_ACCOUNT', 
         'flex_value' => '000', 
         'flex_value_meaning' => '000',
         'description' => 'Default', 
         'parent_flex_value_low' => '19505', 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016833', 
         'flex_value_set_name' => 'TMITH_GL_SUB_ACCOUNT', 
         'parent_flex_value_set_id' => '1016832',
         'parent_flex_value_set_name' => 'TMITH_GL_ACCOUNT', 
         'flex_value' => 'T', 
         'flex_value_meaning' => 'T',
         'description' => 'Total', 
         'parent_flex_value_low' => '19505', 
         'parent_flex_value_high' => null,
         'summary_flag' => 'Y',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);


        /////////////////////////
        //// Branch 

        array_push($lists, [
         'flex_value_set_id' => '1016828', 
         'flex_value_set_name' => 'TMITH_GL_BRANCH', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => '000', 
         'flex_value_meaning' => '000',
         'description' => 'N/A', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016828', 
         'flex_value_set_name' => 'TMITH_GL_BRANCH', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => 'SM', 
         'flex_value_meaning' => 'SM',
         'description' => 'Head Office', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016828', 
         'flex_value_set_name' => 'TMITH_GL_BRANCH', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => 'RP', 
         'flex_value_meaning' => 'RP',
         'description' => 'Rachapruk', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016828', 
         'flex_value_set_name' => 'TMITH_GL_BRANCH', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => 'SK', 
         'flex_value_meaning' => 'SK',
         'description' => 'Srinakarin', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016828', 
         'flex_value_set_name' => 'TMITH_GL_BRANCH', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => 'RM', 
         'flex_value_meaning' => 'RM',
         'description' => 'Rama II', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016828', 
         'flex_value_set_name' => 'TMITH_GL_BRANCH', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => 'DM', 
         'flex_value_meaning' => 'DM',
         'description' => 'Donmuang', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        /////////////////////
        //// DEPARTMENT

        array_push($lists, [
         'flex_value_set_id' => '1016829', 
         'flex_value_set_name' => 'TMITH_GL_DEPARTMENT', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => '000', 
         'flex_value_meaning' => '000',
         'description' => 'N/A', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016829', 
         'flex_value_set_name' => 'TMITH_GL_DEPARTMENT', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => '049', 
         'flex_value_meaning' => '049',
         'description' => 'Corporate management', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016829', 
         'flex_value_set_name' => 'TMITH_GL_DEPARTMENT', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => '050', 
         'flex_value_meaning' => '050',
         'description' => 'Accounting', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016829', 
         'flex_value_set_name' => 'TMITH_GL_DEPARTMENT', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => '051', 
         'flex_value_meaning' => '051',
         'description' => 'Finance', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016829', 
         'flex_value_set_name' => 'TMITH_GL_DEPARTMENT', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => '053', 
         'flex_value_meaning' => '053',
         'description' => 'Human Resources', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016829', 
         'flex_value_set_name' => 'TMITH_GL_DEPARTMENT', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => '054', 
         'flex_value_meaning' => '054',
         'description' => 'IT', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);


        /////////////////////////////
        //// INTER-COMPANY

        array_push($lists, [
         'flex_value_set_id' => '1016835', 
         'flex_value_set_name' => 'TMITH_GL_INTERCOMPANY', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => '00', 
         'flex_value_meaning' => '00',
         'description' => 'N/A', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016835', 
         'flex_value_set_name' => 'TMITH_GL_INTERCOMPANY', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => '02', 
         'flex_value_meaning' => '02',
         'description' => 'Tokio Marine & Nichido Fire Insurance Co.,Ltd.', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016835', 
         'flex_value_set_name' => 'TMITH_GL_INTERCOMPANY', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => '03', 
         'flex_value_meaning' => '03',
         'description' => 'Tokio Management Services (Thailand) Co.,Ltd.', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016835', 
         'flex_value_set_name' => 'TMITH_GL_INTERCOMPANY', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => '04', 
         'flex_value_meaning' => '04',
         'description' => 'TMF Holding (Thailand) Ltd.', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016835', 
         'flex_value_set_name' => 'TMITH_GL_INTERCOMPANY', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => '05', 
         'flex_value_meaning' => '05',
         'description' => 'Tokio Marine Life Insurance (Thailand) Plc.', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);


        //////////////////////////////
        //// PRODUCT

        array_push($lists, [
         'flex_value_set_id' => '1016830', 
         'flex_value_set_name' => 'TMITH_GL_PRODUCT', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => '000', 
         'flex_value_meaning' => '000',
         'description' => 'N/A', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016830', 
         'flex_value_set_name' => 'TMITH_GL_PRODUCT', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => 'APT', 
         'flex_value_meaning' => 'APT',
         'description' => 'Travel accident', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016830', 
         'flex_value_set_name' => 'TMITH_GL_PRODUCT', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => 'VEH', 
         'flex_value_meaning' => 'VEH',
         'description' => 'Motor Voluntary', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016830', 
         'flex_value_set_name' => 'TMITH_GL_PRODUCT', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => 'VCM', 
         'flex_value_meaning' => 'VCM',
         'description' => 'Motor Compulsory', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        //////////////////////////////
        //// PROJECT

        array_push($lists, [
         'flex_value_set_id' => '1016834', 
         'flex_value_set_name' => 'TMITH_GL_PROJECT', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => '000', 
         'flex_value_meaning' => '000',
         'description' => 'N/A', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016834', 
         'flex_value_set_name' => 'TMITH_GL_PROJECT', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => '001', 
         'flex_value_meaning' => '001',
         'description' => 'Project A', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016834', 
         'flex_value_set_name' => 'TMITH_GL_PROJECT', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => '002', 
         'flex_value_meaning' => '002',
         'description' => 'Project X', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        //////////////////////////////
        //// SOURCE

        array_push($lists, [
         'flex_value_set_id' => '1016831', 
         'flex_value_set_name' => 'TMITH_GL_SOURCE', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => '00', 
         'flex_value_meaning' => '00',
         'description' => 'N/A', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016831', 
         'flex_value_set_name' => 'TMITH_GL_SOURCE', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => '20', 
         'flex_value_meaning' => '20',
         'description' => 'Inward', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1016831', 
         'flex_value_set_name' => 'TMITH_GL_SOURCE', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => '90', 
         'flex_value_meaning' => '90',
         'description' => 'Diver for allocation Level 3', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        ///////////////////////
        //// PO LEVEL

        array_push($lists, [
         'flex_value_set_id' => '1017109', 
         'flex_value_set_name' => 'TMITH_PO_LEVEL', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => 'G1G1', 
         'flex_value_meaning' => 'G1G1',
         'description' => 'Staff I, Driver', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1017109', 
         'flex_value_set_name' => 'TMITH_PO_LEVEL', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => 'G2G2', 
         'flex_value_meaning' => 'G2G2',
         'description' => 'Staff II', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1017109', 
         'flex_value_set_name' => 'TMITH_PO_LEVEL', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => 'L1B1', 
         'flex_value_meaning' => 'L1B1',
         'description' => 'Officer', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1017109', 
         'flex_value_set_name' => 'TMITH_PO_LEVEL', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => 'L2B1', 
         'flex_value_meaning' => 'L2B1',
         'description' => 'Senior Officer', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1017109', 
         'flex_value_set_name' => 'TMITH_PO_LEVEL', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => 'L3B2', 
         'flex_value_meaning' => 'L3B2',
         'description' => 'Supervisor', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1017109', 
         'flex_value_set_name' => 'TMITH_PO_LEVEL', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => 'L4B2', 
         'flex_value_meaning' => 'L4B2',
         'description' => 'Assistant Manager', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1017109', 
         'flex_value_set_name' => 'TMITH_PO_LEVEL', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => 'L5B3', 
         'flex_value_meaning' => 'L5B3',
         'description' => 'Manager', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1017109', 
         'flex_value_set_name' => 'TMITH_PO_LEVEL', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => 'L6B3', 
         'flex_value_meaning' => 'L6B3',
         'description' => 'Assistant General Manager', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1017109', 
         'flex_value_set_name' => 'TMITH_PO_LEVEL', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => 'L7B3', 
         'flex_value_meaning' => 'L7B3',
         'description' => 'General Manager', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1017109', 
         'flex_value_set_name' => 'TMITH_PO_LEVEL', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => 'L8B4', 
         'flex_value_meaning' => 'L8B4',
         'description' => 'Vice President', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1017109', 
         'flex_value_set_name' => 'TMITH_PO_LEVEL', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => 'L9B4', 
         'flex_value_meaning' => 'L9B4',
         'description' => 'Senior Vice President', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1017109', 
         'flex_value_set_name' => 'TMITH_PO_LEVEL', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => 'L10B4', 
         'flex_value_meaning' => 'L10B4',
         'description' => 'Executive Vice President', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1017109', 
         'flex_value_set_name' => 'TMITH_PO_LEVEL', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => 'L11B4', 
         'flex_value_meaning' => 'L11B4',
         'description' => 'Managing Director / President', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        ///////////////////////
        //// PO POSITION

        array_push($lists, [
         'flex_value_set_id' => '1017027', 
         'flex_value_set_name' => 'TMITH_PO_POSITION', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => '000', 
         'flex_value_meaning' => '000',
         'description' => 'Multinational Marketing &TOMSEC', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1017027', 
         'flex_value_set_name' => 'TMITH_PO_POSITION', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => '049', 
         'flex_value_meaning' => '049',
         'description' => 'Corporate Management, Finance', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1017027', 
         'flex_value_set_name' => 'TMITH_PO_POSITION', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => '050', 
         'flex_value_meaning' => '050',
         'description' => 'Accounting', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1017027', 
         'flex_value_set_name' => 'TMITH_PO_POSITION', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => '051', 
         'flex_value_meaning' => '051',
         'description' => 'Branch Operations, Finance', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1017027', 
         'flex_value_set_name' => 'TMITH_PO_POSITION', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => '052', 
         'flex_value_meaning' => '052',
         'description' => 'Compliance& Legal, GA, HR', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1017027', 
         'flex_value_set_name' => 'TMITH_PO_POSITION', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => '053', 
         'flex_value_meaning' => '053',
         'description' => 'Human Resources', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);

        array_push($lists, [
         'flex_value_set_id' => '1017027', 
         'flex_value_set_name' => 'TMITH_PO_POSITION', 
         'parent_flex_value_set_id' => null,
         'parent_flex_value_set_name' => null, 
         'flex_value' => '054', 
         'flex_value_meaning' => '054',
         'description' => 'Information Technology', 
         'parent_flex_value_low' => null, 
         'parent_flex_value_high' => null,
         'summary_flag' => 'N',
         'created_at' => date('Y-m-d H:i:s'),
         'updated_at' => date('Y-m-d H:i:s')]);


        return $lists;
        
    }
}
