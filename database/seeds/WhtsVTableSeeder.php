<?php

use Illuminate\Database\Seeder;

class WhtsVTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('xxweb_whts_v')->truncate();
        DB::table('xxweb_whts_v')->insert($this->items());
    }

    public function items()
    {
        $lists = [];

        array_push($lists, [
         'pay_awt_group_id' => '10000', 
         'wht_name' => '01-Transport', 
         'org_id' => '81',
         'tax_rate' => 1]);

        array_push($lists, [
         'pay_awt_group_id' => '10004', 
         'wht_name' => '03-Service', 
         'org_id' => '81',
         'tax_rate' => 3]);

        return $lists;
        
    }
}
