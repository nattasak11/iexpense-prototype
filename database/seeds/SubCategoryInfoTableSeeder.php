<?php

use Illuminate\Database\Seeder;

// use App\HrOperatingUnit;
use App\Category;
use App\SubCategory;

class SubCategoryInfoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        try {

            $subCategoryInfos = $this->subCategoryInfos();
            DB::table('xxweb_sub_category_infos')->truncate();
            DB::table('xxweb_sub_category_infos')->insert($subCategoryInfos);

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e->getMessage());
            throw new \Exception($e->getMessage(), 1);
        }
        DB::commit();
    }

    public function subCategoryInfos()
    {
        $lists = [];
        // $hrOperatingUnits = HrOperatingUnit::all();

        // foreach ($hrOperatingUnits as $key => $hou) {
            $categories = Category::all();
            foreach ($categories as $key => $category) {

                $subCategories = SubCategory::where('category_id',$category->id)
                                    ->get();

                foreach ($subCategories as $key => $subCategory) {

                    switch ($category->name) {

                        case 'ค่าเดินทางในประเทศ (Domestic Business trip)':

                            if($subCategory->name == 'ค่าตั๋วเครื่องบินในประเทศ (Air fare)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Travelling Date',
                                         'purpose' => 'Travelling Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Objective',
                                         'purpose' => 'Objective',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Airline Name',
                                         'purpose' => 'Airline Name',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place from -  to',
                                         'purpose' => 'Place from -  to',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            if($subCategory->name == 'ค่ารถไฟในประเทศ (Train fare)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Travelling Date',
                                         'purpose' => 'Travelling Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Objective',
                                         'purpose' => 'Objective',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place from -  to',
                                         'purpose' => 'Place from -  to',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                            }

                            if($subCategory->name == 'ค่ารถทัวร์ในประเทศ (Bus fare)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Travelling Date',
                                         'purpose' => 'Travelling Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Objective',
                                         'purpose' => 'Objective',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place from -  to',
                                         'purpose' => 'Place from -  to',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                            }

                            if($subCategory->name == 'รถยนต์ส่วนตัว (Personal car)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Travelling Date',
                                         'purpose' => 'Travelling Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Objective',
                                         'purpose' => 'Objective',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place from -  to',
                                         'purpose' => 'Place from -  to',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                            }

                            if($subCategory->name == 'ค่าจอดรถ (Parking fee)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Travelling Date',
                                         'purpose' => 'Travelling Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Objective',
                                         'purpose' => 'Objective',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place',
                                         'purpose' => 'Place',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                            }

                            if($subCategory->name == 'ค่าเดินทางประเภทอื่น (Other travelling)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Travelling Date',
                                         'purpose' => 'Travelling Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Objective',
                                         'purpose' => 'Objective',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place from -  to',
                                         'purpose' => 'Place from -  to',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                            }

                            if($subCategory->name == 'ค่าทางด่วนในประเทศ (Express way)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Travelling Date',
                                         'purpose' => 'Travelling Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Objective',
                                         'purpose' => 'Objective',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place from -  to',
                                         'purpose' => 'Place from -  to',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                            }

                            if($subCategory->name == 'ค่าเช่ารถในประเทศ (Car rental)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Travelling Date',
                                         'purpose' => 'Travelling Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Objective',
                                         'purpose' => 'Objective',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place from -  to',
                                         'purpose' => 'Place from -  to',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                            }

                            if($subCategory->name == 'ค่าน้ำมันสำหรับรถเช่าในประเทศ (Gasoline fee for car rental)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Travelling Date',
                                         'purpose' => 'Travelling Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Objective',
                                         'purpose' => 'Objective',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place from -  to',
                                         'purpose' => 'Place from -  to',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                            }

                            break;

                        case 'ค่าเดินทางในประเทศเพื่อไปทำงานสินไหม (Claim Domestic Business trip)':

                            if($subCategory->name == 'ค่าตั๋วเครื่องบินในประเทศ (Air fare)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Travelling Date',
                                         'purpose' => 'Travelling Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Claim no.',
                                         'purpose' => 'Claim no.',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Airline Name',
                                         'purpose' => 'Airline Name',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place from -  to',
                                         'purpose' => 'Place from -  to',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                            }

                            if($subCategory->name == 'ค่ารถไฟในประเทศ (Train fare)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Travelling Date',
                                         'purpose' => 'Travelling Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Claim no.',
                                         'purpose' => 'Claim no.',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place from -  to',
                                         'purpose' => 'Place from -  to',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            if($subCategory->name == 'ค่ารถทัวร์ในประเทศ (Bus fare)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Travelling Date',
                                         'purpose' => 'Travelling Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Claim no.',
                                         'purpose' => 'Claim no.',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place from -  to',
                                         'purpose' => 'Place from -  to',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            if($subCategory->name == 'รถยนต์ส่วนตัว (Personal car)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Travelling Date',
                                         'purpose' => 'Travelling Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Claim no.',
                                         'purpose' => 'Claim no.',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place from -  to',
                                         'purpose' => 'Place from -  to',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            if($subCategory->name == 'ค่าจอดรถ (Parking fee)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Travelling Date',
                                         'purpose' => 'Travelling Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Claim no.',
                                         'purpose' => 'Claim no.',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place from -  to',
                                         'purpose' => 'Place from -  to',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            if($subCategory->name == 'ค่าเดินทางประเภทอื่น (Other travelling)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Travelling Date',
                                         'purpose' => 'Travelling Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Claim no.',
                                         'purpose' => 'Claim no.',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place from -  to',
                                         'purpose' => 'Place from -  to',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            if($subCategory->name == 'ค่าทางด่วนในประเทศ (Express way)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Travelling Date',
                                         'purpose' => 'Travelling Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Claim no.',
                                         'purpose' => 'Claim no.',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place from -  to',
                                         'purpose' => 'Place from -  to',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            if($subCategory->name == 'ค่าเช่ารถในประเทศ (Car rental)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Travelling Date',
                                         'purpose' => 'Travelling Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Claim no.',
                                         'purpose' => 'Claim no.',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place from -  to',
                                         'purpose' => 'Place from -  to',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            if($subCategory->name == 'ค่าน้ำมันสำหรับรถเช่าในประเทศ (Gasoline fee for car rental)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Travelling Date',
                                         'purpose' => 'Travelling Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Claim no.',
                                         'purpose' => 'Claim no.',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place from -  to',
                                         'purpose' => 'Place from -  to',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            break;

                        case 'ค่าเดินทางต่างประเทศ (Overseas Business trip)':

                            if($subCategory->name == 'ค่าตั๋วเครื่องบินต่างประเทศ (Air fare)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Travelling Date',
                                         'purpose' => 'Travelling Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Objective',
                                         'purpose' => 'Objective',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Airline Name',
                                         'purpose' => 'Airline Name',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Air ticket class',
                                         'purpose' => 'Air ticket class',
                                         'form_type' => 'select',
                                         'form_value' => '["-","First Class","Business","Coach."]',
                                         'required' => false,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place from -  to',
                                         'purpose' => 'Place from -  to',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            if($subCategory->name == 'ค่าจอดรถ (Parking fee)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Travelling Date',
                                         'purpose' => 'Travelling Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Objective',
                                         'purpose' => 'Objective',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place',
                                         'purpose' => 'Place',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            if($subCategory->name == 'ค่าเดินทางประเภทอื่นๆ (Other travelling)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Travelling Date',
                                         'purpose' => 'Travelling Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Objective',
                                         'purpose' => 'Objective',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place from -  to',
                                         'purpose' => 'Place from -  to',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            if($subCategory->name == 'ค่าเช่ารถในต่างประเทศ (Car rental)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Travelling Date',
                                         'purpose' => 'Travelling Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Objective',
                                         'purpose' => 'Objective',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place from -  to',
                                         'purpose' => 'Place from -  to',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            if($subCategory->name == 'ค่าน้ำมันสำหรับรถเช่าในต่างประเทศ (Gasoline fee for car rental)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Travelling Date',
                                         'purpose' => 'Travelling Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Objective',
                                         'purpose' => 'Objective',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place from -  to',
                                         'purpose' => 'Place from -  to',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            if($subCategory->name == 'ค่าประกันการเดินทางต่างประเทศ (Travel Insurance)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Travelling Date',
                                         'purpose' => 'Travelling Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Objective',
                                         'purpose' => 'Objective',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place from -  to',
                                         'purpose' => 'Place from -  to',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            break;

                        case 'ค่าที่พักในประเทศ (Domestic Accomodation)':

                            if($subCategory->name == 'ค่าที่พักรายวัน (Daily Accomodation)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Objective',
                                         'purpose' => 'Objective',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Staff name',
                                         'purpose' => 'Staff name',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Accomodation name',
                                         'purpose' => 'Accomodation name',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                            }

                            if($subCategory->name == 'ค่าที่พักรายเดือน (Monthly accomodation)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Objective',
                                         'purpose' => 'Objective',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Staff name',
                                         'purpose' => 'Staff name',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Accomodation name',
                                         'purpose' => 'Accomodation name',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            break;

                        case 'ค่าที่พักในประเทศเพื่อไปทำงานสินไหม (Claim Domestic Accomodation)':

                            if($subCategory->name == 'ค่าที่พักรายวัน (Daily Accomodation)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Staff name',
                                         'purpose' => 'Staff name',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Hotel name',
                                         'purpose' => 'Hotel name',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Claim No.',
                                         'purpose' => 'Claim No.',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            if($subCategory->name == 'ค่าที่พักรายเดือน (Monthly accomodation)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Staff name',
                                         'purpose' => 'Staff name',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Hotel name',
                                         'purpose' => 'Hotel name',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Claim No.',
                                         'purpose' => 'Claim No.',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            break;

                        case 'ค่าที่พักในต่างประเทศ (Oversea Accomodation)':

                            if($subCategory->name == 'ค่าโรงแรมในต่างประเทศ (Oversea hotel fee)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Objective',
                                         'purpose' => 'Objective',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Country',
                                         'purpose' => 'Country',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Hotel name',
                                         'purpose' => 'Hotel name',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Staff name',
                                         'purpose' => 'Staff name',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            break;

                        case 'ค่าเบี้ยเลี้ยงต่างประเทศ (Overseas daily allowance)':

                            if($subCategory->name == 'ค่าเบี้ยเลี้ยงต่างประเทศ (Overseas daily allowance)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Objective',
                                         'purpose' => 'Objective',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Country',
                                         'purpose' => 'Country',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Number of night',
                                         'purpose' => 'Number of night',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            break;

                        case 'ค่ารับรองลูกค้าและคู่ค้า (Entertain expense)':

                            if($subCategory->name == 'ค่าอาหารและเครื่องดื่ม (Food & Beverage)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Customer name and company name',
                                         'purpose' => 'Customer name and company name',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Objective',
                                         'purpose' => 'Objective',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => false,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            if($subCategory->name == 'ของขวัญ ดอกไม้ และกระเช้า (Gift & Flower)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Customer name and company name',
                                         'purpose' => 'Customer name and company name',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Objective',
                                         'purpose' => 'Objective',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => false,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            if($subCategory->name == "เงินสนับสนุนกิจกรรมคู่ค้า (Cash for support activity's busniess partner)"){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Customer name and company name',
                                         'purpose' => 'Customer name and company name',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Objective',
                                         'purpose' => 'Objective',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => false,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                            }

                            if($subCategory->name == 'สันทนาการ-กอล์ฟ/กีฬา (Sport,Golf,Others)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Customer name and company name',
                                         'purpose' => 'Customer name and company name',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Objective',
                                         'purpose' => 'Objective',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => false,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            if($subCategory->name == 'ค่าที่พักลูกค้า/คู่ค้า (Hotel fee for client & business partner)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Customer name and company name',
                                         'purpose' => 'Customer name and company name',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Objective',
                                         'purpose' => 'Objective',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => false,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            break;

                        case 'ค่าใช้จ่ายสนับสนุนกิจกรรมการตลาด (Marketing activity support expense)':

                            if($subCategory->name == 'ค่าใช้จ่ายในการจัดกิจกรรมส่งเสริมการขาย (Expense to support sale promotion activity)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Customer name',
                                         'purpose' => 'Customer name',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Activity Date',
                                         'purpose' => 'Activity Date',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            if($subCategory->name == 'ค่าจัดทำป้ายมอบค่าสินไหม (Claim compensation billboard)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Claimant',
                                         'purpose' => 'Claimant',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                            }

                            break;

                        case 'ค่าใข้จ่ายในการจัดกิจกรรมกระตุ้นยอดขาย (Campaign expense)':

                            if($subCategory->name == 'Gift voucher,Cash,Electrical equipment'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Campaign code & name',
                                         'purpose' => 'Campaign code & name',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Campaign period',
                                         'purpose' => 'Campaign period',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Channel Name',
                                         'purpose' => 'Channel Name',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            break;

                        case 'เงินบริจาค (Donation expense)':

                            if($subCategory->name == 'เงินบริจาค (Donation expense)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Organizer name',
                                         'purpose' => 'Organizer name',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            break;

                        case 'อบรมตัวแทน/คู่ค้า (Agent training expense)':

                            if($subCategory->name == 'ค่าสถานที่จัดอบรม (Hotel fee for training)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Course',
                                         'purpose' => 'Course',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place',
                                         'purpose' => 'Place',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Date',
                                         'purpose' => 'Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            if($subCategory->name == 'ค่าวิทยากร (Instructor fee)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Course',
                                         'purpose' => 'Course',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place',
                                         'purpose' => 'Place',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Date',
                                         'purpose' => 'Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Instructor name',
                                         'purpose' => 'Instructor name',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            if($subCategory->name == 'ค่าวัสดุ อุปรกณ์อบรม (Material expense)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Course',
                                         'purpose' => 'Course',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place',
                                         'purpose' => 'Place',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Date',
                                         'purpose' => 'Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            if($subCategory->name == 'ค่าอาหาร (Food & beverage for training)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Course',
                                         'purpose' => 'Course',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place',
                                         'purpose' => 'Place',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Date',
                                         'purpose' => 'Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            if($subCategory->name == 'ค่าธรรมเนียมสอบใบอนุญาติตัวแทน (Agent license examination fee)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Course',
                                         'purpose' => 'Course',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place',
                                         'purpose' => 'Place',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Date',
                                         'purpose' => 'Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Agent name',
                                         'purpose' => 'Agent name',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            break;

                        case 'ค่าสมาชิกสมาคมต่าง ๆ (Membership fee)':

                            if($subCategory->name == 'ค่าสมาชิกสมาคมต่าง ๆ (Membership fee)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Organizer name',
                                         'purpose' => 'Organizer name',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Membership Period',
                                         'purpose' => 'Membership Period',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                            }

                            break;

                        case 'ค่าหนังสือ (Books and magazine expense)':

                            if($subCategory->name == 'ค่าหนังสือ (Books and magazine expense)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Megazine Name',
                                         'purpose' => 'Megazine Name',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Objective',
                                         'purpose' => 'Objective',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            break;

                        case 'ค่าธรรมเนียมหน่วยงานราชการ/คปภ. (Government/Regulation fee)':

                            if($subCategory->name == 'ค่าธรรมเนียมหน่วยงานราชการ (Government fee)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Government name',
                                         'purpose' => 'Government name',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Objective',
                                         'purpose' => 'Objective',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            if($subCategory->name == 'ค่าธรรมเนียมขออนุมัติผลิตภัณฑ์ใหม่ (OIC product approval fee)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Government name',
                                         'purpose' => 'Government name',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Objective',
                                         'purpose' => 'Objective',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            break;

                        case 'Visa for oversea businsess trip':

                            if($subCategory->name == 'Visa fee for business trip'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Business trip date',
                                         'purpose' => 'Business trip date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Objective',
                                         'purpose' => 'Objective',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Country',
                                         'purpose' => 'Country',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            break;

                        case 'Visa & Work Permit expense':

                            if($subCategory->name == 'Visa & Work Permit expense'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'EXPAT name',
                                         'purpose' => 'EXPAT name',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            break;

                        case 'ค่าส่งเอกสาร (Postage & Courier expense)':

                            if($subCategory->name == 'ค่าไปรษณีย์ รถตู้ รถทัวร์ (Courier expense)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Document name',
                                         'purpose' => 'Document name',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place from-to',
                                         'purpose' => 'Place from-to',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                            }

                            break;

                        case 'ค่าล้างรถ (Car wash expense)':

                            if($subCategory->name == 'ค่าล้างรถ (Car wash expense)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Car number',
                                         'purpose' => 'Car number',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Car owner',
                                         'purpose' => 'Car owner',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            break;

                        case 'ค่าคัดสำเนาประจำวัน Cliam expense (Daily report copy fee)':

                            if($subCategory->name == 'ค่าคัดสำเนาประจำวัน - Cliam expense (Daily report copy fee)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Claim no.',
                                         'purpose' => 'Claim no.',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            break;

                        case 'ค่าอากรแสตมป์ (Stamp duty expense)':

                            if($subCategory->name == 'ค่าอากรแสตมป์ (Stamp duty expense)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Objective',
                                         'purpose' => 'Objective',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            break;

                        case 'ค่าใช้จ่ายเกี่ยวกับสวัสดิการพนักงาน (Staff welfare related expenses)':

                            if($subCategory->name == "พวงหรีดงานศพ ครอบครัวพนักงาน (Funeral wreath,cash for staff's family)"){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Staff Name',
                                         'purpose' => 'Staff Name',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            break;

                        case 'เครื่องเขียน (Stationery)':

                            if($subCategory->name == 'เครื่องเขียน (Stationery)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Objective',
                                         'purpose' => 'Objective',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            break;

                        case 'ค่าใช้จ่ายในการอบรมพนักงาน (Staff training expense)':

                            if($subCategory->name == 'ค่าอาหารและของที่ระลึกวิทยากร (Food & Gift for instructor)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Course',
                                         'purpose' => 'Course',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Date',
                                         'purpose' => 'Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Instructor name',
                                         'purpose' => 'Instructor name',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            if($subCategory->name == 'ค่าอบรม (Instructor fee)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Course',
                                         'purpose' => 'Course',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Date',
                                         'purpose' => 'Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                            }

                            if($subCategory->name == 'ค่าวัสดุ อุปรกณ์อบรม (Material expense)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Course',
                                         'purpose' => 'Course',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Date',
                                         'purpose' => 'Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                            }

                            break;

                        case 'ค่าซ่อมแซมบำรุง (Repair & maintenance expense)':

                            if($subCategory->name == 'ค่าซ่อมแซมบำรุง (Repair & maintenance expense)'){
                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Detail of repair and maintenance',
                                         'purpose' => 'Detail of repair and maintenance',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            break;


                        case 'ค่าใช้จ่ายเกี่ยวกับกิจกรรมพนักงาน (Staff activity expense)':

                            if($subCategory->name == 'ค่าใช้จ่ายเกี่ยวกับกิจกรรมพนักงาน (Staff activity expense)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Activity name',
                                         'purpose' => 'Activity name',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Date',
                                         'purpose' => 'Date',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);
                            }

                            break;


                        case 'ค่าใช้จ่ายไปเที่ยวประจำปี (Excursion expenses)':

                            if($subCategory->name == 'Excursion - ค่าที่พัก (Accomodation fee for excursion)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Date',
                                         'purpose' => 'Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place',
                                         'purpose' => 'Place',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                            }

                            if($subCategory->name == 'Excursion - ค่าอาหาร,เครื่องดื่ม,ของว่าง (Food & beverage forexcursion)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Date',
                                         'purpose' => 'Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place',
                                         'purpose' => 'Place',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                            }

                            if($subCategory->name == 'Excursion - ค่าใช้จ่ายการแสดง (Admission fare to the show)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Date',
                                         'purpose' => 'Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place',
                                         'purpose' => 'Place',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                            }

                            if($subCategory->name == 'Excursion - ค่าเช่ารถ (Car rental for excursion)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Date',
                                         'purpose' => 'Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place',
                                         'purpose' => 'Place',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                            }

                            if($subCategory->name == 'Excursion - ค่าทางด่วน (express way fee for excursion)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Date',
                                         'purpose' => 'Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place',
                                         'purpose' => 'Place',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                            }

                            if($subCategory->name == 'Excursion - ค่าเช่าเครื่องเสียงและดนตรี (Music expense for excursion)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Date',
                                         'purpose' => 'Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place',
                                         'purpose' => 'Place',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                            }

                            if($subCategory->name == 'Excursion - ค่าใช้จ่ายจัดกิจกรรมต่าง ๆ เช่น เกมส์ (Recreation expense)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Date',
                                         'purpose' => 'Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place',
                                         'purpose' => 'Place',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                            }

                            break;

                        case 'ค่าใช้จ่ายงานปีใหม่ (Staff party expense)':

                            if($subCategory->name == 'Staff party expense - ค่าเช่าสถานที่ (Place fee)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Date',
                                         'purpose' => 'Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place',
                                         'purpose' => 'Place',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                            }

                            if($subCategory->name == 'Staff party expense - ค่าอาหาร (Food & beverage expense)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Date',
                                         'purpose' => 'Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place',
                                         'purpose' => 'Place',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                            }

                            if($subCategory->name == 'Staff party expense - ค่าซุ้มเกมส์, Lucky draw, อื่น ๆ (Recreation expense,games,etc.)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Date',
                                         'purpose' => 'Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place',
                                         'purpose' => 'Place',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                            }

                            break;

                        case 'กิจกรรมชมรมฟุตบอล (TMFC expense)':

                            if($subCategory->name == 'TMFC expense - ค่าเช่าสนาม (Field expense)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Date',
                                         'purpose' => 'Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place',
                                         'purpose' => 'Place',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                            }

                            if($subCategory->name == 'TMFC expense - ค่าอาหารและเครื่องดื่ม (Food & Beverage expense)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Date',
                                         'purpose' => 'Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place',
                                         'purpose' => 'Place',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                            }

                            if($subCategory->name == 'ค่ายา,ค่าชุดและอุปกรณ์ในการฝึกซ้อม (Medicine, sport ware and equipment expense)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Date',
                                         'purpose' => 'Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Place',
                                         'purpose' => 'Place',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                            }

                            if($subCategory->name == 'ค่าผู้ฝึกสอน (Coach fee)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Instructor name',
                                         'purpose' => 'Instructor name',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                            }

                            break;

                        case 'ค่ายาสามัญ (Medical)':

                            if($subCategory->name == 'ค่ายาสามัญ (Medical)'){

                                //

                            }

                            break;

                        case 'WHT-Manual':

                            if($subCategory->name == 'WHT-Manual'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'WHT Number',
                                         'purpose' => 'WHT Number',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Vendor Name',
                                         'purpose' => 'Vendor Name',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'WHT Date',
                                         'purpose' => 'WHT Date',
                                         'form_type' => 'date',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => false,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                            }

                            break;

                        case 'คืนเงิน 100% (Refund cash advance 100%)':

                            if($subCategory->name == 'คืนเงิน 100% (Refund cash advance 100%)'){

                                array_push($lists, [
                                         'category_id' => $category->id,
                                         'sub_category_id' => $subCategory->id,
                                         'attribute_name' => 'Reason in case of refund 100%',
                                         'purpose' => 'Reason in case of refund 100%',
                                         'form_type' => 'text',
                                         'form_value' => null,
                                         'required' => true,
                                         'active' => true,
                                         'is_primary_info' => true,
                                         'created_at' => date('Y-m-d H:i:s'),
                                         'updated_at' => date('Y-m-d H:i:s')]);

                            }

                            break;

                        default:
                            # code...
                            break;
                    }

                }
            }
        // }

        return $lists;
    }
}