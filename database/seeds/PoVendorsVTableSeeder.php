<?php

use Illuminate\Database\Seeder;

class PoVendorsVTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('xxweb_po_vendors_v')->truncate();
        DB::table('xxweb_po_vendors_v')->insert($this->items());
    }

    public function items()
    {
        $lists = [];

        array_push($lists, [
         'vendor_id' => '5213',
         'vendor_no' => '104472',
         'vendor_name' => 'HUAKEE CO.,LTD.',
         'attribute5' => null,
         'vendor_site_id' => '6118',
         'vendor_site_code' => 'HQ',
         'org_id' => '81',
         'tax_id' => '0301233006012',
         'branch_number' => '00000']);

        array_push($lists, [
         'vendor_id' => '5214',
         'vendor_no' => '104611',
         'vendor_name' => 'NIPPON GASKET (THAILAND) CO., LTD.',
         'attribute5' => null,
         'vendor_site_id' => '6333',
         'vendor_site_code' => 'HQ',
         'org_id' => '81',
         'tax_id' => '0221532002052',
         'branch_number' => '00000']);

        array_push($lists, [
         'vendor_id' => '5215',
         'vendor_no' => '104659',
         'vendor_name' => 'NIPA CHONSAIPAN - SUKHANSAR',
         'attribute5' => null,
         'vendor_site_id' => '6334',
         'vendor_site_code' => 'HQ',
         'org_id' => '81',
         'tax_id' => '0135573013218',
         'branch_number' => '00000']);

        array_push($lists, [
         'vendor_id' => '5011',
         'vendor_no' => '100294',
         'vendor_name' => 'CHIANGMAI RAM HOSPITAL CO., LTD.',
         'attribute5' => null,
         'vendor_site_id' => '6052',
         'vendor_site_code' => 'HQ',
         'org_id' => '81',
         'tax_id' => '0505533004012',
         'branch_number' => '00000']);

    	return $lists;

    }
}
