<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashAdvanceInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xxweb_cash_advance_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cash_advance_id');
            $table->integer('ca_sub_category_id');
            $table->integer('ca_sub_category_info_id');
            $table->string('description',2000)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xxweb_cash_advance_infos');
    }
}
