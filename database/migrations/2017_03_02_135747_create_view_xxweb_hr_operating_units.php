<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewXxwebHrOperatingUnits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xxweb_hr_operating_units', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('organization_id');
            $table->string('name')->nullable();
            $table->datetime('date_from');
            $table->datetime('date_to')->nullable();
            $table->string('short_code');
            $table->string('set_of_books_id');
            $table->string('default_legal_context_id')->nullable();
            $table->string('usable_flag')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xxweb_hr_operating_units');
    }
}
