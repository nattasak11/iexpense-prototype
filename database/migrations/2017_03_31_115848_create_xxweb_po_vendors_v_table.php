<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateXxwebPoVendorsVTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xxweb_po_vendors_v', function (Blueprint $table) 
        {
            $table->increments('id');
            $table->string('vendor_id');
            $table->string('vendor_no');
            $table->string('vendor_name'); 
            $table->string('attribute5')->nullable();  
            $table->string('vendor_site_id')->nullable();  
            $table->string('vendor_site_code')->nullable();
            $table->string('org_id');  
            $table->string('tax_id')->nullable();  
            $table->string('branch_number')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xxweb_po_vendors_v');
    }
}
