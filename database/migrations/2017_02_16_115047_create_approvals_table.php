<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApprovalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xxweb_approvals', function (Blueprint $table) {
            $table->increments('id');
            // approver user_id
            $table->integer('user_id');
            // REIMBURSEMENT, CASH-ADVANCE, CLEARING
            $table->string('process_type'); 
            // APPROVER or FINANCE
            $table->string('approver_type'); 
            // Level of Hierarchy for approver_type = 'APPROVER'
            $table->string('hierarchy_level')->nullable(); 
            // reimId or cashAdvanceId
            $table->integer('approvalable_id'); 
            // App\Reimbursement or App\CashAdvance
            $table->string('approvalable_type'); 

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xxweb_approvals');
    }
}
