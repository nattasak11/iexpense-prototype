<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateXxwebApPaymentMethodsVTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xxweb_ap_payment_methods_v', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('payment_method_name');
            $table->string('payment_method_code');
            $table->string('description');
            $table->string('inactive_date')->nullable();
            $table->string('status')->nullable();
            $table->string('status_code')->nullable();
            $table->string('author')->nullable();
            $table->string('pm_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xxweb_ap_payment_methods_v');
    }
}
