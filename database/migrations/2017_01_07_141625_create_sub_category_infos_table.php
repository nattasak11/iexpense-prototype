<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubCategoryInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xxweb_sub_category_infos', function (Blueprint $table) {
            $table->increments('id');
            // $table->string('org_id');
            $table->integer('category_id');
            $table->integer('sub_category_id');
            $table->string('attribute_name');
            $table->text('purpose')->nullable();
            $table->string('form_type'); // text,select,date,check
            $table->string('form_value')->nullable();
            $table->boolean('required');
            $table->boolean('active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xxweb_sub_category_infos');
    }
}
