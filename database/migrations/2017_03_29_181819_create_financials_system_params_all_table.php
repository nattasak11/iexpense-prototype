<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFinancialsSystemParamsAllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('financials_system_params_all', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('set_of_books_id');
            $table->string('payment_method_lookup_code')->nullable();
            $table->string('ship_to_location_id')->nullable();
            $table->string('bill_to_location_id')->nullable();
            $table->string('accts_pay_code_combination_id')->nullable();
            $table->string('prepay_code_combination_id')->nullable();
            $table->string('org_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('financials_system_params_all');
    }
}
