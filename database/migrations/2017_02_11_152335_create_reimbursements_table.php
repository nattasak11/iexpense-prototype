<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReimbursementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xxweb_reimbursements', function (Blueprint $table) {
            $table->increments('id');
            $table->string('org_id');
            $table->string('document_no');
            $table->integer('user_id');
            $table->string('currency_id');
            $table->string('purpose',2000)->nullable();
            $table->string('status');
            $table->integer('next_approver_id')->nullable();
            $table->boolean('over_budget')->nullable();
            $table->boolean('exceed_policy')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xxweb_reimbursements');
    }
}
