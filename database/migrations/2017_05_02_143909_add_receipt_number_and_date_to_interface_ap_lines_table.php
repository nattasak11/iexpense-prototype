<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReceiptNumberAndDateToInterfaceApLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('xxweb_interface_ap_lines', function (Blueprint $table) {
            $table->string('receipt_number')->nullable();
            $table->datetime('receipt_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('xxweb_interface_ap_lines', function (Blueprint $table) {
            $table->dropColumn('receipt_number');
            $table->dropColumn('receipt_date');
        });
    }
}
