<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateXxwebWhtsVTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xxweb_whts_v', function (Blueprint $table) 
        {
            $table->increments('id');
            $table->string('pay_awt_group_id');    
            $table->string('wht_name');    
            $table->string('org_id');  
            $table->integer('tax_rate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xxweb_whts_v');
    }
}
