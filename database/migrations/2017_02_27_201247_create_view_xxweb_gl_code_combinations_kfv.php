<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewXxwebGlCodeCombinationsKfv extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xxweb_gl_code_combinations_kfv', function (Blueprint $table) 
        {
            $table->increments('id');
            $table->string('code_combination_id')->nullable(); 
            $table->string('chart_of_accounts_id')->nullable();    
            $table->string('concatenated_segments')->nullable();   
            $table->string('padded_concatenated_segments')->nullable();    
            $table->string('gl_account_type')->nullable(); 
            $table->string('ledger_segment')->nullable();  
            $table->string('enabled_flag')->nullable();    
            $table->string('summary_flag')->nullable();    
            $table->string('segment1')->nullable();    
            $table->string('segment2')->nullable();    
            $table->string('segment3')->nullable();    
            $table->string('segment4')->nullable();    
            $table->string('segment5')->nullable();    
            $table->string('segment6')->nullable();    
            $table->string('segment7')->nullable();    
            $table->string('segment8')->nullable();    
            $table->string('segment9')->nullable();    
            $table->string('segment10')->nullable();   
            $table->string('segment11')->nullable();   
            $table->string('segment12')->nullable();   
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xxweb_gl_code_combinations_kfv');
    }
}
