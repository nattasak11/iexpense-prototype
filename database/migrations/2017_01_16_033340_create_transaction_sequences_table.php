<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionSequencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xxweb_transaction_sequences', function (Blueprint $table) {
            $table->increments('id');
            $table->string('org_id');
            $table->string('name'); // name of model that use seq
            $table->string('year'); // ex. 16,17
            $table->bigInteger('tran_id')->default(0); // running tran id seq
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xxweb_transaction_sequences');
    }
}
