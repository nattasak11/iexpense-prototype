<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashAdvancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xxweb_cash_advances', function (Blueprint $table) {
            $table->increments('id');
            $table->string('org_id');
            $table->string('document_no');
            $table->string('clearing_document_no');
            $table->integer('user_id');
            $table->datetime('need_by_date');
            $table->integer('ca_category_id');
            $table->integer('ca_sub_category_id');
            $table->datetime('due_date')->nullable();
            $table->datetime('paid_date')->nullable();
            $table->decimal('amount',20,8);
            $table->string('currency_id');
            $table->string('purpose',2000)->nullable();
            $table->string('status');
            $table->integer('next_approver_id')->nullable();
            $table->integer('finance_approver_id')->nullable();
            $table->integer('next_clearing_approver_id')->nullable();
            $table->integer('clearing_finance_approver_id')->nullable();
            $table->boolean('over_budget')->nullable();
            $table->boolean('exceed_policy')->nullable();
            $table->boolean('manual_payment')->nullable();
            $table->string('payment_method_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xxweb_cash_advances');
    }
}
