<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateXxwebAccountInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xxweb_account_info', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('id_flex_structure_code')->nullable();
            $table->string('id_flex_structure_name')->nullable();
            $table->string('application_column_name');
            $table->string('segment_name');
            $table->string('flex_value_set_name');
            $table->string('chart_of_accounts_id');
            $table->string('parent_flex_value_set_name')->nullable();
            $table->string('ledger_id');
            $table->string('org_id');
            $table->string('ou_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xxweb_account_info');
    }
}
