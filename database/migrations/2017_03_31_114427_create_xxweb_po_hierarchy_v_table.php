<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateXxwebPoHierarchyVTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xxweb_po_hierarchy_v', function (Blueprint $table) 
        {
            $table->increments('id');
            $table->string('business_group_id')->nullable();   
            $table->string('pos_structure_version_id')->nullable();    
            $table->string('name');    
            $table->string('hierarchy')->nullable();   
            $table->string('version_number')->nullable();  
            $table->string('rep_level')->nullable();   
            $table->string('parent_position_id')->nullable();  
            $table->string('parent_name')->nullable(); 
            $table->string('parent_job_id')->nullable();   
            $table->string('parent_job')->nullable();  
            $table->string('parent_approval_authority')->nullable();   
            $table->string('child_position_id')->nullable();   
            $table->string('child_name')->nullable();  
            $table->string('child_job_id')->nullable();    
            $table->string('child_job')->nullable();   
            $table->string('child_approval_authority')->nullable();    
            $table->string('subordinate_position_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xxweb_po_hierarchy_v');
    }
}
