<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePolicyRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xxweb_policy_rates', function (Blueprint $table) {
            $table->increments('id');
            // $table->string('org_id');
            $table->integer('category_id');
            $table->integer('sub_category_id');
            $table->integer('policy_id');
            $table->string('location_id')->nullable();
            $table->string('position_po_level')->nullable();
            $table->boolean('unlimit')->default(false);
            $table->decimal('rate',20,8)->nullable();
            $table->string('currency_id');
            $table->boolean('active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xxweb_policy_rates');
    }
}
