<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoliciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xxweb_policies', function (Blueprint $table) {
            $table->increments('id');
            // $table->string('org_id');
            $table->integer('category_id');
            $table->integer('sub_category_id');
            $table->string('type'); // EXPENSE, MILEAGE
            $table->string('mileage_unit')->nullable(); // FOR TYPE MILEAGE
            $table->boolean('active')->default(true);
            $table->timestamps();
        });
    } 

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xxweb_policies');
    }
}
