<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewXxwebGlPeriodStatusesV extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xxweb_gl_period_statuses_v', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('period_name');
            $table->string('set_of_books_id')->nullable();
            $table->string('application_id');
            $table->string('closing_status')->nullable();
            $table->string('org_id');
            $table->datetime('start_date');
            $table->datetime('end_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xxweb_gl_period_statuses_v');
    }
}
