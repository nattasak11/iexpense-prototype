<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateXxwebFndLovsVTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('xxweb_fnd_lovs_v', function (Blueprint $table) 
        {
            $table->increments('id');
            $table->string('flex_value_set_id');   
            $table->string('flex_value_set_name'); 
            $table->string('parent_flex_value_set_id')->nullable();    
            $table->string('parent_flex_value_set_name')->nullable();  
            $table->string('flex_value');  
            $table->string('flex_value_meaning')->nullable();  
            $table->string('description')->nullable(); 
            $table->string('parent_flex_value_low')->nullable();   
            $table->string('parent_flex_value_high')->nullable();  
            $table->string('summary_flag')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xxweb_fnd_lovs_v');
    }
}
