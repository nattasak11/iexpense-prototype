<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateXxwebPoEmployeesVTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('xxweb_po_employees_v', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('employee_number');
            $table->string('full_name');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email_address');
            $table->datetime('effective_start_date')->nullable();
            $table->datetime('effective_end_date')->nullable();
            $table->string('person_id')->nullable();
            $table->string('ad_user_name')->nullable();
            $table->string('concatenated_segments')->nullable();
            $table->string('segment1')->nullable();
            $table->string('segment2')->nullable();
            $table->string('segment3')->nullable();
            $table->string('segment4')->nullable();
            $table->string('segment5')->nullable();
            $table->string('segment6')->nullable();
            $table->string('segment7')->nullable();
            $table->string('segment8')->nullable();
            $table->string('segment9')->nullable();
            $table->string('segment10')->nullable();
            $table->string('segment11')->nullable();
            $table->string('segment12')->nullable();
            $table->string('position_id')->nullable();
            $table->string('position_name')->nullable();
            $table->string('job_id')->nullable();
            $table->string('job_name')->nullable();
            $table->string('approval_authority')->nullable();
            $table->string('vendor_id')->nullable();
            $table->string('payment_method_code')->nullable();
            $table->string('tax_id')->nullable();
            $table->string('vendor_no')->nullable();
            $table->string('vendor_name')->nullable();
            $table->string('vendor_site_id')->nullable();
            $table->string('vendor_site_code')->nullable();
            $table->string('terms_id')->nullable();
            $table->string('accts_pay_code_combination_id')->nullable();
            $table->string('branch_number')->nullable();
            $table->string('bank_account_num')->nullable();
            $table->string('bank_account_name')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('company_name')->nullable();
            $table->string('phor_ngor_dor')->nullable();
            $table->string('department_name')->nullable();
            $table->string('supervisor_id')->nullable();
            $table->string('supervisor_no')->nullable();
            $table->string('supervisor_full_name')->nullable();
            $table->string('chart_of_accounts_id')->nullable();
            $table->string('set_of_books_id')->nullable();
            $table->string('ou_name');
            $table->string('org_id');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xxweb_po_employees_v');
    }
}
