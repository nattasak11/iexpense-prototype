<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableXxwebInterfaceApHeaders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xxweb_interface_ap_headers', function (Blueprint $table) {
            $table->increments('id');

            // FROM WEB
            $table->string('request_type'); // REIMBURSEMENT, CASH-ADVANCE, CLEARING
            $table->integer('request_id'); 

            // INTERFACE TO ORACLE AP
            $table->string('org_id')->nullable();
            $table->string('description',2000)->nullable();
            $table->datetime('due_date')->nullable();
            $table->datetime('invoice_date')->nullable();
            $table->datetime('gl_date')->nullable();
            $table->string('invoice_number')->nullable();
            $table->string('apply_invoice_number')->nullable();
            $table->string('vendor_id')->nullable();
            $table->string('vendor_site_id')->nullable();
            $table->string('currency')->nullable();
            $table->string('invoice_type')->nullable();
            $table->string('payment_method_code')->nullable();
            $table->string('term_id')->nullable();
            $table->decimal('invoice_amount',20,8)->nullable();
            $table->string('accts_pay_code_combination_id')->nullable();

            $table->string('tax_flag')->nullable(); // [ Y | N ]

            // RESPONSE FROM INTERFACE
            $table->string('interface_status')->nullable();
            $table->string('interface_message',2000)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xxweb_interface_ap_headers');
    }
}
