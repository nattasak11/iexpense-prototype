<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnForRechargeAccountCodeInSubCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('xxweb_sub_categories', function (Blueprint $table) {
            $table->string('recharge_account_code')->nullable();
            $table->string('recharge_sub_account_code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('xxweb_sub_categories', function (Blueprint $table) {
            $table->dropColumn('recharge_account_code');
            $table->dropColumn('recharge_sub_account_code');
        });
    }
}
