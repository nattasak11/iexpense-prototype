<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaSubCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xxweb_ca_sub_categories', function (Blueprint $table) {
            $table->increments('id');
            // $table->string('org_id');
            $table->integer('ca_category_id');
            $table->string('name');
            $table->string('description',2000);
            $table->datetime('start_date')->nullable();
            $table->datetime('end_date')->nullable();
            $table->string('account_code')->nullable();
            $table->string('sub_account_code')->nullable();
            $table->string('branch_code')->nullable();
            $table->string('department_code')->nullable();
            $table->string('vat_id')->nullable();
            $table->boolean('required_attachment')->default(false);
            $table->boolean('check_ca_min');
            $table->decimal('ca_min_amount',20,8)->nullable();
            $table->boolean('check_ca_max');
            $table->decimal('ca_max_amount',20,8)->nullable();
            $table->boolean('active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xxweb_ca_sub_categories');
    }
}
