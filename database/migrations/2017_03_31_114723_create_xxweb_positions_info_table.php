<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateXxwebPositionsInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('xxweb_positions_info', function (Blueprint $table) 
        {
            $table->increments('id');
            $table->string('hierarchy_name');  
            $table->string('version_number')->nullable();  
            $table->string('parent_pos_id')->nullable();
            $table->string('parent_pos')->nullable();
            $table->string('parent_job_id')->nullable();
            $table->string('parent_job')->nullable();  
            $table->string('parent_approval_authority')->nullable();
            $table->decimal('parent_amount_limit',20,4)->nullable(); 
            $table->string('control_function_name');
            $table->string('child_pos_id')->nullable();
            $table->string('child_pos')->nullable();   
            $table->string('child_job_id')->nullable();
            $table->string('child_job')->nullable();   
            $table->string('child_approval_authority')->nullable();
            $table->string('control_rule_id')->nullable(); 
            $table->string('org_id');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xxweb_positions_info');
    }
}
