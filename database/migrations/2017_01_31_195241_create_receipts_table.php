<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xxweb_receipts', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('receiptable_id');
            $table->string('receiptable_type');

            $table->string('receipt_number')->nullable();
            $table->datetime('receipt_date')->nullable();
            $table->string('location_id');
            $table->string('vendor_id')->nullable();
            $table->string('vendor_name')->nullable();
            $table->string('vendor_tax_id')->nullable();
            $table->string('vendor_branch_name')->nullable();
            $table->string('establishment_id')->nullable();
            $table->string('establishment_name')->nullable();
            $table->string('currency_id');
            $table->decimal('exchange_rate',20,8)->nullable();
            $table->string('jusification',2000)->nullable();
            $table->string('job')->nullable();
            $table->string('project')->nullable();
            $table->string('recharge')->nullable();
            $table->string('status')->nullable(); // may be use
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xxweb_receipts');
    }
}
