<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateXxwebTaxsVTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xxweb_taxs_v', function (Blueprint $table) 
        {
            $table->increments('id');
            $table->string('tax_rate_code'); 
            $table->string('tax_regime_code'); 
            $table->string('tax'); 
            $table->string('tax_status_code')->nullable();  
            $table->string('tax_account_ccid')->nullable(); 
            $table->integer('percentage_rate');  
            $table->string('org_id');   
            $table->string('tax_account')->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xxweb_taxs_v');
    }
}
