<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDocFlagToInterfaceApHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('xxweb_interface_ap_headers', function (Blueprint $table) {
            $table->string('doc_flag')->nullable(); // [ Y | N ]
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('xxweb_interface_ap_headers', function (Blueprint $table) {
            $table->dropColumn('doc_flag'); // [ Y | N ]
        });
    }
}
