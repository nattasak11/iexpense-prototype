<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xxweb_attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('original_name');
            $table->string('path');
            $table->string('mime_type');
            $table->integer('attachmentable_id');
            $table->string('attachmentable_type');
            $table->string('file_name');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xxweb_attachments');
    }
}
