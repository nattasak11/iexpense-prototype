<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsPrimaryInfoToCaSubCategoryInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('xxweb_ca_sub_category_infos', function (Blueprint $table) {
            $table->string('is_primary_info')->nullable(); // [ Y | N ]
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('xxweb_ca_sub_category_infos', function (Blueprint $table) {
            $table->dropColumn('is_primary_info'); // [ Y | N ]
        });
    }
}
