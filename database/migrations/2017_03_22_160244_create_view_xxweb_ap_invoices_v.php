<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewXxwebApInvoicesV extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // $sql = "CREATE OR REPLACE VIEW xxweb_ap_invoices_v (
        //            invoice_id,
        //            vendor_id,
        //            invoice_num,
        //            set_of_books_id,
        //            invoice_currency_code,
        //            payment_currency_code,
        //            payment_cross_rate,
        //            invoice_amount,
        //            vendor_site_id,
        //            amount_paid,
        //            discount_amount_taken,
        //            invoice_date,
        //            source,
        //            invoice_type_lookup_code,
        //            description,
        //            batch_id,
        //            amount_applicable_to_discount,
        //            tax_amount,
        //            terms_id,
        //            terms_date,
        //            payment_method_lookup_code,
        //            pay_group_lookup_code,
        //            accts_pay_code_combination_id,
        //            payment_status_flag,
        //            creation_date,
        //            voucher_num,
        //            approved_amount,
        //            doc_sequence_id,
        //            doc_sequence_value,
        //            doc_category_code,
        //            attribute1,
        //            prepay_flag,
        //            cancelled_date,
        //            cancelled_by,
        //            cancelled_amount,
        //            temp_cancelled_amount,
        //            vendor_prepay_amount,
        //            org_id,
        //            payment_cross_rate_date,
        //            pay_curr_invoice_amount,
        //            gl_date,
        //            approval_ready_flag,
        //            payment_method_code,
        //            party_id,
        //            party_site_id,
        //            pay_proc_trxn_type_code,
        //            payment_function,
        //            pay_awt_group_id )
        //         AS
        //         SELECT  invoice_id,
        //         vendor_id,
        //         invoice_num,
        //         set_of_books_id,
        //         invoice_currency_code,
        //         payment_currency_code,
        //         payment_cross_rate,
        //         invoice_amount,
        //         vendor_site_id,
        //         amount_paid,
        //         discount_amount_taken,
        //         invoice_date,
        //         source,
        //         invoice_type_lookup_code,
        //         description,
        //         batch_id,
        //         amount_applicable_to_discount,
        //         tax_amount,
        //         terms_id,
        //         terms_date,
        //         payment_method_lookup_code,
        //         pay_group_lookup_code,
        //         accts_pay_code_combination_id,
        //         payment_status_flag,
        //         creation_date,
        //         voucher_num,
        //         approved_amount,
        //         doc_sequence_id,
        //         doc_sequence_value,
        //         doc_category_code,
        //         attribute1,
        //         prepay_flag,
        //         cancelled_date,
        //         cancelled_by,
        //         cancelled_amount,
        //         temp_cancelled_amount,
        //         vendor_prepay_amount,
        //         org_id,
        //         payment_cross_rate_date,
        //         pay_curr_invoice_amount,
        //         gl_date,
        //         approval_ready_flag,
        //         payment_method_code,
        //         party_id,
        //         party_site_id,
        //         pay_proc_trxn_type_code,
        //         payment_function,
        //         pay_awt_group_id
        //         FROM ap_invoices_all@web_to_erp";

        //     DB::connection('oracle')->statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // $sql = "DROP VIEW xxweb_ap_invoices_v";
        // DB::connection('oracle')->statement($sql);
    }
}
