<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInterfaceDocFlagToCaSubCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('xxweb_ca_sub_categories', function (Blueprint $table) {
            $table->string('interface_doc_flag')->nullable(); // [ Y | N ]
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('xxweb_ca_sub_categories', function (Blueprint $table) {
            $table->dropColumn('interface_doc_flag'); // [ Y | N ]
        });
    }
}
