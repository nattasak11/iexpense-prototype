<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CACategory extends Model
{
    protected $table = 'xxweb_ca_categories';
	
    public function scopeActive($query){
        return $query->where('active',true);
    }
}
