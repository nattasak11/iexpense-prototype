<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class ReceiptLineInfo extends Model
{
    // use SoftDeletes;
    protected $table = 'xxweb_receipt_line_infos';
    // protected $dates = ['deleted_at'];

    public function header(){
    	return $this->belongsTo('App\Receipt','receipt_id');
    }

    public function line(){
    	return $this->belongsTo('App\ReceiptLine','receipt_id');
    }

    public function subCategory(){
    	return $this->belongsTo('App\SubCategory','sub_category_id');
    }

    public function subCategoryInfo(){
    	return $this->belongsTo('App\SubCategoryInfo','sub_category_info_id');
    }

    public function getDescriptionForShowAttribute()
    {
        if($this->subCategoryInfo->form_type == 'date'){

            return $this->description ? dateFormatDisplay($this->description) : '-';

        // }else if($this->subCategoryInfo->form_type == 'select'){ // select
        //     $result = '-';
        //     $inputFormValue = $this->subCategoryInfo->input_form_value;
        //     if( array_key_exists($this->description, $inputFormValue) ){
        //         $result = $inputFormValue[$this->description];
        //     }
        //     return $result;
            
        }else{ // text

            return $this->description ? $this->description : '-';

        }
    }
}
