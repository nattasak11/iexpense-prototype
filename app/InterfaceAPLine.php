<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InterfaceAPLine extends Model
{
    protected $table = 'xxweb_interface_ap_lines';
    
    public function header(){
    	return $this->belongsTo('App\InterfaceAPHeader','interface_ap_header_id');
    }
}
