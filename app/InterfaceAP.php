<?php

namespace App;

use DB;
use PDO;

class InterfaceAP
{
	public static function getInvoiceApprovalStatus($orgId,$invoiceNum)
    {
		$result = DB::select(
            DB::raw("select apps.ap_invoices_pkg.get_approval_status@web_to_erp(
            					ai.invoice_id,
								ai.invoice_amount,
								ai.payment_status_flag,
								ai.invoice_type_lookup_code ) as status
					from 	apps.ap_invoices_all@web_to_erp ai 
					where 	org_id = :orgId 
					and 	invoice_num = :invoiceNum"),
        [
        	'orgId' => $orgId,
            'invoiceNum' => $invoiceNum
        ]);

		return $result;
    }

    public static function getBudgetByAccount($orgId,$concatenatedSegments)
    {
    	try {

   //          $db = DB::connection()->getPdo();

   //          $sql = "declare
			// 		v_fund_available                number;
			// 		v_status                        varchar2(2000);
			// 		v_err_msg                       varchar2(4000);
			// 		begin
			// 		    xxweb_utilities.find_fund (p_org_id => :org_id,
			// 		                                p_concatenated_segments => :concatenated_segments,
			// 		                                p_fund_available => :v_fund_available,
			// 		                                p_status => :v_status,
			// 		                                p_err_msg => :v_err_msg);
			// 		end;";

   //          $sql = preg_replace("/[\r\n]*/","",$sql);

   //          $stmt = $db->prepare($sql);

   //          $stmt->bindParam(':org_id', $orgId, PDO::PARAM_STR);
   //          $stmt->bindParam(':concatenated_segments',$concatenatedSegments, PDO::PARAM_STR, 2000);
   //          $stmt->bindParam(':v_fund_available',$result['fund_available'], PDO::PARAM_STR, 2000);
   //          $stmt->bindParam(':v_status',$result['status'], PDO::PARAM_STR, 2000);
   //          $stmt->bindParam(':v_err_msg',$result['err_msg'], PDO::PARAM_STR, 4000);
			// $stmt->execute();

            $result = [];
            $result['fund_available'] = 50000;
            $result['status'] = 'S';
            $result['err_msg'] = '';

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), 1);
        }

        return $result;
    }
}
