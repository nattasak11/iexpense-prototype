<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Carbon\Carbon;

class Category extends Model
{
	protected $table = 'xxweb_categories';

	public function subCategories()
    {
        return $this->hasMany('App\SubCategory','category_id');
    }

    public function scopeActive($query){
        return $query->where('active',true);
    }

    public function scopeAdvanceOver($query){
        return $query->where('name',config('services.category.advance_over_name'));
    }

    public function isAdvanceOver(){
        return $this->name == config('services.category.advance_over_name');
    }
}
