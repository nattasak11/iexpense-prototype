<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReceiptLine extends Model
{
	use SoftDeletes;
    protected $table = 'xxweb_receipt_lines';
    protected $dates = ['deleted_at'];

    public function header(){
    	return $this->belongsTo('App\Receipt','receipt_id');
    }

    public function infos()
    {
        return $this->hasMany('App\ReceiptLineInfo','receipt_line_id');
    }

    public function category(){
    	return $this->belongsTo('App\Category','category_id');
    }

    public function subCategory(){
    	return $this->belongsTo('App\SubCategory','sub_category_id');
    }

    public function policy(){
        return $this->belongsTo('App\Policy','policy_id');
    }

    public function rate(){
        return $this->belongsTo('App\PolicyRate','rate_id');
    }
}
