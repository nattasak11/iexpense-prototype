<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccessibleOrg extends Model
{
    use SoftDeletes;
    protected $table = 'xxweb_accessible_orgs';
    protected $dates = ['deleted_at'];

     /**
     * Get all of the owning receiptable models.
     */
    public function accessibleOrgable()
    {
        return $this->morphTo();
    }
}
