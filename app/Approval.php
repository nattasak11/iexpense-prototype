<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Approval extends Model
{
    use SoftDeletes;
	protected $table = 'xxweb_approvals';
    protected $dates = ['deleted_at'];

	 /**
     * Get all of the owning approvalable models.
     */
    public function approvalable()
    {
        return $this->morphTo();
    }
    
    public function parent()
    {
        if($this->approvalable_type == 'App\CashAdvance'){
            return $this->belongsTo('App\CashAdvance','approvalable_id');
        }elseif($this->approvalable_type == 'App\Reimbursement'){
            return $this->belongsTo('App\Reimbursement','approvalable_id');
        }elseif($this->approvalable_type == 'App\Invoice'){
            return $this->belongsTo('App\Invoice','approvalable_id');
        }else{
            return ;
        }
    }

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }
}
