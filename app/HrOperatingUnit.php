<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HrOperatingUnit extends Model
{
    protected $table = 'xxweb_hr_operating_units';
}
