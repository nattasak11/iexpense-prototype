<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'xxweb_po_employees_v';

    public function user(){
        return $this->hasOne('App\Phone', 'oracle_person_id', 'person_id');
    }

    public function getPositionPoLevelAttribute()
    {
    	$positionPOLevel = null;
    	if($this->position_name){
    		$arrPositionName = explode(".", $this->position_name);
    		if(isset($arrPositionName[2])) {
    			$positionPOLevel = $arrPositionName[2];
    		}
    	}
    	return $positionPOLevel;
    }
}
