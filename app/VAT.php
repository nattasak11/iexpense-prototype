<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VAT extends Model
{
    protected $table = 'xxweb_taxs_v';

    public function scopeApVat($query)
    {
        return $query->whereIn('tax_rate_code',['INPUT','INPUT-ND']);
    }
}
