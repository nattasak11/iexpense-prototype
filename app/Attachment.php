<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attachment extends Model
{
    protected $table = 'xxweb_attachments';
    use SoftDeletes;
    public function attachmentable()
    {
        return $this->morphTo();
    }

    public function getIsImageAttribute()
    {
        $arr_mime_pic = ["jpeg","jpg","bmp","png"];
        if (in_array(strtolower($this->mime_type), $arr_mime_pic)) {
            return true;
        } else {
            return false;
        }
    }
}
