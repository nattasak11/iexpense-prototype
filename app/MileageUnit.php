<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MileageUnit extends Model
{
	protected $table = 'xxweb_mileage_units';
    public function scopeActive($query){
        return $query->where('active',true);
    }
}
