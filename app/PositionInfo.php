<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\POHierarchy;

class PositionInfo extends Model
{
    protected $table = 'xxweb_positions_info';
    
    public function scopeExpensePosition($query,$versionNumber = null)
    {
    	$query->where('hierarchy_name', POHierarchy::getApprovalHierarchyName())->where('control_function_name', POHierarchy::getApprovalControlFunctionName());
    	if($versionNumber){
    		$query->where('version_number', $versionNumber);
    	}
    	return $query;
    }

    public static function getPositionLastVersionNumber()
    {
    	$positions = self::expensePosition()->get();
        $lastVersionPosition = $positions->sortByDesc('version_number')->values()->first();
        $versionNumber = $lastVersionPosition ? $lastVersionPosition->version_number : null;

    	return $versionNumber ? $versionNumber : null;
    }
}
