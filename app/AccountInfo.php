<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountInfo extends Model
{
    protected $table = 'xxweb_account_info';

    public function scopeBranch($query)
    {
        return $query->whereApplicationColumnName('SEGMENT2');
    }

    public function scopeDepartment($query)
    {
        return $query->whereApplicationColumnName('SEGMENT3');
    }

    public function scopeAccount($query)
    {
        return $query->whereApplicationColumnName('SEGMENT6');
    }

    public function scopeSubAccount($query)
    {
        return $query->whereApplicationColumnName('SEGMENT7');
    }

    public function scopeProject($query)
    {
        return $query->whereApplicationColumnName('SEGMENT8');
    }

    public function scopeInterCompany($query)
    {
        return $query->whereApplicationColumnName('SEGMENT9');
    }
}
