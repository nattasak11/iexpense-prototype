<?php


    function isActiveRoute($route, $output = 'active')
    {
        if (Route::currentRouteName() == $route) {
            return $output;
        }
    }

    function genNavElement($navData ,$top_navigation = null)
    {
        $element = '';
        if ($navData) {
            if($top_navigation){
                $element .= '<ul class="nav navbar-nav">';
                foreach ($navData as $nav) {
                    # code...
                    if(array_key_exists('href', $nav) && array_key_exists('text', $nav)){
                        if(array_key_exists('second', $nav)){
                            $element .= '<li class="dropdown">';
                            $element .= '<a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"> '.$nav['text'].'<span class="m-l-xs caret"></span></a>';
                            $element .= '<ul role="menu" class="dropdown-menu">';
                            foreach ($nav['second'] as $second) {
                                # code...
                                if(array_key_exists('href', $second) && array_key_exists('text', $second)){
                                    $element .= '<li><a href="'.$second['href'].'">'.$second['text'].'</a></li>';
                                }
                            }
                            $element .= '</ul>';
                        }else{
                            $element .= '<li>';
                            $element .= '<a aria-expanded="false" role="button" href="'.$nav['href'].'"> '.$nav['text'].'</a>';
                        }
                        $element .= '</li>';
                    }
                }
            }else{
                $element .= '<ul>';
                foreach ($navData as $key => $nav) {
                    # code...
                    if(array_key_exists('href', $nav) && array_key_exists('text', $nav)){
                        if(array_key_exists('second', $nav)){
                            $element .= '<li>';
                            $element .= '<a href="#"> '.$nav['text'].'</a>';
                            $element .= '<ul>';
                            foreach ($nav['second'] as $second) {
                                # code...
                                if(array_key_exists('href', $second) && array_key_exists('text', $second)){
                                    $element .= '<li><a href="'.$second['href'].'">'.$second['text'].'</a></li>';
                                }
                            }
                            $element .= '</ul>';
                        }else{
                            $element .= '<li>';
                            $element .= '<a href="'.$nav['href'].'"> '.$nav['text'].'</a>';
                        }
                        $element .= '</li>';
                    }
                }
            }
            $element .= '</ul>';
        }
        return $element;
    }
