<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Repositories\AttachmentRepo;
use App\Repositories\ActivityLogRepo;
use App\Repositories\RequestRepo;
use App\Repositories\ApprovalRepo;
use App\Repositories\MailRepo;
use App\Repositories\InterfaceRepo;

use App\Invoice;
use App\Preference;
use App\Employee;
use App\Category;
use App\SubCategory;
use App\SubCategoryInfo;
use App\Currency;
use App\Location;
use App\Establishment;
use App\Vendor;
use App\FNDListOfValues;
use App\Receipt;
use App\ReceiptLine;
use App\ReceiptLineInfo;
use App\PaymentMethod;
use App\User;

use Carbon\Carbon;

class InvoiceController extends Controller
{
    protected $perPage = 10;
    protected $orgId;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->orgId = \Auth::user()->org_id;
            return $next($request);
        });
    }
    
    public function index()
    {
        $search = [];
        $invoices = Invoice::orderBy('created_at','desc')
                        ->whereOrgId($this->orgId)
                        ->whereUserId(\Auth::user()->id)
                        ->with('user')
                        ->paginate($this->perPage);
        $allowDuplicate = true;

        return view('invoices.index',
                        compact('invoices',
                                'search',
                                'allowDuplicate'));
    }

    public function indexPending()
    {
        $search = [];
        $invoices = Invoice::orderBy('created_at','desc')
                        ->whereOrgId($this->orgId)
                        ->ByPendingUser(\Auth::user()->id)
                        ->with('user')
                        ->paginate($this->perPage);

        return view('invoices.index_pending',
                        compact('invoices',
                                'search'));
    }

    public function create()
    {
        // GET PENDING TRASACTION
        $pendingRequestLists = RequestRepo::getPendingOverLimitDayRequest(\Auth::user()->id);
        $vendorLists = Vendor::notEmp()->whereOrgId($this->orgId)->pluck('vendor_name','vendor_id')->all();
        return view('invoices.create',
                        compact('pendingRequestLists',
                                'vendorLists'
                                ));
    }

    public function store(Request $request)
    {
        // GET PENDING TRASACTION
        $pendingRequestLists = RequestRepo::getPendingOverLimitDayRequest(\Auth::user()->id);

        \DB::beginTransaction();
        try {
            // CREATE INVOICE
            $invoice = new Invoice();
            $invoice->org_id = $this->orgId;
            // $invoice->organization_id = \Auth::user()->employee->organization_id;
            $invoice->document_no = Invoice::genDocumentNo($this->orgId);
            $invoice->user_id = \Auth::user()->id;
            // $invoice->position_id = $request->position_id;

            $invoice->currency_id  = Preference::getBaseCurrency($this->orgId);
            $invoice->vendor_id = $request->vendor_id ? $request->vendor_id : null;
            if($request->vendor_id == 'other'){
                $invoice->vendor_name = $request->vendor_name ? $request->vendor_name : null;
                $invoice->vendor_tax_id = $request->vendor_tax_id ? $request->vendor_tax_id : null;
                $invoice->vendor_branch_name = $request->vendor_branch_name ? $request->vendor_branch_name : null;
            }
            $invoice->purpose  = trim($request->purpose);
            if(count($pendingRequestLists) == 0){
                $invoice->status = "NEW_REQUEST";
            }else{ // BLOCKED IF HAS PENDING REQUEST
                $invoice->status = "BLOCKED";
            }
            $invoice->save();

            // FILE ATTACHMENTS
            if($request->file('file')){
                $attachmentRepo = new AttachmentRepo;
                $attachmentRepo->create($invoice, $request->file('file'));
            }

            // ACTIVITY LOG 
            $activityLogRepo = new ActivityLogRepo;
            $activityLogRepo->create($invoice, ActivityLogRepo::getActivityMessage('NEW_REQUEST'));

            // SUCCESS CREATE INVOICE
            \DB::commit();
            if($request->ajax()){
                $result['status'] = 'SUCCESS';
                $result['invoiceId'] = $invoice->id;
                return $result;
            }else{
                return redirect()->route('invoices.show',['invoiceId'=>$invoice->id]);
            }

        } catch (\Exception $e) {
            // ERROR CREATE INVOICE
            \DB::rollBack();
            if($request->ajax()){
                $result['status'] = 'ERROR';
                $result['err_msg'] = $e->getMessage();
                return $result;
            }else{
                // throw new \Exception($e->getMessage(), 1);
                \Log::error($e->getMessage());
                return abort('403');
            }
        }
    }

    public function update(Request $request, $invoiceId)
    {
        \DB::beginTransaction();
        try {

            $invoice = Invoice::find($invoiceId);
            $invoice->vendor_id = $request->vendor_id ? $request->vendor_id : null;
            if($request->vendor_id == 'other'){
                $invoice->vendor_name = $request->vendor_name ? $request->vendor_name : null;
                $invoice->vendor_tax_id = $request->vendor_tax_id ? $request->vendor_tax_id : null;
                $invoice->vendor_branch_name = $request->vendor_branch_name ? $request->vendor_branch_name : null;
            }else{
                $invoice->vendor_name = null;
                $invoice->vendor_tax_id = null;
                $invoice->vendor_branch_name = null;
            }
            $invoice->purpose  = trim($request->purpose);
            $invoice->save();

        } catch (\Exception $e) {
            \DB::rollBack();
            \Log::error($e->getMessage());
        }
        \DB::commit();

        return redirect()->route('invoices.show',['invoiceId'=>$invoice->id]);
    }

    public function duplicate(Request $request, $invoiceId)
    {
        // GET PENDING TRASACTION
        $pendingRequestLists = RequestRepo::getPendingOverLimitDayRequest(\Auth::user()->id);

        \DB::beginTransaction();
        try {

            $invoice = Invoice::find($invoiceId);

            ////////////////////////////
            // DUPLICATE REIMBURSEMENT
            // CREATE INVOICE
            $newInvoice = new Invoice();
            $newInvoice->org_id = $this->orgId;
            // $newInvoice->organization_id = \Auth::user()->employee->organization_id;
            $newInvoice->document_no = Invoice::genDocumentNo($this->orgId);
            $newInvoice->user_id = \Auth::user()->id;
            // $newInvoice->position_id = $request->position_id;

            $newInvoice->currency_id  = Preference::getBaseCurrency($this->orgId);
            $newInvoice->vendor_id = $invoice->vendor_id;
            $newInvoice->vendor_name = $invoice->vendor_name;
            $newInvoice->vendor_tax_id = $invoice->vendor_tax_id;
            $newInvoice->vendor_branch_name = $invoice->vendor_branch_name;
            $newInvoice->purpose  = $invoice->purpose;
            if(count($pendingRequestLists) == 0){
                $newInvoice->status = "NEW_REQUEST";
            }else{ // BLOCKED IF HAS PENDING REQUEST
                $newInvoice->status = "BLOCKED";
            }
            $newInvoice->save();

            // ACTIVITY LOG 
            $activityLogRepo = new ActivityLogRepo;
            $activityLogRepo->create($newInvoice, ActivityLogRepo::getActivityMessage('NEW_REQUEST'));

            ////////////////////////////
            // DUPLICATE RECEIPT DATA
            $receipts = $invoice->receipts;
            foreach ($receipts as $key => $receipt) {

                $newReceipt = new Receipt();
                $newReceipt->receipt_number = $receipt->receipt_number;
                $newReceipt->receipt_date = $receipt->receipt_date;
                $newReceipt->location_id = $receipt->location_id;
                $newReceipt->currency_id = $receipt->currency_id;
                $newReceipt->exchange_rate = $receipt->exchange_rate;
                $newReceipt->establishment_id = $receipt->establishment_id;
                $newReceipt->establishment_name = $receipt->establishment_name;
                $newReceipt->vendor_id = $receipt->vendor_id;
                $newReceipt->vendor_name = $receipt->vendor_name;
                $newReceipt->vendor_tax_id = $receipt->vendor_tax_id;
                $newReceipt->vendor_branch_name = $receipt->vendor_branch_name;
                $newReceipt->jusification = $receipt->jusification;
                $newReceipt->project = $receipt->project;
                $newReceipt->job = $receipt->job;
                $newReceipt->recharge = $receipt->recharge;
                // SAVE RECEIPT DATA
                $newInvoice->receipts()->save($newReceipt);

                /////////////////////////////////
                // DUPLICATE RECEIPT LINES DATA
                $receiptLines = $receipt->lines;
                foreach($receiptLines as $receiptLine){

                    // DUPLICATE RECEIPT LINE DATA
                    $newReceiptLine = new ReceiptLine();
                    $newReceiptLine->receipt_id = $newReceipt->id;
                    $newReceiptLine->branch_code = $receiptLine->branch_code;
                    $newReceiptLine->department_code = $receiptLine->department_code;
                    $newReceiptLine->category_id = $receiptLine->category_id;
                    $newReceiptLine->sub_category_id = $receiptLine->sub_category_id;
                    // QUANTITY & AMOUNT
                    $newReceiptLine->quantity = $receiptLine->quantity;
                    $newReceiptLine->second_quantity = $receiptLine->second_quantity;
                    $newReceiptLine->transaction_quantity = $receiptLine->transaction_quantity;
                    $newReceiptLine->amount = $receiptLine->amount;
                    $newReceiptLine->amount_inc_vat = $receiptLine->amount_inc_vat;
                    $newReceiptLine->total_amount = $receiptLine->total_amount;
                    $newReceiptLine->total_amount_inc_vat = $receiptLine->total_amount_inc_vat;
                    $newReceiptLine->policy_id = $receiptLine->policy_id;
                    $newReceiptLine->rate_id = $receiptLine->rate_id;
                    $newReceiptLine->mileage_unit_id = $receiptLine->mileage_unit_id;
                    $newReceiptLine->mileage_distance = $receiptLine->mileage_distance;
                    $newReceiptLine->mileage_start = $receiptLine->mileage_start;
                    $newReceiptLine->mileage_end = $receiptLine->mileage_end;
                    $newReceiptLine->primary_amount = $receiptLine->primary_amount;
                    $newReceiptLine->primary_amount_inc_vat = $receiptLine->primary_amount_inc_vat;
                    // PRIMARY => CALCULATE (WITH EXCHANGE RATE) TO BASE CURRENCY
                    $newReceiptLine->total_primary_amount = $receiptLine->total_primary_amount;
                    $newReceiptLine->total_primary_amount_inc_vat = $receiptLine->total_primary_amount_inc_vat;
                    // VAT
                    $newReceiptLine->vat_id = $receiptLine->vat_id;
                    $newReceiptLine->vat_amount = $receiptLine->vat_amount;
                    $newReceiptLine->primary_vat_amount = $receiptLine->primary_vat_amount;

                    // GL ACCOUNT COMBINATION WILL SET WHEN SEND REQUEST
                    $newReceiptLine->code_combination_id = null;
                    $newReceiptLine->concatenated_segments = null;

                    $newReceiptLine->save();

                    // DUPLICATE RECEIPT LINE INFORMATIONS
                    $infos = SubCategoryInfo::where('sub_category_id',$newReceiptLine->sub_category_id)
                                ->active()
                                ->get();

                    if(count($infos)>0){

                        $subCategoryInfos = $receiptLine->infos->pluck('description','sub_category_info_id')->all();

                        foreach($infos as $info){
                            if(array_key_exists($info->id, $subCategoryInfos)){
                                $newReceiptLineInfo = new ReceiptLineInfo();
                                $newReceiptLineInfo->receipt_id = $newReceipt->id;
                                $newReceiptLineInfo->receipt_line_id = $newReceiptLine->id;
                                $newReceiptLineInfo->sub_category_id = $newReceiptLine->sub_category_id;
                                $newReceiptLineInfo->sub_category_info_id = $info->id;
                                $newReceiptLineInfo->description = $subCategoryInfos[$info->id];
                                $newReceiptLineInfo->save();
                            }
                        }
                    }

                }
            }

            // SUCCESS CREATE REIMBURSEMENT
            \DB::commit();
            if($request->ajax()){
                $result['status'] = 'SUCCESS';
                $result['invoiceId'] = $newInvoice->id;
                return $result;
            }else{
                return redirect()->route('invoices.show',['invoiceId'=>$newInvoice->id]);
            }

        } catch (\Exception $e) {
            // ERROR CREATE REIMBURSEMENT
            \DB::rollBack();
            if($request->ajax()){
                $result['status'] = 'ERROR';
                $result['err_msg'] = $e->getMessage();
                return $result;
            }else{
                // throw new \Exception($e->getMessage(), 1);
                \Log::error($e->getMessage());
                return abort('403');
            }
        }
    }

    public function show($invoiceId)
    {
        // GET REIM DATA
        $invoice = Invoice::find($invoiceId);
        if(!$invoice){ abort(404); }
        if(!$invoice->isRelatedUser()){ abort(404); }

        $invoiceTotalAmount = $invoice->total_receipt_amount + 0;
        // GET ACTIVITY LOG
        $activityLogs = $invoice->activityLogs;
        // GET PENDING TRASACTION
        $pendingRequestLists = RequestRepo::getPendingOverLimitDayRequest(\Auth::user()->id);

        // DATA FOR RECEIPT
        $receipts = $invoice->receipts;
        $receiptParentId = $invoiceId;
        $categoryLists = Category::active()
                                // ->whereOrgId($this->orgId)
                                ->pluck('name','id')
                                ->all();
        $currencyLists = Currency::pluck('currency_code','currency_code')->all();
        $parentCurrencyId = $invoice->currency_id;
        $receiptType = 'INVOICE';
        $locations = Location::accessibleOrg($this->orgId)->active()->get();
        $locationLists = Location::accessibleOrg($this->orgId)->active()->pluck('name','id')->all();
        // change in production when set location establishment
        $defaultEstablishmentId = $invoice->user->employee->establishment_id;
        $establishmentLists = Establishment::whereOrgId($this->orgId)->pluck('establishment_name','establishment_id')->all();
        $vendorLists = Vendor::notEmp()->whereOrgId($this->orgId)->pluck('vendor_name','vendor_id')->all();
        $projectLists = FNDListOfValues::select(\DB::raw("CONCAT(description,' (',flex_value,')') AS full_description"),'flex_value')->project()->orderBy('flex_value')->pluck('full_description','flex_value')->all();
        $rechargeLists = FNDListOfValues::select(\DB::raw("CONCAT(description,' (',flex_value,')') AS full_description"),'flex_value')->interCompany()->orderBy('flex_value')->pluck('full_description','flex_value')->all();

        $branchLists = FNDListOfValues::select(\DB::raw("CONCAT(description,' (',flex_value,')') AS full_description"),'flex_value')->branch($this->orgId)->orderBy('flex_value')->pluck('full_description','flex_value')->all();
        $departmentLists = FNDListOfValues::select(\DB::raw("CONCAT(description,' (',flex_value,')') AS full_description"),'flex_value')->department($this->orgId)->orderBy('flex_value')->pluck('full_description','flex_value')->all();

        $vendorLists = Vendor::notEmp()->whereOrgId($this->orgId)->pluck('vendor_name','vendor_id')->all();

        return view('invoices.show',
                        compact('invoice',
                                'invoiceTotalAmount',
                                'pendingRequestLists',
                                'activityLogs',
                                'receipts',
                                'receiptParentId',
                                'categoryLists',
                                'parentCurrencyId',
                                'currencyLists',
                                'receiptType',
                                'locations',
                                'locationLists',
                                'defaultEstablishmentId',
                                'establishmentLists',
                                'vendorLists',
                                'projectLists',
                                'rechargeLists',
                                'branchLists',
                                'departmentLists',
                                'vendorLists'
                                ));
    }

    public function setStatus(Request $request, $invoiceId)
    {
        $activity = $request->activity;
        $reason = '';
        if($request->reason){
            $reason = $request->reason;
        }
        // Get REIM Data
        $invoice = Invoice::find($invoiceId);

        // FILE ATTACHMENTS
        if($request->file('file')){
            $attachmentRepo = new AttachmentRepo;
            $attachmentRepo->create($invoice, $request->file('file'));
        }

        // Check Permission for Approve
        if(!self::hasPermissionSetStatus($invoice,$activity)){  abort(403);  }

        \DB::beginTransaction();
        try {
            // Set STATUS (approve,reject,sendback)
            switch ($activity) {

                // #### REIM REQUEST ####
                case "UNBLOCK":
                    $invoice->status = 'NEW_REQUEST';
                    $invoice->save();
                    break;
                case "SEND_REQUEST":
                    // SET OVER BUDGET & EXCEED POLICY
                    $resultOverBudget = RequestRepo::validateIsNotOverBudget($invoice);
                    $resultExceedPolicy = RequestRepo::validateIsNotExceedPolicy($invoice);
                    $invoice->over_budget = !$resultOverBudget['valid'];
                    $invoice->exceed_policy = !$resultExceedPolicy['valid'];
                    $invoice->save();
                    // CHECK FOUND APPROVER OR NOT ?
                    $nextApprover = ApprovalRepo::getNextApprover($invoice,'INVOICE');
                    if($nextApprover){ // IF FOUND NEXT APPROVER SET STATUS = 'APPROVER_DECISION'
                        $invoice->status = 'APPROVER_DECISION';
                        $invoice->next_approver_id = $nextApprover['user_id'];
                        $invoice->save();
                    }else{ // IF NOT FOUND NEXT APPROVER AUTO SET STATUS TO 
                        $invoice->status = 'APPROVED';
                        $invoice->next_approver_id = null;
                        $invoice->save();
                        // INTERFACE TO ORACLE AP INVOICE
                        $interfaceResult = InterfaceRepo::invoice($invoice,'INVOICE');
                    }
                    break;
                case "APPROVER_APPROVE":
                    // DO APPROVE PROCESS
                    $approvalRepo = new ApprovalRepo;
                    $approvalRepo->approve($invoice,'INVOICE','APPROVER');
                    // CHECK FOUND NEXT APPROVER OR NOT ?
                    $nextApprover = ApprovalRepo::getNextApprover($invoice,'INVOICE');
                    if($nextApprover){ // IF FOUND NEXT APPROVER SET STATUS = 'APPROVER_DECISION'
                        $invoice->next_approver_id = $nextApprover['user_id'];
                        $invoice->save();
                    }else{ // IF NOT FOUND NEXT APPROVER SET STATUS = 'APPROVED'
                        $invoice->status = 'APPROVED';
                        $invoice->next_approver_id = null;
                        $invoice->save();
                        // INTERFACE TO ORACLE AP INVOICE
                        $interfaceResult = InterfaceRepo::invoice($invoice,'INVOICE');
                    }
                    break;
                case "APPROVER_SENDBACK":
                    // SET STATUS BACK TO 'NEW_REQUEST'
                    $invoice->status = 'NEW_REQUEST';
                    $invoice->next_approver_id = null;
                    $invoice->over_budget = null;
                    $invoice->exceed_policy = null;
                    $invoice->save();
                    break;
                case "APPROVER_REJECT":
                    $invoice->status = 'APPROVER_REJECTED';
                    $invoice->save();
                    break;
                case "CANCEL_REQUEST":
                    // SET STATUS TO 'CANCELLED'
                    $invoice->status = 'CANCELLED';
                    $invoice->save();
                    break;
            }

            // ACTIVITY LOG 
            $activityLogRepo = new ActivityLogRepo;
            $activityLogRepo->create($invoice, ActivityLogRepo::getActivityMessage($activity,$invoice),$reason);

            // RESET APPROVAL (ACTIVITY SENDBACK ONLY)
            self::resetApproval($activity,$invoice);

            // SEND EMAIL
            self::sendEmailByActivity($activity,$invoice,$reason);

        } catch (\Exception $e) {
            \DB::rollBack();
            \Log::error($e->getMessage());

            return redirect()->route('invoices.show',['invoiceId'=>$invoiceId])->withErrors([$e->getMessage()]);;
        }
        \DB::commit();

        // REDIRECT AFTER SET STATUS
        return redirect()->route('invoices.show',['invoiceId'=>$invoiceId]);

    }

    public function addAttachment(Request $request, $invoiceId)
    {
        $invoice = Invoice::find($invoiceId);

        \DB::beginTransaction();
        try {

            if($request->file('file')){
                $attachmentRepo = new AttachmentRepo;
                $attachmentRepo->create($invoice, $request->file('file'));
            }

        } catch (\Exception $e) {
            \DB::rollBack();
            \Log::error($e->getMessage());
        }
        \DB::commit();

        return redirect()->route('invoices.show',['invoiceId'=>$invoice->id]);
        // return redirect()->back();
    }

    //////////////////////////
    //// GET TOTAL AMOUNT
    public function getTotalAmount(Request $request, $invoiceId)
    {
        $invoice = Invoice::find($invoiceId);
        if(!$invoice){ throw new Exception("Error Processing Request", 1); }
        $invoiceTotalAmount = $invoice->total_receipt_amount + 0;
        return number_format($invoiceTotalAmount,2);
    }

    //////////////////////////////////
    //// COMBINE GL CODE COMBINATION
    public function combineReceiptGLCodeCombination(Request $request, $invoiceId)
    {
        try {

            $invoice = Invoice::find($invoiceId);
            if(!$invoice){ 
                throw new \Exception("Not found invoice data.", 1);
            }
            $result = RequestRepo::combineReceiptGLCodeCombination($invoice);

        } catch (\Exception $e) {
            if($request->ajax()){
                return \Response::json(['status'                =>  'error',
                                        'err_msg'               =>  $e->getMessage(),
                                        'err_receipt_line_id'   =>  null], 200);
            }else{
                throw new \Exception($e->getMessage(), 1);
            }
        }

        if($request->ajax()){
            return \Response::json($result, 200);
        }else{
            return redirect()->back();
        }
    }

    //////////////////////////
    //// CHECK OVER BUDGET
    public function checkOverBudget(Request $request, $invoiceId)
    {
        $invoice = Invoice::find($invoiceId);
        if(!$invoice){ 
            throw new \Exception("Not found invoice data.", 1);
        }
        $result = RequestRepo::validateIsNotOverBudget($invoice);

        if($request->ajax()){
            return \Response::json($result, 200);
        }else{
            return $result;
        }
    }
    
    //////////////////////////
    //// CHECK EXCEED POLICY
    public function checkExceedPolicy(Request $request, $invoiceId)
    {
        $invoice = Invoice::find($invoiceId);
        if(!$invoice){ 
            throw new \Exception("Not found invoice data.", 1);
        }
        $result = RequestRepo::validateIsNotExceedPolicy($invoice);

        if($request->ajax()){
            return \Response::json($result, 200);
        }else{
            return $result;
        }
    }

    //////////////////////////////
    //// validateReceipt
    public function validateReceipt(Request $request, $invoiceId)
    {
        $invoice = Invoice::find($invoiceId);
        if(!$invoice){ 
            throw new \Exception("Not found invoice data.", 1);
        }
        $result = RequestRepo::validateReceipt($invoice);

        if($request->ajax()){
            return \Response::json($result, 200);
        }else{
            return $result;
        }
    }

    public function formSendRequestWithReason(Request $request, $invoiceId)
    {
        $title = ''; $text = '';
        $invoice = Invoice::find($invoiceId);
        if(!$invoice){ return ; }

        $resultOverBudget = RequestRepo::validateIsNotOverBudget($invoice);
        $resultExceedPolicy = RequestRepo::validateIsNotExceedPolicy($invoice);
        $notOverBudget = $resultOverBudget['valid'];
        $notExceedPolicy = $resultExceedPolicy['valid'];
        if(!$notOverBudget && !$notExceedPolicy){
            $title = 'Request Over Budget & Exceed Policy !';
            $text = 'please enter reason for request over budget and exceed policy, please contact HR for approval and <strong><u>send the original receipt and supporting document to Accounting Dept. (กรุณาส่งเอกสารให้แผนกบัญชีเพื่อดำเนินการ)</u></strong>.';
        }elseif(!$notOverBudget){
            $title = 'Request Over Budget !';
            $text = 'please enter reason for request over budget, please contact HR for approval and <strong><u>send the original receipt and supporting document to Accounting Dept. (กรุณาส่งเอกสารให้แผนกบัญชีเพื่อดำเนินการ)</u></strong>.';
        }elseif(!$notExceedPolicy){
            $title = 'Request Exceed Policy !';
            $text = 'please enter reason for request exceed policy please contact HR for approval and <strong><u>send the original receipt and supporting document to Accounting Dept. (กรุณาส่งเอกสารให้แผนกบัญชีเพื่อดำเนินการ)</u></strong>.';
        }

        return view('invoices.show._form_send_request_with_reason',
                        compact('invoice','title','text'));
        
    }

    private static function hasPermissionSetStatus($invoice,$activity)
    {
        $permit = false;
        switch ($activity) {
            case "UNBLOCK":
                if($invoice->status == 'BLOCKED' 
                    && \Auth::user()->isUnblocker()){
                    $permit = true;
                }
                break;
            case "SEND_REQUEST":
                if($invoice->status == 'NEW_REQUEST' 
                    && $invoice->isRequester()){
                    $permit = true;
                }
                break;
            case "APPROVER_APPROVE":
                if($invoice->status == 'APPROVER_DECISION'
                    && $invoice->isNextApprover()){
                    $permit = true;
                }
                break;
            case "APPROVER_SENDBACK":
                if($invoice->status == 'APPROVER_DECISION'
                    && $invoice->isNextApprover()){
                    $permit = true;
                }
                break;
            case "APPROVER_REJECT":
                if($invoice->status == 'APPROVER_DECISION'
                    && $invoice->isNextApprover()){
                    $permit = true;
                }
                break;
            case "CANCEL_REQUEST":
                if(($invoice->status == 'NEW_REQUEST' || $invoice->status == 'BLOCKED')
                    && $invoice->isRequester()){
                    $permit = true;
                }
                break;
        }
        return $permit;
    }

    private static function resetApproval($activity,$invoice)
    {
        switch ($activity) {
            case "APPROVER_SENDBACK":
                // RESET RECENT APPROVAL PROCESS
                $approvalRepo = new ApprovalRepo;
                $approvalRepo->reset($invoice,'INVOICE');
                break;
        }
    }

    private static function sendEmailByActivity($activity,$invoice,$reason)
    {
        $financeUsers = User::active()->isFinance()->get(); // FINANCE
        $composedFinanceUsers = MailRepo::composeReceivers($financeUsers);

        switch ($activity) {

            // #### REIM REQUEST ####
            case "UNBLOCK":

                $receivers = MailRepo::composeReceivers($invoice->user); // REQUESTER
                $ccReceivers = MailRepo::composeReceivers($financeUsers); // FINANCE

                MailRepo::unblock('INVOICE',$invoice,$receivers,$ccReceivers,$reason);

                break;

            case "SEND_REQUEST":

                $ccReceivers = MailRepo::composeReceivers(\Auth::user()); // REQUESTER

                if($invoice->approver){
                    // IF HAVE APPROVER
                    $receivers = MailRepo::composeReceivers($invoice->approver); // NEXT APPROVER
                    if(count($composedFinanceUsers) > 0){
                        foreach ($composedFinanceUsers as $composedFinanceUser) {
                            array_push($ccReceivers, $composedFinanceUser);
                        }
                    }
                    MailRepo::sendRequest('INVOICE',$invoice,$receivers,$ccReceivers,$reason);
                }else{
                    // IF NOT HAVE APPROVER
                    $receivers = MailRepo::composeReceivers($financeUsers);
                    MailRepo::sendRequest('INVOICE',$invoice,$receivers,$ccReceivers,$reason,'TO-FINANCE-DEPT');
                } 

                break;

            case "APPROVER_APPROVE":

                // IF FOUND NEXT APPROVER
                if($invoice->next_approver_id){ 
                    $receivers = MailRepo::composeReceivers($invoice->approver); // NEXT APPROVER
                    $ccReceivers = MailRepo::composeReceivers($invoice->user); // REQUESTER
                    $relatedApprovers = ApprovalRepo::getRelatedApprovers($invoice,'INVOICE','APPROVER'); // RELATED APPROVERS
                    $relatedApproverCCReceivers = MailRepo::composeReceivers($relatedApprovers);
                    if(count($relatedApproverCCReceivers) > 0){
                        foreach ($relatedApproverCCReceivers as $relatedApproverCCReceiver) {
                            array_push($ccReceivers, $relatedApproverCCReceiver);
                        }
                    }
                // IF APPROVER APPROVE COMPLETED
                }else{ 
                    $receivers = MailRepo::composeReceivers($invoice->user); // REQUESTER;
                    $relatedApprovers = ApprovalRepo::getRelatedApprovers($invoice,'INVOICE','APPROVER'); // RELATED APPROVERS
                    $ccReceivers = MailRepo::composeReceivers($relatedApprovers);
                }
                if(count($composedFinanceUsers) > 0){
                    foreach ($composedFinanceUsers as $composedFinanceUser) {
                        array_push($ccReceivers, $composedFinanceUser);
                    }
                }
                
                MailRepo::approverProcess('INVOICE',$invoice,'APPROVE',$receivers,$ccReceivers,$reason);

                break;

            case "APPROVER_SENDBACK":

                $receivers = MailRepo::composeReceivers($invoice->user); // REQUESTER;
                $relatedApprovers = ApprovalRepo::getRelatedApprovers($invoice,'INVOICE','APPROVER'); // RELATED APPROVERS
                $ccReceivers = MailRepo::composeReceivers($relatedApprovers);
                if(count($composedFinanceUsers) > 0){
                    foreach ($composedFinanceUsers as $composedFinanceUser) {
                        array_push($ccReceivers, $composedFinanceUser);
                    }
                }

                MailRepo::approverProcess('INVOICE',$invoice,'SENDBACK',$receivers,$ccReceivers,$reason);

                break;

            case "APPROVER_REJECT":

                $receivers = MailRepo::composeReceivers($invoice->user); // REQUESTER;
                $relatedApprovers = ApprovalRepo::getRelatedApprovers($invoice,'INVOICE','APPROVER'); // RELATED APPROVERS
                $ccReceivers = MailRepo::composeReceivers($relatedApprovers);
                if(count($composedFinanceUsers) > 0){
                    foreach ($composedFinanceUsers as $composedFinanceUser) {
                        array_push($ccReceivers, $composedFinanceUser);
                    }
                }

                MailRepo::approverProcess('INVOICE',$invoice,'REJECT',$receivers,$ccReceivers,$reason);

                break;

        }

    }

    public function export(Request $request)
    {
        $yearShowing = $request->year_showing;
        $search = [
            'document_no'=>$request->document_no,
            'status'=>$request->status
        ];
        $search_date = [
            'date_from'=>$request->date_from,
            'date_to'=>$request->date_to
        ];
        $invoices = Invoice::orderBy('created_at','desc')
                        ->whereOrgId($this->orgId)
                        ->byRelatedUser()
                        ->byYearShowing($yearShowing)
                        ->with('user')
                        ->search($search,$search_date)
                        ->get();
        // if(!$invoices){ abort(403); }

        // $establishmentLists = Establishment::whereOrgId($this->orgId)->pluck('establishment_name','establishment_id')->all();
        $vendorLists = Vendor::notEmp()->whereOrgId($this->orgId)->pluck('vendor_name','vendor_id')->all();
        // $projectLists = FNDListOfValues::select(\DB::raw("description || ' (' || flex_value || ')' AS full_description"),'flex_value')->project()->orderBy('flex_value')->pluck('full_description','flex_value')->all();
        // $rechargeLists = FNDListOfValues::select(\DB::raw("description || ' (' || flex_value || ')' AS full_description"),'flex_value')->interCompany()->orderBy('flex_value')->pluck('full_description','flex_value')->all();

        $branchLists = FNDListOfValues::select(\DB::raw("description || ' (' || flex_value || ')' AS full_description"),'flex_value')->branch($this->orgId)->orderBy('flex_value')->pluck('full_description','flex_value')->all();
        $departmentLists = FNDListOfValues::select(\DB::raw("description || ' (' || flex_value || ')' AS full_description"),'flex_value')->department($this->orgId)->orderBy('flex_value')->pluck('full_description','flex_value')->all();


        \Excel::create('IV_'.Carbon::now(), function($excel) use ($invoices,$vendorLists,$branchLists,$departmentLists) {

            $excel->sheet('Report', function($sheet) use ($invoices,$vendorLists,$branchLists,$departmentLists) {

                $sheet->loadView('invoices.export._template', compact('invoices','vendorLists','branchLists','departmentLists'));

            });

        })->export('xlsx');
    }
}
