<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Receipt;
use App\ReceiptLine;
use App\ReceiptLineInfo;
use App\CashAdvance;
use App\Reimbursement;
use App\Category;
use App\SubCategory;
use App\SubCategoryInfo;
use App\Policy;
use App\PolicyRate;
use App\MileageUnit;
use App\Currency;
use App\VAT;
// use App\WHT;
use App\Preference;
use App\FNDListOfValues;

use Carbon\Carbon;

class ReceiptLineController extends Controller
{
    protected $orgId;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->orgId = \Auth::user()->org_id;
            return $next($request);
        });
    }

    public function formCreate(Request $request, $receiptId)
    {
        // GET RECEIPT DATA
        $receipt = Receipt::find($receiptId);
        $receiptParentId = $receipt->receiptable_id;
        $receiptType = $receipt->receipt_type;
        $exchangeRate = $receipt->exchange_rate_for_cal;
        $parent = $receipt->parent;
        $parentCurrencyId = $parent->currency_id;

        // GET LIST DATA (CHANGE WHEN CONNECT ORACLE)
        $categoryLists = Category::active()
                                // ->whereOrgId($this->orgId)
                                ->pluck('name','id')
                                ->all();
        // $subCategoryLists = SubCategory::active()
        //                                 ->onDateActive()
        //                                 ->pluck('name','id')
        //                                 ->all();
        $subCategoryLists = [];

        return view('commons.receipts.lines._form_create',
                        compact('receipt',
                                'receiptType',
                                'receiptParentId',
                                'parent',
                                'categoryLists',
                                'subCategoryLists',
                                'parentCurrencyId',
                                'exchangeRate'));
    }

    public function getSubCategories(Request $request, $receiptId)
    {
        $categoryId = $request->category_id;
        if($categoryId){
            $subCategoryLists = SubCategory::where('category_id',$categoryId)
                                    ->active()
                                    ->accessibleOrg($this->orgId)
                                    ->onDateActive()
                                    ->pluck('name','id')
                                    ->all();
        }else{
            // $subCategoryLists = SubCategory::active()
            //                         ->accessibleOrg($this->orgId)
            //                         ->onDateActive()
            //                         ->pluck('name','id')
            //                         ->all();
            $subCategoryLists = [];
        }

        return view('commons.receipts.lines._ddl_sub_categories',compact('subCategoryLists'));
    }

    public function getInputFormInformations(Request $request, $receiptId, $subCategoryId)
    {
        // GET RECEIPT DATA
        $receipt = Receipt::find($receiptId);
        $parent = $receipt->parent;
        // GET SUB-CATEGORY DATA
        $subCategory = SubCategory::find($subCategoryId);

        // BRANCH & DEPARTMENT
        $branchLists = FNDListOfValues::select(\DB::raw("CONCAT(description,' (',flex_value,')') AS full_description"),'flex_value')->branch($this->orgId)->orderBy('flex_value')->pluck('full_description','flex_value')->all();
        $departmentLists = FNDListOfValues::select(\DB::raw("CONCAT(description,' (',flex_value,')') AS full_description"),'flex_value')->department($this->orgId)->orderBy('flex_value')->pluck('full_description','flex_value')->all();
        // DEFAULT BRANCH & DEPARTMENT
        $defaultBranchCode = $parent->user->employee->segment2;
        if($subCategory->branch_code){
            $defaultBranchCode = $subCategory->branch_code;
        }
        $defaultDepartmentCode = $parent->user->employee->segment3;
        if($subCategory->department_code){
            $defaultDepartmentCode = $subCategory->department_code;
        }

        // SUB-CATEGORY INFORMATION
        $informations = SubCategoryInfo::where('sub_category_id',$subCategoryId)
            ->active()
            ->get();

        return view('commons.receipts.lines._form_informations',
                        compact('branchLists',
                                'departmentLists',
                                'defaultBranchCode',
                                'defaultDepartmentCode',
                                'informations'));
    }

    public function getInputFormAmount(Request $request, $receiptId, $subCategoryId)
    {
        if(!$request->form_id){
            $formId = "#form-create-receipt-line";
        }else{
            $formId = $request->form_id;
        }
        // GET RECEIPT DATA
        $receipt = Receipt::find($receiptId);
        $receiptParentId = $receipt->receiptable_id;
        $receiptType = $receipt->receipt_type;
        $exchangeRate = $receipt->exchange_rate_for_cal;
        $parent = $receipt->parent;
        $parentCurrencyId = $parent->currency_id;

        // GET LIST DATA (CHANGE WHEN CONNECT ORACLE)
        $baseMileageUnit = Preference::getBaseMileageUnit($this->orgId);
        $mileageUnitLists = MileageUnit::active()->pluck('description','id')->all();
        $baseCurrencyId = Preference::getBaseCurrency($this->orgId);
        $VATs = VAT::apVat()->where('org_id',$this->orgId)->get();
        // $WHTs = WHT::where('org_id',$this->orgId)->get();
        $VATLists = VAT::apVat()->where('org_id',$this->orgId)->pluck('tax','tax_rate_code')->all();
        // $WHTLists = WHT::where('org_id',$this->orgId)->pluck('wht_name','pay_awt_group_id')->all();

        $subCategory = SubCategory::find($subCategoryId);
        //GET POLICY BY SUBCATEGORY
        $policyExpense = Policy::where('sub_category_id',$subCategoryId)
                                ->where('type','EXPENSE')
                                ->active()->first();
        if($policyExpense){
            $rateExpense = PolicyRate::getRateByCondition($policyExpense->id,
                                                        $parent->user->employee->position_po_level,
                                                        $receipt->location_id);
        }
        $policyMileage = Policy::where('sub_category_id',$subCategoryId)
                                ->where('type','MILEAGE')
                                ->active()->first();
        if($policyMileage){
            $rateMileage = PolicyRate::getRateByCondition($policyMileage->id,
                                                    $parent->user->employee->position_po_level,
                                                    $receipt->location_id);
        }

        // DEFAULT QUANTITY
        $defaultQuantity = 1;
        $defaultSecondQuantity = 1;

        // DEFAULT AMOUNT
        $defaultAmount = number_format(0,2);
        $defaultAmountIncVat = number_format(0,2);
        $defaultVatId = null;
        $calVat = false;
        $showInputVat = false;
        if($receipt->currency_id == $parentCurrencyId){
            if($subCategory->vat_id){
                $showInputVat = true;
                $defaultVatId = $subCategory->vat_id;
            }
        }

        // DEFAULT DISTANCE
        $defaultMileageDistance = 0;

        return view('commons.receipts.lines._form_amount',
                            compact('receipt',
                                    'receiptType',
                                    'parent',
                                    'baseMileageUnit',
                                    'mileageUnitLists',
                                    'baseCurrencyId',
                                    'parentCurrencyId',
                                    'exchangeRate',
                                    'VATLists',
                                    // 'WHTLists',
                                    'VATs',
                                    // 'WHTs',
                                    'subCategory',
                                    'policyExpense',
                                    'rateExpense',
                                    'policyMileage',
                                    'rateMileage',
                                    'defaultQuantity',
                                    'defaultSecondQuantity',
                                    'defaultAmount',
                                    'defaultAmountIncVat',
                                    'defaultMileageDistance',
                                    'defaultVatId',
                                    'formId',
                                    'calVat',
                                    'showInputVat'));
    }

    public function store(Request $request, $receiptId)
    {
        \DB::beginTransaction();
        try {
            $receipt = Receipt::find($receiptId);
            $parent = $receipt->parent;
            if(!$parent->isPendingReceipt()){
                throw new \Exception("Request enable to add receipt line now.", 1);
            }

            // PREPARE RECEIPT LINE DATA
            $receiptLine = new ReceiptLine();
            $receiptLine->receipt_id = $receiptId;
            $receiptLine->branch_code = $request->branch_code;
            $receiptLine->department_code = $request->department_code;
            if($request->category_id){
                $receiptLine->category_id = $request->category_id;
            }else{
                $subCategory = SubCategory::find($request->sub_category_id);
                $receiptLine->category_id = $subCategory->category->id;
            }
            $receiptLine->sub_category_id = $request->sub_category_id;
            // QUANTITY & AMOUNT
            $receiptLine->quantity = $request->quantity;
            $receiptLine->second_quantity = $request->second_quantity;
            $receiptLine->transaction_quantity = (int)$request->quantity * (int)$request->second_quantity;
            $receiptLine->amount = $request->amount;
            $receiptLine->amount_inc_vat = $request->amount_inc_vat;
            $receiptLine->total_amount = $receiptLine->amount;
            $receiptLine->total_amount_inc_vat = $receiptLine->amount_inc_vat;
            $exchangeRate = $receipt->exchange_rate_for_cal;
            ////////////////////////////////////
            // CASE USE POLICY (EXPENSE, MILEAGE)
            if($request->policy_id){
                $receiptLine->policy_id = $request->policy_id;
                $receiptLine->rate_id = $request->rate_id;
                $linePolicy = Policy::find($receiptLine->policy_id);
                // EXPENSE
                if($linePolicy->type == 'EXPENSE'){
                    $receiptLine->primary_amount = $request->primary_amount;
                    $receiptLine->primary_amount_inc_vat = $request->primary_amount_inc_vat;
                }
                // MILEAGE
                if($linePolicy->type == 'MILEAGE'){
                    // MILEAGE DETAIL
                    $receiptLine->mileage_unit_id = Preference::getBaseMileageUnit($this->orgId);
                    $receiptLine->mileage_distance = $request->mileage_distance;
                    $receiptLine->mileage_start = $request->mileage_start ? $request->mileage_start : null;
                    $receiptLine->mileage_end = $request->mileage_end ? $request->mileage_end : null;
                    // MILEAGE ALWAYS USE BASE CURRENCY (NOT CAL WITH EXCHANGE RATE)
                    $receiptLine->primary_amount = $receiptLine->amount;
                    $receiptLine->primary_amount_inc_vat = $receiptLine->amount_inc_vat;
                }
            }else{
            /////////////////////////////////
            // CASE NOT USE POLICY (ACTUAL)
                $receiptLine->primary_amount = $request->primary_amount;
                $receiptLine->primary_amount_inc_vat = $request->primary_amount_inc_vat;
            }
            // PRIMARY => CALCULATE (WITH EXCHANGE RATE) TO BASE CURRENCY
            $receiptLine->total_primary_amount = $receiptLine->primary_amount;
            $receiptLine->total_primary_amount_inc_vat = $receiptLine->primary_amount_inc_vat;

            // WHT & VAT
            // if($request->wht_id){
            //     $receiptLine->wht_id = $request->wht_id;
            // }
            if($request->vat_id){
                $receiptLine->vat_id = $request->vat_id;
                // $receiptLine->vat_amount = $request->primary_vat_amount / $exchangeRate;
                $receiptLine->vat_amount = (float)$receiptLine->amount_inc_vat - (float)$receiptLine->amount;
                $receiptLine->primary_vat_amount = $request->primary_vat_amount;
            }

            // GL ACCOUNT COMBINATION WILL SET WHEN SEND REQUEST
            $receiptLine->code_combination_id = null;
            $receiptLine->concatenated_segments = null;

            $receiptLine->save();

            // INSERT RECEIPT LINE INFORMATIONS
            $infos = SubCategoryInfo::where('sub_category_id',$receiptLine->sub_category_id)
                        ->active()
                        ->get();
            if(count($infos)>0){
                foreach($infos as $info){
                    if($request->sub_category_infos[$info->id]){
                        $receiptLineInfo = new ReceiptLineInfo();
                        $receiptLineInfo->receipt_id = $receiptId;
                        $receiptLineInfo->receipt_line_id = $receiptLine->id;
                        $receiptLineInfo->sub_category_id = $receiptLine->sub_category_id;
                        $receiptLineInfo->sub_category_info_id = $info->id;
                        if($info->form_type == 'date'){
                            $receiptLineInfo->description = \DateTime::createFromFormat(trans('date.format'), $request->sub_category_infos[$info->id])->format('Y-m-d');
                        }else{
                            $receiptLineInfo->description = $request->sub_category_infos[$info->id];
                        }
                        $receiptLineInfo->save();
                    }
                }
            }

        \DB::commit();

        } catch (\Exception $e) {
            // ERROR CREATE RECEIPT LINE
            \DB::rollBack();
            \Log::error($e->getMessage());
            if($request->ajax()){
                return \Response::json(["status"=>"error","err_msg"=>$e->getMessage()], 200);
            }else{
                return abort('403');
            }
        }

        if($request->ajax()){
            return \Response::json(["status"=>"success","err_msg"=>""], 200);
        }else{
            return redirect()->route('commons.receipts.show',['receiptId'=>$receiptId]);
        }
    }

    public function formEdit(Request $request, $receiptId, $receiptLineId)
    {
        $receiptLine = ReceiptLine::find($receiptLineId);
        if(!$receiptLine){ abort(404); }

        // GET RECEIPT DATA
        $receipt = Receipt::find($receiptId);
        $receiptParentId = $receipt->receiptable_id;
        $receiptType = $receipt->receipt_type;
        $exchangeRate = $receipt->exchange_rate_for_cal;
        $parent = $receipt->parent;
        $parentCurrencyId = $parent->currency_id;

        // GET LIST DATA (CHANGE WHEN CONNECT ORACLE)
        $categoryLists = Category::active()
                                // ->whereOrgId($this->orgId)
                                ->pluck('name','id')
                                ->all();
        $subCategoryLists = SubCategory::where('category_id',$receiptLine->category_id)
                                        ->accessibleOrg($this->orgId)
                                        ->active()
                                        ->onDateActive()
                                        ->pluck('name','id')
                                        ->all();

        return view('commons.receipts.lines._form_edit',
                        compact('receipt',
                                'receiptLine',
                                'receiptType',
                                'receiptParentId',
                                'parent',
                                'parentCurrencyId',
                                'exchangeRate',
                                'categoryLists',
                                'subCategoryLists'));
    }

    public function getInputFormEditInformations(Request $request, $receiptId, $receiptLineId)
    {
        $receiptLine = ReceiptLine::find($receiptLineId);
        if(!$receiptLine){ abort(404); }

        // GET SUB-CATEGORY DATA
        $subCategory = SubCategory::find($receiptLine->subCategory->id);

        // BRANCH & DEPARTMENT
        $branchLists = FNDListOfValues::select(\DB::raw("CONCAT(description,' (',flex_value,')') AS full_description"),'flex_value')->branch($this->orgId)->orderBy('flex_value')->pluck('full_description','flex_value')->all();
        $departmentLists = FNDListOfValues::select(\DB::raw("CONCAT(description,' (',flex_value,')') AS full_description"),'flex_value')->department($this->orgId)->orderBy('flex_value')->pluck('full_description','flex_value')->all();
        // DEFAULT BRANCH & DEPARTMENT
        $defaultBranchCode = $receiptLine->branch_code;
        $defaultDepartmentCode = $receiptLine->department_code;

        // SUB-CATEGORY INFORMATION
        $informations = SubCategoryInfo::where('sub_category_id',$subCategory->id)
            ->active()
            ->get();

        $receiptInfos = ReceiptLineInfo::where('receipt_line_id',$receiptLineId)->get();
        $receiptInfoLists = ReceiptLineInfo::where('receipt_line_id',$receiptLineId)
                                    ->pluck('description','sub_category_info_id')
                                    ->all();

        return view('commons.receipts.lines._form_informations',
                        compact('branchLists',
                                'departmentLists',
                                'defaultBranchCode',
                                'defaultDepartmentCode',
                                'informations',
                                'receiptInfos',
                                'receiptInfoLists'));
    }

    public function getInputFormEditAmount(Request $request, $receiptId, $receiptLineId)
    {
        $formId = "#form-edit-receipt-line";

        $receiptLine = ReceiptLine::find($receiptLineId);
        if(!$receiptLine){ abort(404); }

        // GET RECEIPT DATA
        $receipt = Receipt::find($receiptId);
        $receiptParentId = $receipt->receiptable_id;
        $receiptType = $receipt->receipt_type;
        $exchangeRate = $receipt->exchange_rate_for_cal;
        $parent = $receipt->parent;
        $parentCurrencyId = $parent->currency_id;

        // GET LIST DATA (CHANGE WHEN CONNECT ORACLE)
        $baseMileageUnit = Preference::getBaseMileageUnit($this->orgId);
        $mileageUnitLists = MileageUnit::active()->pluck('description','id')->all();
        $baseCurrencyId = Preference::getBaseCurrency($this->orgId);
        $VATs = VAT::apVat()->where('org_id',$this->orgId)->get();
        // $WHTs = WHT::where('org_id',$this->orgId)->get();
        $VATLists = VAT::apVat()->where('org_id',$this->orgId)->pluck('tax','tax_rate_code')->all();
        // $WHTLists = WHT::where('org_id',$this->orgId)->pluck('wht_name','pay_awt_group_id')->all();

        $subCategory = SubCategory::find($receiptLine->sub_category_id);
        //GET POLICY BY SUBCATEGORY
        if($receiptLine->policy_id){
            $policyExpense = Policy::where('id',$receiptLine->policy_id)
                                ->where('type','EXPENSE')
                                ->active()->first();
            if($policyExpense){
                $rateExpense = PolicyRate::find($receiptLine->rate_id);
            }
            $policyMileage = Policy::where('id',$receiptLine->policy_id)
                                    ->where('type','MILEAGE')
                                    ->active()->first();
            if($policyMileage){
                $rateMileage = PolicyRate::find($receiptLine->rate_id);
            }
        }else{
            $policyExpense = null;
            $policyMileage = null;
        }

        // DEFAULT QUANTITY
        $defaultQuantity = 1;
        if($receiptLine->quantity){
            $defaultQuantity = $receiptLine->quantity;
        }
        $defaultSecondQuantity = 1;
        if($receiptLine->second_quantity){
            $defaultSecondQuantity = $receiptLine->second_quantity;
        }

        // DEFAULT QUANTITY
        $defaultAmount = number_format(0,2);
        if($receiptLine->amount){
            $defaultAmount = $receiptLine->amount;
        }
        $defaultAmountIncVat = number_format(0,2);
        if($receiptLine->amount_inc_vat){
            $defaultAmountIncVat = $receiptLine->amount_inc_vat;
        }
        $defaultVatId = null;
        $calVat = false;
        $showInputVat = false;
        if($receipt->currency_id == $parentCurrencyId){
            if($subCategory->vat_id){
                $showInputVat = true;
                $defaultVatId = $subCategory->vat_id;
            }
            if($receiptLine->vat_id){
                $calVat = true;
            }
        }

        // DEFAULT DISTANCE
        $defaultMileageDistance = $receiptLine->mileage_distance;

        return view('commons.receipts.lines._form_amount',
                            compact('receipt',
                                    'receiptLine',
                                    'receiptType',
                                    'parent',
                                    'baseMileageUnit',
                                    'mileageUnitLists',
                                    'baseCurrencyId',
                                    'parentCurrencyId',
                                    'exchangeRate',
                                    'VATLists',
                                    // 'WHTLists',
                                    'VATs',
                                    // 'WHTs',
                                    'subCategory',
                                    'policyExpense',
                                    'rateExpense',
                                    'policyMileage',
                                    'rateMileage',
                                    'defaultQuantity',
                                    'defaultSecondQuantity',
                                    'defaultAmount',
                                    'defaultAmountIncVat',
                                    'defaultMileageDistance',
                                    'defaultVatId',
                                    'formId',
                                    'calVat',
                                    'showInputVat'));
    }

    public function update(Request $request, $receiptId, $receiptLineId)
    {
        $receiptLine = ReceiptLine::find($receiptLineId);
        if(!$receiptLine){ abort(404); }

        \DB::beginTransaction();
        try {
            $receipt = Receipt::find($receiptId);
            $parent = $receipt->parent;
            if(!$parent->isPendingReceipt()){
                throw new \Exception("Request enable to add receipt line now.", 1);
            }
            if($request->category_id){
                $receiptLine->category_id = $request->category_id;
            }else{
                $subCategory = SubCategory::find($request->sub_category_id);
                $receiptLine->category_id = $subCategory->category->id;
            }
            $receiptLine->sub_category_id = $request->sub_category_id;
            // EDIT RECEIPT LINE DATA
            $receiptLine->branch_code = $request->branch_code;
            $receiptLine->department_code = $request->department_code;
            // QUANTITY & AMOUNT
            $receiptLine->quantity = $request->quantity;
            $receiptLine->second_quantity = $request->second_quantity;
            $receiptLine->transaction_quantity = (int)$request->quantity * (int)$request->second_quantity;
            $receiptLine->amount = $request->amount;
            $receiptLine->amount_inc_vat = $request->amount_inc_vat;
            $receiptLine->total_amount = $receiptLine->amount;
            $receiptLine->total_amount_inc_vat = $receiptLine->amount_inc_vat;
            $exchangeRate = $receipt->exchange_rate_for_cal;
            ////////////////////////////////////
            // CASE USE POLICY (EXPENSE, MILEAGE)
            if($request->policy_id){
                $receiptLine->policy_id = $request->policy_id;
                $receiptLine->rate_id = $request->rate_id;
                $linePolicy = Policy::find($receiptLine->policy_id);
                // EXPENSE
                if($linePolicy->type == 'EXPENSE'){
                    $receiptLine->primary_amount = $request->primary_amount;
                    $receiptLine->primary_amount_inc_vat = $request->primary_amount_inc_vat;
                }
                // MILEAGE
                if($linePolicy->type == 'MILEAGE'){
                    // MILEAGE DETAIL
                    $receiptLine->mileage_unit_id = Preference::getBaseMileageUnit($this->orgId);
                    $receiptLine->mileage_distance = $request->mileage_distance;
                    $receiptLine->mileage_start = $request->mileage_start ? $request->mileage_start : null;
                    $receiptLine->mileage_end = $request->mileage_end ? $request->mileage_end : null;
                    // MILEAGE ALWAYS USE BASE CURRENCY (NOT CAL WITH EXCHANGE RATE)
                    $receiptLine->primary_amount = $receiptLine->amount;
                    $receiptLine->primary_amount_inc_vat = $receiptLine->amount_inc_vat;
                }
            }else{
            /////////////////////////////////
            // CASE NOT USE POLICY (ACTUAL)
                $receiptLine->primary_amount = $request->primary_amount;
                $receiptLine->primary_amount_inc_vat = $request->primary_amount_inc_vat;
            }
            // PRIMARY => CALCULATE (WITH EXCHANGE RATE) TO BASE CURRENCY
            $receiptLine->total_primary_amount = $receiptLine->primary_amount;
            $receiptLine->total_primary_amount_inc_vat = $receiptLine->primary_amount_inc_vat;

            // WHT & VAT
            // if($request->wht_id){
            //     $receiptLine->wht_id = $request->wht_id;
            // }
            if($request->vat_id){
                $receiptLine->vat_id = $request->vat_id;
                // $receiptLine->vat_amount = $request->primary_vat_amount / $exchangeRate;
                $receiptLine->vat_amount = (float)$receiptLine->amount_inc_vat - (float)$receiptLine->amount;
                $receiptLine->primary_vat_amount = $request->primary_vat_amount;
            }else{
                $receiptLine->vat_id = null;
                $receiptLine->vat_amount = 0;
                $receiptLine->primary_vat_amount = 0;
            }

            // GL ACCOUNT COMBINATION WILL SET WHEN SEND REQUEST
            $receiptLine->code_combination_id = null;
            $receiptLine->concatenated_segments = null;

            $receiptLine->save();

            // REMOVE OLD RECEIPT LINE INFORMATIONS
            foreach ($receiptLine->infos as $key => $oldInfo) {
                $oldInfo->delete();
            }

            // INSERT RECEIPT LINE INFORMATIONS INSTEAD
            $infos = SubCategoryInfo::where('sub_category_id',$receiptLine->sub_category_id)
                        ->active()
                        ->get();

            if(count($infos)>0){
                foreach($infos as $info){
                    if($request->sub_category_infos[$info->id]){
                        $receiptLineInfo = new ReceiptLineInfo();
                        $receiptLineInfo->receipt_id = $receiptId;
                        $receiptLineInfo->receipt_line_id = $receiptLine->id;
                        $receiptLineInfo->sub_category_id = $receiptLine->sub_category_id;
                        $receiptLineInfo->sub_category_info_id = $info->id;
                        if($info->form_type == 'date'){
                            $receiptLineInfo->description = \DateTime::createFromFormat(trans('date.format'), $request->sub_category_infos[$info->id])->format('Y-m-d');
                        }else{
                            $receiptLineInfo->description = $request->sub_category_infos[$info->id];
                        }
                        $receiptLineInfo->save();
                    }
                }
            }

            // // EDIT RECEIPT LINE INFORMATIONS
            // $infos = SubCategoryInfo::where('sub_category_id',$receiptLine->sub_category_id)
            //             ->active()
            //             ->get();
            // if(count($infos)>0){
            //     foreach($infos as $info){
            //         if($request->sub_category_infos[$info->id]){
            //             $receiptLineInfo = ReceiptLineInfo::where('receipt_id',$receiptId)
            //                                 ->where('receipt_line_id',$receiptLineId)
            //                                 ->where('sub_category_id',$receiptLine->sub_category_id)
            //                                 ->where('sub_category_info_id',$info->id)
            //                                 ->first();
            //             if($info->form_type == 'date'){
            //                 $receiptLineInfo->description = \DateTime::createFromFormat(trans('date.format'), $request->sub_category_infos[$info->id])->format('Y-m-d');
            //             }else{
            //                 $receiptLineInfo->description = $request->sub_category_infos[$info->id];
            //             }
            //             $receiptLineInfo->save();
            //         }
            //     }
            // }

        \DB::commit();

        } catch (\Exception $e) {
            // ERROR CREATE RECEIPT LINE
            \DB::rollBack();
            \Log::error($e->getMessage());
            if($request->ajax()){
                return \Response::json(["status"=>"error","err_msg"=>$e->getMessage()], 200);
            }else{
                return abort('403');
            }
        }

        if($request->ajax()){
            return \Response::json(["status"=>"success","err_msg"=>""], 200);
        }else{
            return redirect()->route('commons.receipts.show',['receiptId'=>$receiptId]);
        }

    }

    public function getShowInfos(Request $request, $receiptId, $receiptLineId)
    {
        $receiptLine = ReceiptLine::find($receiptLineId);
        if(!$receiptLine){ abort(404); }

        // GET RECEIPT DATA
        $receipt = Receipt::find($receiptId);
        $receiptParentId = $receipt->receiptable_id;
        $receiptType = $receipt->receipt_type;
        $exchangeRate = $receipt->exchange_rate_for_cal;
        $parent = $receipt->parent;
        $parentCurrencyId = $parent->currency_id;

        // GET LIST DATA (CHANGE WHEN CONNECT ORACLE)
        $baseMileageUnit = Preference::getBaseMileageUnit($this->orgId);
        $mileageUnitLists = MileageUnit::active()->pluck('description','id')->all();
        $baseCurrencyId = Preference::getBaseCurrency($this->orgId);

        $branchLists = FNDListOfValues::select(\DB::raw("CONCAT(description,' (',flex_value,')') AS full_description"),'flex_value')->branch($this->orgId)->orderBy('flex_value')->pluck('full_description','flex_value')->all();
        $departmentLists = FNDListOfValues::select(\DB::raw("CONCAT(description,' (',flex_value,')') AS full_description"),'flex_value')->department($this->orgId)->orderBy('flex_value')->pluck('full_description','flex_value')->all();

        return view('commons.receipts.lines._show_informations',compact('receiptLine',
                                                'receipt',
                                                'baseMileageUnit',
                                                'mileageUnitLists',
                                                'baseCurrencyId',
                                                'parentCurrencyId',
                                                'exchangeRate',
                                                'branchLists',
                                                'departmentLists'
                                                ));

    }

    public function duplicate(Request $request, $receiptId, $receiptLineId)
    {
        $receiptLine = ReceiptLine::find($receiptLineId);

        \DB::beginTransaction();
        try {

            $receipt = Receipt::find($receiptId);
            $parent = $receipt->parent;
            if(!$parent->isPendingReceipt()){
                throw new \Exception("Request enable to add receipt line now.", 1);
            }

            // DUPLICATE RECEIPT LINE DATA
            $newReceiptLine = new ReceiptLine();
            $newReceiptLine->receipt_id = $receiptId;
            $newReceiptLine->branch_code = $receiptLine->branch_code;
            $newReceiptLine->department_code = $receiptLine->department_code;
            $newReceiptLine->category_id = $receiptLine->category_id;
            $newReceiptLine->sub_category_id = $receiptLine->sub_category_id;
            // QUANTITY & AMOUNT
            $newReceiptLine->quantity = $receiptLine->quantity;
            $newReceiptLine->second_quantity = $receiptLine->second_quantity;
            $newReceiptLine->transaction_quantity = $receiptLine->transaction_quantity;
            $newReceiptLine->amount = $receiptLine->amount;
            $newReceiptLine->amount_inc_vat = $receiptLine->amount_inc_vat;
            $newReceiptLine->total_amount = $receiptLine->total_amount;
            $newReceiptLine->total_amount_inc_vat = $receiptLine->total_amount_inc_vat;
            $newReceiptLine->policy_id = $receiptLine->policy_id;
            $newReceiptLine->rate_id = $receiptLine->rate_id;
            $newReceiptLine->mileage_unit_id = $receiptLine->mileage_unit_id;
            $newReceiptLine->mileage_distance = $receiptLine->mileage_distance;
            $newReceiptLine->mileage_start = $receiptLine->mileage_start;
            $newReceiptLine->mileage_end = $receiptLine->mileage_end;
            $newReceiptLine->primary_amount = $receiptLine->primary_amount;
            $newReceiptLine->primary_amount_inc_vat = $receiptLine->primary_amount_inc_vat;
            // PRIMARY => CALCULATE (WITH EXCHANGE RATE) TO BASE CURRENCY
            $newReceiptLine->total_primary_amount = $receiptLine->total_primary_amount;
            $newReceiptLine->total_primary_amount_inc_vat = $receiptLine->total_primary_amount_inc_vat;
            // VAT
            $newReceiptLine->vat_id = $receiptLine->vat_id;
            $newReceiptLine->vat_amount = $receiptLine->vat_amount;
            $newReceiptLine->primary_vat_amount = $receiptLine->primary_vat_amount;

            // GL ACCOUNT COMBINATION WILL SET WHEN SEND REQUEST
            $newReceiptLine->code_combination_id = null;
            $newReceiptLine->concatenated_segments = null;

            $newReceiptLine->save();

            // DUPLICATE RECEIPT LINE INFORMATIONS
            $infos = SubCategoryInfo::where('sub_category_id',$newReceiptLine->sub_category_id)
                        ->active()
                        ->get();

            if(count($infos)>0){

                $subCategoryInfos = $receiptLine->infos->pluck('description','sub_category_info_id')->all();

                foreach($infos as $info){
                    if(array_key_exists($info->id, $subCategoryInfos)){
                        $newReceiptLineInfo = new ReceiptLineInfo();
                        $newReceiptLineInfo->receipt_id = $receiptId;
                        $newReceiptLineInfo->receipt_line_id = $newReceiptLine->id;
                        $newReceiptLineInfo->sub_category_id = $newReceiptLine->sub_category_id;
                        $newReceiptLineInfo->sub_category_info_id = $info->id;
                        $newReceiptLineInfo->description = $subCategoryInfos[$info->id];
                        $newReceiptLineInfo->save();
                    }
                }
            }

        \DB::commit();

        } catch (\Exception $e) {
            // ERROR CREATE RECEIPT LINE
            \DB::rollBack();
            \Log::error($e->getMessage());
            if($request->ajax()){
                return \Response::json("error", 200);
            }else{
                return abort('403');
            }
        }

        if($request->ajax()){
            return \Response::json("success", 200);
        }else{
            return redirect()->route('commons.receipts.show',['receiptId'=>$receiptId]);
        }

    }

    public function remove(Request $request, $receiptId, $receiptLineId)
    {
        $receiptLine = ReceiptLine::find($receiptLineId);
        if($receiptLine){
            $receiptLine->delete();
        }
        if($request->ajax()){
            return \Response::json("success", 200);
        }else{
            return redirect()->back()->withInput();
        }
    }

    // private function calWHTAmount($amount, $whtId)
    // {
    //     $wht = WHT::where('org_id',$this->orgId)->where('pay_awt_group_id',$whtId)->first();
    //     $whtRate = $wht->tax_rate;
    //     return (float)$amount * (float)$whtRate / 100;
    // }

    private function calVATAmount($amount, $vatId)
    {
        $vat = VAT::where('org_id',$this->orgId)->where('tax_rate_code',$vatId)->first();
        $vatRate = $vat->percentage_rate;
        return (float)$amount * (float)$vatRate / 100;
    }

}
