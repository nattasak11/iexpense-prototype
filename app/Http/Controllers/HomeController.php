<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Reimbursement;
use App\CashAdvance;
use App\Invoice;
use App\Preference;


class HomeController extends Controller
{
    protected $orgId;
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->orgId = \Auth::user()->org_id;
            return $next($request);
        });
    }

    public function index(Request $request)
    {
        // KEYWORD FOR SEARCH
        $keyword = [
            'REIMBURSEMENT' => $request->keyword_reim,
            'CASH-ADVANCE' => $request->keyword_ca,
            'INVOICE' => $request->keyword_invoice,
        ];

        // YEAR SHOWING
        $yearShowing = date('Y');
        if($request->year_showing)
        $yearShowing = $request->year_showing;

        // DEFAULT TAB
        $defaultTab = 'REIMBURSEMENT'; // REIMBURSEMENT,CASH-ADVANCE,INVOICE
        if($request->default_tab)
        $defaultTab = $request->default_tab;

        $reims = Reimbursement::orderBy('created_at','desc')
                        ->whereOrgId($this->orgId)
                        ->byRelatedUser()
                        ->byKeyword($keyword['REIMBURSEMENT'])
                        ->byYearShowing($yearShowing)
                        ->with('user')
                        ->get();

        $cashAdvances = CashAdvance::orderBy('created_at','desc')
                        ->whereOrgId($this->orgId)
                        ->byRelatedUser()
                        ->byKeyword($keyword['CASH-ADVANCE'])
                        ->byYearShowing($yearShowing)
                        ->with('user')
                        ->get();

        $invoices = Invoice::orderBy('created_at','desc')
                        ->whereOrgId($this->orgId)
                        ->byRelatedUser()
                        ->byKeyword($keyword['INVOICE'])
                        ->byYearShowing($yearShowing)
                        ->with('user')
                        ->get();

        return view('home/index',
                compact('reims',
                        'cashAdvances',
                        'invoices',
                        'yearShowing',
                        'keyword',
                        'defaultTab'));
    }

    public function minor()
    {
        return view('home/minor');
    }

    public function icon(Request $request)
    {
        $iconClass = $request->icon;
        if(!$iconClass){ return ; }
        return view('layouts._icon',compact('iconClass'));
    }

    public function overBudgetErrMsgByAccount(Request $request)
    {
        $arrErrLines = $request->arr_err_lines;
        $baseCurrencyId = Preference::getBaseCurrency();
        return view('commons._over_budget_err_msg_by_account',compact('arrErrLines','baseCurrencyId'));
    }
}
