<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Repositories\AttachmentRepo;

use App\Receipt;
use App\ReceiptLine;
use App\ReceiptLineInfo;
use App\CashAdvance;
use App\Reimbursement;
use App\Invoice;
use App\Category;
use App\SubCategory;
use App\SubCategoryInfo;
use App\Currency;
use App\Location;
use App\Establishment;
use App\Vendor;
use App\FNDListOfValues;

use Carbon\Carbon;

class ReceiptController extends Controller
{

    protected $orgId;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->orgId = \Auth::user()->org_id;
            return $next($request);
        });
    }

    public function show($receiptId)
    {
        // GET RECEIPT DATA
        $receipt = Receipt::find($receiptId);
        $receipt->receipt_date = dateFormatDisplay($receipt->receipt_date);
        $receiptParentId = $receipt->receiptable_id;
        $receiptType = $receipt->receipt_type;
        $parent = $receipt->parent;
        $receiptLines = $receipt->lines;

        // GET LIST DATA (CHANGE WHEN CONNECT ORACLE)
        $categoryLists = Category::active()
                                ->whereOrgId($this->orgId)
                                ->pluck('name','id')
                                ->all();
        $currencyLists = Currency::pluck('currency_code','currency_code')->all();
        $parentCurrencyId = $parent->currency_id;

        $establishmentLists = Establishment::whereOrgId($this->orgId)->pluck('establishment_name','establishment_id')->all();
        $vendorLists = Vendor::notEmp()->whereOrgId($this->orgId)->pluck('vendor_name','vendor_id')->all();
        $projectLists = FNDListOfValues::select(\DB::raw("CONCAT(description,' (',flex_value,')') AS full_description"),'flex_value')->project()->orderBy('flex_value')->pluck('full_description','flex_value')->all();
        $rechargeLists = FNDListOfValues::select(\DB::raw("CONCAT(description,' (',flex_value,')') AS full_description"),'flex_value')->interCompany()->orderBy('flex_value')->pluck('full_description','flex_value')->all();

        $branchLists = FNDListOfValues::select(\DB::raw("CONCAT(description,' (',flex_value,')') AS full_description"),'flex_value')->branch($this->orgId)->orderBy('flex_value')->pluck('full_description','flex_value')->all();
        $departmentLists = FNDListOfValues::select(\DB::raw("CONCAT(description,' (',flex_value,')') AS full_description"),'flex_value')->department($this->orgId)->orderBy('flex_value')->pluck('full_description','flex_value')->all();

        return view('commons.receipts.show',
                        compact('receipt',
                                'parent',
                                'receiptParentId',
                                'receiptType',
                                'categoryLists',
                                'currencyLists',
                                'parentCurrencyId',
                                'vendorLists',
                                'projectLists',
                                'rechargeLists',
                                'receiptLines',
                                'branchLists',
                                'departmentLists',
                                'establishmentLists'));
    }

    public function formCreate(Request $request)
    {
        // GET RECEIPT DATA
        $receiptParentId = $request->receipt_parent_id;
        $receiptType = $request->receipt_type;
        if($receiptType == 'CLEARING'){
            $parent = CashAdvance::find($receiptParentId);
        }elseif($receiptType == 'REIMBURSEMENT'){
            $parent = Reimbursement::find($receiptParentId);
        }elseif($receiptType == 'INVOICE'){
            $parent = Invoice::find($receiptParentId);
        }
        $parentCurrencyId = $parent->currency_id;

        // GET LIST DATA (CHANGE WHEN CONNECT ORACLE)
        $currencyLists = Currency::pluck('currency_code','currency_code')->all();
        $locations = Location::whereOrgId($this->orgId)->active()->get();
        $locationLists = Location::whereOrgId($this->orgId)->active()->pluck('name','id')->all();
        // change in production when set location establishment
        $defaultEstablishmentId = $parent->user->employee->establishment_id;
        $establishmentLists = Establishment::whereOrgId($this->orgId)->pluck('establishment_name','establishment_id')->all();
        $vendorLists = Vendor::notEmp()->whereOrgId($this->orgId)->pluck('vendor_name','vendor_id')->all();
        $projectLists = FNDListOfValues::select(\DB::raw("CONCAT(description,' (',flex_value,')') AS full_description"),'flex_value')->project()->orderBy('flex_value')->pluck('full_description','flex_value')->all();
        $rechargeLists = FNDListOfValues::select(\DB::raw("CONCAT(description,' (',flex_value,')') AS full_description"),'flex_value')->interCompany()->orderBy('flex_value')->pluck('full_description','flex_value')->all();

        return view('commons.receipts._form_create',
                        compact('receiptParentId',
                                'receiptType',
                                // 'baseCurrencyId',
                                'parentCurrencyId',
                                'currencyLists',
                                'locations',
                                'locationLists',
                                'establishmentLists',
                                'defaultEstablishmentId',
                                'vendorLists',
                                'projectLists',
                                'rechargeLists'));
    }

    public function store(Request $request)
    {
        \DB::beginTransaction();
        try {

            // PREPARE PARENT DATA
            $receiptParentId = $request->receipt_parent_id;
            $receiptType = $request->receipt_type;
            if($receiptType == 'CLEARING'){
                $parent = CashAdvance::find($receiptParentId);
            }elseif($receiptType == 'REIMBURSEMENT'){
                $parent = Reimbursement::find($receiptParentId);
            }elseif($receiptType == 'INVOICE'){
                $parent = Invoice::find($receiptParentId);
            }
            if(!$parent->isPendingReceipt()){
                throw new \Exception("Request enable to add receipt now.", 1);
            }
            $parentCurrencyId = $parent->currency_id;

            // CREATE RECEIPT DATA
            $receipt = new Receipt();
            // $receipt->organization_id = \Auth::user()->employee->organization_id;
            $receipt->receipt_number = $request->receipt_number ? $request->receipt_number : null;
            $receipt->receipt_date = $request->receipt_date ? \DateTime::createFromFormat(trans('date.format'), $request->receipt_date)->format('Y-m-d') : null;
            $receipt->location_id = $request->location_id;
            $receipt->currency_id = $request->currency_id;
            if($request->currency_id != $parentCurrencyId){
                $receipt->exchange_rate = $request->exchange_rate;
            }
            if($request->establishment_id){
                $establishment = Establishment::whereOrgId($this->orgId)->where('establishment_id',$request->establishment_id)->first();
                $receipt->establishment_id = $establishment->establishment_id;
                $receipt->establishment_name = $establishment->establishment_name;
            }
            $receipt->vendor_id = $request->vendor_id ? $request->vendor_id : null;
            if($request->vendor_id == 'other'){
                $receipt->vendor_name = $request->vendor_name ? $request->vendor_name : null;
                $receipt->vendor_tax_id = $request->vendor_tax_id ? $request->vendor_tax_id : null;
                $receipt->vendor_branch_name = $request->vendor_branch_name ? $request->vendor_branch_name : null;
            }
            $receipt->jusification = trim($request->jusification);
            $receipt->project = $request->project;
            $receipt->job = $request->job;
            $receipt->recharge = $request->recharge;
            // SAVE RECEIPT DATA
            $parent->receipts()->save($receipt);

            // FILE ATTACHMENTS
            if($request->file('file')){
                $attachmentRepo = new AttachmentRepo;
                $attachmentRepo->create($receipt, $request->file('file'));
            }

            // SUCCESS CREATE CASH ADVANCE
            \DB::commit();

        } catch (\Exception $e) {
            // ERROR CREATE CASH ADVANCE
            \DB::rollBack();
            \Log::error($e->getMessage());
            if($request->ajax()){
                $result['status'] = 'ERROR';
                $result['err_msg'] = $e->getMessage();
                return $result;
            }else{
                return abort('403');
            }
        }

        if($request->ajax()){
            $result['status'] = 'SUCCESS';
            $result['receiptId'] = $receipt->id;
            return $result;
        }else{
            if($receiptType == 'CLEARING'){
                return redirect()->route('cash-advances.clear',
                                            ['cashAdvanceId'=>$receiptParentId]);
            }elseif($receiptType == 'REIMBURSEMENT'){
                return redirect()->route('reimbursements.show',
                                            ['reimId'=>$receiptParentId]);
            }elseif($receiptType == 'INVOICE'){
                return redirect()->route('invoices.show',
                                            ['invoiceId'=>$receiptParentId]);
            }
        }
    }

    public function formEdit(Request $request, $receiptId)
    {
        // GET RECEIPT DATA
        $receipt = Receipt::find($receiptId);
        $receipt->receipt_date = dateFormatDisplay($receipt->receipt_date);
        $receiptParentId = $receipt->receiptable_id;
        $receiptType = $receipt->receipt_type;

        // GET LIST DATA (CHANGE WHEN CONNECT ORACLE)
        // $currencyLists = Currency::pluck('currency_code','currency_code')->all();
        // $locations = Location::whereOrgId($this->orgId)->active()->get();
        // $locationLists = Location::whereOrgId($this->orgId)->active()->pluck('name','id')->all();
        $establishmentLists = Establishment::whereOrgId($this->orgId)->pluck('establishment_name','establishment_id')->all();
        $vendorLists = Vendor::notEmp()->whereOrgId($this->orgId)->pluck('vendor_name','vendor_id')->all();
        $projectLists = FNDListOfValues::select(\DB::raw("CONCAT(description,' (',flex_value,')') AS full_description"),'flex_value')->project()->orderBy('flex_value')->pluck('full_description','flex_value')->all();
        $rechargeLists = FNDListOfValues::select(\DB::raw("CONCAT(description,' (',flex_value,')') AS full_description"),'flex_value')->interCompany()->orderBy('flex_value')->pluck('full_description','flex_value')->all();

        return view('commons.receipts._form_edit',
                        compact('receipt',
                                'receiptParentId',
                                'receiptType',
                                // 'baseCurrencyId',
                                // 'currencyLists',
                                // 'locations',
                                // 'locationLists',
                                'establishmentLists',
                                'vendorLists',
                                'projectLists',
                                'rechargeLists'));
    }

    public function update(Request $request, $receiptId)
    {
        // PREPARE PARENT DATA
        $receipt = Receipt::find($receiptId);
        $receiptParentId = $receipt->receiptable_id;
        $receiptType = $receipt->receipt_type;
        $parent = $receipt->parent;

        \DB::beginTransaction();
        try {
            // UPDATE RECEIPT
            $receipt->receipt_number = $request->receipt_number ? $request->receipt_number : null;
            $receipt->receipt_date = $request->receipt_date ? \DateTime::createFromFormat(trans('date.format'), $request->receipt_date)->format('Y-m-d') : null;
            if($request->establishment_id){
                $establishment = Establishment::whereOrgId($this->orgId)->where('establishment_id',$request->establishment_id)->first();
                $receipt->establishment_id = $establishment->establishment_id;
                $receipt->establishment_name = $establishment->establishment_name;
            }
            if($request->vendor_id){
                $receipt->vendor_id = $request->vendor_id;
            }else{
                $receipt->vendor_id = null;
                $receipt->vendor_name = null;
                $receipt->vendor_tax_id = null;
                $receipt->vendor_branch_name = null;
            }
            if($request->vendor_id == 'other'){
                $receipt->vendor_name = $request->vendor_name ? $request->vendor_name : null;
                $receipt->vendor_tax_id = $request->vendor_tax_id ? $request->vendor_tax_id : null;
                $receipt->vendor_branch_name = $request->vendor_branch_name ? $request->vendor_branch_name : null;
            }
            $receipt->jusification = trim($request->jusification);
            $receipt->project = $request->project;
            $receipt->job = $request->job;
            $receipt->recharge = $request->recharge;
            // $receipt->save();
            $parent->receipts()->save($receipt);

            // SUCCESS CREATE CASH ADVANCE
            \DB::commit();
            if($request->ajax()){
                $result['status'] = 'SUCCESS';
                $result['receiptId'] = $receipt->id;
                return $result;
            }else{
                if($receiptType == 'CLEARING'){
                    return redirect()->route('cash-advances.clear',
                                                ['cashAdvanceId'=>$receiptParentId]);
                }elseif($receiptType == 'REIMBURSEMENT'){
                    return redirect()->route('reimbursements.show',
                                                ['reimId'=>$receiptParentId]);
                }elseif($receiptType == 'INVOICE'){
                    return redirect()->route('invoices.show',
                                                ['invoiceId'=>$receiptParentId]);
                }
            }

        } catch (\Exception $e) {
            \DB::rollBack();
            if($request->ajax()){
                $result['status'] = 'ERROR';
                $result['err_msg'] = $e->getMessage();
                return $result;
            }else{
                \Log::error($e->getMessage());
                return abort('403');
            }
        }
    }

    public function getRows(Request $request)
    {
        // GET RECEIPT DATA
        $receiptParentId = $request->receipt_parent_id;
        $receiptType = $request->receipt_type;
        if($receiptType == 'CLEARING'){
            $parent = CashAdvance::find($receiptParentId);
        }elseif($receiptType == 'REIMBURSEMENT'){
            $parent = Reimbursement::find($receiptParentId);
        }elseif($receiptType == 'INVOICE'){
            $parent = Invoice::find($receiptParentId);
        }
        $receipts = $parent->receipts;

        // GET LIST DATA (CHANGE WHEN CONNECT ORACLE)
        $currencyLists = Currency::pluck('currency_code','currency_code')->all();
        $parentCurrencyId = $parent->currency_id;
        // $locations = Location::whereOrgId($this->orgId)->active()->get();
        // $locationLists = Location::whereOrgId($this->orgId)->active()->pluck('name','id')->all();
        $vendorLists = Vendor::notEmp()->whereOrgId($this->orgId)->pluck('vendor_name','vendor_id')->all();
        // $projectLists = FNDListOfValues::select(\DB::raw("CONCAT(description,' (',flex_value,')') AS full_description"),'flex_value')->project()->orderBy('flex_value')->pluck('full_description','flex_value')->all();
        $rechargeLists = FNDListOfValues::select(\DB::raw("CONCAT(description,' (',flex_value,')') AS full_description"),'flex_value')->interCompany()->orderBy('flex_value')->pluck('full_description','flex_value')->all();
        $branchLists = FNDListOfValues::select(\DB::raw("CONCAT(description,' (',flex_value,')') AS full_description"),'flex_value')->branch($this->orgId)->orderBy('flex_value')->pluck('full_description','flex_value')->all();
        $departmentLists = FNDListOfValues::select(\DB::raw("CONCAT(description,' (',flex_value,')') AS full_description"),'flex_value')->department($this->orgId)->orderBy('flex_value')->pluck('full_description','flex_value')->all();

        return view('commons.receipts._table_rows',
                        compact('receipts',
                                'receiptType',
                                'currencyLists',
                                'parentCurrencyId',
                                // 'locations',
                                // 'locationLists',
                                'vendorLists',
                                // 'projectLists',
                                'rechargeLists',
                                'branchLists',
                                'departmentLists'));
    }

    public function getTableTotalRows(Request $request)
    {
        // GET RECEIPT DATA
        $receiptParentId = $request->receipt_parent_id;
        $receiptType = $request->receipt_type;
        if($receiptType == 'CLEARING'){
            $parent = CashAdvance::find($receiptParentId);
        }elseif($receiptType == 'REIMBURSEMENT'){
            $parent = Reimbursement::find($receiptParentId);
        }elseif($receiptType == 'INVOICE'){
            $parent = Invoice::find($receiptParentId);
        }
        $parentCurrencyId = $parent->currency_id;

        return view('commons.receipts._table_total_rows',
                        compact('parent',
                                'parentCurrencyId'));
    }

    public function getVendorDetail(Request $request, $vendorId)
    {
        $vendor = Vendor::notEmp()->whereOrgId($this->orgId)
                        ->where('vendor_id',$vendorId)
                        ->first();
        return view('commons.receipts._vendor_detail',
                        compact('vendor'));
    }

    public function addAttachment(Request $request, $receiptId)
    {
        $receipt = Receipt::find($receiptId);
        \DB::beginTransaction();
        try {
            // ADD FILE ATTACHMENT
            if($request->file('file')){
                $attachmentRepo = new AttachmentRepo;
                $attachmentRepo->create($receipt, $request->file('file'));
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            \Log::error($e->getMessage());
        }
        \DB::commit();

        if($request->ajax()){
            return \Response::json("success", 200);
        }else{
            return redirect()->back()->withInput();
        }
    }

    public function duplicate(Request $request, $receiptId)
    {
        \DB::beginTransaction();
        try {

            // PREPARE PARENT DATA
            $receiptParentId = $request->receipt_parent_id;
            $receiptType = $request->receipt_type;
            if($receiptType == 'CLEARING'){
                $parent = CashAdvance::find($receiptParentId);
            }elseif($receiptType == 'REIMBURSEMENT'){
                $parent = Reimbursement::find($receiptParentId);
            }elseif($receiptType == 'INVOICE'){
                $parent = Invoice::find($receiptParentId);
            }
            if(!$parent->isPendingReceipt()){
                throw new \Exception("Request enable to add receipt now.", 1);
            }
            $parentCurrencyId = $parent->currency_id;

            $receipt = Receipt::find($receiptId);

            ////////////////////////////
            // DUPLICATE RECEIPT DATA
            $newReceipt = new Receipt();
            $newReceipt->receipt_number = $receipt->receipt_number;
            $newReceipt->receipt_date = $receipt->receipt_date;
            $newReceipt->location_id = $receipt->location_id;
            $newReceipt->currency_id = $receipt->currency_id;
            $newReceipt->exchange_rate = $receipt->exchange_rate;
            $newReceipt->establishment_id = $receipt->establishment_id;
            $newReceipt->establishment_name = $receipt->establishment_name;
            $newReceipt->vendor_id = $receipt->vendor_id;
            $newReceipt->vendor_name = $receipt->vendor_name;
            $newReceipt->vendor_tax_id = $receipt->vendor_tax_id;
            $newReceipt->vendor_branch_name = $receipt->vendor_branch_name;
            $newReceipt->jusification = $receipt->jusification;
            $newReceipt->project = $receipt->project;
            $newReceipt->job = $receipt->job;
            $newReceipt->recharge = $receipt->recharge;
            // SAVE RECEIPT DATA
            $parent->receipts()->save($newReceipt);

            /////////////////////////////////
            // DUPLICATE RECEIPT LINES DATA
            $receiptLines = $receipt->lines;
            foreach($receiptLines as $receiptLine){

                // DUPLICATE RECEIPT LINE DATA
                $newReceiptLine = new ReceiptLine();
                $newReceiptLine->receipt_id = $newReceipt->id;
                $newReceiptLine->branch_code = $receiptLine->branch_code;
                $newReceiptLine->department_code = $receiptLine->department_code;
                $newReceiptLine->category_id = $receiptLine->category_id;
                $newReceiptLine->sub_category_id = $receiptLine->sub_category_id;
                // QUANTITY & AMOUNT
                $newReceiptLine->quantity = $receiptLine->quantity;
                $newReceiptLine->second_quantity = $receiptLine->second_quantity;
                $newReceiptLine->transaction_quantity = $receiptLine->transaction_quantity;
                $newReceiptLine->amount = $receiptLine->amount;
                $newReceiptLine->amount_inc_vat = $receiptLine->amount_inc_vat;
                $newReceiptLine->total_amount = $receiptLine->total_amount;
                $newReceiptLine->total_amount_inc_vat = $receiptLine->total_amount_inc_vat;
                $newReceiptLine->policy_id = $receiptLine->policy_id;
                $newReceiptLine->rate_id = $receiptLine->rate_id;
                $newReceiptLine->mileage_unit_id = $receiptLine->mileage_unit_id;
                $newReceiptLine->mileage_distance = $receiptLine->mileage_distance;
                $newReceiptLine->mileage_start = $receiptLine->mileage_start;
                $newReceiptLine->mileage_end = $receiptLine->mileage_end;
                $newReceiptLine->primary_amount = $receiptLine->primary_amount;
                $newReceiptLine->primary_amount_inc_vat = $receiptLine->primary_amount_inc_vat;
                // PRIMARY => CALCULATE (WITH EXCHANGE RATE) TO BASE CURRENCY
                $newReceiptLine->total_primary_amount = $receiptLine->total_primary_amount;
                $newReceiptLine->total_primary_amount_inc_vat = $receiptLine->total_primary_amount_inc_vat;
                // VAT
                $newReceiptLine->vat_id = $receiptLine->vat_id;
                $newReceiptLine->vat_amount = $receiptLine->vat_amount;
                $newReceiptLine->primary_vat_amount = $receiptLine->primary_vat_amount;

                // GL ACCOUNT COMBINATION WILL SET WHEN SEND REQUEST
                $newReceiptLine->code_combination_id = null;
                $newReceiptLine->concatenated_segments = null;

                $newReceiptLine->save();

                // DUPLICATE RECEIPT LINE INFORMATIONS
                $infos = SubCategoryInfo::where('sub_category_id',$newReceiptLine->sub_category_id)
                            ->active()
                            ->get();

                if(count($infos)>0){

                    $subCategoryInfos = $receiptLine->infos->pluck('description','sub_category_info_id')->all();

                    foreach($infos as $info){
                        if(array_key_exists($info->id, $subCategoryInfos)){
                            $newReceiptLineInfo = new ReceiptLineInfo();
                            $newReceiptLineInfo->receipt_id = $newReceipt->id;
                            $newReceiptLineInfo->receipt_line_id = $newReceiptLine->id;
                            $newReceiptLineInfo->sub_category_id = $newReceiptLine->sub_category_id;
                            $newReceiptLineInfo->sub_category_info_id = $info->id;
                            $newReceiptLineInfo->description = $subCategoryInfos[$info->id];
                            $newReceiptLineInfo->save();
                        }
                    }
                }

            }

            \DB::commit();

        } catch (\Exception $e) {
            // ERROR CREATE CASH ADVANCE
            \DB::rollBack();
            \Log::error($e->getMessage());
            if($request->ajax()){
                return \Response::json("error", 200);
            }else{
                return abort('403');
            }
        }

        if($request->ajax()){
            return \Response::json("success", 200);
        }else{
            return redirect()->back();
        }
    }

    public function remove(Request $request, $receiptId)
    {
        $receipt = Receipt::find($receiptId);
        \DB::beginTransaction();
        try {
            // DELETE RECEIPT
            $receipt->delete();

        } catch (\Exception $e) {
            \DB::rollBack();
            \Log::error($e->getMessage());
        }
        \DB::commit();

        if($request->ajax()){
            return \Response::json("success", 200);
        }else{
            return redirect()->back()->withInput();
        }
    }
}
