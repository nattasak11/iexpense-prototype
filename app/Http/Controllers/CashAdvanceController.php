<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Requests\CASetDueDateRequest;

use App\Repositories\AttachmentRepo;
use App\Repositories\ActivityLogRepo;
use App\Repositories\RequestRepo;
use App\Repositories\ApprovalRepo;
use App\Repositories\MailRepo;
use App\Repositories\InterfaceRepo;

use App\CashAdvance;
use App\CashAdvanceInfo;
use App\Preference;
use App\Employee;
use App\Category;
use App\Currency;
use App\Location;
use App\Establishment;
use App\Vendor;
use App\FNDListOfValues;
use App\CACategory;
use App\CASubCategory;
use App\CASubCategoryInfo;
use App\Receipt;
use App\ReceiptLine;
use App\PaymentMethod;
use App\User;
// use App\APTerm;
use App\APPaymentMethod;

use Carbon\Carbon;

class CashAdvanceController extends Controller
{
    protected $perPage = 10;
    protected $orgId;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->orgId = \Auth::user()->org_id;
            return $next($request);
        });
    }

    public function index()
    {
        $search = [];
        $cashAdvances = CashAdvance::orderBy('created_at','desc')
                        ->whereOrgId($this->orgId)
                        ->whereUserId(\Auth::user()->id)
                        ->with('user')
                        ->paginate($this->perPage);
        $allowDuplicate = true;

        return view('cash-advances.index',
                        compact('cashAdvances',
                                'search',
                                'allowDuplicate'));
    }

    public function indexPending()
    {
        $search = [];
        $cashAdvances = CashAdvance::orderBy('created_at','desc')
                        ->whereOrgId($this->orgId)
                        ->ByPendingUser(\Auth::user()->id)
                        ->with('user')
                        ->paginate($this->perPage);

        return view('cash-advances.index_pending',
                        compact('cashAdvances',
                                'search'));
    }

    public function create()
    {
        // GET LIST DATA (CHANGE WHEN CONNECT ORACLE)
        $CACategoryLists = CACategory::active()
                                    // ->whereOrgId($this->orgId)
                                    ->pluck('name','id')
                                    ->all();
        // $CASubCategoryLists = CASubCategory::active()
        //                             ->accessibleOrg($this->orgId)
        //                             ->onDateActive()
        //                             ->pluck('name','id')
        //                             ->all();
        $CASubCategoryLists = [];
        $baseCurrencyId = Preference::getBaseCurrency($this->orgId);

        // GET PENDING TRASACTION
        $pendingRequestLists = RequestRepo::getPendingOverLimitDayRequest(\Auth::user()->id);

        return view('cash-advances.create',
                        compact('baseCurrencyId',
                                'CACategoryLists',
                                'CASubCategoryLists',
                                'pendingRequestLists'));
    }

    public function store(Request $request)
    {
        // GET PENDING TRASACTION
        $pendingRequestLists = RequestRepo::getPendingOverLimitDayRequest(\Auth::user()->id);

        \DB::beginTransaction();
        try {
            // CREATE CASH ADVANCE
            $cashAdvance = new CashAdvance();
            $cashAdvance->org_id = $this->orgId;
            // $cashAdvance->organization_id = \Auth::user()->employee->organization_id;
            $documentNo = CashAdvance::genDocumentNo($this->orgId);
            $cashAdvance->document_no = $documentNo['CA'];
            $cashAdvance->clearing_document_no = $documentNo['CL'];
            $cashAdvance->user_id = \Auth::user()->id;
            // $cashAdvance->position_id = $request->position_id;

            if($request->ca_category_id){
                $cashAdvance->ca_category_id = $request->ca_category_id;
            }else{
                $caSubCategory = CASubCategory::find($request->ca_sub_category_id);
                $cashAdvance->ca_category_id = $caSubCategory->category->id;
            }
            $cashAdvance->ca_sub_category_id = $request->ca_sub_category_id;
            $cashAdvance->need_by_date = \DateTime::createFromFormat(trans('date.format'), $request->need_by_date)->format('Y-m-d');
            $cashAdvance->amount = $request->amount;
            $cashAdvance->currency_id  = $request->currency_id;
            $cashAdvance->purpose  = trim($request->purpose);
            if(count($pendingRequestLists) == 0){
                $cashAdvance->status = "NEW_REQUEST";
            }else{ // BLOCKED IF HAS PENDING REQUEST
                $cashAdvance->status = "BLOCKED";
            }
            $cashAdvance->save();

            // INSERT CASH-ADVANCE INFORMATIONS
            $infos = CASubCategoryInfo::where('ca_sub_category_id',$cashAdvance->ca_sub_category_id)->active()->get();

            if(count($infos)>0){
                foreach($infos as $info){
                    if($request->ca_sub_category_infos[$info->id]){
                        $cashAdvaneInfo = new CashAdvanceInfo();
                        $cashAdvaneInfo->cash_advance_id = $cashAdvance->id;
                        $cashAdvaneInfo->ca_sub_category_id = $cashAdvance->ca_sub_category_id;
                        $cashAdvaneInfo->ca_sub_category_info_id = $info->id;
                        if($info->form_type == 'date'){
                            $cashAdvaneInfo->description = \DateTime::createFromFormat(trans('date.format'), $request->ca_sub_category_infos[$info->id])->format('Y-m-d');
                        }else{
                            $cashAdvaneInfo->description = $request->ca_sub_category_infos[$info->id];
                        }
                        $cashAdvaneInfo->save();
                    }
                }
            }

            // FILE ATTACHMENTS
            if($request->file('file')){
                $attachmentRepo = new AttachmentRepo;
                $attachmentRepo->create($cashAdvance, $request->file('file'));
            }

            // ACTIVITY LOG
            $activityLogRepo = new ActivityLogRepo;
            $activityLogRepo->create($cashAdvance, ActivityLogRepo::getActivityMessage('NEW_REQUEST'));

            // SUCCESS CREATE CASH ADVANCE
            \DB::commit();
            if($request->ajax()){
                $result['status'] = 'SUCCESS';
                $result['cashAdvanceId'] = $cashAdvance->id;
                return $result;
            }else{
                return redirect()->route('cash-advances.show',['cashAdvanceId'=>$cashAdvance->id]);
            }

        } catch (\Exception $e) {
            // ERROR CREATE CASH ADVANCE
            \DB::rollBack();
            if($request->ajax()){
                $result['status'] = 'ERROR';
                $result['err_msg'] = $e->getMessage();
                return $result;
            }else{
                // throw new \Exception($e->getMessage(), 1);
                \Log::error($e->getMessage());
                return abort('403');
            }
        }
    }

    public function update(Request $request, $cashAdvanceId)
    {
        \DB::beginTransaction();
        try {

            $cashAdvance = CashAdvance::find($cashAdvanceId);
            // $cashAdvance->ca_category_id = $request->ca_category_id;
            $cashAdvance->need_by_date = \DateTime::createFromFormat(trans('date.format'), $request->need_by_date)->format('Y-m-d');
            $cashAdvance->amount = $request->amount;
            $cashAdvance->purpose  = trim($request->purpose);
            $cashAdvance->save();

            // UPDATE CASH-ADVANCE INFORMATIONS
            $infos = CASubCategoryInfo::where('ca_sub_category_id',$cashAdvance->ca_sub_category_id)
                        ->active()->get();

            if(count($infos)>0){
                foreach($infos as $info){
                    if($request->ca_sub_category_infos[$info->id]){
                        $cashAdvaneInfo = CashAdvanceInfo::where('cash_advance_id',$cashAdvanceId)
                                        ->where('ca_sub_category_id',$cashAdvance->ca_sub_category_id)
                                        ->where('ca_sub_category_info_id',$info->id)
                                        ->first();
                        if($info->form_type == 'date'){
                            $cashAdvaneInfo->description = \DateTime::createFromFormat(trans('date.format'), $request->ca_sub_category_infos[$info->id])->format('Y-m-d');
                        }else{
                            $cashAdvaneInfo->description = $request->ca_sub_category_infos[$info->id];
                        }
                        $cashAdvaneInfo->save();
                    }
                }
            }

        } catch (\Exception $e) {
            \DB::rollBack();
            \Log::error($e->getMessage());
        }
        \DB::commit();

        return redirect()->route('cash-advances.show',['cashAdvanceId'=>$cashAdvance->id]);
    }

    public function show($cashAdvanceId)
    {
        // GET CA DATA
        $cashAdvance = CashAdvance::find($cashAdvanceId);
        if(!$cashAdvance){ abort(404); }
        if(!$cashAdvance->isRelatedUser()){ abort(404); }

        $cashAdvance->amount = $cashAdvance->amount + 0;
        $cashAdvance->need_by_date = dateFormatDisplay($cashAdvance->need_by_date);
        $cashAdvance->due_date = dateFormatDisplay($cashAdvance->due_date);
        $parentCurrencyId = $cashAdvance->currency_id;

        // CASH-ADVANCE INFOMATIONS
        $CASubCategoryInfos = CASubCategoryInfo::where('ca_sub_category_id',$cashAdvance->ca_sub_category_id)
            ->active()
            ->get();
        $cashAdvanceInfos = CashAdvanceInfo::where('cash_advance_id',$cashAdvance->id)->get();
        $cashAdvanceInfoLists = CashAdvanceInfo::where('cash_advance_id',$cashAdvance->id)
                                                ->pluck('description','ca_sub_category_info_id')
                                                ->all();

        // GET ACTIVITY LOG
        $activityLogs = $cashAdvance->activityLogs;

        // GET LIST DATA (CHANGE WHEN CONNECT ORACLE)
        $currencyLists = Currency::pluck('currency_code','currency_code')->all();
        $CACategoryLists = CACategory::active()
                                    // ->whereOrgId($this->orgId)
                                    ->pluck('name','id')
                                    ->all();
        $APPaymentMethodLists = APPaymentMethod::forCashAdvance()->pluck('payment_method_name','payment_method_code')->all();

        // GET PENDING TRASACTION
        $pendingRequestLists = RequestRepo::getPendingOverLimitDayRequest(\Auth::user()->id);

        return view('cash-advances.show',
                        compact('cashAdvance',
                                'currencyLists',
                                'CACategoryLists',
                                'parentCurrencyId',
                                'CASubCategoryInfos',
                                'cashAdvanceInfos',
                                'cashAdvanceInfoLists',
                                'paymentMethodLists',
                                'pendingRequestLists',
                                'activityLogs',
                                'APPaymentMethodLists',
                                'branchLists',
                                'departmentLists'));
    }

    public function clearRequest($cashAdvanceId)
    {
        \DB::beginTransaction();
        try {

            $cashAdvance = CashAdvance::find($cashAdvanceId);
            if($cashAdvance->status != 'APPROVED' || !$cashAdvance->isRequester()){ abort(403); }
            $cashAdvance->status = 'CLEARING_REQUEST';
            $cashAdvance->save();

            // ACTIVITY LOG
            $activityLogRepo = new ActivityLogRepo;
            $activityLogRepo->create($cashAdvance, ActivityLogRepo::getActivityMessage('CLEARING_CREATE_REQUEST'));

        } catch (\Exception $e) {
            \DB::rollBack();
            \Log::error($e->getMessage());
        }
        \DB::commit();

        return redirect()->route('cash-advances.clear',['cashAdvanceId'=>$cashAdvanceId]);

    }

    public function clear($cashAdvanceId)
    {
        // GET CA DATA
        $cashAdvance = CashAdvance::find($cashAdvanceId);
        if(!$cashAdvance){ abort(404); }
        if(!$cashAdvance->onClearing()){ abort(404); }
        if(!$cashAdvance->isRelatedUser()){ abort(404); }

        $cashAdvance->need_by_date = dateFormatDisplay($cashAdvance->need_by_date);
        $cashAdvance->due_date = dateFormatDisplay($cashAdvance->due_date);
        // GET ACTIVITY LOG
        $activityLogs = $cashAdvance->activityLogs;

        // GET LIST DATA (CHANGE WHEN CONNECT ORACLE)
        $currencyLists = Currency::pluck('currency_code','currency_code')->all();
        // $currency = Currency::where('currency_code', $cashAdvance->currency_id)->first();
        $CACategoryLists = CACategory::active()
                                // ->whereOrgId($this->orgId)
                                ->pluck('name','id')->all();

        // CASH-ADVANCE INFOMATIONS
        $cashAdvanceInfos = CashAdvanceInfo::where('cash_advance_id',$cashAdvance->id)->get();

        // DATA FOR RECEIPT
        $receipts = $cashAdvance->receipts;
        $receiptParentId = $cashAdvanceId;
        $categoryLists = Category::active()->pluck('name','id')->all();
        $parentCurrencyId = $cashAdvance->currency_id;
        $receiptType = 'CLEARING';
        $locations = Location::accessibleOrg($this->orgId)->active()->get();
        $locationLists = Location::accessibleOrg($this->orgId)->active()->pluck('name','id')->all();
        // change in production when set location establishment
        $defaultEstablishmentId = $cashAdvance->user->employee->establishment_id;
        $establishmentLists = Establishment::whereOrgId($this->orgId)->pluck('establishment_name','establishment_id')->all();
        $vendorLists = Vendor::notEmp()->whereOrgId($this->orgId)->pluck('vendor_name','vendor_id')->all();
        $projectLists = FNDListOfValues::select(\DB::raw("CONCAT(description,' (',flex_value,')') AS full_description"),'flex_value')->project()->orderBy('flex_value')->pluck('full_description','flex_value')->all();
        $rechargeLists = FNDListOfValues::select(\DB::raw("CONCAT(description,' (',flex_value,')') AS full_description"),'flex_value')->interCompany()->orderBy('flex_value')->pluck('full_description','flex_value')->all();
        $branchLists = FNDListOfValues::select(\DB::raw("CONCAT(description,' (',flex_value,')') AS full_description"),'flex_value')->branch($this->orgId)->orderBy('flex_value')->pluck('full_description','flex_value')->all();
        $departmentLists = FNDListOfValues::select(\DB::raw("CONCAT(description,' (',flex_value,')') AS full_description"),'flex_value')->department($this->orgId)->orderBy('flex_value')->pluck('full_description','flex_value')->all();

        // DIFF
        $diffCAAndClearingData = self::calDiffCAAndClearing($cashAdvance);

        return view('cash-advances.clear',
                        compact('cashAdvance',
                                // 'currency',
                                'currencyLists',
                                'CACategoryLists',
                                'cashAdvanceInfos',
                                'categoryLists',
                                'parentCurrencyId',
                                'activityLogs',
                                'receipts',
                                'receiptParentId',
                                'receiptType',
                                'locations',
                                'locationLists',
                                'establishmentLists',
                                'vendorLists',
                                'projectLists',
                                'rechargeLists',
                                'branchLists',
                                'departmentLists',
                                'diffCAAndClearingData',
                                'defaultEstablishmentId'));
    }

    public function getSubCategories(Request $request)
    {
        $caCategoryId = $request->ca_category_id;
        if($caCategoryId){
            $CASubCategoryLists = CASubCategory::where('ca_category_id',$caCategoryId)
                                    ->active()
                                    ->accessibleOrg($this->orgId)
                                    ->onDateActive()
                                    ->pluck('name','id')
                                    ->all();
        }else{
            // $CASubCategoryLists = CASubCategory::active()
            //                         ->accessibleOrg($this->orgId)
            //                         ->onDateActive()
            //                         ->pluck('name','id')
            //                         ->all();
            $CASubCategoryLists = [];
        }

        return view('cash-advances._ddl_sub_categories',compact('CASubCategoryLists'));
    }

    public function getInputFormInformations(Request $request, $caSubCategoryId)
    {
        $CASubCategoryInfos = CASubCategoryInfo::where('ca_sub_category_id',$caSubCategoryId)
            ->active()
            ->get();

        return view('cash-advances._form_informations',compact('CASubCategoryInfos'));
    }

    public function setStatus(Request $request, $cashAdvanceId)
    {
        // PARAMETER FOR SET STATUS
        $type = $request->type; // CASH-ADVANCE, CLEARING
        $activity = $request->activity;
        $reason = ''; // REASON WILL SHOW IN ACIVITY LOG DESCTIPRION
        if($request->reason){
            $reason = $request->reason;
        }

        // Get CA Header Data
        $cashAdvance = CashAdvance::find($cashAdvanceId);

        // FILE ATTACHMENTS
        if($request->file('file')){
            $attachmentRepo = new AttachmentRepo;
            $attachmentRepo->create($cashAdvance, $request->file('file'));
        }

        // Check Permission for Approve
        if(!self::hasPermissionSetStatus($cashAdvance,$activity)){  abort(403);  }

        \DB::beginTransaction();
        try {
            // Set STATUS (approve,reject,sendback)
            switch ($activity) {

                // #### CA REQUEST ####
                case "UNBLOCK":
                    $cashAdvance->status = 'NEW_REQUEST';
                    $cashAdvance->save();
                    break;
                case "SEND_REQUEST":
                    // CHECK FOUND APPROVER OR NOT ?
                    $nextApprover = ApprovalRepo::getNextApprover($cashAdvance,'CASH-ADVANCE');
                    if($nextApprover){ // IF FOUND NEXT APPROVER SET STATUS = 'APPROVER_DECISION'
                        $cashAdvance->status = 'APPROVER_DECISION';
                        $cashAdvance->next_approver_id = $nextApprover['user_id'];
                    }else{
                        // IF NOT FOUND NEXT APPROVER AUTO SET STATUS TO NEXT STEP
                        $cashAdvance->next_approver_id = null;
                        $cashAdvance->status = 'FINANCE_PROCESS';
                    }
                    $cashAdvance->save();
                    break;
                case "APPROVER_APPROVE":
                    // DO APPROVE PROCESS
                    $approvalRepo = new ApprovalRepo;
                    $approvalRepo->approve($cashAdvance,'CASH-ADVANCE','APPROVER');
                    // CHECK FOUND NEXT APPROVER OR NOT ?
                    $nextApprover = ApprovalRepo::getNextApprover($cashAdvance,'CASH-ADVANCE');
                    if($nextApprover){ // IF FOUND NEXT APPROVER SET NEXT APPROVER
                        $cashAdvance->next_approver_id = $nextApprover['user_id'];
                    }else{
                        // IF NOT FOUND NEXT APPROVER SET STATUS = 'FINANCE_PROCESS'
                        $cashAdvance->next_approver_id = null;
                        $cashAdvance->status = 'FINANCE_PROCESS';
                    }
                    $cashAdvance->save();
                    break;
                case "APPROVER_SENDBACK":
                    // SET STATUS BACK TO 'NEW_REQUEST'
                    $cashAdvance->status = 'NEW_REQUEST';
                    $cashAdvance->next_approver_id = null;
                    $cashAdvance->save();
                    break;
                case "APPROVER_REJECT":
                    $cashAdvance->status = 'APPROVER_REJECTED';
                    $cashAdvance->save();
                    break;
                case "FINANCE_APPROVE":
                    // DO FINANCE APPROVE PROCESS
                    $approvalRepo = new ApprovalRepo;
                    $approvalRepo->approve($cashAdvance,'CASH-ADVANCE','FINANCE');
                    // SET STATUS TO 'APPROVED'
                    $cashAdvance->status = 'APPROVED';
                    $cashAdvance->save();
                    // INTERFACE TO ORACLE AP PRE-PAYMENT
                    $interfaceResult = InterfaceRepo::prePayment($cashAdvance,'CASH-ADVANCE');
                    break;
                case "FINANCE_SENDBACK":
                    // SET STATUS BACK TO 'NEW_REQUEST'
                    $cashAdvance->status = 'NEW_REQUEST';
                    $cashAdvance->next_approver_id = null;
                    $cashAdvance->finance_approver_id = null;
                    $cashAdvance->save();
                    break;
                case "FINANCE_REJECT":
                    $cashAdvance->status = 'FINANCE_REJECTED';
                    $cashAdvance->save();
                    break;

                // #### CLEARING REQUEST ####

                case "CLEARING_SEND_REQUEST":
                    // SET OVER BUDGET & EXCEED POLICY
                    $resultOverBudget = RequestRepo::validateIsNotOverBudget($cashAdvance);
                    $resultExceedPolicy = RequestRepo::validateIsNotExceedPolicy($cashAdvance);
                    $cashAdvance->over_budget = !$resultOverBudget['valid'];
                    $cashAdvance->exceed_policy = !$resultExceedPolicy['valid'];
                    $cashAdvance->save();
                    // CHECK FOUND NEXT APPROVER OR NOT ?
                    $nextApprover = ApprovalRepo::getNextApprover($cashAdvance,'CLEARING');
                    if($nextApprover){ // IF FOUND NEXT APPROVER SET STATUS = 'CLEARING_APPROVER_DECISION'
                        $cashAdvance->status = 'CLEARING_APPROVER_DECISION';
                        $cashAdvance->next_clearing_approver_id = $nextApprover['user_id'];
                    }else{
                        // IF NOT FOUND NEXT APPROVER AUTO SET STATUS TO NEXT STEP
                        $cashAdvance->next_clearing_approver_id = null;
                        $cashAdvance->status = 'CLEARING_FINANCE_PROCESS';
                    }
                    $cashAdvance->save();
                    break;
                case "CLEARING_APPROVER_APPROVE":
                    // DO APPROVE PROCESS
                    $approvalRepo = new ApprovalRepo;
                    $approvalRepo->approve($cashAdvance,'CLEARING','APPROVER');
                    // CHECK FOUND NEXT APPROVER OR NOT ?
                    $nextApprover = ApprovalRepo::getNextApprover($cashAdvance,'CLEARING');
                    if($nextApprover){ // IF FOUND NEXT APPROVER SET NEXT APPROVER
                        $cashAdvance->next_clearing_approver_id = $nextApprover['user_id'];
                    }else{
                        // IF NOT FOUND NEXT APPROVER SET STATUS = 'FINANCE_PROCESS'
                        $cashAdvance->next_clearing_approver_id = null;
                        $cashAdvance->status = 'CLEARING_FINANCE_PROCESS';
                    }
                    $cashAdvance->save();
                    break;
                case "CLEARING_APPROVER_SENDBACK":
                    // SET STATUS BACK TO 'CLEARING_REQUEST'
                    $cashAdvance->status = 'CLEARING_REQUEST';
                    $cashAdvance->next_clearing_approver_id = null;
                    $cashAdvance->over_budget = null;
                    $cashAdvance->exceed_policy = null;
                    $cashAdvance->save();
                    break;
                case "CLEARING_FINANCE_APPROVE":
                    // DO CLEARING FINANCE APPROVE PROCESS
                    $approvalRepo = new ApprovalRepo;
                    $approvalRepo->approve($cashAdvance,'CLEARING','FINANCE');
                    // SET STATUS TO 'CLEARED'
                    $cashAdvance->status = 'CLEARED';
                    $cashAdvance->save();
                    // INTERFACE TO ORACLE AP INVOICE
                    $interfaceResult = InterfaceRepo::invoice($cashAdvance,'CLEARING');
                    break;
                case "CLEARING_FINANCE_SENDBACK":
                    // SET STATUS BACK TO 'CLEARING_REQUEST'
                    $cashAdvance->status = 'CLEARING_REQUEST';
                    $cashAdvance->next_clearing_approver_id = null;
                    $cashAdvance->clearing_finance_approver_id = null;
                    $cashAdvance->over_budget = null;
                    $cashAdvance->exceed_policy = null;
                    $cashAdvance->save();
                    break;
                case "CANCEL_REQUEST":
                    // SET STATUS TO 'CANCELLED'
                    $cashAdvance->status = 'CANCELLED';
                    $cashAdvance->save();
                    break;
            }

            // SEND EMAIL
            self::sendEmailByActivity($activity,$cashAdvance,$reason);

            // RESET APPROVAL (ACTIVITY SENDBACK ONLY)
            self::resetApproval($activity,$cashAdvance);

            // ACTIVITY LOG
            $activityLogRepo = new ActivityLogRepo;
            $activityLogRepo->create($cashAdvance, ActivityLogRepo::getActivityMessage($activity,$cashAdvance),$reason);

        } catch (\Exception $e) {
            \DB::rollBack();
            \Log::error($e->getMessage());

            if($type == 'CASH-ADVANCE'){
                return redirect()->route('cash-advances.show',['cashAdvanceId'=>$cashAdvanceId])->withErrors([$e->getMessage()]);;
            }else{ // CLEARING
                return redirect()->route('cash-advances.clear',['cashAdvanceId'=>$cashAdvanceId])->withErrors([$e->getMessage()]);;
            }
        }
        \DB::commit();

        // REDIRECT AFTER SET STATUS
        if($type == 'CASH-ADVANCE'){
            return redirect()->route('cash-advances.show',['cashAdvanceId'=>$cashAdvanceId]);
        }else{ // CLEARING
            return redirect()->route('cash-advances.clear',['cashAdvanceId'=>$cashAdvanceId]);
        }
    }

    ////////////////////////////
    //// SET DUE DATE
    public function setDueDate(CASetDueDateRequest $request, $cashAdvanceId)
    {
        $cashAdvance = CashAdvance::find($cashAdvanceId);

        \DB::beginTransaction();
        try {
            // SET DUE DATE
            if($request->due_date){

                $cashAdvance->due_date = \DateTime::createFromFormat(trans('date.format'), $request->due_date)->format('Y-m-d');
                if($request->payment_method_type =='method'){
                    $cashAdvance->manual_payment = false;
                    $cashAdvance->payment_method_id = $request->payment_method_id;
                }else{ // MANUAL PAYMENT
                    $cashAdvance->manual_payment = true;
                    // MANUAL PAYMENT => DEFAULT METHOD SAME DAY
                    $paymentMethodSameDay = APPaymentMethod::forCashAdvance()->sameDay()->first();
                    $cashAdvance->payment_method_id = $paymentMethodSameDay->payment_method_code;
                }

            }else{
                $cashAdvance->due_date = null;
            }
            $cashAdvance->save();

             // ACTIVITY LOG
            $activityLogRepo = new ActivityLogRepo;
            $activityLogRepo->create($cashAdvance, ActivityLogRepo::getActivityMessage('SET_DUE_DATE'));

        } catch (\Exception $e) {
            \DB::rollBack();
            \Log::error($e->getMessage());
        }
        \DB::commit();

        return redirect()->route('cash-advances.show',['cashAdvanceId'=>$cashAdvanceId]);
    }

    ///////////////////////////
    ///// ADD ATTACHMENT
    public function addAttachment(Request $request, $cashAdvanceId)
    {
        $cashAdvance = CashAdvance::find($cashAdvanceId);

        \DB::beginTransaction();
        try {
            // ADD FILE ATTACHMENT
            if($request->file('file')){
                $attachmentRepo = new AttachmentRepo;
                $attachmentRepo->create($cashAdvance, $request->file('file'));
            }

        } catch (\Exception $e) {
            \DB::rollBack();
            \Log::error($e->getMessage());
        }
        \DB::commit();

        return redirect()->back();
    }

    //////////////////////////
    //// CHECK CA ATTACHMENT
    public function checkCAAttachment(Request $request, $cashAdvanceId)
    {
        $valid = false;
        $cashAdvance = CashAdvance::find($cashAdvanceId);
        if($cashAdvance){
            // IF NOT REQUIRED ATTACHMENT
            if(!$cashAdvance->subCategory->required_attachment){
                $valid = true;
            }else{
                // IF ALREADY ATTACH FILE
                if(count($cashAdvance->attachments) > 0){
                    $valid = true;
                }
            }
        }
        if($request->ajax()){
            return \Response::json(['valid'=>$valid], 200);
        }else{
            return $valid;
        }
    }

    //////////////////////////
    //// CHECK CA MIN AMOUNT
    public function checkCAMinAmount(Request $request, $cashAdvanceId)
    {
        $valid = false;
        $cashAdvance = CashAdvance::find($cashAdvanceId);
        if($cashAdvance){
            // IF NOT CHECK MIN AMOUNT
            if(!$cashAdvance->subCategory->check_ca_min){
                $valid = true;
            }else{
                // IF CHECK AND AMOUNT > MIN AMOUNT
                if((float)$cashAdvance->amount >= (float)$cashAdvance->subCategory->ca_min_amount) {
                    $valid = true;
                }
            }
        }
        if($request->ajax()){
            return \Response::json(['valid'=>$valid,'ca_min_amount'=>$cashAdvance->subCategory->ca_min_amount], 200);
        }else{
            return $valid;
        }
    }

    //////////////////////////
    //// CHECK CA MAX AMOUNT
    public function checkCAMaxAmount(Request $request, $cashAdvanceId)
    {
        $valid = false;
        $cashAdvance = CashAdvance::find($cashAdvanceId);
        if($cashAdvance){
            // IF NOT CHECK MAX AMOUNT
            if(!$cashAdvance->subCategory->check_ca_max){
                $valid = true;
            }else{
                // IF CHECK AND AMOUNT > MAX AMOUNT
                if((float)$cashAdvance->amount <= (float)$cashAdvance->subCategory->ca_max_amount) {
                    $valid = true;
                }
            }
        }
        if($request->ajax()){
            return \Response::json(['valid'=>$valid,'ca_max_amount'=>$cashAdvance->subCategory->ca_max_amount], 200);
        }else{
            return $valid;
        }
    }

    //////////////////////////
    //// GET TOTAL AMOUNT
    public function getTotalAmount(Request $request, $cashAdvanceId)
    {
        $cashAdvance = CashAdvance::find($cashAdvanceId);
        if(!$cashAdvance){ throw new Exception("Error Processing Request", 1); }
        $cashAdvanceTotalAmount = $cashAdvance->total_receipt_amount + 0;
        return number_format($cashAdvanceTotalAmount,2);
    }

    /////////////////////////////////////
    //// GET DIFF CA AND CLEARING AMOUNT
    public function getDiffCAAndClearingAmount(Request $request, $cashAdvanceId)
    {
        $cashAdvance = CashAdvance::onStatusClearing()
                                ->where('id',$cashAdvanceId)
                                ->first();
        if(!$cashAdvance){ throw new Exception("Error Processing Request", 1); }
        $diffCAAndClearingData = self::calDiffCAAndClearing($cashAdvance);
        if(!$diffCAAndClearingData){ throw new Exception("Error Processing Request", 1); }
        return view('cash-advances.clear._diff_ca_and_clearing_amount',compact('cashAdvance','diffCAAndClearingData'));
    }

    /////////////////////////////////////
    //// GET DIFF CA AND CLEARING DATA
    public function getDiffCAAndClearingData(Request $request, $cashAdvanceId)
    {
        $cashAdvance = CashAdvance::onStatusClearing()
                                ->where('id',$cashAdvanceId)
                                ->first();
        if(!$cashAdvance){ throw new Exception("Error Processing Request", 1); }
        $result = self::calDiffCAAndClearing($cashAdvance);
        if(!$result){ throw new Exception("Error Processing Request", 1); }
        $result['amount'] = number_format($result['amount'],2);

        if($request->ajax()){
            return \Response::json($result, 200);
        }else{
            return redirect()->back();
        }
    }

    ////////////////////////////////////////
    // CALCULATE DIFF CA & CLEARING AMOUNT
    private static function calDiffCAAndClearing($cashAdvance)
    {
        $result = [];
        if($cashAdvance->total_receipt_amount){
            if((float)$cashAdvance->amount > (float)$cashAdvance->total_receipt_amount){
                $result['type'] = 'paybacktocompany';
                $result['amount'] = (float)$cashAdvance->amount - (float)$cashAdvance->total_receipt_amount;
            }elseif((float)$cashAdvance->amount < (float)$cashAdvance->total_receipt_amount){
                $result['type'] = 'paybacktorequester';
                $result['amount'] = (float)$cashAdvance->total_receipt_amount - (float)$cashAdvance->amount;
            }else{ // CASHADVANCE == CLEARING
                $result['type'] = 'normal';
                $result['amount'] = 0;
            }
        }else{
            $result['type'] = 'paybacktocompany';
            $result['amount'] = $cashAdvance->amount;
        }
        return $result;
    }

    //////////////////////////////////
    //// COMBINE GL CODE COMBINATION
    public function combineReceiptGLCodeCombination(Request $request, $cashAdvanceId)
    {
        try {

            $cashAdvance = CashAdvance::find($cashAdvanceId);
            if(!$cashAdvance){
                throw new \Exception("Not found cash advance data.", 1);
            }
            $result = RequestRepo::combineReceiptGLCodeCombination($cashAdvance);

        } catch (\Exception $e) {
            if($request->ajax()){
                return \Response::json(['status'                =>  'error',
                                        'err_msg'               =>  $e->getMessage(),
                                        'err_receipt_line_id'   =>  null], 200);
            }else{
                throw new \Exception($e->getMessage(), 1);
            }
        }

        if($request->ajax()){
            return \Response::json($result, 200);
        }else{
            return redirect()->back();
        }
    }

    //////////////////////////
    //// CHECK OVER BUDGET
    public function checkOverBudget(Request $request, $cashAdvanceId)
    {
        $valid = false;
        $cashAdvance = CashAdvance::find($cashAdvanceId);
        if(!$cashAdvance){
            throw new \Exception("Not found cash advance data.", 1);
        }
        $valid = RequestRepo::validateIsNotOverBudget($cashAdvance);

        if($request->ajax()){
            return \Response::json($valid, 200);
        }else{
            return $valid;
        }
    }

    //////////////////////////
    //// CHECK EXCEED POLICY
    public function checkExceedPolicy(Request $request, $cashAdvanceId)
    {
        $cashAdvance = CashAdvance::find($cashAdvanceId);
        if(!$cashAdvance){
            throw new \Exception("Not found cash advance data.", 1);
        }
        $result = RequestRepo::validateIsNotExceedPolicy($cashAdvance);

        if($request->ajax()){
            return \Response::json($result, 200);
        }else{
            return $result;
        }
    }

    //////////////////////////////
    //// validateReceipt
    public function validateReceipt(Request $request, $cashAdvanceId)
    {
        $cashAdvance = CashAdvance::find($cashAdvanceId);
        if(!$cashAdvance){
            throw new \Exception("Not found cash advance data.", 1);
        }
        $result = RequestRepo::validateReceipt($cashAdvance);

        if($request->ajax()){
            return \Response::json($result, 200);
        }else{
            return $result;
        }
    }

    public function formSendRequestWithReason(Request $request, $cashAdvanceId)
    {
        $title = ''; $headingText = ''; $text = '';
        $cashAdvance = CashAdvance::find($cashAdvanceId);
        if(!$cashAdvance){ return ; }

        $resultOverBudget = RequestRepo::validateIsNotOverBudget($cashAdvance);
        $resultExceedPolicy = RequestRepo::validateIsNotExceedPolicy($cashAdvance);
        $notOverBudget = $resultOverBudget['valid'];
        $notExceedPolicy = $resultExceedPolicy['valid'];
        if(!$notOverBudget && !$notExceedPolicy){
            $title = 'Request Over Budget & Exceed Policy !';
            $text = 'please enter reason for request over budget and exceed policy, please contact HR for approval and <strong><u>send the original receipt and supporting document to Finance Dept. (กรุณาส่งเอกสารให้แผนกการเงินเพื่อดำเนินการ)</u></strong>.';
        }elseif(!$notOverBudget){
            $title = 'Request Over Budget !';
            $text = 'please enter reason for request over budget, please contact HR for approval and <strong><u>send the original receipt and supporting document to Finance Dept. (กรุณาส่งเอกสารให้แผนกการเงินเพื่อดำเนินการ)</u></strong>.';
        }elseif(!$notExceedPolicy){
            $title = 'Request Exceed Policy !';
            $text = 'please enter reason for request exceed policy please contact HR for approval and <strong><u>send the original receipt and supporting document to Finance Dept. (กรุณาส่งเอกสารให้แผนกการเงินเพื่อดำเนินการ)</u></strong>.';
        }

        if($cashAdvance->onClearing()){
            $diffCAAndClearingData = self::calDiffCAAndClearing($cashAdvance);
            if($diffCAAndClearingData['type'] == 'paybacktocompany'){
                $headingText = 'Payback to company (ชำระเงินคืนบริษัท) : '.number_format($diffCAAndClearingData['amount'],2).' '.$cashAdvance->currency_id;
            }
        }

        return view('cash-advances.clear._form_send_request_with_reason',
                        compact('cashAdvance','title','headingText','text'));

    }

    private static function hasPermissionSetStatus($cashAdvance,$activity)
    {
        $permit = false;
        switch ($activity) {

            case "UNBLOCK":
                if($cashAdvance->status == 'BLOCKED'
                    && \Auth::user()->isUnblocker()){
                    $permit = true;
                }
                break;
            case "SEND_REQUEST":
                if($cashAdvance->status == 'NEW_REQUEST'
                    && $cashAdvance->isRequester()){
                    $permit = true;
                }
                break;
            case "APPROVER_APPROVE":
                if($cashAdvance->status == 'APPROVER_DECISION'
                    && $cashAdvance->isNextApprover()){
                    $permit = true;
                }
                break;
            case "APPROVER_SENDBACK":
                if($cashAdvance->status == 'APPROVER_DECISION'
                    && $cashAdvance->isNextApprover()){
                    $permit = true;
                }
                break;
            case "APPROVER_REJECT":
                if($cashAdvance->status == 'APPROVER_DECISION'
                    && $cashAdvance->isNextApprover()){
                    $permit = true;
                }
                break;
            case "FINANCE_APPROVE":
                if($cashAdvance->status == 'FINANCE_PROCESS'
                    && \Auth::user()->isFinance()){
                    $permit = true;
                }
                break;
            case "FINANCE_SENDBACK":
                if($cashAdvance->status == 'FINANCE_PROCESS'
                    && \Auth::user()->isFinance()){
                    $permit = true;
                }
                break;
            case "FINANCE_REJECT":
                if($cashAdvance->status == 'FINANCE_PROCESS'
                    && \Auth::user()->isFinance()){
                    $permit = true;
                }
                break;

            // #### CLEARING REQUEST ####

            case "CLEARING_SEND_REQUEST":
                if($cashAdvance->status == 'CLEARING_REQUEST'
                    && $cashAdvance->isRequester()){
                    $permit = true;
                }
                break;
            case "CLEARING_APPROVER_APPROVE":
                if($cashAdvance->status == 'CLEARING_APPROVER_DECISION'
                    && $cashAdvance->isNextClearingApprover()){
                    $permit = true;
                }
                break;
            case "CLEARING_APPROVER_SENDBACK":
                if($cashAdvance->status == 'CLEARING_APPROVER_DECISION'
                    && $cashAdvance->isNextClearingApprover()){
                    $permit = true;
                }
                break;
            case "CLEARING_FINANCE_APPROVE":
                if($cashAdvance->status == 'CLEARING_FINANCE_PROCESS'
                    && \Auth::user()->isFinance()){
                    $permit = true;
                }
                break;
            case "CLEARING_FINANCE_SENDBACK":
                if($cashAdvance->status == 'CLEARING_FINANCE_PROCESS'
                    && \Auth::user()->isFinance()){
                    $permit = true;
                }
                break;
            case "CANCEL_REQUEST":
                if(($cashAdvance->status == 'NEW_REQUEST' || $cashAdvance->status == 'BLOCKED')
                    && $cashAdvance->isRequester()){
                    $permit = true;
                }
                break;

        }
        return $permit;
    }

    private static function resetApproval($activity,$cashAdvance)
    {
        switch ($activity) {
            case "APPROVER_SENDBACK":
                // RESET RECENT APPROVAL PROCESS
                $approvalRepo = new ApprovalRepo;
                $approvalRepo->reset($cashAdvance,'CASH-ADVANCE');
                break;
            case "FINANCE_SENDBACK":
                // RESET RECENT APPROVAL PROCESS
                $approvalRepo = new ApprovalRepo;
                $approvalRepo->reset($cashAdvance,'CASH-ADVANCE');
                break;
            case "CLEARING_APPROVER_SENDBACK":
                // RESET RECENT CLEARING APPROVAL PROCESS
                $approvalRepo = new ApprovalRepo;
                $approvalRepo->reset($cashAdvance,'CLEARING');
                break;
            case "CLEARING_FINANCE_SENDBACK":
                // RESET RECENT CLEARING APPROVAL PROCESS
                $approvalRepo = new ApprovalRepo;
                $approvalRepo->reset($cashAdvance,'CLEARING');
                break;
        }
    }

    private static function sendEmailByActivity($activity,$cashAdvance,$reason)
    {
        $financeUsers = User::active()->isFinance()->get(); // FINANCE
        $composedFinanceUsers = MailRepo::composeReceivers($financeUsers);

        switch ($activity) {

            ///////////////////////
            // #### CA REQUEST ####
            case "UNBLOCK":

                $receivers = MailRepo::composeReceivers($cashAdvance->user); // REQUESTER
                $ccReceivers = MailRepo::composeReceivers($financeUsers); // FINANCE

                MailRepo::unblock('CASH-ADVANCE',$cashAdvance,$receivers,$ccReceivers,$reason);

                break;

            case "SEND_REQUEST":

                $ccReceivers = MailRepo::composeReceivers(\Auth::user()); // REQUESTER
                if($cashAdvance->approver){
                    // IF HAVE APPROVER
                    $receivers = MailRepo::composeReceivers($cashAdvance->approver); // NEXT APPROVER
                    if(count($composedFinanceUsers) > 0){
                        foreach ($composedFinanceUsers as $composedFinanceUser) {
                            array_push($ccReceivers, $composedFinanceUser);
                        }
                    }
                    MailRepo::sendRequest('CASH-ADVANCE',$cashAdvance,$receivers,$ccReceivers,$reason);
                }else{
                    // IF NOT HAVE APPROVER
                    $receivers = MailRepo::composeReceivers($financeUsers);
                    MailRepo::sendRequest('CASH-ADVANCE',$cashAdvance,$receivers,$ccReceivers,$reason,'TO-FINANCE-DEPT');
                }

                break;

            case "APPROVER_APPROVE":

                $ccReceivers = MailRepo::composeReceivers($cashAdvance->user); // REQUESTER
                $relatedApprovers = ApprovalRepo::getRelatedApprovers($cashAdvance,'CASH-ADVANCE','APPROVER'); // RELATED APPROVERS
                $relatedApproverCCReceivers = MailRepo::composeReceivers($relatedApprovers);
                if(count($relatedApproverCCReceivers) > 0){
                    foreach ($relatedApproverCCReceivers as $relatedApproverCCReceiver) {
                        array_push($ccReceivers, $relatedApproverCCReceiver);
                    }
                }

                // IF FOUND NEXT APPROVER
                if($cashAdvance->next_approver_id){
                    $receivers = MailRepo::composeReceivers($cashAdvance->approver); // NEXT APPROVER
                    if(count($composedFinanceUsers) > 0){
                        foreach ($composedFinanceUsers as $composedFinanceUser) {
                            array_push($ccReceivers, $composedFinanceUser);
                        }
                    }
                    MailRepo::approverProcess('CASH-ADVANCE',$cashAdvance,'APPROVE',$receivers,$ccReceivers,$reason);

                // IF APPROVER APPROVE COMPLETED
                }else{
                    $receivers = MailRepo::composeReceivers($financeUsers); // FINANCE;
                    MailRepo::approverProcess('CASH-ADVANCE',$cashAdvance,'APPROVE',$receivers,$ccReceivers,$reason,'TO-FINANCE-DEPT');
                }

                break;

            case "APPROVER_SENDBACK":

                $receivers = MailRepo::composeReceivers($cashAdvance->user); // REQUESTER;
                $relatedApprovers = ApprovalRepo::getRelatedApprovers($cashAdvance,'CASH-ADVANCE','APPROVER'); // RELATED APPROVERS
                $ccReceivers = MailRepo::composeReceivers($relatedApprovers);
                if(count($composedFinanceUsers) > 0){
                    foreach ($composedFinanceUsers as $composedFinanceUser) {
                        array_push($ccReceivers, $composedFinanceUser);
                    }
                }

                MailRepo::approverProcess('CASH-ADVANCE',$cashAdvance,'SENDBACK',$receivers,$ccReceivers,$reason);

                break;

            case "APPROVER_REJECT":

                $receivers = MailRepo::composeReceivers($cashAdvance->user); // REQUESTER;
                $relatedApprovers = ApprovalRepo::getRelatedApprovers($cashAdvance,'CASH-ADVANCE','APPROVER'); // RELATED APPROVERS
                $ccReceivers = MailRepo::composeReceivers($relatedApprovers);
                if(count($composedFinanceUsers) > 0){
                    foreach ($composedFinanceUsers as $composedFinanceUser) {
                        array_push($ccReceivers, $composedFinanceUser);
                    }
                }

                MailRepo::approverProcess('CASH-ADVANCE',$cashAdvance,'REJECT',$receivers,$ccReceivers,$reason);

                break;

            case "FINANCE_APPROVE":

                $receivers = MailRepo::composeReceivers($cashAdvance->user); // REQUESTER;
                $ccReceivers = MailRepo::composeReceivers($financeUsers); // FINANCE
                $relatedApprovers = ApprovalRepo::getRelatedApprovers($cashAdvance,'CASH-ADVANCE','APPROVER'); // RELATED APPROVERS
                $relatedApproverCCReceivers = MailRepo::composeReceivers($relatedApprovers);
                if(count($relatedApproverCCReceivers) > 0){
                    foreach ($relatedApproverCCReceivers as $relatedApproverCCReceiver) {
                        array_push($ccReceivers, $relatedApproverCCReceiver);
                    }
                }

                MailRepo::financeProcess('CASH-ADVANCE',$cashAdvance,'APPROVE',$receivers,$ccReceivers);

                break;

            case "FINANCE_SENDBACK":

                $receivers = MailRepo::composeReceivers($cashAdvance->user); // REQUESTER;
                $ccReceivers = MailRepo::composeReceivers($financeUsers); // FINANCE
                $relatedApprovers = ApprovalRepo::getRelatedApprovers($cashAdvance,'CASH-ADVANCE','APPROVER'); // RELATED APPROVERS
                $relatedApproverCCReceivers = MailRepo::composeReceivers($relatedApprovers);
                if(count($relatedApproverCCReceivers) > 0){
                    foreach ($relatedApproverCCReceivers as $relatedApproverCCReceiver) {
                        array_push($ccReceivers, $relatedApproverCCReceiver);
                    }
                }

                MailRepo::financeProcess('CASH-ADVANCE',$cashAdvance,'SENDBACK',$receivers,$ccReceivers,$reason);

                break;

            case "FINANCE_REJECT":

                $receivers = MailRepo::composeReceivers($cashAdvance->user); // REQUESTER;
                $ccReceivers = MailRepo::composeReceivers($financeUsers); // FINANCE
                $relatedApprovers = ApprovalRepo::getRelatedApprovers($cashAdvance,'CASH-ADVANCE','APPROVER'); // RELATED APPROVERS
                $relatedApproverCCReceivers = MailRepo::composeReceivers($relatedApprovers);
                if(count($relatedApproverCCReceivers) > 0){
                    foreach ($relatedApproverCCReceivers as $relatedApproverCCReceiver) {
                        array_push($ccReceivers, $relatedApproverCCReceiver);
                    }
                }

                MailRepo::financeProcess('CASH-ADVANCE',$cashAdvance,'REJECT',$receivers,$ccReceivers,$reason);

                break;

            /////////////////////////////
            // #### CLEARING REQUEST ####

            case "CLEARING_SEND_REQUEST":

                $ccReceivers = MailRepo::composeReceivers(\Auth::user()); // REQUESTER
                if($cashAdvance->clearApprover){
                    $receivers = MailRepo::composeReceivers($cashAdvance->clearApprover); // NEXT APPROVER
                    if(count($composedFinanceUsers) > 0){
                        foreach ($composedFinanceUsers as $composedFinanceUser) {
                            array_push($ccReceivers, $composedFinanceUser);
                        }
                    }
                    MailRepo::sendRequest('CLEARING',$cashAdvance,$receivers,$ccReceivers,$reason);

                }else{
                    // IF NOT HAVE APPROVER
                    $receivers = MailRepo::composeReceivers($financeUsers);
                    MailRepo::sendRequest('CLEARING',$cashAdvance,$receivers,$ccReceivers,$reason,'TO-FINANCE-DEPT');
                }
                break;

            case "CLEARING_APPROVER_APPROVE":

                $ccReceivers = MailRepo::composeReceivers($cashAdvance->user); // REQUESTER
                $relatedApprovers = ApprovalRepo::getRelatedApprovers($cashAdvance,'CLEARING','APPROVER'); // RELATED APPROVERS
                $relatedApproverCCReceivers = MailRepo::composeReceivers($relatedApprovers);
                if(count($relatedApproverCCReceivers) > 0){
                    foreach ($relatedApproverCCReceivers as $relatedApproverCCReceiver) {
                        array_push($ccReceivers, $relatedApproverCCReceiver);
                    }
                }
                // IF FOUND NEXT CLEARING APPROVER
                if($cashAdvance->next_clearing_approver_id){
                    $receivers = MailRepo::composeReceivers($cashAdvance->clearApprover); // NEXT APPROVER
                    if(count($composedFinanceUsers) > 0){
                        foreach ($composedFinanceUsers as $composedFinanceUser) {
                            array_push($ccReceivers, $composedFinanceUser);
                        }
                    }
                    MailRepo::approverProcess('CLEARING',$cashAdvance,'APPROVE',$receivers,$ccReceivers,$reason);

                // IF CLEARING APPROVER APPROVE COMPLETED
                }else{
                    $receivers = MailRepo::composeReceivers($financeUsers); // FINANCE;
                    MailRepo::approverProcess('CLEARING',$cashAdvance,'APPROVE',$receivers,$ccReceivers,$reason,'TO-FINANCE-DEPT');
                }

                break;

            case "CLEARING_APPROVER_SENDBACK":

                $receivers = MailRepo::composeReceivers($cashAdvance->user); // REQUESTER;
                $relatedApprovers = ApprovalRepo::getRelatedApprovers($cashAdvance,'CLEARING','APPROVER'); // RELATED APPROVERS
                $ccReceivers = MailRepo::composeReceivers($relatedApprovers);
                if(count($composedFinanceUsers) > 0){
                    foreach ($composedFinanceUsers as $composedFinanceUser) {
                        array_push($ccReceivers, $composedFinanceUser);
                    }
                }

                MailRepo::approverProcess('CLEARING',$cashAdvance,'SENDBACK',$receivers,$ccReceivers,$reason);

                break;

            case "CLEARING_FINANCE_APPROVE":

                $receivers = MailRepo::composeReceivers($cashAdvance->user); // REQUESTER;
                $ccReceivers = MailRepo::composeReceivers($financeUsers); // FINANCE
                $relatedApprovers = ApprovalRepo::getRelatedApprovers($cashAdvance,'CLEARING','APPROVER'); // RELATED APPROVERS
                $relatedApproverCCReceivers = MailRepo::composeReceivers($relatedApprovers);
                if(count($relatedApproverCCReceivers) > 0){
                    foreach ($relatedApproverCCReceivers as $relatedApproverCCReceiver) {
                        array_push($ccReceivers, $relatedApproverCCReceiver);
                    }
                }

                MailRepo::financeProcess('CLEARING',$cashAdvance,'APPROVE',$receivers,$ccReceivers);

                break;

            case "CLEARING_FINANCE_SENDBACK":

                $receivers = MailRepo::composeReceivers($cashAdvance->user); // REQUESTER;
                $ccReceivers = MailRepo::composeReceivers($financeUsers); // FINANCE
                $relatedApprovers = ApprovalRepo::getRelatedApprovers($cashAdvance,'CLEARING','APPROVER'); // RELATED APPROVERS
                $relatedApproverCCReceivers = MailRepo::composeReceivers($relatedApprovers);
                if(count($relatedApproverCCReceivers) > 0){
                    foreach ($relatedApproverCCReceivers as $relatedApproverCCReceiver) {
                        array_push($ccReceivers, $relatedApproverCCReceiver);
                    }
                }

                MailRepo::financeProcess('CLEARING',$cashAdvance,'SENDBACK',$receivers,$ccReceivers,$reason);

                break;

        }
    }

    public function export(Request $request)
    {
        $yearShowing = $request->year_showing;
        $exportType = $request->type;
        $search = [
            'document_no'=>$request->document_no,
            'status'=>$request->status
        ];
        $search_date = [
            'date_from'=>$request->date_from,
            'date_to'=>$request->date_to
        ];
        $cashAdvances = CashAdvance::orderBy('created_at','desc')
                        ->whereOrgId($this->orgId)
                        ->byRelatedUser()
                        ->byYearShowing($yearShowing)
                        ->with('user')
                        ->search($search,$search_date)
                        ->get();
        // if(!$cashAdvances){ abort(403); }

        if($exportType == 'CASH-ADVANCE'){

            \Excel::create('CA_'.Carbon::now(), function($excel) use ($cashAdvances) {

                $excel->sheet('Report', function($sheet) use ($cashAdvances) {

                    $sheet->loadView('cash-advances.export._template', compact('cashAdvances'));

                });

            })->export('xlsx');

        }elseif($exportType == 'CLEARING'){

            $establishmentLists = Establishment::whereOrgId($this->orgId)->pluck('establishment_name','establishment_id')->all();
            $vendorLists = Vendor::notEmp()->whereOrgId($this->orgId)->pluck('vendor_name','vendor_id')->all();
            // $projectLists = FNDListOfValues::select(\DB::raw("description || ' (' || flex_value || ')' AS full_description"),'flex_value')->project()->orderBy('flex_value')->pluck('full_description','flex_value')->all();
            // $rechargeLists = FNDListOfValues::select(\DB::raw("description || ' (' || flex_value || ')' AS full_description"),'flex_value')->interCompany()->orderBy('flex_value')->pluck('full_description','flex_value')->all();

            $branchLists = FNDListOfValues::select(\DB::raw("description || ' (' || flex_value || ')' AS full_description"),'flex_value')->branch($this->orgId)->orderBy('flex_value')->pluck('full_description','flex_value')->all();
            $departmentLists = FNDListOfValues::select(\DB::raw("description || ' (' || flex_value || ')' AS full_description"),'flex_value')->department($this->orgId)->orderBy('flex_value')->pluck('full_description','flex_value')->all();

            \Excel::create('CC_'.Carbon::now(), function($excel) use ($cashAdvances,$establishmentLists,$vendorLists,$branchLists,$departmentLists) {

                $excel->sheet('Report', function($sheet) use ($cashAdvances,$establishmentLists,$vendorLists,$branchLists,$departmentLists) {

                    $sheet->loadView('cash-advances.export._template_clearing', compact('cashAdvances','establishmentLists','vendorLists','projectLists','rechargeLists','branchLists','departmentLists'));

                });

            })->export('xlsx');

        }
    }

    public function duplicate(Request $request, $cashAdvanceId)
    {
        // GET PENDING TRASACTION
        $pendingRequestLists = RequestRepo::getPendingOverLimitDayRequest(\Auth::user()->id);

        \DB::beginTransaction();
        try {
            // SELECTED CASH ADVANCE FOR DUPLICATE
            $cashAdvance = CashAdvance::find($cashAdvanceId);
            // NEW CASH-ADVANCE
            $newCashAdvance = new CashAdvance();
            $newCashAdvance->org_id = $this->orgId;
            // $newCashAdvance->organization_id = \Auth::user()->employee->organization_id;
            $documentNo = CashAdvance::genDocumentNo($this->orgId);
            $newCashAdvance->document_no = $documentNo['CA'];
            $newCashAdvance->clearing_document_no = $documentNo['CL'];
            $newCashAdvance->user_id = \Auth::user()->id;
            // $newCashAdvance->position_id = $request->position_id;
            $newCashAdvance->ca_category_id = $cashAdvance->ca_category_id;
            $newCashAdvance->ca_sub_category_id = $cashAdvance->ca_sub_category_id;
            $newCashAdvance->need_by_date = $cashAdvance->need_by_date;
            $newCashAdvance->amount = $cashAdvance->amount;
            $newCashAdvance->currency_id  = $cashAdvance->currency_id;
            $newCashAdvance->purpose  = $cashAdvance->purpose;
            if(count($pendingRequestLists) == 0){
                $newCashAdvance->status = "NEW_REQUEST";
            }else{ // BLOCKED IF HAS PENDING REQUEST
                $newCashAdvance->status = "BLOCKED";
            }
            $newCashAdvance->save();

            // NEW CASH-ADVANCE INFOS
            $infos = CASubCategoryInfo::where('ca_sub_category_id',$newCashAdvance->ca_sub_category_id)->active()->get();

            $cashAdvaneInfos = $cashAdvance->infos->pluck('description','ca_sub_category_info_id')->all();

            if(count($infos)>0){
                foreach($infos as $info){
                    if(array_key_exists($info->id, $cashAdvaneInfos)){
                        $newCashAdvaneInfo = new CashAdvanceInfo();
                        $newCashAdvaneInfo->cash_advance_id = $newCashAdvance->id;
                        $newCashAdvaneInfo->ca_sub_category_id = $newCashAdvance->ca_sub_category_id;
                        $newCashAdvaneInfo->ca_sub_category_info_id = $info->id;
                        $newCashAdvaneInfo->description = $cashAdvaneInfos[$info->id];
                        $newCashAdvaneInfo->save();
                    }
                }
            }
            // ACTIVITY LOG
            $activityLogRepo = new ActivityLogRepo;
            $activityLogRepo->create($newCashAdvance, ActivityLogRepo::getActivityMessage('NEW_REQUEST'));

            // SUCCESS CREATE CASH ADVANCE
            \DB::commit();
            if($request->ajax()){
                $result['status'] = 'SUCCESS';
                $result['cashAdvanceId'] = $newCashAdvance->id;
                return $result;
            }else{
                return redirect()->route('cash-advances.show',['cashAdvanceId'=>$newCashAdvance->id]);
            }

        } catch (\Exception $e) {
            // ERROR CREATE CASH ADVANCE
            \DB::rollBack();
            if($request->ajax()){
                $result['status'] = 'ERROR';
                $result['err_msg'] = $e->getMessage();
                return $result;
            }else{
                // throw new \Exception($e->getMessage(), 1);
                \Log::error($e->getMessage());
                return abort('403');
            }
        }

    }

}
