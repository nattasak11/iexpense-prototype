<?php

namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;

use App\Http\Requests\UserUpdateRequest;
use App\Repositories\UserRepo;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Employee;

class UserController extends Controller
{
    protected $orgId;
    protected $perPage = 20;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->orgId = \Auth::user()->org_id;
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('name')->with('employee')->paginate($this->perPage);
        $employees = Employee::whereOrgId($this->orgId)->get();

        return view('settings.users.index', compact('users','employees'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roleLists = ['user'=>'USER','finance'=>'FINANCE','admin'=>'ADMIN'];

        return view('settings.users._modal_edit_form',compact('user','roleLists'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
        $user = User::find($id);
        $user->role = $request->role;
        $user->active = $request->active ? true : false;
        $user->save();

        return redirect()->route('settings.users.index');
    }


    public function syncAllUsers(Request  $request)
    {
        try {

            // SYNC TO GET USER DATA FROM ORACLE EMPLOYEE
            UserRepo::syncAll();

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), 1);
        }
        if($request->ajax()) {

            return \Response::json("success", 200);

        }else{
            return redirect()->back()->withInput();
        }
    }

}
