<?php

namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;

use App\Http\Requests\CASubCategoriesStoreRequest;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\CACategory;
use App\CASubCategory;
use App\Policy;
use App\Preference;
use App\AccountInfo;
use App\FNDListOfValues;
use App\VAT;
use App\AccessibleOrg;
use App\HrOperatingUnit;

class CASubCategoryController extends Controller
{
    protected $perPage = 10;
    protected $orgId;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->orgId = \Auth::user()->org_id;
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(CACategory $ca_category)
    {

        $ca_sub_categories = CASubCategory::where('ca_category_id',$ca_category->id)
                                // ->accessibleOrg($this->orgId)
                                ->paginate($this->perPage);
        $operatingUnits = HrOperatingUnit::all();

        return view('settings.ca_sub_categories.index',
        	compact('ca_category',
        			'ca_sub_categories',
                    'operatingUnits'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  SubCategory $ca_sub_category
     * @return \Illuminate\Http\Response
     */
    public function create(CACategory $ca_category)
    {
        $defaultSubAccountCode = '';
        $accountLists = FNDListOfValues::select(\DB::raw("CONCAT(description,' (',flex_value,')') AS full_description"),'flex_value')->account($this->orgId)->orderBy('flex_value')->pluck('full_description','flex_value')->all();
        $VATLists = VAT::apVat()->where('org_id',$this->orgId)->pluck('tax','tax_rate_code')->all();
        $branchLists = FNDListOfValues::select(\DB::raw("CONCAT(description,' (',flex_value,')') AS full_description"),'flex_value')->branch($this->orgId)->orderBy('flex_value')->pluck('full_description','flex_value')->all();
        $departmentLists = FNDListOfValues::select(\DB::raw("CONCAT(description,' (',flex_value,')') AS full_description"),'flex_value')->department($this->orgId)->orderBy('flex_value')->pluck('full_description','flex_value')->all();
        $operatingUnits = HrOperatingUnit::all();

        return view('settings.ca_sub_categories.create',
            compact('ca_category',
                    'accountLists',
                    'defaultSubAccountCode',
                    'VATLists',
                    'branchLists',
                    'departmentLists',
                    'operatingUnits'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  SubCategory $ca_sub_category
     * @return \Illuminate\Http\Response
     */
    public function store(CASubCategoriesStoreRequest $request, CACategory $ca_category)
    {
        $ca_sub_category  = new CASubCategory();
        // $ca_sub_category->org_id = $this->orgId;
        $ca_sub_category->ca_category_id = $ca_category->id;
        $ca_sub_category->name = $request->name;
        $ca_sub_category->description = $request->description;
        $ca_sub_category->start_date = \DateTime::createFromFormat(trans('date.format'), $request->start_date)->format('Y-m-d');
        if($request->end_date){
            $ca_sub_category->end_date = \DateTime::createFromFormat(trans('date.format'), $request->end_date)->format('Y-m-d');
        }else{
            $ca_sub_category->end_date = null;
        }
        $ca_sub_category->account_code = $request->account_code;
        $ca_sub_category->sub_account_code = $request->sub_account_code;
        $ca_sub_category->vat_id = $request->vat_id;
        $ca_sub_category->branch_code = $request->branch_code ? $request->branch_code : null;
        $ca_sub_category->department_code = $request->department_code ? $request->department_code : null;
        $ca_sub_category->required_attachment = $request->required_attachment ? true : false ;
        if($request->ca_min_amount){
            $ca_sub_category->check_ca_min =  true;
            $ca_sub_category->ca_min_amount = $request->ca_min_amount;
        }else{
            $ca_sub_category->check_ca_min =  false;
            $ca_sub_category->ca_min_amount = null;
        }
        if($request->ca_max_amount){
            $ca_sub_category->check_ca_max =  true;
            $ca_sub_category->ca_max_amount = $request->ca_max_amount;
        }else{
            $ca_sub_category->check_ca_max =  false;
            $ca_sub_category->ca_max_amount = null;
        }
        $ca_sub_category->active = $request->active ? true : false ;
        $ca_sub_category->save();

        // SAVE ACCESIBLE ORG
        foreach($request->accessible_orgs as $accessible_org_id){
            $accessibleOrg = new AccessibleOrg();
            $accessibleOrg->org_id = $accessible_org_id;
            $ca_sub_category->accessibleOrgs()->save($accessibleOrg);
        }

        return redirect()->route('settings.ca_sub_categories.index',[$ca_category->id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  SubCategory $ca_sub_category
     * @return \Illuminate\Http\Response
     */
    public function edit(CACategory $ca_category, CASubCategory $ca_sub_category)
    {
        $ca_sub_category->accessible_orgs = $ca_sub_category->accessibleOrgs()->pluck('org_id')->all();
        $accountLists = FNDListOfValues::select(\DB::raw("CONCAT(description,' (',flex_value,')') AS full_description"),'flex_value')->account($this->orgId)->orderBy('flex_value')->pluck('full_description','flex_value')->all();
        $ca_sub_category->start_date = dateFormatDisplay($ca_sub_category->start_date);
        $ca_sub_category->end_date = dateFormatDisplay($ca_sub_category->end_date);
        $ca_sub_category->accessible_orgs = $ca_sub_category->accessibleOrgs()->pluck('org_id')->all();

        $defaultSubAccountCode = $ca_sub_category->sub_account_code;
        $VATLists = VAT::apVat()->where('org_id',$this->orgId)->pluck('tax','tax_rate_code')->all();

        $branchLists = FNDListOfValues::select(\DB::raw("CONCAT(description,' (',flex_value,')') AS full_description"),'flex_value')->branch($this->orgId)->orderBy('flex_value')->pluck('full_description','flex_value')->all();
        $departmentLists = FNDListOfValues::select(\DB::raw("CONCAT(description,' (',flex_value,')') AS full_description"),'flex_value')->department($this->orgId)->orderBy('flex_value')->pluck('full_description','flex_value')->all();
        $operatingUnits = HrOperatingUnit::all();

        return view('settings.ca_sub_categories.edit',
            compact('ca_category',
                    'ca_sub_category',
                    'accountLists',
                    'defaultSubAccountCode',
                    'VATLists',
                    'branchLists',
                    'departmentLists',
                    'operatingUnits'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  SubCategory $ca_sub_category
     * @return \Illuminate\Http\Response
     */
    public function update(CASubCategoriesStoreRequest $request, CACategory $ca_category, CASubCategory $ca_sub_category)
    {
        $ca_sub_category->name = $request->name;
        $ca_sub_category->description = $request->description;
        $ca_sub_category->start_date = \DateTime::createFromFormat(trans('date.format'), $request->start_date)->format('Y-m-d');
        if($request->end_date){
            $ca_sub_category->end_date = \DateTime::createFromFormat(trans('date.format'), $request->end_date)->format('Y-m-d');
        }else{
            $ca_sub_category->end_date = null;
        }

        $ca_sub_category->account_code = $request->account_code;
        $ca_sub_category->sub_account_code = $request->sub_account_code;
        $ca_sub_category->vat_id = $request->vat_id;
        $ca_sub_category->branch_code = $request->branch_code ? $request->branch_code : null;
        $ca_sub_category->department_code = $request->department_code ? $request->department_code : null;
        if($request->ca_min_amount){
            $ca_sub_category->check_ca_min =  true;
            $ca_sub_category->ca_min_amount = $request->ca_min_amount;
        }else{
            $ca_sub_category->check_ca_min =  false;
            $ca_sub_category->ca_min_amount = null;
        }
        if($request->ca_max_amount){
            $ca_sub_category->check_ca_max =  true;
            $ca_sub_category->ca_max_amount = $request->ca_max_amount;
        }else{
            $ca_sub_category->check_ca_max =  false;
            $ca_sub_category->ca_max_amount = null;
        }
        $ca_sub_category->required_attachment = $request->required_attachment ? true : false ;
        $ca_sub_category->active = $request->active ? true : false ;
        $ca_sub_category->save();

        $oldAccessibleOrgs = $ca_sub_category->accessibleOrgs()->pluck('org_id')->all();
        if($request->accessible_orgs != $oldAccessibleOrgs){
            // DELETE OLD ACCESIBLE ORG
            foreach($ca_sub_category->accessibleOrgs as $accessibleOrg){
                $accessibleOrg->delete();
            }
            // SAVE NEW ACCESIBLE ORG
            foreach($request->accessible_orgs as $accessible_org_id){
                $accessibleOrg = new AccessibleOrg();
                $accessibleOrg->org_id = $accessible_org_id;
                $ca_sub_category->accessibleOrgs()->save($accessibleOrg);
            }
        }

        return redirect()->route('settings.ca_sub_categories.index',[$ca_category->id]);
    }

    public function inputSubAccountCode(Request $request, CACategory $ca_category)
    {
        $accountInfo = AccountInfo::whereOrgId($this->orgId)
                        ->subAccount()
                        ->first();
        $accountCode = $request->account_code;
        $subAccountCode = $request->sub_account_code;
        $subAccountLists = [''=>'-'];
        if($accountCode){
            $subAccountLists = FNDListOfValues::select(\DB::raw("CONCAT(description,' (',flex_value,')') AS full_description"),'flex_value')->subAccount($this->orgId)->byParentValue($accountInfo->parent_flex_value_set_name,$accountCode)->orderBy('flex_value')->pluck('full_description','flex_value')->all();
        }
        return view('settings.ca_sub_categories._ddl_sub_account_code',
            compact('subAccountLists','subAccountCode'));
    }
}
