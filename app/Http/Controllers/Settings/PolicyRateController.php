<?php

namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;

use App\Http\Requests\PolicyRateStoreRequest;
use App\Http\Requests\PolicyRateUpdateRequest;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Category;
use App\SubCategory;
use App\Policy;
use App\PolicyRate;
use App\Location;
use App\FNDListOfValues;
use App\Preference;
use App\Currency;
use App\MileageUnit;

class PolicyRateController extends Controller
{
    protected $orgId;
    protected $perPage = 20;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->orgId = \Auth::user()->org_id;
            return $next($request);
        });
    }
    
    public function index(Category $category, SubCategory $sub_category, Policy $policy)
    {
    	$rates = PolicyRate::wherePolicyId($policy->id)
                        // ->whereOrgId($this->orgId)
                        ->active()
                        ->orderBy('rate')
                        ->paginate($this->perPage);

    	// LISTS FOR CREATE
    	$positionLists = FNDListOfValues::select(\DB::raw("CONCAT(description,' (',flex_value,')') AS full_description"),'flex_value')->poLevel()->pluck('full_description','flex_value')->all();
    	$locationLists = Location::accessibleOrg($this->orgId)->active()->pluck('name','id')->all();

        $baseCurrencyId = Preference::getBaseCurrency();

        $mileageUnitLists = MileageUnit::active()->pluck('code','id')->all();
        $baseMileageUnit = Preference::getBaseMileageUnit();

    	return view('settings.policies.rates.index',
                        compact('rates',
                        		'policy',
                                'category',
                                'sub_category',
                                'positionLists',
                                'locationLists',
                                'baseCurrencyId',
                                'mileageUnitLists',
                                'baseMileageUnit'));
    }

    public function store(PolicyRateStoreRequest $request,Category $category, SubCategory $sub_category, Policy $policy)
    {
        try {
        	// CHECK DUPLICATE RATE
        	$isDuplicate = $this->checkDuplicateRate($policy,$request);
        	if($isDuplicate){
        		return redirect()->route('settings.policies.rates.index',[$category->id, $sub_category->id, $policy->id])->withErrors(['this rate is already created.']);
        	}
            
    		// INSERT TO TABLE
        	$rate = new PolicyRate();
            // $rate->org_id = $this->orgId;
        	$rate->category_id = $category->id;
        	$rate->sub_category_id = $sub_category->id;
        	$rate->policy_id = $policy->id;
        	$rate->position_po_level = $request->position_po_level ? $request->position_po_level : null;
        	$rate->location_id = $request->location_id ? $request->location_id : null;
            if($request->unlimit){
                $rate->unlimit = true;
                $rate->rate = null;
            }else{
                $rate->unlimit = false;
                $rate->rate = $request->rate;
            }
            $rate->currency_id = $request->currency_id;
        	$rate->save();

        } catch (\Exception $e) {
            return redirect()->route('settings.policies.rates.index',[$category->id, $sub_category->id, $policy->id])->withErrors([$e->getMessage()]);
        }

        return redirect()->route('settings.policies.rates.index',[$category->id, $sub_category->id, $policy->id]);
	
    }

    public function edit(Category $category, SubCategory $sub_category, Policy $policy, $rateId)
    {
        $rate = PolicyRate::find($rateId);

        // LISTS FOR CREATE
        $positionLists = FNDListOfValues::select(\DB::raw("CONCAT(description,' (',flex_value,')') AS full_description"),'flex_value')->poLevel()->pluck('full_description','flex_value')->all();
        $locationLists = Location::accessibleOrg($this->orgId)->active()->pluck('name','id')->all();
        
        $baseCurrencyId = Preference::getBaseCurrency();

        $mileageUnitLists = MileageUnit::active()->pluck('code','id')->all();
        $baseMileageUnit = Preference::getBaseMileageUnit();

        return view('settings.policies.rates._modal_edit_form',
                        compact('rate',
                                'policy',
                                'category',
                                'sub_category',
                                'positionLists',
                                'locationLists',
                                'baseCurrencyId',
                                'mileageUnitLists',
                                'baseMileageUnit'));
    }

    public function update(PolicyRateUpdateRequest $request,Category $category, SubCategory $sub_category, Policy $policy, $rateId)
    {
        try {
            $rate = PolicyRate::find($rateId);
            // UPDATE TABLE PolicyRate
            if($request->unlimit){
                $rate->unlimit = true;
                $rate->rate = null;
            }else{
                $rate->unlimit = false;
                $rate->rate = $request->rate;
            }
            $rate->currency_id = $request->currency_id;
            $rate->save();

        } catch (\Exception $e) {
            return redirect()->route('settings.policies.rates.index',[$category->id, $sub_category->id, $policy->id])->withErrors([$e->getMessage()]);
        }

        return redirect()->route('settings.policies.rates.index',[$category->id, $sub_category->id, $policy->id]);
    }

    public function inactive(Request $request,Category $category, SubCategory $sub_category, Policy $policy, $rateId)
    {
        try {
            $rate = PolicyRate::find($rateId);
            $rate->active = !$rate->active;
            $rate->save();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), 1);
        }
    }

    private function checkDuplicateRate($policy,$request)
    {
    	$rates = PolicyRate::active()->wherePolicyId($policy->id);
        if($request->position_po_level){
            $rates->where('position_po_level',$request->position_po_level);
        }else{
            $rates->whereNull('position_po_level');
        }
    	if($request->location_id){
            $rates->whereLocationId($request->location_id);
        }else{
            $rates->whereNull('location_id');
        }
        // IF THIS RATE EXIST 
    	if($rates->count() > 0){
            return true;
        }
        return false;
    }


}
