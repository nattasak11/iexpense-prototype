<?php

namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;

use App\Http\Requests\PolicyStoreRequest;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Category;
use App\SubCategory;
use App\Policy;
use App\PolicyRate;
use App\Preference;
use App\Currency;
use App\MileageUnit;

class PolicyController extends Controller
{
    protected $orgId;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->orgId = \Auth::user()->org_id;
            return $next($request);
        });
    }
    
    //
    public function index(Category $category, SubCategory $sub_category)
    {
    	// $policies = Policy::whereSubCategoryId($sub_category->id)->active()->get();
        $policy_expense = Policy::whereSubCategoryId($sub_category->id)->where('type','EXPENSE')->active()->first();
        $policy_mileage = Policy::whereSubCategoryId($sub_category->id)->where('type','MILEAGE')->active()->first();
    	return view('settings.policies.index',
                        compact('policy_expense',
                                'policy_mileage',
                                'category',
                                'sub_category'));
    }

    public function create(Request $request, Category $category, SubCategory $sub_category)
    {
        // get preference currency
        $baseCurrencyId = Preference::getBaseCurrency();

        $mileageUnitLists = MileageUnit::active()->pluck('code','id')->all();
        $baseMileageUnit = Preference::getBaseMileageUnit();
        
        $type = $request->type;
        return view('settings.policies._modal_use_policy_form',
                                compact('type',
                                        'category',
                                        'sub_category',
                                        'baseCurrencyId',
                                        'mileageUnitLists',
                                        'baseMileageUnit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PolicyStoreRequest $request,Category $category, SubCategory $sub_category)
    {
        try {

            $policy  = Policy::firstOrNew(
                            [
                             'category_id' => $category->id,
                             'sub_category_id' => $sub_category->id,
                             'type' => $request->type]);
            if($request->type == 'MILEAGE'){
                $policy->mileage_unit = $request->mileage_unit;
            }
            $policy->active = true;
            if($policy->save()){
                // ###################################
                // ## Default Rate ( for any case ) ##
                // ###################################
                $rate = PolicyRate::firstOrNew(
                            [
                             'category_id' => $category->id,
                             'sub_category_id' => $sub_category->id,
                             'policy_id' => $policy->id,
                             'position_po_level' => null, // null = any
                             'location_id' => null]); // null = any
                if($request->unlimit){
                    $rate->unlimit = true;
                    $rate->rate = null;
                }else{
                    $rate->unlimit = false;
                    $rate->rate = $request->rate;
                }
                $rate->currency_id = $request->currency_id;
                $rate->save();
            }

        } catch (\Exception $e) {
            return redirect()->route('settings.policies.index',[
                                    $category->id,
                                    $sub_category->id
                                ])->withErrors([$e->getMessage()]);
        }

        return redirect()->route('settings.policies.index',[
                                    $category->id,
                                    $sub_category->id
                                ]);
    }

    public function inactive(Request $request,Category $category, SubCategory $sub_category, $policyId)
    {
        try {
            $policy = Policy::find($policyId);
            $policy->active = !$policy->active;
            $policy->save();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), 1);
        }
    }

}
