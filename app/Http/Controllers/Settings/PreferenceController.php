<?php

namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Currency;
use App\MileageUnit;
use App\Preference;
use App\User;
use App\Job;

class PreferenceController extends Controller
{
    protected $orgId;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->orgId = \Auth::user()->org_id;
            return $next($request);
        });
    }

    public function index()
    {
        $currency = Preference::getBaseCurrency();
        $mileageUnit = Preference::getBaseMileageUnit();
    	$mileageUnitLists = MileageUnit::active()->pluck('description','id')->all();
        $pendingDayBlocking = Preference::getPendingDayBlocking();
        $jobLists = Job::orderBy('approval_authority','desc')->pluck('job_name','job_id')->all();
        $overBudgetApproverJob = Preference::getOverBudgetApproverJob();
        $unblockers = Preference::getUnblockers();
        $users = User::active()->isFinance()->get();

    	return view('settings.preferences.index',
    					compact('currency',
                                'mileageUnit',
                                'users',
                                'unblockers',
    							'mileageUnitLists',
                                'pendingDayBlocking',
                                'jobLists',
                                'overBudgetApproverJob'));
    }

    public function update(Request $request)
    {
        $code = $request->code;
        $dataValue = $request->data_value;
        $preference = Preference::whereCode($code)->first();
        $preference->data_value = encodeDataValue($preference->data_type,$dataValue);
        $preference->save();
    }

    public function sliceJson(Request $request)
    {
        $code = $request->code;
        $sliceDataValue = $request->data_value;
        $preference = Preference::whereCode($code)->first();
        $jsonData = decodeDataValue($preference->data_type,
                                        $preference->data_value);
        // SLICE DATA IN JSON ARRAY
        if(($key = array_search($sliceDataValue, $jsonData)) !== false) {
            unset($jsonData[$key]);
        }
        $preference->data_value = encodeDataValue($preference->data_type,$jsonData);
        $preference->save();
    }
}
