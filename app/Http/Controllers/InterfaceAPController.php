<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\InterfaceAPHeader;
use App\InterfaceAPLine;

class InterfaceAPController extends Controller
{
    protected $perPage = 20;
	protected $orgId;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->orgId = \Auth::user()->org_id;
            return $next($request);
        });
    }

    public function index(Request $request)
    {
        $search = [
            'request_type'=>$request->request_type,
            'interface_status'=>$request->interface_status
        ];
        $search_date = [
            'date_from'=>$request->date_from,
            'date_to'=>$request->date_to
        ];

    	$interfaceHeaders = InterfaceAPHeader::whereOrgId($this->orgId)
                            ->search($search,$search_date)
                            ->orderBy('created_at','desc')
                            ->paginate($this->perPage);

    	return view('interface_ap.index',compact('interfaceHeaders','search','search_date'));
    }

    public function show(Request $request, $interfaceApHeaderId)
    {
        try {
            
            $interfaceHeader = InterfaceAPHeader::find($interfaceApHeaderId);
            if(!$interfaceHeader){ 
                throw new \Exception("Not Found Interface AP Header Data.", 1);
            }
            $interfaceLines = $interfaceHeader->lines;

            return view('interface_ap._show_information',
                                compact('interfaceHeader','interfaceLines'));

        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return $e->getMessage();
        }
    }

    public function reProcess(Request $request, $interfaceApHeaderId)
    {
    	\DB::beginTransaction();
    	try {
    		
    		$interfaceHeader = InterfaceAPHeader::find($interfaceApHeaderId);
	    	if(!$interfaceHeader){ 
	    		throw new \Exception("Not Found Interface AP Header Data.", 1);
	    	}
	    	$interfaceHeader->interface_status = 'P';
			$interfaceHeader->interface_message = null;
			$interfaceHeader->save();
			if(count($interfaceHeader->lines) > 0){
				foreach ($interfaceHeader->lines as $key => $line) {
					$line->interface_status = 'P';
					$line->interface_message = null;
					$line->save();
				}
			}

			\DB::commit();

	    	if($request->ajax()){
                return 'SUCCESS';
	        }else{
	    		return redirect()->route('interface_ap.index');
	    	}

    	} catch (\Exception $e) {
    		\DB::rollBack();
            \Log::error($e->getMessage());
            if($request->ajax()){
                throw new \Exception($e->getMessage(), 1);
            }else{
                return abort('403');
            }
    	}
    }
}
