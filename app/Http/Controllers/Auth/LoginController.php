<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function loginAD(Request $request)
    {
        if($request->username && $request->password){
            // if(\Adldap::auth()->attempt($request->username, $request->password)){
            // LOGIN PASSED
            // $user = \App\User::findByADUserName($request->username);
            $user = \App\User::where('username',$request->username)->first();
            if($user){
                if($user->active){
                    \Auth::login($user, $request->remember);
                    return redirect()->route('main');
                }else{
                    $err_login = 'login failed, this user was not allowed to access this site, please contact administrator.';
                }
            }else{
                $err_login = 'login failed, not found this user in oracle employee data, please contact oracle system administrator.';
            }
            // }else{
            //     $err_login = 'login failed, these credentials do not match.';
            // }
        }else{
            $err_login = 'login failed, please enter username & password.';
        }
        return redirect()->route('login')->with('err_login',$err_login);
        
    }

    public function username()
    {
        return 'username';
    }

    public function loginById($userId)
    {
        \Auth::loginUsingId($userId);
        return redirect()->route('main');
    }
}
