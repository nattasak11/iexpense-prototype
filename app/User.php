<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Repositories\UserRepo;

class User extends Authenticatable
{
    protected $table = 'xxweb_users';
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function employee(){
        return $this->hasOne('App\Employee', 'person_id', 'oracle_person_id');
    }

    public function scopeActive($query){
        return $query->where('active',true);
    }

    public function scopeIsAdmin($query){
        return $query->where('role','admin');
    }

    public function scopeIsFinance($query){
        return $query->where('role','finance');
    }

    public function scopeIsUser($query){
        return $query->where('role','user');
    }

    public function isAdmin()
    {
        return $this->role == 'admin';
    }

    public function isFinance()
    {
        return $this->role == 'finance';
    }

    public function isUser()
    {
        return $this->role == 'user';
    }

    public function isUnblocker()
    {
        $unblockers = \App\Preference::getUnblockers();
        if(count($unblockers) > 0){
            return in_array($this->id, $unblockers);
        }
        return false;
    }

    public function isAllowCreateRequest()
    {
        $allow = true;
        if($this->employee){
            if(!$this->employee->position_id){
                $allow = false;
            }
            if(!$this->employee->email_address){
                $allow = false;
            }
        }
        return $allow;
    }

    public static function findByADUserName($ADUserName)
    {
        $employee = Employee::where('ad_user_name',$ADUserName)->first();
        if($employee){
            if($employee->person_id){
                $user = User::where('oracle_person_id',$employee->person_id)->first();
                if(!$user){
                    // IF NOT FOUND USER IN WEB SYSTEM >> SYNC WITH ORACLE TO GET EMPLOYEE DATA
                    $user = UserRepo::sync($oraclePersonId);
                }
                return $user;
            }
        }
        return ;
    }

    public static function findByOraclePersonId($oraclePersonId)
    {
        $user = User::where('oracle_person_id',$oraclePersonId)->first();
        if(!$user){
            // IF NOT FOUND USER IN WEB SYSTEM >> SYNC WITH ORACLE TO GET EMPLOYEE DATA
            $user = UserRepo::sync($oraclePersonId);
        }
        return $user;
    }


}
