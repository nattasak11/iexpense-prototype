<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionSequence extends Model
{
	protected $table = 'xxweb_transaction_sequences';
    protected $fillable = ['org_id','name','year'];

    public static function getTranID($orgId,$name,$year){ 
        
        // year format date('y') ex. 16,17
    	// $last_seq = self::where('name',$name)->first();

    	$last_seq = self::firstOrCreate(['org_id'=> $orgId,'name' => $name,'year' => $year]);
        $new_tran_id = (int)$last_seq->tran_id+1;
    	$last_seq->tran_id = $new_tran_id;
    	$last_seq->save();

    	return $new_tran_id;
    }
}
