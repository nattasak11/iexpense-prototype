<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
	protected $table = 'xxweb_locations';

    public function accessibleOrgs()
    {
        return $this->morphMany('App\AccessibleOrg', 'accessible_orgable');
    }

    public function scopeAccessibleOrg($query, $orgId = null){
        if(!$orgId)
        $orgId = \Auth::user()->org_id;

        return $query->whereHas('accessibleOrgs', function ($query1) use ($orgId) {
            $query1->where('org_id', $orgId);
        });
    }

    public function scopeActive($query){
        return $query->where('active',true);
    }
}


