<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class APInvoice extends Model
{
    protected $table = 'xxweb_ap_invoices_v';
}
