<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InterfaceAPHeader extends Model
{
    protected $table = 'xxweb_interface_ap_headers';

    public function scopeSearch($query,$search,$search_date = null)
    {
        $query->where(function($query) use ($search){
            foreach ($search as $key => $value) {
              	if ($value) {
                  	$query->where($key, '=',  $value);
              	}
            }
        });

        if($search_date){
        	if (array_key_exists("date_from",$search_date)){
        		if($search_date['date_from']){
	        		$from_date = DateTime::createFromFormat(trans('date.format'), $search_date['date_from'])->format('Y-m-d');
	            	$query->where('created_at', '>=' ,$from_date) ;
	            }
        	}
        	if (array_key_exists("date_to",$search_date)){
        		if($search_date['date_to']){
	        		$to_date = DateTime::createFromFormat(trans('date.format'), $search_date['date_to'])->format('Y-m-d');
	            	$query->where('created_at', '<=' ,$to_date) ;
	            }
        	}
        }
        return $query;
    }

    public function lines()
    {
        return $this->hasMany('App\InterfaceAPLine','interface_ap_header_id');
    }

    public function request()
    {
        if($this->request_type == 'CASH-ADVANCE' || $this->request_type == 'CLEARING'){
            return $this->belongsTo('App\CashAdvance','request_id');
        }elseif($this->request_type == 'REIMBURSEMENT'){
            return $this->belongsTo('App\Reimbursement','request_id');
        }elseif($this->request_type == 'INVOICE'){
            return $this->belongsTo('App\Invoice','request_id');
        }else{
            return ;
        }
    }
    
}
