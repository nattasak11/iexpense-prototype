<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\PositionInfo;
use DB;
use PDO;

class POHierarchy extends Model
{
    protected $table = 'xxweb_po_hierarchy_v';

    public function scopeExpenseHierarchy($query,$versionNumber = null)
    {
        $query->where('name', self::getApprovalHierarchyName());
        if($versionNumber){
            $query->where('version_number', $versionNumber);
        }
    	return $query;
    }

    public static function getApprovalHierarchyName()
    {
        return config('services.po_hierarchy.hierarchy_name');
    }

    public static function getApprovalControlFunctionName()
    {
        return config('services.po_hierarchy.control_function_name');
    }


    // REMARK ** CAN GET TOP APPROVAL LIMIT AMOUNT ONLY BY SUBORDINATE(CHILD) POSITION ID
    public static function getTopApprovalLimitAmount($hierarchyName,$controlFunction,$versionNumber,$positionId,$orgId)
    {
        if(!$hierarchyName || !$controlFunction || !$versionNumber || !$positionId || !$orgId){
            throw new \Exception('Error : Unable to find max approval limit amount , please contact adminintrator to solve this issue. (hierarchy name:'.$hierarchyName.', control function:'.$controlFunction.', version number:'.$versionNumber.', position id:'.$positionId.', org id:'.$orgId.')', 1);
        }

        // try {

        //     $db = DB::connection()->getPdo();

        //     $sql = "declare
        //             max_position_id   number;
        //             max_approve       number;
        //             status            varchar2(20);
        //             err_msg           varchar2(2000);
        //             begin
        //                 xxweb_utilities.find_max_app(p_hierarchy_name => :hierarchy_name,
        //                         p_ctl_function => :ctl_function,
        //                         p_position_id => :position_id,
        //                         p_version=> :version,
        //                         p_org_id => :org_id,
        //                         p_max_position_id => :max_position_id,
        //                         p_max_approve => :max_approve,
        //                         p_status => :status,
        //                         p_err_msg => :err_msg);
        //             end;";

        //     $sql = preg_replace("/[\r\n]*/","",$sql);

        //     $stmt = $db->prepare($sql);

        //     $stmt->bindParam(':hierarchy_name', $hierarchyName, PDO::PARAM_STR, 2000);
        //     $stmt->bindParam(':ctl_function',$controlFunction, PDO::PARAM_STR, 2000);
        //     $stmt->bindParam(':position_id',$positionId, PDO::PARAM_STR, 2000);
        //     $stmt->bindParam(':version',$versionNumber, PDO::PARAM_STR, 2000);
        //     $stmt->bindParam(':org_id',$orgId, PDO::PARAM_STR, 2000);
        //     $stmt->bindParam(':max_position_id',$result['max_position_id'], PDO::PARAM_STR, 2000);
        //     $stmt->bindParam(':max_approve',$result['max_approve'], PDO::PARAM_STR, 2000);
        //     $stmt->bindParam(':status',$result['status'], PDO::PARAM_STR, 2000);
        //     $stmt->bindParam(':err_msg',$result['err_msg'], PDO::PARAM_STR, 2000);
        //     $stmt->execute();

        // } catch (\Exception $e) {
        //     throw new \Exception($e->getMessage(), 1);
        // }

        $result = [];
        $result['status'] = 'S';
        $result['max_position_id'] = '6063';
        $result['max_approve'] = 999999999;

        return $result;
    }

    public static function findAllChildByParent($parentPersonId,$parentPositionId)
    {
        if(!$parentPersonId || !$parentPositionId){ return ; }
        
        // $query->select(\DB::raw("pps.business_group_id,
        //       pse.pos_structure_version_id,
        //       pps.NAME,
        //       LPAD (' ', 3 * LEVEL) || has.NAME hierarchy,
        //       pve.version_number,
        //       LEVEL rep_level,
        //       pse.parent_position_id,
        //       ppf1.person_id parent_person_id,
        //       ppf1.employee_number parent_emp_no,
        //       ppf1.full_name parent_emp_name,
        //       ppf1.first_name parent_first_name,
        //       ppf1.last_name parent_last_name,
        //       hap.NAME parent_pos_name,
        //       pj1.job_id parent_job_id,
        //       pj1.name parent_job,
        //       pj1.approval_authority parent_approval_authority,
        //       has.position_id child_position_id,
        //       ppf.person_id child_person_id,
        //       ppf.employee_number child_emp_no,
        //       ppf.full_name child_emp_name,
        //       ppf.first_name child_first_name,
        //       ppf.last_name child_last_name,
        //       has.NAME child_pos_name,
        //       pj.job_id child_job_id,
        //       pj.name child_job,
        //       pj.approval_authority child_approval_authority,
        //       pse.subordinate_position_id"));
        // $query->from(\DB::raw("apps.per_all_positions@web_to_erp hap,
        //       apps.per_all_positions@web_to_erp has,
        //       apps.per_pos_structure_elements@web_to_erp pse,
        //       apps.per_pos_structure_versions@web_to_erp pve,
        //       apps.per_position_structures@web_to_erp pps,
        //       apps.per_jobs@web_to_erp pj,
        //       apps.per_jobs@web_to_erp pj1,
        //       apps.per_all_assignments_f@web_to_erp paa1,
        //       apps.per_all_people_f@web_to_erp ppf1,
        //       apps.per_all_assignments_f@web_to_erp paa,
        //       apps.per_all_people_f@web_to_erp ppf"));
        // $query->whereRaw('pve.position_structure_id = pps.position_structure_id');
        // $query->whereRaw('pse.POS_STRUCTURE_VERSION_ID = pve.POS_STRUCTURE_VERSION_ID');
        // $query->whereRaw('SYSDATE BETWEEN pve.date_from AND NVL(pve.date_to, SYSDATE)');
        // $query->whereRaw('hap.position_id = pse.parent_position_id');
        // $query->whereRaw('has.position_id = pse.subordinate_position_id');
        // $query->whereRaw('has.job_id = pj.job_id');
        // $query->whereRaw('hap.job_id = pj1.job_id');
        // $query->whereRaw('pse.parent_position_id = paa1.position_id');
        // $query->where('paa1.assignment_type','E');
        // $query->whereRaw('sysdate between nvl(paa1.effective_start_date,sysdate) and nvl(paa1.effective_end_date,sysdate)');
        // $query->whereRaw('paa1.person_id = ppf1.person_id');
        // $query->where('paa.assignment_type','E');
        // $query->whereRaw('sysdate between nvl(paa.effective_start_date,sysdate) and nvl(paa.effective_end_date,sysdate)');
        // $query->whereRaw('paa.person_id = ppf.person_id');
        // $query->where('pps.name',self::getApprovalHierarchyName());
        // $query->whereRaw('pse.subordinate_position_id = paa.position_id              
        //         START WITH pse.parent_position_id = '.$parentPositionId.'
        //                 AND ppf1.person_id = '.$parentPersonId.'
        //         CONNECT BY     PRIOR pse.subordinate_position_id = pse.parent_position_id
        //                   AND PRIOR pse.pos_structure_version_id = pse.pos_structure_version_id
        //                   AND PRIOR pse.business_group_id = pse.business_group_id');

        // return $query->get();
        // return $query->pluck('child_person_id')->unique();

        $parentPositionInfo = \App\PositionInfo::where('parent_pos_id',$parentPositionId)
                                      ->first();
        if(!$parentPositionInfo){ return ; }
        $childPositionHierarchies = POHierarchy::where('child_approval_authority','<',$parentPositionInfo->parent_approval_authority)->pluck('child_position_id')->unique();
        if(count($childPositionHierarchies) <= 0){ return ; }
        
        return \App\Employee::whereIn('position_id',$childPositionHierarchies)
                                  ->pluck('person_id')->unique(); 

    }

      //   public function scopeFindTopHierarchyByPositionId($query,$positionId)
      //   {
            // $query->select(\DB::raw('business_group_id,
            //                      name,
            //                      hierarchy,
            //                      rep_level,
            //                      parent_position_id,
            //                      parent_name,
            //                      parent_job_id,
            //                      parent_job,
            //                      parent_approval_authority'));
      //       $query->from(\DB::raw('xxweb_po_hierarchy_v'));
      //       $query->where('name', 'TMITH_iExpense_Hierarchy');
      //       $query->whereRaw('rep_level = 1
            //              connect by prior subordinate_position_id = parent_position_id');
      //       $query->where('parent_position_id',$positionId);
      //       $query->orderBy('rep_level');

      //    return $query;
      //   }
}
