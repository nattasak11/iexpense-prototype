<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActivityLog extends Model
{
	protected $table = 'xxweb_activity_logs';
    use SoftDeletes;
    public function activity_logable()
    {
        return $this->morphTo();
    }

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }
}
