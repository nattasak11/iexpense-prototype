<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Receipt extends Model
{
	use SoftDeletes;
    protected $table = 'xxweb_receipts';
    protected $dates = ['deleted_at'];

     /**
     * Get all of the owning receiptable models.
     */
    public function receiptable()
    {
        return $this->morphTo();
    }

    public function location(){
        return $this->belongsTo('App\Location','location_id');
    }

    public function parent()
    {
        if($this->receiptable_type == 'App\CashAdvance'){
            return $this->belongsTo('App\CashAdvance','receiptable_id');
        }elseif($this->receiptable_type == 'App\Reimbursement'){
            return $this->belongsTo('App\Reimbursement','receiptable_id');
        }elseif($this->receiptable_type == 'App\Invoice'){
            return $this->belongsTo('App\Invoice','receiptable_id');
        }else{
            return ;
        }
    }

    public function lines()
    {
        return $this->hasMany('App\ReceiptLine','receipt_id')->orderBy('created_at');
    }

    public function attachments()
    {
        return $this->morphMany('App\Attachment', 'attachmentable');
    }

    public function getreceiptTypeAttribute(){
        if($this->receiptable_type == 'App\CashAdvance'){
            return 'CLEARING';
        }elseif($this->receiptable_type == 'App\Reimbursement'){
            return 'REIMBURSEMENT';
        }elseif($this->receiptable_type == 'App\Invoice'){
            return 'INVOICE';
        }else{
            return ;
        }
    }

    public function getExchangeRateForCalAttribute()
    {
        if($this->exchange_rate){
            return $this->exchange_rate;
        }
        return 1;
    }

    public function isNotLock()
    {
        if($this->receiptable_type == 'App\CashAdvance'){
            // CA CLEARING NOT LOCK ON STATUS 'CLEARING_REQUEST'
            return $this->parent->status == 'CLEARING_REQUEST';
        }elseif($this->receiptable_type == 'App\Reimbursement'){
            // REIM NOT LOCK ON STATUS 'NEW_REQUEST','BLOCKED'
            return ($this->parent->status == 'NEW_REQUEST' || $this->parent->status == 'BLOCKED');
        }elseif($this->receiptable_type == 'App\Invoice'){
            // REIM NOT LOCK ON STATUS 'NEW_REQUEST','BLOCKED'
            return ($this->parent->status == 'NEW_REQUEST' || $this->parent->status == 'BLOCKED');
        }else{
            return false;
        }
    }

    public function getTotalAmountAttribute()
    {
        $totalAmount = 0;
        if(count($this->lines)>0){
            $totalAmount = $this->lines->sum('total_primary_amount_inc_vat');
        }
        return $totalAmount;
    }

}
