<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class APPaymentMethod extends Model
{
    protected $table = 'xxweb_ap_payment_methods_v';

    public function scopeForCashAdvance($query)
    {
    	return $query->where('author','MERCURY');
    }

    public function scopeSameDay($query)
    {
    	return $query->where('payment_method_code','SAME DAY');
    }
}
