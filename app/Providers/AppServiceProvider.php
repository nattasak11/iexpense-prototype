<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        require_once __DIR__ . '/../Http/Helpers/Navigation.php';
        require_once __DIR__ . '/../Http/Helpers/Date.php';
        require_once __DIR__ . '/../Http/Helpers/Icon.php';
        require_once __DIR__ . '/../Http/Helpers/Preference.php';
    }
}
