<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $table = 'xxweb_po_vendors_v';

    public function scopeNotEmp($query)
    {
        return $query->whereNull('attribute5');
    }
    
}
