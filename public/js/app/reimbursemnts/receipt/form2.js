$(document).ready(function(){
    showOtherVendorForm();

    $('select[name="vendor"').change(function(e){
        showOtherVendorForm();
    });


    function showOtherVendorForm() {
        var selectValue =  $('select[name="vendor"');
        var tagShowOtherVendor = $('#showOtherVendor');

        if (selectValue.val() == 'other') {
            tagShowOtherVendor.fadeIn('slow')
        } else {
            tagShowOtherVendor.fadeOut('slow');
        }
    }
});