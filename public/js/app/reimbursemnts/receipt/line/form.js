/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmory imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmory exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		Object.defineProperty(exports, name, {
/******/ 			configurable: false,
/******/ 			enumerable: true,
/******/ 			get: getter
/******/ 		});
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports) {

eval("var app = new Vue({\n    el: '#app',\n    data: {\n        lineForm: {\n            before_vat_amount: '',\n            vat_rate: '',\n        },\n        category: '',\n\n        // amountWithVat: 0,\n        maxReimAmount: 0,\n        totalReimburse: 0,\n        showLineForm: false,\n    },\n\n    methods: {\n        toggleLineForm: function() {\n            this.showLineForm = ! this.showLineForm;\n        },\n        getMaxReimAmount: function() {\n            if (this.category == 'cat1') {\n                this.maxReimAmount = 1000;\n                return\n            }\n            if (this.category == 'cat2') {\n                this.maxReimAmount = 2000;\n                return\n            }\n            if (this.category == 'cat3') {\n                this.maxReimAmount = 3000;\n                return\n            }\n\n            this.maxReimAmount = 0;\n        },\n    },\n\n    computed: {\n        amountWithVat: function() {\n            if (this.lineForm.vat_rate > 100) {\n                this.lineForm.vat_rate = 100;\n            }\n            var vatAmount =  this.lineForm.before_vat_amount * this.lineForm.vat_rate / 100;\n            var totalAmount = parseFloat(this.lineForm.before_vat_amount) + parseFloat(vatAmount);\n            return totalAmount || 0;\n        },\n\n        totalReimburse: function() {\n            return Math.min(this.lineForm.before_vat_amount, this.maxReimAmount);\n        },\n\n        reimAmount: function() {\n            return Math.min(this.amountWithVat, this.maxReimAmount);\n        }\n\n    }\n})//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiMC5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy9yZXNvdXJjZXMvYXNzZXRzL2pzL2FwcC9yZWltYnVyc2VtbnRzL3JlY2VpcHQvbGluZS9mb3JtLmpzP2ZjOTciXSwic291cmNlc0NvbnRlbnQiOlsidmFyIGFwcCA9IG5ldyBWdWUoe1xuICAgIGVsOiAnI2FwcCcsXG4gICAgZGF0YToge1xuICAgICAgICBsaW5lRm9ybToge1xuICAgICAgICAgICAgYmVmb3JlX3ZhdF9hbW91bnQ6ICcnLFxuICAgICAgICAgICAgdmF0X3JhdGU6ICcnLFxuICAgICAgICB9LFxuICAgICAgICBjYXRlZ29yeTogJycsXG5cbiAgICAgICAgLy8gYW1vdW50V2l0aFZhdDogMCxcbiAgICAgICAgbWF4UmVpbUFtb3VudDogMCxcbiAgICAgICAgdG90YWxSZWltYnVyc2U6IDAsXG4gICAgICAgIHNob3dMaW5lRm9ybTogZmFsc2UsXG4gICAgfSxcblxuICAgIG1ldGhvZHM6IHtcbiAgICAgICAgdG9nZ2xlTGluZUZvcm06IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdGhpcy5zaG93TGluZUZvcm0gPSAhIHRoaXMuc2hvd0xpbmVGb3JtO1xuICAgICAgICB9LFxuICAgICAgICBnZXRNYXhSZWltQW1vdW50OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLmNhdGVnb3J5ID09ICdjYXQxJykge1xuICAgICAgICAgICAgICAgIHRoaXMubWF4UmVpbUFtb3VudCA9IDEwMDA7XG4gICAgICAgICAgICAgICAgcmV0dXJuXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAodGhpcy5jYXRlZ29yeSA9PSAnY2F0MicpIHtcbiAgICAgICAgICAgICAgICB0aGlzLm1heFJlaW1BbW91bnQgPSAyMDAwO1xuICAgICAgICAgICAgICAgIHJldHVyblxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKHRoaXMuY2F0ZWdvcnkgPT0gJ2NhdDMnKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5tYXhSZWltQW1vdW50ID0gMzAwMDtcbiAgICAgICAgICAgICAgICByZXR1cm5cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdGhpcy5tYXhSZWltQW1vdW50ID0gMDtcbiAgICAgICAgfSxcbiAgICB9LFxuXG4gICAgY29tcHV0ZWQ6IHtcbiAgICAgICAgYW1vdW50V2l0aFZhdDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5saW5lRm9ybS52YXRfcmF0ZSA+IDEwMCkge1xuICAgICAgICAgICAgICAgIHRoaXMubGluZUZvcm0udmF0X3JhdGUgPSAxMDA7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB2YXIgdmF0QW1vdW50ID0gIHRoaXMubGluZUZvcm0uYmVmb3JlX3ZhdF9hbW91bnQgKiB0aGlzLmxpbmVGb3JtLnZhdF9yYXRlIC8gMTAwO1xuICAgICAgICAgICAgdmFyIHRvdGFsQW1vdW50ID0gcGFyc2VGbG9hdCh0aGlzLmxpbmVGb3JtLmJlZm9yZV92YXRfYW1vdW50KSArIHBhcnNlRmxvYXQodmF0QW1vdW50KTtcbiAgICAgICAgICAgIHJldHVybiB0b3RhbEFtb3VudCB8fCAwO1xuICAgICAgICB9LFxuXG4gICAgICAgIHRvdGFsUmVpbWJ1cnNlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHJldHVybiBNYXRoLm1pbih0aGlzLmxpbmVGb3JtLmJlZm9yZV92YXRfYW1vdW50LCB0aGlzLm1heFJlaW1BbW91bnQpO1xuICAgICAgICB9LFxuXG4gICAgICAgIHJlaW1BbW91bnQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuIE1hdGgubWluKHRoaXMuYW1vdW50V2l0aFZhdCwgdGhpcy5tYXhSZWltQW1vdW50KTtcbiAgICAgICAgfVxuXG4gICAgfVxufSlcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gcmVzb3VyY2VzL2Fzc2V0cy9qcy9hcHAvcmVpbWJ1cnNlbW50cy9yZWNlaXB0L2xpbmUvZm9ybS5qcyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOyIsInNvdXJjZVJvb3QiOiIifQ==");

/***/ }
/******/ ]);