$(document).ready(function(){

    showOtherVendorForm();

    $('select[name="vendor_id"').change(function(e){
        showOtherVendorForm();
    });

    function showOtherVendorForm() {
        var selectValue =  $('select[name="vendor_id"');
        var tagShowOtherVendor = $('#showOtherVendor');

        if (selectValue.val() == 'other') {
            tagShowOtherVendor.fadeIn('slow')
        } else {
            tagShowOtherVendor.fadeOut('slow');
        }
    }

});